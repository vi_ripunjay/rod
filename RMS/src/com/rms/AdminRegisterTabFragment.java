package com.rms;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.rms.adapter.AdminTabPagerAdapter;

public class AdminRegisterTabFragment extends Fragment {
	ViewPager Tab;
    AdminTabPagerAdapter TabAdapter;
	ActionBar actionBar;
	private FragmentActivity myContext;
    int tabvalue=0;

    //Mandatory Constructor
    public AdminRegisterTabFragment() {
    }

    public AdminRegisterTabFragment(int i) {
    	tabvalue = i;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    
   

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.admin_register_fragment_tab,container, false);
        
   //     TabAdapter = new TabsPagerAdapter(getActivity().getSupportFragmentManager());
     
        TabAdapter = new AdminTabPagerAdapter(getChildFragmentManager());
        
        Tab = (ViewPager) rootView.findViewById(R.id.pager);
        
        Tab.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                       
                    	actionBar = getActivity().getActionBar();
                    	actionBar.setSelectedNavigationItem(position);                    }
                });
        
        Tab.setAdapter(TabAdapter);
        
        
        actionBar = getActivity().getActionBar();
        //Enable Tabs on Action Bar
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.TabListener tabListener = new ActionBar.TabListener(){

			@Override
			public void onTabReselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}

			@Override
			 public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
	          
	            Tab.setCurrentItem(tab.getPosition());
	        }

			@Override
			public void onTabUnselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}};
			
			actionBar.removeAllTabs();
			if(actionBar.getTabCount() == 0)
		    {
		     

				
		    //Add New Tab
				
			actionBar.addTab(actionBar.newTab().setText("Register").setTabListener(tabListener));
			actionBar.addTab(actionBar.newTab().setText("Synchronize").setTabListener(tabListener));
			actionBar.addTab(actionBar.newTab().setText("Admin").setTabListener(tabListener));
			actionBar.setSelectedNavigationItem(tabvalue);

		    }
        return rootView;
    }
}

