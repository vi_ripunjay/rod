package com.rms.receiver;

import com.rms.service.CronService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * @author Ripunjay.S
 * below receiver for run a cron for sync data
 *
 */
public class RmsBroadcastReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context cx, Intent intent) {
		
		Intent i = new Intent(cx, CronService.class);		
		cx.startService(i);

	}

}
