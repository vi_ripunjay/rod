/**
 * @file  SyncHandler.java
 * 
 * @brief  This class work as a handler for incremental sync
 * 
 * @author Ripunjay Shukla
 * 
 * @date : May 05 2016
 */
package com.rms;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.rms.service.BackGroundTask;
import com.rms.utils.CommonUtil;

public class SyncHandler {

	public static Context context = null;
	public static final int MSG_GET_TASK = 1;
	public static final int MSG_GET_CREWDEPARTMENT = 2;
	public static final int MSG_GET_CREWDEPARTMENT_TASK = 3;
	public static final int MSG_GET_ROLE_CREWDEPARTMENT = 4;
	public static final int MSG_GET_USERMASTER = 5;
	public static final int MSG_GET_USER_SERVICETERM = 6;
	public static final int MSG_GET_USER_SERVICETERM_ROLE = 7;
	public static final int MSG_GET_SHIPMASTER = 8;
	public static final int MSG_GET_SYNCHISTORY = 9;
	public static final int MSG_GET_SYNCSTATUS = 10;
	public static final int MSG_GET_WORKTIMESHEET = 11;
	public static final int MSG_GET_WORKTIMESHEET_TASK = 12;
	public static final int MSG_SEND_WORKTIMESHEET = 13;
	public static final int MSG_SEND_WORKTIMESHEET_TASK = 14;
	public static final int MSG_GET_ROLES = 15;
	public static final int MSG_SEND_SYNCHISTORY = 16;
	public static final int MSG_SEND_SYNCSTATUS = 17;
	public static final int MSG_MATCH_MANUAL_SYNC_HISTORY = 18;
	public static final int MSG_GET_SHIP_EXIST = 19;
	public static final int MSG_GET_IDL = 20;
	public static final int MSG_GET_ROLE_TABLET_FUNCTION = 21;
	public static final int MSG_SEND_SYNC_HISTORY = 22;
	public static final int MSG_GET_USER_NFC_TAGDATA = 23;
	public static final int MSG_SEND_USER_NFC_TAGDATA = 24;
	public static final int MSG_GET_TRANSACTIONAL_DATA = 25;
	public static final int MSG_SEND_ACK_TRANSATIONAL_DATA = 26;
	public static final int MSG_GET_MODIFIED_MASTER_DATA = 27;
	public static final int MSG_GET_SYNC_STATUS_DATA = 28;
	public static final int MSG_SEND_SYNC_STATUS_DATA = 29;
	
	

	static String toastMsg = "";

	/**
	 * This will initialize the context on creation of syncHandler object.
	 * 
	 * @param contextTemp
	 */
	public SyncHandler(Context contextTemp) {
		context = contextTemp;
	}

	/**
	 * This method will take string as input,add input to previous string and
	 * gives output as a string.
	 * 
	 * @param toast
	 * @return string
	 */
	public static String buildToast(String toast) {
		toastMsg = toastMsg + toast;
		return toastMsg;
	}

	/**
	 * This method will clear the current string.
	 */
	public static void releaseToast() {
		toastMsg = "";
	}

	/**
	 * @brief A handler which perform the task based on the message.what it
	 *        gets. e.g: message.what = MSG_UPDATE_FORMTEMPLATE_SYNC then it
	 *        calls updateAllFormTemplates method from web service
	 */
	public static Handler handler = new Handler(new Handler.Callback() {

		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == MSG_GET_TASK) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetTask = true;
				backGroundTask.getTasks(CommonUtil.getTenantId(context),
						CommonUtil.getIMEI(context));

			} else if (msg.what == MSG_GET_CREWDEPARTMENT) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetCrewdepartment = true;
				backGroundTask.getCrewDepartment(CommonUtil.getIMEI(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_GET_ROLE_CREWDEPARTMENT) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetRoleCrewdepartment = true;
				backGroundTask.getRoleCrewDepartment(
						CommonUtil.getIMEI(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_GET_CREWDEPARTMENT_TASK) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetCrewdepartmentTask = true;
				backGroundTask.getCrewDepartmentTask(
						CommonUtil.getIMEI(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_GET_USER_SERVICETERM_ROLE) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetUserServiceTermRole = true;
				backGroundTask.getUserServiceTermRoleTask(
						CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context),
						CommonUtil.getIMEI(context));

			} else if (msg.what == MSG_GET_USER_SERVICETERM) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetUserServiceTerm = true;
				backGroundTask.getUserServiceTermTask(
						CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context),
						CommonUtil.getIMEI(context));

			} else if (msg.what == MSG_GET_USERMASTER) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetUserMaster = true;
				backGroundTask.getUserMaster(CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context),
						CommonUtil.getIMEI(context));

			} else if (msg.what == MSG_GET_SHIPMASTER) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetShipmaster = true;
				backGroundTask.getShipMasterTask(
						CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context),
						CommonUtil.getMacId(context));

			} else if (msg.what == MSG_GET_SYNCHISTORY) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetSyncHistory = true;
				/*
				 * backGroundTask.getSyncHistory( CommonUtil.getIMEI(context),
				 * CommonUtil.getShipId(context));
				 */
			} else if (msg.what == MSG_GET_SYNCSTATUS) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetSyncStatus = true;
				/*
				 * backGroundTask.getCheckListSection("",
				 * CommonUtil.getTenantId(context));
				 */
			} else if (msg.what == MSG_GET_WORKTIMESHEET) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetWorktimesheet = true;

				backGroundTask.getWorktimeSheet(CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_GET_WORKTIMESHEET_TASK) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetWorktimesheetTask = true;
				backGroundTask.getWorktimeSheetTask(
						CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_GET_ROLES) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetRoles = true;
				backGroundTask.getRoles(CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_SEND_SYNCHISTORY) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkSendSyncHistory = true;
				/*
				 * backGroundTask.sendSyncHistory(CommonUtil.getUserId(context));
				 */
			} else if (msg.what == MSG_SEND_SYNCSTATUS) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkSendSyncstatus = true;
				/*
				 * backGroundTask.sendSyncStatus("",
				 * CommonUtil.getShipId(context));
				 */
			} else if (msg.what == MSG_MATCH_MANUAL_SYNC_HISTORY) {
				// CommonUtil.setUserLogin(context, "0");
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkUpdateManualSyncHistory = true;
				/*
				 * backGroundTask.matchManualSyncHistory(
				 * CommonUtil.getShipId(context), CommonUtil.getMacId(context));
				 */
			} else if (msg.what == MSG_GET_SHIP_EXIST) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetShipExist = true;
				/*
				 * backGroundTask.getShipExistOrNot(CommonUtil
				 * .getActivatingShip(context));
				 */
			} else if (msg.what == MSG_GET_IDL) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetIdl = true;
				backGroundTask.getInternationalDates(
						CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context));

			} else if (msg.what == MSG_GET_ROLE_TABLET_FUNCTION) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetRoleTabletFunction = true;
				backGroundTask.getRoleTabletFunction(
						CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context),
						CommonUtil.getMacId(context));

			} else if (msg.what == MSG_GET_USER_NFC_TAGDATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkGetUserNfcTagData = true;
				backGroundTask.getUserNfcTagData(CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context),
						CommonUtil.getShipId(context));

			} else if (msg.what == MSG_SEND_SYNC_HISTORY) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkSendSyncHistory = true;
				backGroundTask.sendRmsSyncHistory();

			} else if (msg.what == MSG_SEND_WORKTIMESHEET) {
				BackGroundTask backGroundTask = new BackGroundTask(context);
				BackGroundTask.checkSendWorktimesheet = true;
				backGroundTask.sendWorktimeSheet(CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_SEND_WORKTIMESHEET_TASK) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkSendWorktimesheetTask = true;
				backGroundTask.sendWorktimeSheetTask(
						CommonUtil.getMacId(context),
						CommonUtil.getTenantId(context));

			} else if (msg.what == MSG_SEND_USER_NFC_TAGDATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkSendUserNfcTagData = true;
				backGroundTask.sendUserNfcTagData();

			} else if (msg.what == MSG_GET_TRANSACTIONAL_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetTransactionalData = true;
				backGroundTask.getTransactionalDataData();

			} else if (msg.what == MSG_SEND_ACK_TRANSATIONAL_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkSendAckTransactionalData = true;
				backGroundTask.sendAckForTransactionalData();

			} else if (msg.what == MSG_GET_MODIFIED_MASTER_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetModifiedMasterData = true;
				backGroundTask.getModifiedMasterData();

			} else if (msg.what == MSG_GET_SYNC_STATUS_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkGetSyncStatusData = true;
				backGroundTask.getSyncStatusData();

			} else if (msg.what == MSG_SEND_SYNC_STATUS_DATA) {
				BackGroundTask backGroundTask = new BackGroundTask(context);

				BackGroundTask.checkSendSyncStatusData = true;
				backGroundTask.sendSyncStatusData();

			}
			
			return true;
		}

	});
}
