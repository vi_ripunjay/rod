package com.rms.reader;

public class PairWorkDate {
	
	public String yesterdayDate;
	public String todayDate;
	
	public PairWorkDate(){
		super();
	}

	public PairWorkDate(String yesterdayDate, String todayDate) {
		super();
		this.yesterdayDate = yesterdayDate;
		this.todayDate = todayDate;
	}

	public String getYesterdayDate() {
		return yesterdayDate;
	}

	public void setYesterdayDate(String yesterdayDate) {
		this.yesterdayDate = yesterdayDate;
	}

	public String getTodayDate() {
		return todayDate;
	}

	public void setTodayDate(String todayDate) {
		this.todayDate = todayDate;
	}
}
