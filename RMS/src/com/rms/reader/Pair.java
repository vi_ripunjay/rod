package com.rms.reader;

import java.util.Date;

public class Pair {
	public Date begining, end;

	public Pair() {
		super();
	}

	public Pair(Date begining, Date end) {
		super();
		this.begining = begining;
		this.end = end;
	}

	public Date getBegining() {
		return begining;
	}

	public void setBegining(Date begining) {
		this.begining = begining;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}
}
