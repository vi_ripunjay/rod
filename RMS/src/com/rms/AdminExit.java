package com.rms;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class AdminExit extends Fragment {
	ListView lv;

	public AdminExit() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.exit, container, false);
		Intent i = new Intent(getActivity(), WelcomeAdmin.class);
		startActivity(i);
		getActivity().finish();

		return rootView;
	}

}
