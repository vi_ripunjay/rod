package com.rms;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.rms.adapter.TaskSpinnerAdapter;
import com.rms.db.DatabaseSupport;
import com.rms.model.CrewDepartmentTask;
import com.rms.model.IdlLogWork;
import com.rms.model.InternationalDates;
import com.rms.model.RoleCrewDepartment;
import com.rms.model.RoleTabletFunction;
import com.rms.model.TaskDataItem;
import com.rms.model.Tasks;
import com.rms.model.UserMaster;
import com.rms.model.UserServiceTerm;
import com.rms.model.UserServiceTermRole;
import com.rms.model.UserWorkTimeSheet;
import com.rms.model.UserWorkTimeSheetTask;
import com.rms.reader.CustomTimePickerDialog;
import com.rms.reader.PairWorkDate;
import com.rms.utils.CommonUtil;
import com.rms.utils.L;

public class WorkTimesheetActivity extends Activity {

	Intent intent;
	TextView workStartTxt;
	TextView usernameText, yesterdayDateTxt, todayDateTxt, errorTxt;
	RadioButton radioButtonYesterday;
	RadioButton radioButtonToday;
	EditText workTime;
	Spinner taskLis;
	Button saveWork;
	Context mContext;
	LinearLayout errorTxtLayOut;
	public static MenuItem adminSettingItem;

	public String saveYesterdaydate, saveTodayDate;
	public static String finalWorkDate;

	private String userName;
	public static String strUserid;
	private String strRole;

	private static final DateFormat TIME_FORMAT = SimpleDateFormat
			.getDateTimeInstance();
	private SimpleDateFormat dateFormatter;
	// private TimePickerDialog timePickerDialog;
	private CustomTimePickerDialog customTimePickerDialog;
	private final static int TIME_PICKER_INTERVAL = 30;
	private AlertDialog mDialog;

	private List<UserWorkTimeSheet> uwtSheetGlbList = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);

		/**
		 * below code for hide the icon.
		 */
		ab.setIcon(new ColorDrawable(getResources().getColor(
				android.R.color.transparent)));
		/**
		 * work on 4.4.2
		 */
		// ab.setDisplayUseLogoEnabled(false);

		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
				| ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? android.R.id.home
				: android.support.v7.appcompat.R.id.home);
		((View) homeIcon.getParent()).setVisibility(View.GONE);
		((View) homeIcon).setVisibility(View.GONE);

		setContentView(R.layout.activity_work_timesheet);
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

		mContext = this;
		intent = getIntent();

		errorTxtLayOut = (LinearLayout) findViewById(R.id.errorTxtLayOut);
		errorTxtLayOut.setVisibility(View.GONE);

		workStartTxt = (TextView) findViewById(R.id.workStartTxt);
		usernameText = (TextView) findViewById(R.id.usernameText);
		errorTxt = (TextView) findViewById(R.id.errorTxt);
		yesterdayDateTxt = (TextView) findViewById(R.id.yesterdayDateTxt);
		todayDateTxt = (TextView) findViewById(R.id.todayDateTxt);
		radioButtonYesterday = (RadioButton) findViewById(R.id.radioButtonYesterday);
		radioButtonToday = (RadioButton) findViewById(R.id.radioButtonToday);
		radioButtonToday.setChecked(true);

		workTime = (EditText) findViewById(R.id.workTime);
		taskLis = (Spinner) findViewById(R.id.taskList);
		String showTask = getResources().getString(R.string.taskAllow);
		if (Integer.parseInt(showTask) == 1) {
			taskLis.setVisibility(View.VISIBLE);
		} else {
			taskLis.setVisibility(View.GONE);
		}

		saveWork = (Button) findViewById(R.id.saveWork);

		userName = intent.getStringExtra("userName");
		strUserid = intent.getStringExtra("userId");
		strRole = "";// intent.getStringExtra("role");

		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		List<UserServiceTermRole> serviceTermRoleData = new ArrayList<UserServiceTermRole>();
		DatabaseSupport db = new DatabaseSupport(mContext);
		serviceTermRoleData = db.getUserServiceTermRoleRowBiId(strUserid,
				format.format(curDate));
		if (serviceTermRoleData != null && serviceTermRoleData.size() > 0) {

			strRole = db.getRoleName(serviceTermRoleData.get(0).getiRoleId());
		}

		List<UserMaster> umList = db.getUserMasterSingleRow(strUserid);
		UserMaster um = new UserMaster();
		if (umList != null && umList.size() > 0) {
			um = umList.get(0);
		}
		String userName = "";
		if (um.getStrFirstName() != null && um.getStrLastName() != null) {
			userName = um.getStrFirstName() + " " + um.getStrLastName();
		} else if (um.getStrFirstName() != null) {
			userName = um.getStrFirstName();
		}

		usernameText.setText(userName + "/" + strRole);

		uwtSheetGlbList = new ArrayList<UserWorkTimeSheet>();
		/*
		 * uwtSheetGlbList = db.getUserWorkTimeSheetDataByUserIdAndStatus(
		 * strUserid, finalWorkDate, "0");
		 */

		uwtSheetGlbList = db.getUserWorkTimeSheetDataByStatus(strUserid, "0");

		if (uwtSheetGlbList != null && uwtSheetGlbList.size() > 0) {
			workStartTxt.setText(getResources().getString(
					R.string.workEndedString));
		} else {
			workStartTxt.setText(getResources().getString(
					R.string.workStartedString));
		}

		Date dt = new Date();

		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();

		int hours = dt.getHours();
		int minutes = dt.getMinutes();

		String strHours = "0";
		String strMinuts = "0";

		/**
		 * This method return pair of date which we want to display as: Today
		 * and yesterday
		 */
		PairWorkDate pwd = new PairWorkDate();
		pwd = getPrevTodayNextWorkingDate(curDate,
				CommonUtil.getShipId(mContext));

		/**
		 * Today date display here.
		 */
		String fullDate = dateFormatter.format(dt);
		// String dtArray[] = fullDate.split("-");
		String dtArray[] = pwd.getTodayDate().split("-");
		todayDateTxt.setText("(" + dtArray[2] + " "
				+ getMonth(Integer.parseInt(dtArray[1])) + " " + dtArray[0]
				+ ")");

		saveTodayDate = fullDate;
		finalWorkDate = saveTodayDate;

		/**
		 * Yesterday date display here.
		 */
		// String dtYestArray[] = getDateBeforePassingDays(1).split("-");
		String dtYestArray[] = pwd.getYesterdayDate().split("-");
		yesterdayDateTxt.setText("(" + dtYestArray[2] + " "
				+ getMonth(Integer.parseInt(dtYestArray[1])) + " "
				+ dtYestArray[0] + ")");

		saveYesterdaydate = getDateBeforePassingDays(1);

		/**
		 * below work done for 30 minust slots. According to PN Sir, 21-05-2016
		 */
		if (String.valueOf(minutes).length() == 1) {

			// strMinuts = "0" + minutes;
			strMinuts = "0" + 0;
		} else {
			String actMinut = "";
			if (minutes <= 15) {
				actMinut = "00";
			} else if (minutes > 15 && minutes <= 45) {
				actMinut = "30";
			} else if (minutes > 45) {
				actMinut = "00";
				hours = hours + 1;
			}
			strMinuts = "" + actMinut;
		}

		if (String.valueOf(hours).length() == 1) {
			strHours = "0" + hours;
		} else {
			strHours = "" + hours;
		}

		String curTime = strHours + ":" + strMinuts;

		// pickDate.setText(fullDate);
		workTime.setText(curTime);

		workTime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Calendar mcurrentTime = Calendar.getInstance();
				int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				int minute = mcurrentTime.get(Calendar.MINUTE);

				String editTime = (workTime.getText() != null
						&& !"".equals(workTime.getText().toString()) ? workTime
						.getText().toString() : "");
				if (!"".equals(editTime)) {
					String hrString[] = editTime.split(":");
					if (hrString != null && hrString.length > 0) {
						hour = Integer.parseInt(hrString[0]);
						minute = Integer.parseInt(hrString[1]);
					}
				}

				customTimePickerDialog = new CustomTimePickerDialog(mContext,
						new OnTimeSetListener() {

							public void onTimeSet(TimePicker view,
									int hourOfDay, int minute) {
								// TODO Auto-generated method stub

								String strMinuts = "00";
								String strHours = "00";
								if (String.valueOf(minute).length() == 1) {
									strMinuts = "0" + minute;
								} else {
									strMinuts = "" + minute;
								}

								if (String.valueOf(hourOfDay).length() == 1) {
									strHours = "0" + hourOfDay;
								} else {
									strHours = "" + hourOfDay;
								}
								workTime.setText(strHours + ":" + strMinuts);
							}
						}, hour, minute, true); // this constructor have true
												// for 24 hrs format and false
												// for 12 hrs format
				customTimePickerDialog.setTitle("Select Time");
				customTimePickerDialog.show();

			}
		});

		/**
		 * task list uploaded and spinner populated.
		 */
		List<TaskDataItem> taskDataitemList = getUserTaskList(strUserid);
		if (taskDataitemList != null && taskDataitemList.size() > 0) {
			TaskSpinnerAdapter spin = new TaskSpinnerAdapter(taskDataitemList,
					mContext);
			taskLis.setAdapter(spin);

			/**
			 * This work done for task selected at tap out time which select at
			 * tap in time.
			 */
			if (uwtSheetGlbList != null && uwtSheetGlbList.size() > 0) {
				UserWorkTimeSheet uwts = uwtSheetGlbList.get(0);
				List<UserWorkTimeSheetTask> uwtSheetTaskListTaskList = new ArrayList<UserWorkTimeSheetTask>();
				uwtSheetTaskListTaskList = db
						.getUserWorkTimeSheetTaskByWorkTimeSheetIdAndUserId(
								uwts.getiUserWorkTimeSheetId(),
								uwts.getiUserId());
				if (uwtSheetTaskListTaskList != null
						&& uwtSheetTaskListTaskList.size() > 0) {
					UserWorkTimeSheetTask userWorkTimeSheetTask = new UserWorkTimeSheetTask();
					userWorkTimeSheetTask = uwtSheetTaskListTaskList.get(0);
					int index = 0;
					for (TaskDataItem tdi : taskDataitemList) {
						if (tdi.getiTaskId().equalsIgnoreCase(
								userWorkTimeSheetTask.getItaskId())) {
							break;
						}
						index++;
					}
					taskLis.setSelection(index, true);
				}
			}
		}

		/**
		 * Radio Button for date.
		 */
		radioButtonYesterday.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				radioButtonToday.setChecked(false);
				finalWorkDate = saveYesterdaydate;

			}
		});

		radioButtonToday.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				radioButtonYesterday.setChecked(false);
				finalWorkDate = saveTodayDate;
			}
		});

		/**
		 * Ok button click and save work time sheet and go to login.
		 */
		saveWork.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Date curDate = new Date();
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				List<UserServiceTermRole> serviceTermRoleData = new ArrayList<UserServiceTermRole>();
				DatabaseSupport db = new DatabaseSupport(mContext);
				serviceTermRoleData = db.getUserServiceTermRoleRowBiId(
						strUserid, format.format(curDate));
				UserServiceTermRole ustd = new UserServiceTermRole();
				if (serviceTermRoleData != null
						&& serviceTermRoleData.size() > 0) {
					ustd = serviceTermRoleData.get(0);
				}

				errorTxtLayOut.setVisibility(View.GONE);

				/**
				 * finalWorkDate is selected from screen
				 */
				List<UserWorkTimeSheet> uwtSheetList = new ArrayList<UserWorkTimeSheet>();
				/*
				 * uwtSheetList = db.getUserWorkTimeSheetDataByUserIdAndStatus(
				 * strUserid, finalWorkDate, "0");
				 */

				uwtSheetList = db.getUserWorkTimeSheetDataByStatus(strUserid,
						"0");

				if (uwtSheetList != null && uwtSheetList.size() > 0) {
					/**
					 * Update work timesheet
					 */
					UserWorkTimeSheet uwts = new UserWorkTimeSheet();
					uwts = uwtSheetList.get(0);

					String startTmeArr[] = uwts.getWorkStartTime().split(":");
					String endTimeArr[] = workTime.getText().toString()
							.split(":");
					boolean tapOutTimeCorect = false;

					Date workStartDate = null;
					Date workEndDate = null;
					try {
						workStartDate = format.parse(uwts.getWorkStartDate());
						workEndDate = format.parse(finalWorkDate);
					} catch (ParseException e) {

						e.printStackTrace();
					}

					if (workEndDate.getTime() > workStartDate.getTime()) {
						tapOutTimeCorect = true;
					} 
					else if (workEndDate.getTime() < workStartDate.getTime()) {
                        tapOutTimeCorect = false;
					}
					else {
						int endTime24Format = Integer.parseInt(endTimeArr[0]);
						if(endTime24Format == 0){
							endTime24Format = 24;
						}
						if (endTime24Format > Integer
								.parseInt(startTmeArr[0])) {

							tapOutTimeCorect = true;

						} else if (Integer.parseInt(endTimeArr[0]) == Integer
								.parseInt(startTmeArr[0])
								&& Integer.parseInt(endTimeArr[1]) > Integer
										.parseInt(startTmeArr[1])) {

							tapOutTimeCorect = true;
						} else {
							tapOutTimeCorect = false;
						}
					}

					if (tapOutTimeCorect) {
						uwts.setDtUpdated(format.format(curDate));
						uwts.setFlgIsDirty("1");
						uwts.setWorkEndDate(finalWorkDate);
						
						 String worktimeEnd24 [] = workTime.getText().toString().split(":");
                         if(Integer.parseInt(worktimeEnd24[0]) == 0 && Integer.parseInt(worktimeEnd24[1]) == 0){                                                        

                                 uwts.setWorkEndTime("24:00");
                         }
                         else{
                                 uwts.setWorkEndTime(workTime.getText().toString());
                         }
						
						
						//uwts.setWorkEndTime(workTime.getText().toString());
						uwts.setWorkEndDateTime(uwts.getWorkEndDate() + ":"
								+ uwts.getWorkEndTime());
						uwts.setFlgStatus("1");
						uwts.setUpdatedBy(CommonUtil.getUserId(mContext));

						/**
						 * This code check that selected date have idl or not.
						 * if have idl then check repeated or missed
						 */
						List<InternationalDates> idlList = new ArrayList<InternationalDates>();
						idlList = db.getInternationalDatesByShipIdAndDate(
								uwts.getiShipId(), finalWorkDate);
						InternationalDates idlDate = new InternationalDates();
						if (idlList != null && idlList.size() > 0) {
							idlDate = idlList.get(0);
						}
						/**
						 * Check data into IdlLogWork table. if idl stared 2nd
						 * day then both date should be same.
						 */
						List<IdlLogWork> idlLogWorlList = new ArrayList<IdlLogWork>();
						idlLogWorlList = db
								.getIdlLogWorkByInternationalDateId(idlDate
										.getIinternationalDateId());
						IdlLogWork idlLogWorkCheck = new IdlLogWork();
						if (idlLogWorlList != null && idlLogWorlList.size() > 0) {
							idlLogWorkCheck = idlLogWorlList.get(0);
						}

						uwts.setFlgInternationalOrderEnd(idlLogWorkCheck
								.getFlgDayStart());

						db.updateUserWorkTimeSheetTable(uwts);

						/**
						 * Update work time sheet task and end date
						 */
						List<UserWorkTimeSheetTask> uwtSheetTaskListTaskList = new ArrayList<UserWorkTimeSheetTask>();
						uwtSheetTaskListTaskList = db
								.getUserWorkTimeSheetTaskByWorkTimeSheetIdAndUserId(
										uwts.getiUserWorkTimeSheetId(),
										uwts.getiUserId());
						if (uwtSheetTaskListTaskList != null
								&& uwtSheetTaskListTaskList.size() > 0) {
							UserWorkTimeSheetTask userWorkTimeSheetTask = new UserWorkTimeSheetTask();
							userWorkTimeSheetTask = uwtSheetTaskListTaskList
									.get(0);

							userWorkTimeSheetTask.setFlgIsDirty("1");
							userWorkTimeSheetTask.setDtUpdated(format
									.format(curDate));
							userWorkTimeSheetTask.setFlgStatus("1");
							userWorkTimeSheetTask.setWorkEndDate(uwts
									.getWorkEndDate());
							userWorkTimeSheetTask.setWorkEndTime(uwts
									.getWorkEndTime());
							userWorkTimeSheetTask.setWorkEndDateTime(uwts
									.getWorkEndDateTime());
							userWorkTimeSheetTask.setUpdatedBy(CommonUtil
									.getUserId(mContext));

							TaskDataItem tdi = (TaskDataItem) taskLis
									.getSelectedItem();
							if (tdi != null && tdi.getiTaskId() != null) {
								userWorkTimeSheetTask.setItaskId(tdi
										.getiTaskId());
								userWorkTimeSheetTask
										.setIcrewDepartmentTaskId(tdi
												.getiCrewDepartmentTaskId());
							}

							db.updateUserWorkTimeSheetTaskTable(userWorkTimeSheetTask);
						}

						/**
						 * Start tap card activity
						 */
						Intent i = new Intent(WorkTimesheetActivity.this,
								WelcomeAdmin.class);
						i.putExtra("workNature", "end");
						startActivity(i);
						finish();

					} else {

						/**
						 * Tap out time should be after time of tap in.
						 */
						errorTxtLayOut.setVisibility(View.VISIBLE);
						errorTxt.setText(getResources().getString(
								R.string.tapOutErrorTxt));
					}

				} else {

					/**
					 * Save UserWorkTimeSheet.
					 */
					UserWorkTimeSheet uwts = new UserWorkTimeSheet();
					uwts.setiUserWorkTimeSheetId(getPk());
					uwts.setDtCreated(format.format(curDate));
					uwts.setDtUpdated(format.format(curDate));
					uwts.setiUserId(strUserid);
					uwts.setiUserServiceTermId(ustd.getIuserServiceTermId());
					uwts.setiShipId(CommonUtil.getShipId(mContext));
					uwts.setiTenantId(CommonUtil.getTenantId(mContext));
					uwts.setWorkStartDate(finalWorkDate);
					uwts.setWorkStartTime(workTime.getText().toString());
					uwts.setWorkStartDateTime(uwts.getWorkStartDate() + ":"
							+ uwts.getWorkStartTime());

					/**
					 * This code check that selected date have idl or not. if
					 * have idl then check repeated or missed
					 */
					List<InternationalDates> idlList = new ArrayList<InternationalDates>();
					idlList = db.getInternationalDatesByShipIdAndDate(
							uwts.getiShipId(), finalWorkDate);
					InternationalDates idlDate = new InternationalDates();
					if (idlList != null && idlList.size() > 0) {
						idlDate = idlList.get(0);
					}
					/**
					 * Check data into IdlLogWork table. if idl stared 2nd day
					 * then both date should be same.
					 */
					List<IdlLogWork> idlLogWorlList = new ArrayList<IdlLogWork>();
					idlLogWorlList = db
							.getIdlLogWorkByInternationalDateId(idlDate
									.getIinternationalDateId());
					IdlLogWork idlLogWorkCheck = new IdlLogWork();
					if (idlLogWorlList != null && idlLogWorlList.size() > 0) {
						idlLogWorkCheck = idlLogWorlList.get(0);
					}

					uwts.setFlgInternationalOrderStart(idlLogWorkCheck
							.getFlgDayStart());

					uwts.setWorkEndDate(null);
					uwts.setWorkEndTime(null);
					uwts.setWorkEndDateTime(null);
					uwts.setCreatedBy(CommonUtil.getUserId(mContext));
					uwts.setUpdatedBy(CommonUtil.getUserId(mContext));

					db.insertUserWorkTimeSheetTable(uwts);

					/**
					 * Save UserWorkTimeSheetTask.
					 */
					TaskDataItem tdi = (TaskDataItem) taskLis.getSelectedItem();
					if (tdi != null && tdi.getiTaskId() != null) {
						UserWorkTimeSheetTask uwtst = new UserWorkTimeSheetTask(
								getPk(), uwts.getiUserWorkTimeSheetId(), uwts
										.getWorkStartDate(), uwts
										.getWorkStartTime(), uwts
										.getWorkEndDate(), uwts
										.getWorkEndTime(), uwts
										.getWorkStartDateTime(), uwts
										.getWorkEndDateTime(), uwts
										.getiUserId(), uwts
										.getiUserServiceTermId(), tdi
										.getiTaskId(), tdi
										.getiCrewDepartmentTaskId(),
								"Description", "0", format.format(curDate),
								format.format(curDate), "0", "1", CommonUtil
										.getShipId(mContext), CommonUtil
										.getTenantId(mContext), CommonUtil
										.getUserId(mContext), CommonUtil
										.getUserId(mContext));
						db.insertUserWorkTimeSheetTaskTable(uwtst);
					}

					/**
					 * Start tap card activity
					 */
					Intent i = new Intent(WorkTimesheetActivity.this,
							WelcomeAdmin.class);
					i.putExtra("workNature", "start");
					startActivity(i);
					finish();

				}

				db.close();

			}
		});

	}

	/**
	 * Below method return task list for spiner.
	 * 
	 * @param strUserId
	 * @return
	 */
	public List<TaskDataItem> getUserTaskList(String strUserId) {
		List<TaskDataItem> taskList = new ArrayList<TaskDataItem>();
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		DatabaseSupport db = new DatabaseSupport(mContext);
		List<UserServiceTerm> ustList = db.getCurrentUserServiceTerm(strUserId,
				format.format(curDate));
		if (ustList != null && ustList.size() > 0) {

			List<UserServiceTermRole> ustRoleList = new ArrayList<UserServiceTermRole>();
			ustRoleList = db.getUserServiceTermRoleByuserServicetermId(ustList
					.get(0).getiUserServiceTermId());
			if (ustRoleList != null && ustRoleList.size() > 0) {
				List<RoleCrewDepartment> rcdList = new ArrayList<RoleCrewDepartment>();
				rcdList = db.getRoleCrewDepartMentDataByRoleId(ustRoleList.get(
						0).getiRoleId());
				if (rcdList != null && rcdList.size() > 0) {

					List<CrewDepartmentTask> cdTaskList = new ArrayList<CrewDepartmentTask>();
					cdTaskList = db
							.getCrewDepartmentTaskDataByCrewDepartmentId(rcdList
									.get(0).getIcrewDepartmentId());

					if (cdTaskList != null && cdTaskList.size() > 0) {
						for (CrewDepartmentTask cdt : cdTaskList) {
							List<Tasks> innerTaskList = db.getTasksDataById(cdt
									.getItaskId());
							if (innerTaskList != null
									&& innerTaskList.size() > 0) {

								Tasks tasks = innerTaskList.get(0);
								TaskDataItem tdi = new TaskDataItem(
										tasks.getItaskId(),
										tasks.getStrTaskName(),
										cdt.getIcrewDepartmentTaskId(),
										tasks.getiTenantId());
								taskList.add(tdi);
							}

						}
					}

				}
			}

		}

		return taskList;
	}

	/**
	 * Ripunjay.s This method return today and yesterday date on the basis of
	 * IDL 1: Repeated 2: Missed
	 * 
	 * @param cdate
	 * @param userDailyWorkDetail
	 */
	public PairWorkDate getPrevTodayNextWorkingDate(Date cdate, String strShipId) {

		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		DatabaseSupport db = new DatabaseSupport(mContext);

		L.fv("Step : 1 Enter into idl bassed set date : " + curDate);

		// List<InternationalDates> idList = db.getAllInternationalDates();
		// L.fv(" Step : 2 All Idl list is : "+(idList != null ? idList.size() :
		// "0" ));
		/**
		 * This code check that current date have idl or not. if have idl then
		 * check repeated or missed
		 */
		List<InternationalDates> idlList = new ArrayList<InternationalDates>();
		idlList = db.getInternationalDatesByShipIdAndDate(strShipId,
				format.format(curDate));
		L.fv("Step 3 : passing date for idl current is : "
				+ format.format(curDate) + " and ship id is : " + strShipId);
		InternationalDates idlDate = new InternationalDates();
		if (idlList != null && idlList.size() > 0) {
			idlDate = idlList.get(0);
			L.fv(" Step 4 : Today idl exist and idl is : "
					+ idlDate.getFlgRepeated() + " and date is : "
					+ idlDate.getInternationalDate());
		}

		PairWorkDate pwd = new PairWorkDate();

		if (idlDate != null && idlDate.getFlgRepeated() == 2) {
			/**
			 * Missed logic implemented
			 */
			// Set device datetime
			Calendar calendar = getCalendarForNow();
			calendar.add(Calendar.DAY_OF_MONTH, 1);
			setDeviceTime(String.valueOf(calendar.getTimeInMillis()));

			pwd.setYesterdayDate(getDateBeforePassingDays(2));
			pwd.setTodayDate(format.format(calendar.getTime()));

			L.fv("Step 5: current day Idl missed date then today is : "
					+ pwd.getTodayDate() + " and yesterday date is : "
					+ pwd.getYesterdayDate());

		} else if (idlDate != null && idlDate.getFlgRepeated() == 1) {

			/**
			 * Check data into IdlLogWork table. if idl stared 2nd day then both
			 * date should be same.
			 */
			List<IdlLogWork> idlLogWorlList = new ArrayList<IdlLogWork>();
			idlLogWorlList = db.getIdlLogWorkByInternationalDateId(idlDate
					.getIinternationalDateId());
			IdlLogWork idlLogWorkCheck = new IdlLogWork();
			L.fv("Step : 6 going to check idl : " + idlLogWorlList);
			if (idlLogWorlList != null && idlLogWorlList.size() > 0) {
				idlLogWorkCheck = idlLogWorlList.get(0);

				L.fv("Step : 7 check idl : flag day start is : "
						+ idlLogWorkCheck.getFlgDayStart());
			}

			if (idlLogWorkCheck != null
					&& idlLogWorkCheck.getFlgDayStart() == 2) {
				/**
				 * Repeated logic implemented
				 */
				pwd.setYesterdayDate(format.format(curDate));
				pwd.setTodayDate(format.format(curDate));

				L.fv("Step : 8 second day start of idl repeat : today date is : "
						+ pwd.getTodayDate()
						+ " and yesterday date is : "
						+ pwd.getYesterdayDate());
			} else {

				L.fv("Step : 9 first day of idl repeate : ");
				/**
				 * Repeated logic implemented
				 */
				pwd.setYesterdayDate(getDateBeforePassingDays(1));
				pwd.setTodayDate(format.format(curDate));

				/**
				 * insert data into IdlLogWork table.
				 */
				IdlLogWork idlLogWork = new IdlLogWork();
				idlLogWork.setiIdlLogWorkId(getPk());
				idlLogWork.setIinternationalDateId(idlDate
						.getIinternationalDateId());
				idlLogWork.setInternationalDate(idlDate.getInternationalDate());
				idlLogWork.setDtCreated(format.format(curDate));
				idlLogWork.setDtUpdated(format.format(curDate));
				idlLogWork.setiTenantId(CommonUtil.getTenantId(mContext));
				idlLogWork.setiShipId(CommonUtil.getShipId(mContext));
				idlLogWork.setFlgIsDirty(1);
				idlLogWork.setFlgRepeated(idlDate.getFlgRepeated());
				// day start 1 : i.e. first day
				idlLogWork.setFlgDayStart(1);

				db.insertIdlLogWorkRow(idlLogWork);
			}
			L.fv("Step 9 : Current day Idl repeated date then today is : "
					+ pwd.getTodayDate() + " and yesterday date is : "
					+ pwd.getYesterdayDate());

		} else {

			/**
			 * This code check that yesterday date have idl or not. if have idl
			 * then check repeated or missed
			 */

			idlList = new ArrayList<InternationalDates>();
			idlList = db.getInternationalDatesByShipIdAndDate(strShipId,
					getDateBeforePassingDays(1));
			L.fv("Step 10 : date passing for idl : "
					+ getDateBeforePassingDays(1));
			L.fv("Step 11 : yesterday idl exist : " + idlList);
			InternationalDates idlYesterday = new InternationalDates();
			if (idlList != null && idlList.size() > 0) {
				idlYesterday = idlList.get(0);

				L.fv("Step 12: yesterday idl exist then flag is : "
						+ idlYesterday.getFlgRepeated() + " and date is "
						+ idlYesterday.getInternationalDate());
				if (idlYesterday.getFlgRepeated() == 2) {

					pwd.setYesterdayDate(getDateBeforePassingDays(2));

					L.fv(" Step 13 : yesterday Idl missed date then today is : "
							+ pwd.getTodayDate()
							+ " and yesterday date is : "
							+ pwd.getYesterdayDate());
				} else if (idlYesterday.getFlgRepeated() == 1) {

					/**
					 * Check data into IdlLogWork table. if idl end 2nd day then
					 * date should be different.
					 */
					List<IdlLogWork> idlLogWorlListForCheck = new ArrayList<IdlLogWork>();
					idlLogWorlListForCheck = db
							.getIdlLogWorkByInternationalDateId(idlYesterday
									.getIinternationalDateId());
					IdlLogWork idlLogWorkCheck = new IdlLogWork();
					L.fv("Step : 14 going to check idl in case of yesterday : "
							+ idlLogWorlListForCheck);
					if (idlLogWorlListForCheck != null
							&& idlLogWorlListForCheck.size() > 0) {
						idlLogWorkCheck = idlLogWorlListForCheck.get(0);

						L.fv("Step : 15 idl check exist : day start :  "
								+ idlLogWorkCheck.getFlgDayStart());
					}

					if (idlLogWorkCheck != null
							&& idlLogWorkCheck.getFlgDayStart() == 2) {

						pwd.setYesterdayDate(getDateBeforePassingDays(1));
						pwd.setTodayDate(format.format(curDate));

						L.fv("Step : 16 second day end ");
					} else {

						L.fv("Step : 17 second day started : ");

						Calendar calendar = getCalendarForNow();
						calendar.add(Calendar.DAY_OF_MONTH, -1);
						setDeviceTime(String
								.valueOf(calendar.getTimeInMillis()));
						curDate = new Date();

						pwd.setYesterdayDate(format.format(curDate));
						pwd.setTodayDate(format.format(curDate));

						/**
						 * update data into IdlLogWork table.
						 */
						List<IdlLogWork> idlLogWorlList = new ArrayList<IdlLogWork>();
						idlLogWorlList = db
								.getIdlLogWorkByInternationalDateId(idlYesterday
										.getIinternationalDateId());
						IdlLogWork idlLogWork = new IdlLogWork();
						if (idlLogWorlList != null && idlLogWorlList.size() > 0) {
							idlLogWork = idlLogWorlList.get(0);

							L.fv("Step : 18 set day start as 2nd : ");

							idlLogWork.setFlgIsDirty(1);
							idlLogWork.setFlgDayStart(2);
							idlLogWork.setDtUpdated(format.format(curDate));

							long result = db.updateIdlLogWorkRow(idlLogWork);
							L.fv("Step : 19 after result is : " + result);

						} else {
							/**
							 * insert data into IdlLogWork table.
							 */
							idlLogWork = new IdlLogWork();
							idlLogWork.setiIdlLogWorkId(getPk());
							idlLogWork.setIinternationalDateId(idlDate
									.getIinternationalDateId());
							idlLogWork.setInternationalDate(idlDate
									.getInternationalDate());
							idlLogWork.setDtCreated(format.format(curDate));
							idlLogWork.setDtUpdated(format.format(curDate));
							idlLogWork.setiTenantId(CommonUtil
									.getTenantId(mContext));
							idlLogWork.setiShipId(CommonUtil
									.getShipId(mContext));
							idlLogWork.setFlgIsDirty(1);
							idlLogWork.setFlgRepeated(idlDate.getFlgRepeated());
							// day start as 2nd 2 : i.e. first day
							idlLogWork.setFlgDayStart(2);

							long result = db.insertIdlLogWorkRow(idlLogWork);
							L.fv("Step : 20 safter insert as 2nd day : "
									+ result);
						}
					}

					L.fv("Step 21 : yesterday Idl repeate date then today is : "
							+ pwd.getTodayDate()
							+ " and yesterday date is : "
							+ pwd.getYesterdayDate());

				} else {
					pwd.setYesterdayDate(getDateBeforePassingDays(1));
					L.fv(" Step 22 : yesterday nothing Idl date then today is : "
							+ pwd.getTodayDate()
							+ " and yesterday date is : "
							+ pwd.getYesterdayDate());
				}

			} else {
				pwd.setYesterdayDate(getDateBeforePassingDays(1));

				L.fv("Step 23 : today nothing Idl date then today is : "
						+ pwd.getTodayDate() + " and yesterday date is : "
						+ pwd.getYesterdayDate());
			}
			pwd.setTodayDate(format.format(curDate));

			L.fv("Step 24 : without idl date then today is : "
					+ pwd.getTodayDate() + " and yesterday date is : "
					+ pwd.getYesterdayDate());
		}

		return pwd;
	}

	/**
	 * set device date if missed and repeat
	 */
	@SuppressWarnings("static-access")
	public void setDeviceTime(String strDateTme) {
		try {

			Calendar c = Calendar.getInstance();
			// c.set(2013, 8, 15, 12, 34, 56);
			c.setTimeInMillis(Long.parseLong(strDateTme));
			AlarmManager am = (AlarmManager) mContext
					.getSystemService(mContext.ALARM_SERVICE);
			am.setTime(c.getTimeInMillis());
			// am.setTimeZone(timeZone);

		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	/**
	 * get date before passing number of days.
	 * 
	 * @return
	 */
	public String getDateBeforePassingDays(int nod) {

		Calendar calendar = getCalendarForNow();
		Date curDate = new Date();
		calendar.add(Calendar.DAY_OF_MONTH, -nod);
		/*
		 * calendar.set(1900 + curDate.getYear(), curDate.getMonth(),
		 * curDate.getDate() - nod);
		 */

		return dateFormatter.format(calendar.getTime());
	}

	private static Calendar getCalendarForNow() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		return calendar;
	}

	private String getMonth(int month) {
		String monthName = "";
		switch (month) {
		case 1:
			monthName = "Jan";
			break;
		case 2:
			monthName = "Feb";
			break;
		case 3:
			monthName = "Mar";
			break;
		case 4:
			monthName = "Apr";
			break;
		case 5:
			monthName = "May";
			break;
		case 6:
			monthName = "Jun";
			break;
		case 7:
			monthName = "Jul";
			break;
		case 8:
			monthName = "Aug";
			break;
		case 9:
			monthName = "Sep";
			break;
		case 10:
			monthName = "Oct";
			break;
		case 11:
			monthName = "Nov";
			break;
		case 12:
			monthName = "Dec";
			break;

		default:
			break;
		}

		return monthName;
	}

	public String getPk() {
		Date curDate = new Date();
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

		String pkId = (tm.getDeviceId() != null ? tm.getDeviceId() : "tmp_dv")
				+ "_" + curDate.getTime();

		return pkId;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.work_timesheet, menu);

		adminSettingItem = menu.findItem(R.id.action_settings);
		adminSettingItem.setVisible(false);
		DatabaseSupport db = new DatabaseSupport(mContext);
		List<RoleTabletFunction> trfList = new ArrayList<RoleTabletFunction>();
		trfList = db.getTabletRoleFunctionsRowByRoleIdAndFunctionCode(db
				.getUserMasterSingleRow(strUserid).get(0).getiRoleId(), "sync");
		if (trfList != null && trfList.size() > 0) {
			adminSettingItem.setVisible(true);
		}

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {

			Intent i = new Intent(WorkTimesheetActivity.this,
					AdminMainActivity.class);
			startActivity(i);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
