package com.rms.db;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.rms.model.CardTypeData;
import com.rms.model.CrewDepartment;
import com.rms.model.CrewDepartmentTask;
import com.rms.model.IdlLogWork;
import com.rms.model.InternationalDates;
import com.rms.model.ModelData;
import com.rms.model.RoleCrewDepartment;
import com.rms.model.RoleTabletFunction;
import com.rms.model.Roles;
import com.rms.model.ShipMaster;
import com.rms.model.SyncHistory;
import com.rms.model.SyncStatus;
import com.rms.model.Tasks;
import com.rms.model.TenantData;
import com.rms.model.TimeZoneData;
import com.rms.model.UserMaster;
import com.rms.model.UserNfcTagData;
import com.rms.model.UserServiceTerm;
import com.rms.model.UserServiceTermRole;
import com.rms.model.UserWorkTimeSheet;
import com.rms.model.UserWorkTimeSheetTask;
import com.rms.utils.L;

public class DatabaseSupport extends SQLiteOpenHelper {

	public static final int DATABASE_VERSION = 2;
	public static final String DATABASE_NAME = "rms.db";

	// table user and its field
	// public static final String KEY_ID = "id";

	public static final String TENANT = "Tenant";
	public static final String TABLE_SYNC_STATUS = "SyncStatus";
	public static final String TABLE_TASKS = "Tasks";
	public static final String TABLE_CREW_DEPARTMENT = "CrewDepartment";
	public static final String TABLE_ROLE_CREW_DEPARTMENT = "RoleCrewDepartment";
	public static final String TABLE_CREW_DEPARTMENT_TASK = "CrewDepartmentTask";

	// Common Columns fields
	public static final String MAC_ID = "strMacId";
	public static final String VERSION = "version";
	public static final String FLG_DELETED = "flgDeleted";
	public static final String FLG_IS_DIRTY = "flgIsDirty";
	public static final String FLG_DEVICE_IS_DIRTY = "flgIsDeviceDirty";
	public static final String FLG_STATUS = "flgStatus";
	public static final String FLG_REVERTED = "flgReverted";
	public static final String FLG_LARGE = "flgIsLarge";
	public static final String CREATED_DATE = "dtCreated";
	public static final String FLG_IS_EDITED = "flgIsEdited";
	public static final String MODIFIED_DATE = "dtUpdated";
	public static final String CREATED_BY = "createdBy";
	public static final String MODIFIED_BY = "updatedBy";
	public static final String SEQUENCE_NUMBER = "sequence";
	public static final String FIRST_AND_LAST_NAME = "strFirstNameLastName";
	public static final String TENANT_ID = "iTenantId";
	public static final String SHIP_ID = "iShipId";

	public static final String REMARKS = "strRemarks";
	public static final String INSPECTED_BY = "inspectedBy";
	public static final String REVIEWED_BY = "reviewedBy";
	public static final String APPROVED_BY = "approvedBy";
	public static final String ACCEPTED_BY = "acceptedBy";
	public static final String COMPLETED_BY = "completedBy";

	// sync_status table fields
	public static final String SYNC_STATUS_ID = "iSyncStatusId";
	public static final String SYNC_MODE = "strSyncMode";
	public static final String SYNC_DATE = "dtSyncDate";
	public static final String DATA_SYNC_MODE = "dataSyncMode";
	public static final String SERVER_ADDRESS = "server_address";
	public static final String SYNC_TIME="syncTime";

	// Table TENANT and its field
	public static final String TABLE_TENANT = "Tenant";
	public static final String KEY_TENANT_ID = "iTenantId";
	public static final String KEY_STR_COMPANY_NAME = "strCompanyName";

	// Table Model and its field
	public static final String TABLE_MODEL = "Model";
	public static final String KEY_MODEL_ID = "strModelId";
	public static final String KEY_MODEL_NAME = "strModelName";

	// Table SHIP_MASTER and its field
	public static final String TABLE_SHIP_MASTER = "ShipMaster";
	public static final String KEY_SHIP_ID = "iShipId";
	public static final String KEY_SOCIETY_ID = "iClassificationSocietyId";
	public static final String KEY_SHIP_TYPE_ID = "iShipTypeId";
	public static final String KEY_SHIP_NAME = "strShipName";
	public static final String KEY_S_DESCRIPTION = "strDescription";
	public static final String KEY_DT_CREATED = "dtCreated";
	public static final String KEY_DT_UPDATED = "dtUpdated";
	public static final String KEY_FLG_STATUS = "flgStatus";
	public static final String KEY_FLG_DELETED = "flgDeleted";
	public static final String KEY_FLG_IS_DIRTY = "flgIsDirty";
	public static final String KEY_RULE_LIST_ID = "iRuleListId";
	public static final String KEY_SHIP_IMO_NUMBER = "iShipIMONumber";
	public static final String KEY_STR_FLAG = "strFlag";
	public static final String KEY_LOGO = "strLogo";
	public static final String KEY_FILE_SIZE = "fileSize";
	public static final String KEY_FILE_NAME = "strFileName";
	public static final String KEY_FILE_PATH = "strFilePath";
	public static final String KEY_FILE_TYPE = "strFileType";
	public static final String KEY_SHIP_CODE = "strShipCode";

	// Table ROLE and its field
	public static final String TABLE_ROLE = "Roles";
	public static final String KEY_ROLE_ID = "iRoleId";
	public static final String KEY_ROLE = "strRole";
	public static final String KEY_STR_TYPE = "strType";
	public static final String KEY_TXT_DESCRIPTION = "txtDescription";
	public static final String KEY_RANK_PRIORITY = "iRankPriority";
	public static final String KEY_STR_ROLE_TYPE = "strRoleType"; // PICK FROM
																	// TENENT

	// Table UserMaster and its field
	public static final String TABLE_USER_MASTER = "UserMaster";
	public static final String KEY_iUSER_ID = "iUserId";
	public static final String KEY_USER_NAME = "strUserName";
	public static final String KEY_USER_PASSWORD = "strPassword";
	public static final String KEY_FIRST_NAME = "strFirstName";
	public static final String KEY_LAST_NAME = "strLastName";
	public static final String KEY_DATE_OF_BIRTH = "dtDateOfBirth";
	public static final String KEY_EMAIL = "strEmail";
	public static final String KEY_SHIP_HOLIDAY_LIST_ID = "iShipHolidayListId";
	public static final String KEY_MIN_WORK_HOUR_WEEK_DAYS = "fltMinWorkHourWeekDays";
	public static final String KEY_MIN_WORK_HOUR_SATUREDAY = "fltMinWorkHourSaturdays";
	public static final String KEY_MIN_WORK_HOUR_SUNDAY = "fltMinWorkHourSundays";
	public static final String KEY_MIN_WORK_HOUR_HOLIDAY = "fltMinWorkHourHolidays";
	public static final String KEY_FLTOT_INCLUDED_IN_WAGE = "fltOTIncludedInWage";
	public static final String KEY_FLTOT_RATE_PER_HOUR = "fltOTRatePerHour";
	public static final String KEY_FLG_IS_OVER_TIME_ENABLED = "flgIsOverTimeEnabled";
	public static final String KEY_FLG_IS_WATCHKEEPER = "flgIsWatchkeeper";
	public static final String KEY_FAX_NUMBER = "faxNumber";
	public static final String KEY_HAND_PHONE = "handPhone";
	public static final String KEY_LANDLINE_NUMBER = "landLineNumber";
	public static final String KEY_PAN_NUMBER = "panNumber";
	public static final String KEY_PIN_CODE = "pinCode";
	public static final String KEY_ADDRESS_FIRST = "addressFirst";
	public static final String KEY_ADDRESS_SECOND = "addressSecond";
	public static final String KEY_ADDRESS_THIRD = "addressThird";
	public static final String KEY_CITY = "city";
	public static final String KEY_STATE = "state";
	public static final String KEY_COUNTRY_ID = "iCountryId";

	// new field
	public static final String KEY_EMPLOYEE_NO = "strEmployeeNo";
	public static final String KEY_MIDDLE_NAME = "strMiddleName";

	// Table UserServiceTerm and its field
	public static final String TABLE_USER_SERVICE_TERM = "UserServiceTerm";
	public static final String KEY_USER_SERVICE_TERM_ID = "iUserServiceTermId";
	public static final String KEY_DT_TERM_FROM = "dtTermFrom";
	public static final String KEY_DT_TERM_TO = "dtTermTo";

	// Table UserServiceTermRole and its field
	public static final String TABLE_USER_SERVICE_TERM_ROLE = "UserServiceTermRole";
	public static final String KEY_USER_SERVICE_TERM_ROLE_ID = "strUserServiceTermRoleId";
	public static final String POWER_PLUS_SCORE = "powerPlusScore";
	public static final String FIELD_ONE = "strField1";
	public static final String FIELD_TWO = "strField2";

	// Table CARD TYPE and its field
	public static final String TABLE_CARD_TYPE = "CardType";
	public static final String KEY_CARD_ID = "strCardId";
	public static final String KEY_CARD_TYPE = "strCardType";
	public static final String KEY_NFC_TAG_ID = "strNfcTagId";

	// Table UserNfc and its field
	public static final String TABLE_USER_NFC = "UserNfcTagData";

	public static final String KEY_NFC_CARD_ID = "iUserNfcTagDataId";
	public static final String KEY_NFC_CARD_TYPE = "strCardType";

	public static final String KEY_FLG_DIRTY = "flgIsDirty";

	// Table InternationalDates and field
	public static final String TABLE_INTERNATIONALDATES = "InternationalDates";
	public static final String KEY_INTERNATIONALDATES_ID = "iInternationalDatesId";
	public static final String FLG_REPEATED = "flgRepeated";
	public static final String KEY_INTERNATIONALDATE = "internationalDate";
	
	public static final String KEY_MODULE_NAME="moduleName";

	/**
	 * VIWebDirtyTables
	 * 
	 * @param context
	 */
	public static final String TABLE_RMS_WEB_DIRTY_TABLES = "RMSWebDirtyTables";

	/**
	 * Column VIWebDirtyTables
	 * 
	 * @param context
	 */
	public static final String KEY_VI_WEB_DIRTY_TABLES_ID = "iVIWebDirtyTablesId";
	public static final String STR_TABLE_NAME = "strTableName";
	public static final String FLAG_DIRTY_STATUS = "flgDirtyStatus";

	/**
	 * RmsSyncHistory
	 * 
	 * @param context
	 */
	public static final String TABLE_SYNC_HISTORY = "RmsSyncHistory";

	/**
	 * Column RmsSyncHistory
	 * 
	 * @param context
	 */
	public static final String KEY_SYNC_HISTORY_ID = "iRmsSyncHistoryId";
	public static final String LOG_MSG = "logMessage";
	public static final String SYNC_PROGRESS = "progress";
	public static final String SYNC_GENERATOR = "generator";
	public static final String SYNC_FILE_NAME = "strFilename";
	public static final String SYNC_ORDER_ID = "syncOrderid";
	public static final String SYNC_ACK_DATE = "dtAcknowledgeDate";
	public static final String SYNC_LAST_DOWN_DATE = "lastDownLoadDate";
	public static final String SYNC_GEN_DATE = "dtGeneratedDate";
	public static final String REG_TABLET_ID = "strRegisterTabletId";

	/**
	 * Tasks table fields
	 */
	public static final String TASK_ID = "iTaskId";
	public static final String TASK_NAME = "strTaskName";

	/**
	 * CrewDepartment table fields
	 */
	public static final String CREW_DEPARTMENT_ID = "icrewDepartmentId";
	public static final String DEPARTMENT_NAME = "strDepartmentName";

	/**
	 * RoleCrewDepartment table fields
	 */
	public static final String ROLE_CREW_DEPARTMENT_ID = "iroleCrewDepartmentId";

	/**
	 * CrewDepartmentTask table fields
	 */
	public static final String CREW_DEPARTMENT_TASK_ID = "icrewDepartmentTaskId";

	/**
	 * RoleTabletFunctions
	 * 
	 * @param context
	 */
	public static final String TABLE_ROLE_FUNCTION = "RoleTabletFunctions";

	/**
	 * Column RoleTabletFunctions
	 * 
	 * @param context
	 */
	public static final String KEY_ROLE_FUNCTION_ID = "iRoleTabletFunctionsId";
	public static final String ROLE_FUNCTION_CODE = "strFunctionCode";

	/**
	 * UserWorkTimeSheet table and fields
	 * 
	 * @param context
	 */
	public static final String TABLE_USER_WORKTIME_SHEET = "UserWorkTimeSheet";
	public static final String USER_WORK_TIMESHEET_ID = "iUserWorkTimeSheetId";
	public static final String WORK_START_DATE = "workStartDate";
	public static final String WORK_START_TIME = "workStartTime";
	public static final String WORK_END_DATE = "workEndDate";
	public static final String WORK_END_TIME = "workEndTime";
	public static final String WORK_START_DATE_TIME = "workStartDateTime";
	public static final String WORK_END_DATE_TIME = "workEndDateTime";
	public static final String FLG_INTERNATIONAL_ORDER_START = "flgInterNationalOrderStart";
	public static final String FLG_INTERNATIONAL_ORDER_END = "flgInterNationalOrderEnd";
	public static final String FLG_PROCESSED = "flgProcessed";

	/**
	 * UserWorkTimeSheet table and fields
	 * 
	 * @param context
	 */
	public static final String TABLE_USER_WORKTIME_SHEET_TASK = "UserWorkTimeSheetTask";
	public static final String USER_WORK_TIMESHEET_TASK_ID = "iUserWorkTimeSheetTaskId";

	/**
	 * IdlLogWork
	 * 
	 * @param context
	 */
	public static final String TABLE_IDL_LOGWORK = "IdlLogWork";
	public static final String FLG_DAY_START = "flgDayStart";
	public static final String IDL_LOGWORK_ID = "iIdlLogWorkId";

	// Table TimeZoneData and field
	public static final String TABLE_TIMEZONEDATA = "TimeZoneData";
	public static final String KEY_TIMEZONEDATA_ID = "iTimeZoneDataId";
	public static final String KEY_STRZONE_ID = "strZoneId";
	public static final String KEY_STRZONE_NAME = "strTimeZoneName";
	public static final String KEY_STRTIME = "strTime";

	public DatabaseSupport(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL(CREATE_TENANT_TABLE);
		db.execSQL(CREATE_SHIP_MASTER_TABLE);
		db.execSQL(CREATE_USER_MASTER_TABLE);
		db.execSQL(CREATE_ROLE_TABLE);
		db.execSQL(CREATE_USER_SERVICE_TERM_TABLE);
		db.execSQL(CREATE_USER_SERVICE_TERM_ROLE_TABLE);
		db.execSQL(CREATE_CARD_TYPE_TABLE);
		db.execSQL(CREATE_USER_NFC_TABLE);
		db.execSQL(CREATE_MODEL_TABLE);

		// SYNC HISTORY
		db.execSQL(CREATE_SYNC_HISTORY_TABLE);

		// SYNC ROLEFUNCTION
		db.execSQL(CREATE_ROLE_FUNCTION_TABLE);

		db.execSQL(SYNC_STATUS_CREATE);
		db.execSQL(CREATE_TABLE_TASK);
		db.execSQL(CREATE_TABLE_CREW_DEPARTMENT);
		db.execSQL(CREATE_TABLE_ROLE_CREW_DEPARTMENT);
		db.execSQL(CREATE_TABLE_CREW_DEPARTMENT_TASK);
		db.execSQL(CREATE_INTERNATIONALDATE_TABLE);
		db.execSQL(CREATE_IDL_LOGWORK_TABLE);

		db.execSQL(CREATE_USER_WORKTIME_SHEET_TABLE);
		db.execSQL(CREATE_USER_WORKTIME_SHEET_TASK_TABLE);

		db.execSQL(CREATE_TIMEZONEDATA_TABLE);
		

	}

	public void updateDataBase() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TENANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIP_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARD_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_NFC);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODEL);

		// SYNC HISTORY
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_HISTORY);

		// ROLE FUNCTION
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_FUNCTION);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_STATUS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREW_DEPARTMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_CREW_DEPARTMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREW_DEPARTMENT_TASK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_INTERNATIONALDATES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_IDL_LOGWORK);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_WORKTIME_SHEET);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_WORKTIME_SHEET_TASK);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMEZONEDATA);

		// Create tables again
		onCreate(db);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TENANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIP_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARD_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_NFC);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODEL);

		// SYNC HISTORY
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_HISTORY);

		// ROLE FUNCTION
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_FUNCTION);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_STATUS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREW_DEPARTMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_CREW_DEPARTMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREW_DEPARTMENT_TASK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_INTERNATIONALDATES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_IDL_LOGWORK);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_WORKTIME_SHEET);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_WORKTIME_SHEET_TASK);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMEZONEDATA);
		// Create tables again
		onCreate(db);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TENANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIP_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARD_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_NFC);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODEL);

		// SYNC HISTORY
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_HISTORY);

		// ROLE FUNCTION
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_FUNCTION);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_STATUS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TASKS);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREW_DEPARTMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_CREW_DEPARTMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREW_DEPARTMENT_TASK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_INTERNATIONALDATES);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_IDL_LOGWORK);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_WORKTIME_SHEET);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_WORKTIME_SHEET_TASK);

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TIMEZONEDATA);
		// Create tables again
		onCreate(db);

	}

	/**
	 * @author ripunjay.s Creating SYNC_STATUS table
	 */
	public static final String SYNC_STATUS_CREATE = "CREATE TABLE "
			+ TABLE_SYNC_STATUS + "(" + SYNC_STATUS_ID
			+ " TEXT PRIMARY KEY, " + SYNC_DATE + " TEXT, " + SYNC_MODE
			+ " TEXT, " + DATA_SYNC_MODE + " TEXT, " + SERVER_ADDRESS
			+ " TEXT, " + MAC_ID + " TEXT, " + FLG_DEVICE_IS_DIRTY + " TEXT, " + SYNC_TIME 
			+ " TEXT, " + TENANT_ID + " TEXT, " + SHIP_ID + " TEXT, "  + FLG_STATUS
			+ " INTEGER DEFAULT 0, " + FLG_DELETED + " INTEGER DEFAULT 0, "
			+ FLG_IS_DIRTY + " INTEGER DEFAULT 0, " + CREATED_DATE
			+ " TEXT, " + MODIFIED_DATE + " TEXT, " + CREATED_BY + " TEXT, "
			+ MODIFIED_BY + " TEXT, " + KEY_MODULE_NAME + " TEXT " + ")";

	String CREATE_TENANT_TABLE = "CREATE TABLE " + TABLE_TENANT + "("
			+ KEY_TENANT_ID + " INTEGER PRIMARY KEY," + KEY_STR_COMPANY_NAME
			+ " TEXT" + ")";

	String CREATE_MODEL_TABLE = "CREATE TABLE " + TABLE_MODEL + "("
			+ KEY_MODEL_ID + " TEXT PRIMARY KEY," + KEY_MODEL_NAME + " TEXT"
			+ ")";

	String CREATE_ROLE_FUNCTION_TABLE = "CREATE TABLE " + TABLE_ROLE_FUNCTION
			+ "(" + KEY_ROLE_FUNCTION_ID + " TEXT PRIMARY KEY,"
			+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
			+ KEY_FLG_DELETED + " INTEGER," + ROLE_FUNCTION_CODE + " TEXT,"
			+ KEY_ROLE_ID + " TEXT," + FIELD_ONE + " TEXT," + FIELD_TWO
			+ " TEXT," + KEY_TENANT_ID + " TEXT" + ")";

	String CREATE_SYNC_HISTORY_TABLE = "CREATE TABLE " + TABLE_SYNC_HISTORY
			+ "(" + KEY_SYNC_HISTORY_ID + " TEXT PRIMARY KEY," + SYNC_DATE
			+ " DATE," + LOG_MSG + " TEXT," + SYNC_PROGRESS + " TEXT,"
			+ SYNC_GENERATOR + " TEXT, " + SYNC_MODE + " TEXT, "
			+ SYNC_FILE_NAME + " TEXT," + SYNC_ORDER_ID + " INTEGER,"
			+ KEY_FLG_DELETED + " INTEGER," + KEY_FLG_IS_DIRTY + " TEXT,"
			+ SYNC_ACK_DATE + " TEXT," + SYNC_LAST_DOWN_DATE + " DATE,"
			+ SYNC_GEN_DATE + " TEXT," + MAC_ID + " TEXT," + REG_TABLET_ID
			+ " TEXT," + KEY_SHIP_ID + " TEXT," + KEY_TENANT_ID + " TEXT" + ")";

	String CREATE_SHIP_MASTER_TABLE = "CREATE TABLE " + TABLE_SHIP_MASTER + "("
			+ KEY_SHIP_ID + " TEXT PRIMARY KEY," + KEY_TENANT_ID + " TEXT,"
			+ KEY_SOCIETY_ID + " TEXT," + KEY_SHIP_TYPE_ID + " TEXT,"
			+ KEY_SHIP_NAME + " TEXT," + KEY_S_DESCRIPTION + " TEXT,"
			+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
			+ KEY_FLG_STATUS + " TEXT," + KEY_FLG_DELETED + " TEXT,"
			+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_RULE_LIST_ID + " TEXT,"
			+ KEY_SHIP_IMO_NUMBER + " TEXT," + KEY_STR_FLAG + " TEXT,"
			+ KEY_LOGO + " TEXT," + KEY_FILE_SIZE + " TEXT," + KEY_FILE_NAME
			+ " TEXT," + KEY_FILE_PATH + " TEXT," + KEY_FILE_TYPE + " TEXT,"
			+ KEY_SHIP_CODE + " TEXT" + ")";

	String CREATE_ROLE_TABLE = "CREATE TABLE " + TABLE_ROLE + "(" + KEY_ROLE_ID
			+ " TEXT PRIMARY KEY," + KEY_ROLE + " TEXT," + KEY_DT_CREATED
			+ " TEXT," + KEY_DT_UPDATED + " TEXT," + KEY_FLG_IS_DIRTY
			+ " TEXT," + KEY_STR_TYPE + " TEXT," + KEY_FLG_DELETED + " TEXT,"
			+ KEY_TXT_DESCRIPTION + " TEXT," + KEY_RANK_PRIORITY + " TEXT,"
			+ KEY_TENANT_ID + " TEXT," + KEY_STR_ROLE_TYPE + " TEXT" + ")";

	String CREATE_USER_MASTER_TABLE = "CREATE TABLE " + TABLE_USER_MASTER + "("
			+ KEY_iUSER_ID + " TEXT PRIMARY KEY," + KEY_TENANT_ID + " TEXT,"
			+ KEY_USER_NAME + " TEXT," + KEY_USER_PASSWORD + " TEXT,"
			+ KEY_FIRST_NAME + " TEXT," + KEY_LAST_NAME + " TEXT,"
			+ KEY_DATE_OF_BIRTH + " TEXT," + KEY_TXT_DESCRIPTION + " TEXT,"
			+ KEY_EMAIL + " TEXT," + KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED
			+ " TEXT," + KEY_FLG_STATUS + " TEXT," + KEY_FLG_DELETED + " TEXT,"
			+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_SHIP_HOLIDAY_LIST_ID + " TEXT,"
			+ KEY_MIN_WORK_HOUR_WEEK_DAYS + " TEXT,"
			+ KEY_MIN_WORK_HOUR_SATUREDAY + " TEXT," + KEY_MIN_WORK_HOUR_SUNDAY
			+ " TEXT," + KEY_MIN_WORK_HOUR_HOLIDAY + " TEXT,"
			+ KEY_FLTOT_INCLUDED_IN_WAGE + " TEXT," + KEY_FLTOT_RATE_PER_HOUR
			+ " TEXT," + KEY_FLG_IS_OVER_TIME_ENABLED + " TEXT," + KEY_ROLE_ID
			+ " TEXT," + KEY_FLG_IS_WATCHKEEPER + " TEXT," + KEY_FAX_NUMBER
			+ " TEXT," + KEY_HAND_PHONE + " TEXT," + KEY_LANDLINE_NUMBER
			+ " TEXT," + KEY_PAN_NUMBER + " TEXT," + KEY_PIN_CODE + " TEXT,"
			+ KEY_ADDRESS_FIRST + " TEXT," + KEY_ADDRESS_SECOND + " TEXT,"
			+ KEY_ADDRESS_THIRD + " TEXT," + KEY_CITY + " TEXT," + KEY_STATE
			+ " TEXT," + KEY_COUNTRY_ID + " TEXT," + KEY_EMPLOYEE_NO + " TEXT,"
			+ KEY_MIDDLE_NAME + " TEXT" + ")";

	String CREATE_USER_SERVICE_TERM_TABLE = "CREATE TABLE "
			+ TABLE_USER_SERVICE_TERM + "(" + KEY_USER_SERVICE_TERM_ID
			+ " TEXT PRIMARY KEY," + KEY_iUSER_ID + " TEXT," + KEY_DT_TERM_FROM
			+ " DATE," + KEY_DT_TERM_TO + " DATE," + KEY_FLG_DELETED + " TEXT,"
			+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_DT_CREATED + " TEXT,"
			+ KEY_DT_UPDATED + " TEXT," + KEY_SHIP_ID + " TEXT,"
			+ KEY_TENANT_ID + " TEXT" + ")";

	String CREATE_INTERNATIONALDATE_TABLE = "CREATE TABLE "
			+ TABLE_INTERNATIONALDATES + "(" + KEY_INTERNATIONALDATES_ID
			+ " TEXT PRIMARY KEY," + KEY_INTERNATIONALDATE + " TEXT,"
			+ FLG_REPEATED + " INTERGER  DEFAULT 0 ," + KEY_TXT_DESCRIPTION
			+ " TEXT," + KEY_FLG_DELETED + " INTERGER  DEFAULT 0,"
			+ KEY_FLG_IS_DIRTY + " INTERGER  DEFAULT 0," + KEY_DT_CREATED
			+ " TEXT," + KEY_DT_UPDATED + " TEXT," + KEY_SHIP_ID + " TEXT,"
			+ KEY_TENANT_ID + " TEXT" + ")";

	String CREATE_IDL_LOGWORK_TABLE = "CREATE TABLE " + TABLE_IDL_LOGWORK + "("
			+ IDL_LOGWORK_ID + " TEXT PRIMARY KEY," + KEY_INTERNATIONALDATES_ID
			+ " TEXT," + KEY_INTERNATIONALDATE + " TEXT, " + FLG_DAY_START
			+ " INTERGER  DEFAULT 0 ," + FLG_REPEATED
			+ " INTERGER  DEFAULT 0 ," + KEY_TXT_DESCRIPTION + " TEXT,"
			+ KEY_FLG_DELETED + " INTERGER  DEFAULT 0," + KEY_FLG_IS_DIRTY
			+ " INTERGER  DEFAULT 0," + KEY_DT_CREATED + " TEXT,"
			+ KEY_DT_UPDATED + " TEXT," + KEY_SHIP_ID + " TEXT,"
			+ KEY_TENANT_ID + " TEXT" + ")";

	String CREATE_USER_SERVICE_TERM_ROLE_TABLE = "CREATE TABLE "
			+ TABLE_USER_SERVICE_TERM_ROLE + "("
			+ KEY_USER_SERVICE_TERM_ROLE_ID + " TEXT PRIMARY KEY,"
			+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
			+ KEY_FLG_DELETED + " TEXT," + KEY_FLG_IS_DIRTY + " TEXT,"
			+ KEY_ROLE_ID + " TEXT," + KEY_SHIP_ID + " TEXT," + KEY_TENANT_ID
			+ " TEXT," + KEY_iUSER_ID + " TEXT," + KEY_USER_SERVICE_TERM_ID
			+ " TEXT," + KEY_DT_TERM_FROM + " DATE," + KEY_DT_TERM_TO
			+ " DATE," + POWER_PLUS_SCORE + " INTEGER," + FIELD_ONE
			+ " INTEGER," + FIELD_TWO + " INTEGER" + ")";

	String CREATE_CARD_TYPE_TABLE = "CREATE TABLE " + TABLE_CARD_TYPE + "("
			+ KEY_CARD_ID + " TEXT PRIMARY KEY," + KEY_CARD_TYPE + " TEXT"
			+ ")";

	String CREATE_USER_NFC_TABLE = "CREATE TABLE " + TABLE_USER_NFC + "("
			+ KEY_NFC_CARD_ID + " TEXT PRIMARY KEY," + KEY_NFC_CARD_TYPE
			+ " TEXT," + KEY_iUSER_ID + " TEXT," + KEY_NFC_TAG_ID + " TEXT,"
			+ KEY_FLG_DIRTY + " TEXT," + KEY_FLG_DELETED + " TEXT,"
			+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
			+ KEY_TENANT_ID + " TEXT," + KEY_SHIP_ID + " TEXT" + ")";

	/**
	 * @author pushkar.m
	 * @param md
	 */

	String CREATE_TIMEZONEDATA_TABLE = "CREATE TABLE " + TABLE_TIMEZONEDATA
			+ "(" + KEY_TIMEZONEDATA_ID + " TEXT PRIMARY KEY," + KEY_STRZONE_ID
			+ " TEXT," + KEY_STRZONE_NAME + " TEXT, " + KEY_STRTIME + " TEXt, "
			+ CREATED_BY + " TEXT, " + MODIFIED_BY + " TEXT, "
			+ KEY_FLG_DELETED + " INTEGER," + KEY_FLG_IS_DIRTY + " TEXT,"
			+ KEY_S_DESCRIPTION + " TEXT," + CREATED_DATE + " TEXT , "
			+ MODIFIED_DATE + " TEXT, " + KEY_SHIP_ID + " TEXT,"
			+ KEY_TENANT_ID + " TEXT" + ")";

	/**
	 * creating VIWebDirtyTables table
	 */
	public static final String CREATE_RMS_WEBDIRTY_TABLE = "CREATE TABLE "
			+ TABLE_RMS_WEB_DIRTY_TABLES + "(" + KEY_VI_WEB_DIRTY_TABLES_ID
			+ " TEXT  PRIMARY KEY," + REMARKS + " TEXT," + STR_TABLE_NAME
			+ " TEXT," + FLAG_DIRTY_STATUS + " INTEGER," + SEQUENCE_NUMBER
			+ " INTEGER," + KEY_FLG_DELETED + " INTEGER," + KEY_FLG_IS_DIRTY
			+ " INTEGER," + TENANT_ID + " INTEGER " + ")";

	/**
	 * @author ripunjay.s Creating Task table
	 */
	public static final String CREATE_TABLE_TASK = "CREATE TABLE "
			+ TABLE_TASKS + " ( " + TASK_ID + " TEXT PRIMARY KEY , "
			+ TENANT_ID + " TEXT ," + TASK_NAME + " TEXT , " + FLG_DELETED
			+ " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY + " INTEGER DEFAULT 0 , "
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT , "
			+ KEY_S_DESCRIPTION + " TEXT " + ");";

	/**
	 * @author ripunjay.s Creating CrewDepartment table
	 */
	public static final String CREATE_TABLE_CREW_DEPARTMENT = "CREATE TABLE "
			+ TABLE_CREW_DEPARTMENT + " ( " + CREW_DEPARTMENT_ID
			+ " TEXT PRIMARY KEY , " + TENANT_ID + " TEXT ," + DEPARTMENT_NAME
			+ " TEXT , " + FLG_DELETED + " INTEGER DEFAULT 0 , " + FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , " + CREATED_DATE + " TEXT , "
			+ MODIFIED_DATE + " TEXT " + ");";

	/**
	 * @author ripunjay.s Creating RoleCrewDepartMent table
	 */
	public static final String CREATE_TABLE_ROLE_CREW_DEPARTMENT = "CREATE TABLE "
			+ TABLE_ROLE_CREW_DEPARTMENT
			+ " ( "
			+ ROLE_CREW_DEPARTMENT_ID
			+ " TEXT PRIMARY KEY , "
			+ TENANT_ID
			+ " TEXT ,"
			+ KEY_ROLE_ID
			+ " TEXT , "
			+ CREW_DEPARTMENT_ID
			+ " TEXT, "
			+ FLG_DELETED
			+ " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , "
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT " + ");";

	/**
	 * @author ripunjay.s Creating RoleCrewDepartMent table
	 */
	public static final String CREATE_TABLE_CREW_DEPARTMENT_TASK = "CREATE TABLE "
			+ TABLE_CREW_DEPARTMENT_TASK
			+ " ( "
			+ CREW_DEPARTMENT_TASK_ID
			+ " TEXT PRIMARY KEY , "
			+ TENANT_ID
			+ " TEXT ,"
			+ TASK_ID
			+ " TEXT , "
			+ CREW_DEPARTMENT_ID
			+ " TEXT, "
			+ FLG_DELETED
			+ " INTEGER DEFAULT 0 , "
			+ FLG_IS_DIRTY
			+ " INTEGER DEFAULT 0 , "
			+ CREATED_DATE + " TEXT , " + MODIFIED_DATE + " TEXT " + ");";

	/**
	 * 
	 * @author ripunjay.s Creating UserWorkTimeSheet table.
	 */
	String CREATE_USER_WORKTIME_SHEET_TABLE = "CREATE TABLE "
			+ TABLE_USER_WORKTIME_SHEET + "(" + USER_WORK_TIMESHEET_ID
			+ " TEXT PRIMARY KEY," + WORK_START_DATE + " DATE,"
			+ WORK_START_TIME + " TEXT," + WORK_END_DATE + " DATE,"
			+ WORK_END_TIME + " TEXT," + WORK_START_DATE_TIME + " TEXT,"
			+ WORK_END_DATE_TIME + " TEXT," + KEY_iUSER_ID + " TEXT,"
			+ KEY_USER_SERVICE_TERM_ID + " TEXT," + KEY_FLG_DELETED
			+ " INTEGER," + KEY_FLG_IS_DIRTY + " TEXT,"
			+ FLG_INTERNATIONAL_ORDER_START + " INTEGER DEFAULT 0 ,"
			+ FLG_INTERNATIONAL_ORDER_END + " INTEGER DEFAULT 0 ,"
			+ FLG_PROCESSED + " INTEGER DEFAULT 0 ," + KEY_FLG_STATUS
			+ " TEXT," + KEY_S_DESCRIPTION + " TEXT," + CREATED_DATE
			+ " TEXT , " + MODIFIED_DATE + " TEXT, " + KEY_SHIP_ID + " TEXT,"
			+ KEY_TENANT_ID + " TEXT" + ")";

	/**
	 * 
	 * @author ripunjay.s Creating UserWorkTimeSheetTask table.
	 */
	String CREATE_USER_WORKTIME_SHEET_TASK_TABLE = "CREATE TABLE "
			+ TABLE_USER_WORKTIME_SHEET_TASK + "("
			+ USER_WORK_TIMESHEET_TASK_ID + " TEXT PRIMARY KEY,"
			+ USER_WORK_TIMESHEET_ID + " TEXT," + WORK_START_DATE + " DATE,"
			+ WORK_START_TIME + " TEXT," + WORK_END_DATE + " DATE,"
			+ WORK_END_TIME + " TEXT," + WORK_START_DATE_TIME + " TEXT,"
			+ WORK_END_DATE_TIME + " TEXT," + KEY_iUSER_ID + " TEXT,"
			+ KEY_USER_SERVICE_TERM_ID + " TEXT," + CREW_DEPARTMENT_TASK_ID
			+ " TEXT , " + TASK_ID + " TEXT, " + KEY_FLG_DELETED + " INTEGER,"
			+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_FLG_STATUS + " TEXT,"
			+ KEY_S_DESCRIPTION + " TEXT," + CREATED_DATE + " TEXT , "
			+ MODIFIED_DATE + " TEXT, " + KEY_SHIP_ID + " TEXT,"
			+ KEY_TENANT_ID + " TEXT" + ")";

	// Code to add the Tenant Row
	public void addModelRow(ModelData md) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_MODEL_ID, md.getModeId());
		values.put(KEY_MODEL_NAME, md.getModelName());

		db.insert(TABLE_MODEL, null, values);

		db.close();
	}

	// Code to add the Tenant Row
	public void addTenantRow(TenantData td) {

		try {
			L.fv("I m Here");
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();

			values.put(KEY_TENANT_ID, td.getiTenantId());
			values.put(KEY_STR_COMPANY_NAME, td.getStrCompanyName());

			db.insert(TABLE_TENANT, null, values);

			db.close(); // Closing database connection
		} catch (Exception e) {

			e.printStackTrace();
			L.fv("Exception :" + e);
		}

	}

	// Code to add the new Ship Master row
	public long addShipMasterRow(ShipMaster smd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SHIP_ID, smd.getiShipId());
		values.put(KEY_TENANT_ID, smd.getiTenantId());
		values.put(KEY_SOCIETY_ID, smd.getiClassificationSocietyId());
		values.put(KEY_SHIP_TYPE_ID, smd.getiShipTypeId());
		values.put(KEY_SHIP_NAME, smd.getStrShipName());
		values.put(KEY_S_DESCRIPTION, smd.getStrDescription());
		values.put(KEY_DT_CREATED, smd.getDtCreated());
		values.put(KEY_DT_UPDATED, smd.getDtUpdated());
		values.put(KEY_FLG_STATUS, smd.getFlgStatus());
		values.put(KEY_FLG_DELETED, smd.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, smd.getFlgIsDirty());
		values.put(KEY_RULE_LIST_ID, smd.getiRuleListId());
		values.put(KEY_SHIP_IMO_NUMBER, smd.getiShipIMONumber());
		values.put(KEY_STR_FLAG, smd.getStrFlag());
		values.put(KEY_LOGO, smd.getStrLogo());
		values.put(KEY_FILE_SIZE, smd.getFileSize());
		values.put(KEY_FILE_NAME, smd.getStrFileName());
		values.put(KEY_FILE_PATH, smd.getStrFilePath());
		values.put(KEY_FILE_TYPE, smd.getStrFileType());
		values.put(KEY_SHIP_CODE, smd.getStrShipCode());

		// Inserting Row
		long result = db.insert(TABLE_SHIP_MASTER, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection

		return result;
	}

	/**
	 * Update ship record
	 * 
	 * @param smd
	 * @return
	 */
	public long updateShipMasterRow(ShipMaster smd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SHIP_ID, smd.getiShipId());
		values.put(KEY_TENANT_ID, smd.getiTenantId());
		values.put(KEY_SOCIETY_ID, smd.getiClassificationSocietyId());
		values.put(KEY_SHIP_TYPE_ID, smd.getiShipTypeId());
		values.put(KEY_SHIP_NAME, smd.getStrShipName());
		values.put(KEY_S_DESCRIPTION, smd.getStrDescription());
		values.put(KEY_DT_CREATED, smd.getDtCreated());
		values.put(KEY_DT_UPDATED, smd.getDtUpdated());
		values.put(KEY_FLG_STATUS, smd.getFlgStatus());
		values.put(KEY_FLG_DELETED, smd.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, smd.getFlgIsDirty());
		values.put(KEY_RULE_LIST_ID, smd.getiRuleListId());
		values.put(KEY_SHIP_IMO_NUMBER, smd.getiShipIMONumber());
		values.put(KEY_STR_FLAG, smd.getStrFlag());
		values.put(KEY_LOGO, smd.getStrLogo());
		values.put(KEY_FILE_SIZE, smd.getFileSize());
		values.put(KEY_FILE_NAME, smd.getStrFileName());
		values.put(KEY_FILE_PATH, smd.getStrFilePath());
		values.put(KEY_FILE_TYPE, smd.getStrFileType());
		values.put(KEY_SHIP_CODE, smd.getStrShipCode());

		String whereClause = KEY_SHIP_ID + " =  '" + smd.getiShipId() + "'";

		long result = db.update(TABLE_SHIP_MASTER, values, whereClause, null);
		db.close(); // Closing database connection

		return result;
	}

	// Code to add the new Role Row
	public long addRoleRow(Roles rd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ROLE_ID, rd.getiRoleId());
		values.put(KEY_ROLE, rd.getStrRole());
		values.put(KEY_DT_CREATED, rd.getDtCreated());
		values.put(KEY_DT_UPDATED, rd.getDtUpdated());
		values.put(KEY_FLG_IS_DIRTY, rd.getFlgIsDirty());
		values.put(KEY_STR_TYPE, rd.getStrType());
		values.put(KEY_FLG_DELETED, rd.getFlgDeleted());
		values.put(KEY_TXT_DESCRIPTION, rd.getTxtDescription());
		values.put(KEY_RANK_PRIORITY, rd.getiRankPriority());
		values.put(KEY_TENANT_ID, rd.getiTenantId());
		values.put(KEY_STR_ROLE_TYPE, rd.getStrRoleType());

		// Inserting Row
		long result = db.insert(TABLE_ROLE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
		return result;
	}

	// Code to add the new Role Row
	public long updateRoleRow(Roles rd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ROLE_ID, rd.getiRoleId());
		values.put(KEY_ROLE, rd.getStrRole());
		values.put(KEY_DT_CREATED, rd.getDtCreated());
		values.put(KEY_DT_UPDATED, rd.getDtUpdated());
		values.put(KEY_FLG_IS_DIRTY, rd.getFlgIsDirty());
		values.put(KEY_STR_TYPE, rd.getStrType());
		values.put(KEY_FLG_DELETED, rd.getFlgDeleted());
		values.put(KEY_TXT_DESCRIPTION, rd.getTxtDescription());
		values.put(KEY_RANK_PRIORITY, rd.getiRankPriority());
		values.put(KEY_TENANT_ID, rd.getiTenantId());
		values.put(KEY_STR_ROLE_TYPE, rd.getStrRoleType());

		String whereClause = KEY_ROLE_ID + " =  '" + rd.getiRoleId() + "'";

		long result = db.update(TABLE_ROLE, values, whereClause, null);
		db.close(); // Closing database connection

		return result;
	}

	// Code to add the new User Master Row
	public long addUserMasterRow(UserMaster umd) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_iUSER_ID, umd.getiUserId());
		values.put(KEY_TENANT_ID, umd.getiTenantId());
		values.put(KEY_USER_NAME, umd.getStrUserName());
		values.put(KEY_USER_PASSWORD, umd.getStrPassword());
		values.put(KEY_FIRST_NAME, umd.getStrFirstName());
		values.put(KEY_LAST_NAME, umd.getStrLastName());
		values.put(KEY_DATE_OF_BIRTH, umd.getDtDateOfBirth());
		values.put(KEY_TXT_DESCRIPTION, umd.getTxtDescription());
		values.put(KEY_EMAIL, umd.getStrEmail());
		values.put(KEY_DT_CREATED, umd.getDtCreated());
		values.put(KEY_DT_UPDATED, umd.getDtUpdated());
		values.put(KEY_FLG_STATUS, umd.getFlgStatus());
		values.put(KEY_FLG_DELETED, umd.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, umd.getFlgIsDirty());
		values.put(KEY_SHIP_HOLIDAY_LIST_ID, umd.getiShipHolidayListId());
		values.put(KEY_MIN_WORK_HOUR_WEEK_DAYS, umd.getFltMinWorkHourWeekDays());
		values.put(KEY_MIN_WORK_HOUR_SATUREDAY,
				umd.getFltMinWorkHourSaturdays());
		values.put(KEY_MIN_WORK_HOUR_SUNDAY, umd.getFltMinWorkHourSundays());
		values.put(KEY_MIN_WORK_HOUR_HOLIDAY, umd.getFltMinWorkHourHolidays());
		values.put(KEY_FLTOT_INCLUDED_IN_WAGE, umd.getFltOTIncludedInWage());
		values.put(KEY_FLTOT_RATE_PER_HOUR, umd.getFltOTRatePerHour());
		values.put(KEY_FLG_IS_OVER_TIME_ENABLED, umd.getFlgIsOverTimeEnabled());
		values.put(KEY_ROLE_ID, umd.getiRoleId());
		values.put(KEY_FLG_IS_WATCHKEEPER, umd.getFltMinWorkHourHolidays());
		values.put(KEY_FAX_NUMBER, umd.getFaxNumber());
		values.put(KEY_HAND_PHONE, umd.getHandPhone());
		values.put(KEY_LANDLINE_NUMBER, umd.getLandLineNumber());
		values.put(KEY_PAN_NUMBER, umd.getPanNumber());
		values.put(KEY_PIN_CODE, umd.getPinCode());
		values.put(KEY_ADDRESS_FIRST, umd.getAddressFirst());
		values.put(KEY_ADDRESS_SECOND, umd.getAddressSecond());
		values.put(KEY_ADDRESS_THIRD, umd.getAddressThird());
		values.put(KEY_CITY, umd.getCity());
		values.put(KEY_STATE, umd.getState());
		values.put(KEY_COUNTRY_ID, umd.getiCountryId());

		values.put(KEY_EMPLOYEE_NO, umd.getStrEmployeeNo());
		values.put(KEY_MIDDLE_NAME, umd.getStrMiddleName());

		// Inserting Row
		long result = db.insert(TABLE_USER_MASTER, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection

		return result;
	}

	/*
	 * Update usermaster table for User Master Row
	 */
	public long updateUserMasterRow(UserMaster umd) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_iUSER_ID, umd.getiUserId());
		values.put(KEY_TENANT_ID, umd.getiTenantId());
		values.put(KEY_USER_NAME, umd.getStrUserName());
		values.put(KEY_USER_PASSWORD, umd.getStrPassword());
		values.put(KEY_FIRST_NAME, umd.getStrFirstName());
		values.put(KEY_LAST_NAME, umd.getStrLastName());
		values.put(KEY_DATE_OF_BIRTH, umd.getDtDateOfBirth());
		values.put(KEY_TXT_DESCRIPTION, umd.getTxtDescription());
		values.put(KEY_EMAIL, umd.getStrEmail());
		values.put(KEY_DT_CREATED, umd.getDtCreated());
		values.put(KEY_DT_UPDATED, umd.getDtUpdated());
		values.put(KEY_FLG_STATUS, umd.getFlgStatus());
		values.put(KEY_FLG_DELETED, umd.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, umd.getFlgIsDirty());
		values.put(KEY_SHIP_HOLIDAY_LIST_ID, umd.getiShipHolidayListId());
		values.put(KEY_MIN_WORK_HOUR_WEEK_DAYS, umd.getFltMinWorkHourWeekDays());
		values.put(KEY_MIN_WORK_HOUR_SATUREDAY,
				umd.getFltMinWorkHourSaturdays());
		values.put(KEY_MIN_WORK_HOUR_SUNDAY, umd.getFltMinWorkHourSundays());
		values.put(KEY_MIN_WORK_HOUR_HOLIDAY, umd.getFltMinWorkHourHolidays());
		values.put(KEY_FLTOT_INCLUDED_IN_WAGE, umd.getFltOTIncludedInWage());
		values.put(KEY_FLTOT_RATE_PER_HOUR, umd.getFltOTRatePerHour());
		values.put(KEY_FLG_IS_OVER_TIME_ENABLED, umd.getFlgIsOverTimeEnabled());
		values.put(KEY_ROLE_ID, umd.getiRoleId());
		values.put(KEY_FLG_IS_WATCHKEEPER, umd.getFltMinWorkHourHolidays());
		values.put(KEY_FAX_NUMBER, umd.getFaxNumber());
		values.put(KEY_HAND_PHONE, umd.getHandPhone());
		values.put(KEY_LANDLINE_NUMBER, umd.getLandLineNumber());
		values.put(KEY_PAN_NUMBER, umd.getPanNumber());
		values.put(KEY_PIN_CODE, umd.getPinCode());
		values.put(KEY_ADDRESS_FIRST, umd.getAddressFirst());
		values.put(KEY_ADDRESS_SECOND, umd.getAddressSecond());
		values.put(KEY_ADDRESS_THIRD, umd.getAddressThird());
		values.put(KEY_CITY, umd.getCity());
		values.put(KEY_STATE, umd.getState());
		values.put(KEY_COUNTRY_ID, umd.getiCountryId());

		values.put(KEY_EMPLOYEE_NO, umd.getStrEmployeeNo());
		values.put(KEY_MIDDLE_NAME, umd.getStrMiddleName());

		String whereClause = KEY_iUSER_ID + " =  '" + umd.getiUserId() + "'";

		long result = db.update(TABLE_USER_MASTER, values, whereClause, null);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
		return result;
	}

	/*
	 * Code to add the New UserServiceTerm Row
	 */
	public long addUserServiceTermRow(UserServiceTerm usrSData) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_USER_SERVICE_TERM_ID, usrSData.getiUserServiceTermId());
		values.put(KEY_iUSER_ID, usrSData.getiUserId());
		values.put(KEY_DT_TERM_FROM, usrSData.getDtTermFrom());
		values.put(KEY_DT_TERM_TO, usrSData.getDtTermTo());
		values.put(KEY_FLG_DELETED, usrSData.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, usrSData.getFlgIsDirty());
		values.put(KEY_DT_CREATED, usrSData.getDtCreated());
		values.put(KEY_DT_UPDATED, usrSData.getDtUpdated());
		values.put(KEY_SHIP_ID, usrSData.getiShipId());
		values.put(KEY_TENANT_ID, usrSData.getiTenantId());

		// Inserting Row
		long result = db.insert(TABLE_USER_SERVICE_TERM, null, values);

		db.close(); // Closing database connection

		return result;
	}

	/*
	 * Code to add the New UserServiceTerm Row
	 */
	public long updateUserServiceTermRow(UserServiceTerm usrSData) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_USER_SERVICE_TERM_ID, usrSData.getiUserServiceTermId());
		values.put(KEY_iUSER_ID, usrSData.getiUserId());
		values.put(KEY_DT_TERM_FROM, usrSData.getDtTermFrom());
		values.put(KEY_DT_TERM_TO, usrSData.getDtTermTo());
		values.put(KEY_FLG_DELETED, usrSData.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, usrSData.getFlgIsDirty());
		values.put(KEY_DT_CREATED, usrSData.getDtCreated());
		values.put(KEY_DT_UPDATED, usrSData.getDtUpdated());
		values.put(KEY_SHIP_ID, usrSData.getiShipId());
		values.put(KEY_TENANT_ID, usrSData.getiTenantId());

		String whereClause = KEY_USER_SERVICE_TERM_ID + " =  '"
				+ usrSData.getiUserServiceTermId() + "'";

		long result = db.update(TABLE_USER_SERVICE_TERM, values, whereClause,
				null);

		db.close(); // Closing database connection

		return result;
	}

	/*
	 * Code to add the New InternationalDates Row
	 */
	public long insertInternationaldatesRow(
			InternationalDates internationalDates) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_INTERNATIONALDATES_ID,
				internationalDates.getIinternationalDateId());
		values.put(KEY_INTERNATIONALDATE,
				internationalDates.getInternationalDate());
		values.put(FLG_REPEATED, internationalDates.getFlgRepeated());
		values.put(KEY_TXT_DESCRIPTION, internationalDates.getTxtDescription());
		values.put(KEY_FLG_DELETED, internationalDates.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, internationalDates.getFlgIsDirty());
		values.put(KEY_DT_CREATED, internationalDates.getDtCreated());
		values.put(KEY_DT_UPDATED, internationalDates.getDtUpdated());
		values.put(KEY_SHIP_ID, internationalDates.getiShipId());
		values.put(KEY_TENANT_ID, internationalDates.getiTenantId());

		// Inserting Row
		long result = db.insert(TABLE_INTERNATIONALDATES, null, values);

		db.close(); // Closing database connection

		return result;
	}

	/*
	 * Code to update InternationalDates Row
	 */
	public long updateInternationalDatesRow(
			InternationalDates internationalDates) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_INTERNATIONALDATES_ID,
				internationalDates.getIinternationalDateId());
		values.put(KEY_INTERNATIONALDATE,
				internationalDates.getInternationalDate());
		values.put(FLG_REPEATED, internationalDates.getFlgRepeated());
		values.put(KEY_TXT_DESCRIPTION, internationalDates.getTxtDescription());
		values.put(KEY_FLG_DELETED, internationalDates.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, internationalDates.getFlgIsDirty());
		values.put(KEY_DT_CREATED, internationalDates.getDtCreated());
		values.put(KEY_DT_UPDATED, internationalDates.getDtUpdated());
		values.put(KEY_SHIP_ID, internationalDates.getiShipId());
		values.put(KEY_TENANT_ID, internationalDates.getiTenantId());

		String whereClause = KEY_INTERNATIONALDATES_ID + " =  '"
				+ internationalDates.getIinternationalDateId() + "'";

		long result = db.update(TABLE_INTERNATIONALDATES, values, whereClause,
				null);

		db.close(); // Closing database connection

		return result;
	}

	/*
	 * Code to add the New IdlLogWork Row
	 */
	public long insertIdlLogWorkRow(IdlLogWork idlLogWork) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(IDL_LOGWORK_ID, idlLogWork.getiIdlLogWorkId());
		values.put(KEY_INTERNATIONALDATES_ID,
				idlLogWork.getIinternationalDateId());
		values.put(KEY_INTERNATIONALDATE, idlLogWork.getInternationalDate());
		values.put(FLG_REPEATED, idlLogWork.getFlgRepeated());
		values.put(FLG_DAY_START, idlLogWork.getFlgDayStart());
		values.put(KEY_TXT_DESCRIPTION, idlLogWork.getTxtDescription());
		values.put(KEY_FLG_DELETED, idlLogWork.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, idlLogWork.getFlgIsDirty());
		values.put(KEY_DT_CREATED, idlLogWork.getDtCreated());
		values.put(KEY_DT_UPDATED, idlLogWork.getDtUpdated());
		values.put(KEY_SHIP_ID, idlLogWork.getiShipId());
		values.put(KEY_TENANT_ID, idlLogWork.getiTenantId());

		// Inserting Row
		long result = db.insert(TABLE_IDL_LOGWORK, null, values);

		db.close(); // Closing database connection

		return result;
	}

	/*
	 * Code to update IdlLogWork Row
	 */
	public long updateIdlLogWorkRow(IdlLogWork idlLogWork) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(IDL_LOGWORK_ID, idlLogWork.getiIdlLogWorkId());
		values.put(KEY_INTERNATIONALDATES_ID,
				idlLogWork.getIinternationalDateId());
		values.put(KEY_INTERNATIONALDATE, idlLogWork.getInternationalDate());
		values.put(FLG_REPEATED, idlLogWork.getFlgRepeated());
		values.put(FLG_DAY_START, idlLogWork.getFlgDayStart());
		values.put(KEY_TXT_DESCRIPTION, idlLogWork.getTxtDescription());
		values.put(KEY_FLG_DELETED, idlLogWork.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, idlLogWork.getFlgIsDirty());
		values.put(KEY_DT_CREATED, idlLogWork.getDtCreated());
		values.put(KEY_DT_UPDATED, idlLogWork.getDtUpdated());
		values.put(KEY_SHIP_ID, idlLogWork.getiShipId());
		values.put(KEY_TENANT_ID, idlLogWork.getiTenantId());

		String whereClause = IDL_LOGWORK_ID + " =  '"
				+ idlLogWork.getiIdlLogWorkId() + "'";

		long result = db.update(TABLE_IDL_LOGWORK, values, whereClause, null);

		db.close(); // Closing database connection

		return result;
	}

	/*
	 * Code to add the New UserServiceTermRole row
	 */
	public long addUserServiceTermRoleRow(UserServiceTermRole usrSRData) {
		SQLiteDatabase db = this.getWritableDatabase();
		long cnt = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(KEY_USER_SERVICE_TERM_ROLE_ID,
					usrSRData.getStrUserServiceTermRoleId());
			values.put(KEY_DT_CREATED, usrSRData.getDtCreated());
			values.put(KEY_DT_UPDATED, usrSRData.getDtUpdated());
			values.put(KEY_FLG_DELETED, usrSRData.getFlgDeleted());
			values.put(KEY_FLG_IS_DIRTY, usrSRData.getFlgIsDirty());
			values.put(KEY_ROLE_ID, usrSRData.getiRoleId());
			values.put(KEY_SHIP_ID, usrSRData.getiShipId());
			values.put(KEY_TENANT_ID, usrSRData.getiTenantId());
			values.put(KEY_iUSER_ID, usrSRData.getIuserId());
			values.put(KEY_USER_SERVICE_TERM_ID,
					usrSRData.getIuserServiceTermId());
			values.put(KEY_DT_TERM_FROM, usrSRData.getDtTermFrom());
			values.put(KEY_DT_TERM_TO, usrSRData.getDtTermTo());
			values.put(POWER_PLUS_SCORE, usrSRData.getPowerPlusScore());
			values.put(FIELD_ONE, usrSRData.getField1());
			values.put(FIELD_TWO, usrSRData.getField2());

			// Inserting Row
			cnt = db.insert(TABLE_USER_SERVICE_TERM_ROLE, null, values);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
		return cnt;
	}

	/*
	 * Code to update the existing UserServiceTermRole row
	 */
	public long updateUserServiceTermRole(UserServiceTermRole usrSRData) {
		SQLiteDatabase db = this.getWritableDatabase();
		long cnt = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(KEY_USER_SERVICE_TERM_ROLE_ID,
					usrSRData.getStrUserServiceTermRoleId());
			values.put(KEY_DT_CREATED, usrSRData.getDtCreated());
			values.put(KEY_DT_UPDATED, usrSRData.getDtUpdated());
			values.put(KEY_FLG_DELETED, usrSRData.getFlgDeleted());
			values.put(KEY_FLG_IS_DIRTY, usrSRData.getFlgIsDirty());
			values.put(KEY_ROLE_ID, usrSRData.getiRoleId());
			values.put(KEY_SHIP_ID, usrSRData.getiShipId());
			values.put(KEY_TENANT_ID, usrSRData.getiTenantId());
			values.put(KEY_iUSER_ID, usrSRData.getIuserId());
			values.put(KEY_USER_SERVICE_TERM_ID,
					usrSRData.getIuserServiceTermId());
			values.put(KEY_DT_TERM_FROM, usrSRData.getDtTermFrom());
			values.put(KEY_DT_TERM_TO, usrSRData.getDtTermTo());
			values.put(POWER_PLUS_SCORE, usrSRData.getPowerPlusScore());
			values.put(FIELD_ONE, usrSRData.getField1());
			values.put(FIELD_TWO, usrSRData.getField2());

			String whereClause = KEY_USER_SERVICE_TERM_ROLE_ID + " =  '"
					+ usrSRData.getStrUserServiceTermRoleId() + "'";

			cnt = db.update(TABLE_USER_SERVICE_TERM_ROLE, values, whereClause,
					null);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		db.close(); // Closing database connection
		return cnt;
	}

	// Code to add New Card Type
	public void addCardTypeRow(CardTypeData ctd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_CARD_ID, ctd.getStrCardId());
		values.put(KEY_CARD_TYPE, ctd.getStrCardType());
		// Inserting Row
		db.insert(TABLE_CARD_TYPE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new User NFC row
	public void addUserNFCRow(UserNfcTagData usrNFCData) {
		try {

			List<UserNfcTagData> list = getUserNFCRowByUserIdAndCardId(
					usrNFCData.getiUserId(), usrNFCData.getStrNfcTagId());

			SQLiteDatabase db = this.getWritableDatabase();

			if (list == null || list.size() == 0) {
				ContentValues values = new ContentValues();
				values.put(KEY_NFC_CARD_ID, usrNFCData.getiUserNfcTagDataId());
				values.put(KEY_CARD_TYPE, usrNFCData.getCardType());
				values.put(KEY_iUSER_ID, usrNFCData.getiUserId());
				values.put(KEY_NFC_TAG_ID, usrNFCData.getStrNfcTagId());
				values.put(KEY_FLG_DIRTY, usrNFCData.getFlgIsDirty());
				values.put(KEY_FLG_DELETED, usrNFCData.getFlgDeleted());
				values.put(KEY_DT_CREATED, usrNFCData.getDtCreated());
				values.put(KEY_DT_UPDATED, usrNFCData.getDtUpdated());
				values.put(KEY_TENANT_ID, usrNFCData.getiTenantId());
				values.put(KEY_SHIP_ID, usrNFCData.getiShipId());

				// Inserting Row
				long counter = db.insert(TABLE_USER_NFC, null, values);
			}

			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Code to add the new User NFC row
	public void updateUserNFCRowByUsrid(String strUserid, String tenantid,
			String shipId) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TENANT_ID, tenantid);
		values.put(KEY_SHIP_ID, shipId);

		String whereClause = KEY_iUSER_ID + " =  '" + strUserid + "'";

		db.update(TABLE_USER_NFC, values, whereClause, null);

		db.close(); // Closing database connection
	}

	/**
	 * @author pushkar.m Insert UserNfcTagData record into DB
	 * @param usrNFCData
	 */
	public long insertUserNFCRow(UserNfcTagData usrNFCData) {
		SQLiteDatabase db = null;
		long result = 0;
		try {

			db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_NFC_CARD_ID, usrNFCData.getiUserNfcTagDataId());
			values.put(KEY_CARD_TYPE, usrNFCData.getCardType());
			values.put(KEY_iUSER_ID, usrNFCData.getiUserId());
			values.put(KEY_NFC_TAG_ID, usrNFCData.getStrNfcTagId());
			values.put(KEY_FLG_DIRTY, usrNFCData.getFlgIsDirty());
			values.put(KEY_FLG_DELETED, usrNFCData.getFlgDeleted());
			values.put(KEY_DT_CREATED, usrNFCData.getDtCreated());
			values.put(KEY_DT_UPDATED, usrNFCData.getDtUpdated());
			values.put(KEY_TENANT_ID, usrNFCData.getiTenantId());
			values.put(KEY_SHIP_ID, usrNFCData.getiShipId());

			result = db.insert(TABLE_USER_NFC, null, values);

			db.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	// Code to delete the new User NFC row
	public long updateUserNFCRow(UserNfcTagData usrNFCData) {
		long result = 0;
		SQLiteDatabase db = null;

		try {
			db = this.getWritableDatabase();
			ContentValues values = new ContentValues();
			values.put(KEY_NFC_CARD_ID, usrNFCData.getiUserNfcTagDataId());
			values.put(KEY_CARD_TYPE, usrNFCData.getCardType());
			values.put(KEY_iUSER_ID, usrNFCData.getiUserId());
			values.put(KEY_NFC_TAG_ID, usrNFCData.getStrNfcTagId());
			values.put(KEY_FLG_DIRTY, usrNFCData.getFlgIsDirty());
			values.put(KEY_FLG_DELETED, usrNFCData.getFlgDeleted());
			values.put(KEY_DT_CREATED, usrNFCData.getDtCreated());
			values.put(KEY_DT_UPDATED, usrNFCData.getDtUpdated());
			values.put(KEY_TENANT_ID, usrNFCData.getiTenantId());
			values.put(KEY_SHIP_ID, usrNFCData.getiShipId());

			String whereClause = KEY_NFC_CARD_ID + " =  '"
					+ usrNFCData.getiUserNfcTagDataId() + "'";
			result = db.update(TABLE_USER_NFC, values, whereClause, null);

			db.close(); // Closing database connection
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	// Code to get all UserNfc row in a list
	public List<UserNfcTagData> getUserNFCRow() {
		List<UserNfcTagData> nfcDataList = new ArrayList<UserNfcTagData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcTagData data = new UserNfcTagData();
				data.setiUserNfcTagDataId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_ID)));
				data.setCardType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_TYPE)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setStrNfcTagId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_TAG_ID)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return nfcDataList;
	}

	// Code to get all UserNfc row in a list
	public ArrayList<UserNfcTagData> getUserNFCSingleRow(String id) {
		ArrayList<UserNfcTagData> nfcDataList = new ArrayList<UserNfcTagData>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_NFC_TAG_ID + " = " + id + " and " + KEY_FLG_DELETED
				+ " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcTagData data = new UserNfcTagData();
				data.setiUserNfcTagDataId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_ID)));
				data.setCardType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_TYPE)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setStrNfcTagId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_TAG_ID)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return nfcDataList;
	}

	// Code to get all UserNfc row in a list
	public ArrayList<UserNfcTagData> getUserNFCSingleRowByUserId(String id) {
		ArrayList<UserNfcTagData> nfcDataList = new ArrayList<UserNfcTagData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_iUSER_ID + " = '" + id + "' and " + KEY_FLG_DELETED
				+ " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcTagData data = new UserNfcTagData();
				data.setiUserNfcTagDataId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_ID)));
				data.setCardType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_TYPE)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setStrNfcTagId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_TAG_ID)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return nfcDataList;
	}

	/**
	 * get data on the basis of userid and card id
	 * getUserNFCRowByUserIdAndCardId
	 * 
	 * @param id
	 * @return
	 */
	public ArrayList<UserNfcTagData> getUserNFCRowByUserIdAndCardId(
			String userid, String strCardId) {
		ArrayList<UserNfcTagData> nfcDataList = new ArrayList<UserNfcTagData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_iUSER_ID + " = '" + userid + "' and " + KEY_FLG_DELETED
				+ " = '0' and " + KEY_NFC_TAG_ID + " = '" + strCardId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcTagData data = new UserNfcTagData();
				data.setiUserNfcTagDataId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_ID)));
				data.setCardType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_TYPE)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setStrNfcTagId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_TAG_ID)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		// return contact list
		return nfcDataList;
	}

	/**
	 * @author pushkar.m
	 * @param id
	 * @return This method return single UserNfcTagData row by iUserNfcTagDataId
	 */
	public List<UserNfcTagData> getUserNFCTagDataById(String id) {
		List<UserNfcTagData> nfcDataList = new ArrayList<UserNfcTagData>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_NFC_CARD_ID + " = '" + id + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				UserNfcTagData data = new UserNfcTagData();
				data.setiUserNfcTagDataId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_ID)));
				data.setCardType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_CARD_TYPE)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setStrNfcTagId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_NFC_TAG_ID)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return nfcDataList;
	}

	/**
	 * @author pushkar.m make row as unDirty
	 */
	public long updateDirtyFlagUserNfcTagData(UserNfcTagData userNfcTagData) {
		long result = 0;

		try {
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();
			values.put(KEY_FLG_IS_DIRTY, "0");

			String whereClause = KEY_NFC_CARD_ID + " =  '"
					+ userNfcTagData.getiUserNfcTagDataId() + "'";

			result = db.update(TABLE_USER_NFC, values, whereClause, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	// Code to get all UserNfc row in a list
	public ArrayList<TenantData> getTenantRow() {
		ArrayList<TenantData> tenantDataList = new ArrayList<TenantData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TENANT;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TenantData data = new TenantData();
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setStrCompanyName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_COMPANY_NAME)));
				// Adding contact to list
				tenantDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return tenantDataList;
	}

	// Code to get all UserNfc row in a list
	public List<ModelData> getModelRow() {
		List<ModelData> modelDataList = new ArrayList<ModelData>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_MODEL;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ModelData data = new ModelData();
				data.setModeId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MODEL_ID)));
				data.setModelName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MODEL_NAME)));
				// Adding contact to list
				modelDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return modelDataList;
	}

	// Code to get all CardType row in a list
	public List<CardTypeData> getCardTypeRow() {
		List<CardTypeData> cardDataList = new ArrayList<CardTypeData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CARD_TYPE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				CardTypeData data = new CardTypeData();
				data.setStrCardId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CARD_ID)));
				data.setStrCardType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CARD_TYPE)));
				// Adding contact to list
				cardDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return cardDataList;
	}

	/**
	 * ShipMaster get from db on the basis of id.
	 * 
	 * @return
	 */
	public List<ShipMaster> getShipMasterRowById(String shipId) {
		List<ShipMaster> shipMasterDataList = new ArrayList<ShipMaster>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SHIP_MASTER + " where "
				+ KEY_SHIP_ID + " ='" + shipId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ShipMaster data = new ShipMaster();
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiClassificationSocietyId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SOCIETY_ID)));
				data.setiShipTypeId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_TYPE_ID)));
				data.setStrShipName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_NAME)));
				data.setStrDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_S_DESCRIPTION)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiRuleListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_RULE_LIST_ID)));
				data.setiShipIMONumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_IMO_NUMBER)));
				data.setStrFlag(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_FLAG)));
				data.setStrLogo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LOGO)));
				data.setFileSize(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_SIZE)));
				data.setStrFileName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_NAME)));
				data.setStrFilePath(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_PATH)));
				data.setStrFileType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_TYPE)));
				data.setStrShipCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_CODE)));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all Ship Master row in a list
	public List<ShipMaster> getShipMasterRow() {
		List<ShipMaster> shipMasterDataList = new ArrayList<ShipMaster>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SHIP_MASTER;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ShipMaster data = new ShipMaster();
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiClassificationSocietyId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SOCIETY_ID)));
				data.setiShipTypeId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_TYPE_ID)));
				data.setStrShipName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_NAME)));
				data.setStrDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_S_DESCRIPTION)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiRuleListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_RULE_LIST_ID)));
				data.setiShipIMONumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_IMO_NUMBER)));
				data.setStrFlag(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_FLAG)));
				data.setStrLogo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LOGO)));
				data.setFileSize(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_SIZE)));
				data.setStrFileName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_NAME)));
				data.setStrFilePath(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_PATH)));
				data.setStrFileType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_TYPE)));
				data.setStrShipCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_CODE)));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	/**
	 * Code to get all Ship Master deleted row in a list
	 * 
	 * @return
	 */
	public List<ShipMaster> getShipMasterDeletedRow() {
		List<ShipMaster> shipMasterDataList = new ArrayList<ShipMaster>();

		String selectQuery = "SELECT  * FROM " + TABLE_SHIP_MASTER + " where "
				+ KEY_FLG_DELETED + " ='1'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				ShipMaster data = new ShipMaster();
				data.setiShipId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setiClassificationSocietyId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SOCIETY_ID)));
				data.setiShipTypeId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_TYPE_ID)));
				data.setStrShipName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_NAME)));
				data.setStrDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_S_DESCRIPTION)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiRuleListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_RULE_LIST_ID)));
				data.setiShipIMONumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_IMO_NUMBER)));
				data.setStrFlag(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_FLAG)));
				data.setStrLogo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LOGO)));
				data.setFileSize(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_SIZE)));
				data.setStrFileName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_NAME)));
				data.setStrFilePath(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_PATH)));
				data.setStrFileType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FILE_TYPE)));
				data.setStrShipCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_CODE)));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all Role table row in a list
	public List<Roles> getRoleRow() {
		List<Roles> roleDataList = new ArrayList<Roles>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ROLE + " where "
				+ KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Roles data = new Roles();
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrRole(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setStrType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_TYPE)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setiRankPriority(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_RANK_PRIORITY)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setStrRoleType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_ROLE_TYPE)));

				// Adding contact to list
				roleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return roleDataList;
	}

	// Code to get all Role table row in a list
	public List<Roles> getRoleRow(String roletype) {
		List<Roles> roleDataList = new ArrayList<Roles>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ROLE + " where "
				+ KEY_STR_ROLE_TYPE + " = '" + roletype + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Roles data = new Roles();
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrRole(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setStrType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_TYPE)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setiRankPriority(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_RANK_PRIORITY)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setStrRoleType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_ROLE_TYPE)));

				// Adding contact to list
				roleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return roleDataList;
	}

	// Code to get all Role table row in a list
	public List<Roles> getRolesById(String roleId) {
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ROLE + " where "
				+ KEY_ROLE_ID + " = '" + roleId + "'";

		List<Roles> dataList = new ArrayList<Roles>();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Roles data = new Roles();

				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrRole(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setStrType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_TYPE)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setiRankPriority(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_RANK_PRIORITY)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setStrRoleType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_ROLE_TYPE)));

				dataList.add(data);

			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return dataList;
	}

	// Code to get all Role table row in a list
	public Roles getRoleSingleRow(String roleId) {
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ROLE + " where "
				+ KEY_ROLE_ID + " = '" + roleId + "'";

		Roles data = new Roles();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {

				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrRole(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setStrType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_TYPE)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setiRankPriority(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_RANK_PRIORITY)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));
				data.setStrRoleType(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STR_ROLE_TYPE)));

			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return data;
	}

	/**
	 * Code to get all User Master row in a list
	 * 
	 * @param roleId
	 * @return
	 */
	public List<UserMaster> getUserMasterForRoles(String roleId) {
		List<UserMaster> userMasterDataList = new ArrayList<UserMaster>();

		String selectQuery = "SELECT  *  FROM " + TABLE_USER_MASTER + " WHERE "
				+ KEY_ROLE_ID + " in( " + roleId + ") and " + KEY_FLG_DELETED
				+ " ='0'";
		System.out.println("Query : " + selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(TENANT_ID)));
				data.setStrUserName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_NAME)));
				data.setStrPassword(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_PASSWORD)));
				data.setStrFirstName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FIRST_NAME)));
				data.setStrLastName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LAST_NAME)));
				data.setDtDateOfBirth(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DATE_OF_BIRTH)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setStrEmail(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMAIL)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiShipHolidayListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_HOLIDAY_LIST_ID)));
				data.setFltMinWorkHourWeekDays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_WEEK_DAYS)));
				data.setFltMinWorkHourSaturdays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SATUREDAY)));
				data.setFltMinWorkHourSundays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SUNDAY)));
				data.setFltMinWorkHourHolidays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_HOLIDAY)));
				data.setFltOTIncludedInWage(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_INCLUDED_IN_WAGE)));
				data.setFltOTRatePerHour(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_RATE_PER_HOUR)));
				data.setFlgIsOverTimeEnabled(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_OVER_TIME_ENABLED)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setFlgIsWatchkeeper(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_WATCHKEEPER)));
				data.setFaxNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FAX_NUMBER)));
				data.setHandPhone(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_HAND_PHONE)));
				data.setLandLineNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LANDLINE_NUMBER)));
				data.setPanNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PAN_NUMBER)));
				data.setPinCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PIN_CODE)));
				data.setAddressFirst(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_FIRST)));
				data.setAddressSecond(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_SECOND)));
				data.setAddressThird(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_THIRD)));
				data.setCity(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CITY)));
				data.setState(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STATE)));
				data.setiCountryId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_COUNTRY_ID)));

				data.setStrEmployeeNo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMPLOYEE_NO)));
				data.setStrMiddleName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIDDLE_NAME)));

				Roles rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		// return contact list

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		return userMasterDataList;
	}

	// Code to get all User Master row in a list
	public ArrayList<UserMaster> getUserMasterRow() {
		ArrayList<UserMaster> userMasterDataList = new ArrayList<UserMaster>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_MASTER + " where "
				+ KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();

				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(TENANT_ID)));
				data.setStrUserName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_NAME)));
				data.setStrPassword(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_PASSWORD)));
				data.setStrFirstName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FIRST_NAME)));
				data.setStrLastName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LAST_NAME)));
				data.setDtDateOfBirth(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DATE_OF_BIRTH)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setStrEmail(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMAIL)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiShipHolidayListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_HOLIDAY_LIST_ID)));
				data.setFltMinWorkHourWeekDays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_WEEK_DAYS)));
				data.setFltMinWorkHourSaturdays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SATUREDAY)));
				data.setFltMinWorkHourSundays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SUNDAY)));
				data.setFltMinWorkHourHolidays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_HOLIDAY)));
				data.setFltOTIncludedInWage(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_INCLUDED_IN_WAGE)));
				data.setFltOTRatePerHour(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_RATE_PER_HOUR)));
				data.setFlgIsOverTimeEnabled(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_OVER_TIME_ENABLED)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setFlgIsWatchkeeper(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_WATCHKEEPER)));
				data.setFaxNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FAX_NUMBER)));
				data.setHandPhone(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_HAND_PHONE)));
				data.setLandLineNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LANDLINE_NUMBER)));
				data.setPanNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PAN_NUMBER)));
				data.setPinCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PIN_CODE)));
				data.setAddressFirst(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_FIRST)));
				data.setAddressSecond(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_SECOND)));
				data.setAddressThird(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_THIRD)));
				data.setCity(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CITY)));
				data.setState(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STATE)));
				data.setiCountryId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_COUNTRY_ID)));

				data.setStrEmployeeNo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMPLOYEE_NO)));
				data.setStrMiddleName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIDDLE_NAME)));

				Roles rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		// return contact list
		return userMasterDataList;
	}

	public ArrayList<UserMaster> getUserMasterRowByRoleIds(String rolesid) {
		ArrayList<UserMaster> userMasterDataList = new ArrayList<UserMaster>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_MASTER + " where "
				+ KEY_ROLE_ID + " in (" + rolesid + ") and " + KEY_FLG_DELETED
				+ " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();

				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(TENANT_ID)));
				data.setStrUserName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_NAME)));
				data.setStrPassword(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_PASSWORD)));
				data.setStrFirstName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FIRST_NAME)));
				data.setStrLastName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LAST_NAME)));
				data.setDtDateOfBirth(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DATE_OF_BIRTH)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setStrEmail(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMAIL)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiShipHolidayListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_HOLIDAY_LIST_ID)));
				data.setFltMinWorkHourWeekDays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_WEEK_DAYS)));
				data.setFltMinWorkHourSaturdays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SATUREDAY)));
				data.setFltMinWorkHourSundays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SUNDAY)));
				data.setFltMinWorkHourHolidays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_HOLIDAY)));
				data.setFltOTIncludedInWage(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_INCLUDED_IN_WAGE)));
				data.setFltOTRatePerHour(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_RATE_PER_HOUR)));
				data.setFlgIsOverTimeEnabled(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_OVER_TIME_ENABLED)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setFlgIsWatchkeeper(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_WATCHKEEPER)));
				data.setFaxNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FAX_NUMBER)));
				data.setHandPhone(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_HAND_PHONE)));
				data.setLandLineNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LANDLINE_NUMBER)));
				data.setPanNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PAN_NUMBER)));
				data.setPinCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PIN_CODE)));
				data.setAddressFirst(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_FIRST)));
				data.setAddressSecond(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_SECOND)));
				data.setAddressThird(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_THIRD)));
				data.setCity(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CITY)));
				data.setState(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STATE)));
				data.setiCountryId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_COUNTRY_ID)));

				data.setStrEmployeeNo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMPLOYEE_NO)));
				data.setStrMiddleName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIDDLE_NAME)));

				Roles rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		// return contact list
		return userMasterDataList;
	}

	// Code to get all User Master row in a list
	public ArrayList<UserMaster> getUserMasterSingleRow(String id) {
		ArrayList<UserMaster> userMasterDataList = new ArrayList<UserMaster>();
		// Select All Query
		String selectQuery = "SELECT  *  FROM " + TABLE_USER_MASTER + " WHERE "
				+ KEY_iUSER_ID + " = " + "'" + id + "' and " + KEY_FLG_DELETED
				+ " ='0'";
		System.out.println("Query : " + selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(TENANT_ID)));
				data.setStrUserName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_NAME)));
				data.setStrPassword(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_PASSWORD)));
				data.setStrFirstName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FIRST_NAME)));
				data.setStrLastName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LAST_NAME)));
				data.setDtDateOfBirth(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DATE_OF_BIRTH)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setStrEmail(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMAIL)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiShipHolidayListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_HOLIDAY_LIST_ID)));
				data.setFltMinWorkHourWeekDays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_WEEK_DAYS)));
				data.setFltMinWorkHourSaturdays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SATUREDAY)));
				data.setFltMinWorkHourSundays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SUNDAY)));
				data.setFltMinWorkHourHolidays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_HOLIDAY)));
				data.setFltOTIncludedInWage(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_INCLUDED_IN_WAGE)));
				data.setFltOTRatePerHour(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_RATE_PER_HOUR)));
				data.setFlgIsOverTimeEnabled(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_OVER_TIME_ENABLED)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setFlgIsWatchkeeper(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_WATCHKEEPER)));
				data.setFaxNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FAX_NUMBER)));
				data.setHandPhone(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_HAND_PHONE)));
				data.setLandLineNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LANDLINE_NUMBER)));
				data.setPanNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PAN_NUMBER)));
				data.setPinCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PIN_CODE)));
				data.setAddressFirst(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_FIRST)));
				data.setAddressSecond(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_SECOND)));
				data.setAddressThird(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_THIRD)));
				data.setCity(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CITY)));
				data.setState(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STATE)));
				data.setiCountryId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_COUNTRY_ID)));

				data.setStrEmployeeNo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMPLOYEE_NO)));
				data.setStrMiddleName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIDDLE_NAME)));

				Roles rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		// return contact list
		return userMasterDataList;
	}

	/*
	 * Get UserMaster record by Id
	 */
	public UserMaster getUserMasterById(String id) {
		ArrayList<UserMaster> userMasterDataList = new ArrayList<UserMaster>();
		// Select All Query
		String selectQuery = "SELECT  *  FROM " + TABLE_USER_MASTER + " WHERE "
				+ KEY_iUSER_ID + " = " + "'" + id + "'";
		System.out.println("Query : " + selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		UserMaster data = null;

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				data = new UserMaster();
				data.setiUserId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_iUSER_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(TENANT_ID)));
				data.setStrUserName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_NAME)));
				data.setStrPassword(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_USER_PASSWORD)));
				data.setStrFirstName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FIRST_NAME)));
				data.setStrLastName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LAST_NAME)));
				data.setDtDateOfBirth(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DATE_OF_BIRTH)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TXT_DESCRIPTION)));
				data.setStrEmail(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMAIL)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgStatus(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_STATUS)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
				data.setiShipHolidayListId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SHIP_HOLIDAY_LIST_ID)));
				data.setFltMinWorkHourWeekDays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_WEEK_DAYS)));
				data.setFltMinWorkHourSaturdays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SATUREDAY)));
				data.setFltMinWorkHourSundays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_SUNDAY)));
				data.setFltMinWorkHourHolidays(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIN_WORK_HOUR_HOLIDAY)));
				data.setFltOTIncludedInWage(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_INCLUDED_IN_WAGE)));
				data.setFltOTRatePerHour(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLTOT_RATE_PER_HOUR)));
				data.setFlgIsOverTimeEnabled(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_OVER_TIME_ENABLED)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setFlgIsWatchkeeper(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FLG_IS_WATCHKEEPER)));
				data.setFaxNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_FAX_NUMBER)));
				data.setHandPhone(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_HAND_PHONE)));
				data.setLandLineNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_LANDLINE_NUMBER)));
				data.setPanNumber(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PAN_NUMBER)));
				data.setPinCode(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_PIN_CODE)));
				data.setAddressFirst(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_FIRST)));
				data.setAddressSecond(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_SECOND)));
				data.setAddressThird(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ADDRESS_THIRD)));
				data.setCity(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_CITY)));
				data.setState(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_STATE)));
				data.setiCountryId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_COUNTRY_ID)));

				data.setStrEmployeeNo(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_EMPLOYEE_NO)));
				data.setStrMiddleName(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_MIDDLE_NAME)));

				Roles rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		// return contact list
		return data;
	}

	public String getCurrentUserServiceTermIdFor(String userId) {
		// Select All Query

		String userServiceTermId = "";
		try {
			Date curDate = new Date();
			String selectQuery = "SELECT " + KEY_USER_SERVICE_TERM_ID
					+ " FROM " + TABLE_USER_SERVICE_TERM + " WHERE "
					+ KEY_iUSER_ID + " = " + "'" + userId + "' and "
					+ KEY_DT_TERM_FROM + " <='" + curDate + "' and "
					+ KEY_DT_TERM_TO + ">='" + curDate + "' and "
					+ KEY_FLG_DELETED + " ='0'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				userServiceTermId = cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("UserServiceTermId :" + userServiceTermId);
		return userServiceTermId;

	}

	// Code to get all UserServiceTermData row in a list
	public List<UserServiceTerm> getUserServiceTermRow() {
		List<UserServiceTerm> userServiceTermDataList = new ArrayList<UserServiceTerm>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE_TERM
				+ " where " + KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTerm data = new UserServiceTerm();
				data.setiUserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				userServiceTermDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermDataList;
	}

	/*
	 * Code to get UserServiceTermData by id
	 */
	public List<UserServiceTerm> getUserServiceTermById(
			String strUserServiceTermId) {
		List<UserServiceTerm> userServiceTermDataList = new ArrayList<UserServiceTerm>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE_TERM
				+ " where " + KEY_USER_SERVICE_TERM_ID + " ='"
				+ strUserServiceTermId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTerm data = new UserServiceTerm();
				data.setiUserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				userServiceTermDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermDataList;
	}

	/**
	 * Get current UserServiceterm by user id and date.
	 * 
	 * @param strUserId
	 * @param strDate
	 * @return
	 */
	public List<UserServiceTerm> getCurrentUserServiceTerm(String strUserId,
			String strDate) {
		List<UserServiceTerm> userServiceTermDataList = new ArrayList<UserServiceTerm>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM
				+ " WHERE " + KEY_iUSER_ID + " = " + "'" + strUserId + "' and "
				+ KEY_DT_TERM_FROM + " <='" + strDate + "' and "
				+ KEY_DT_TERM_TO + ">='" + strDate + "' and " + KEY_FLG_DELETED
				+ " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTerm data = new UserServiceTerm();
				data.setiUserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setiUserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				userServiceTermDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermDataList;
	}

	/**
	 * Get userservicetermRole by user id.
	 * 
	 * @return
	 */
	public List<UserServiceTermRole> getUserServiceTermRoleByUser(
			String strUserId, String strRoleId) {
		List<UserServiceTermRole> userServiceTermRoleDataList = new ArrayList<UserServiceTermRole>();
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " where " + KEY_FLG_DELETED + " ='0'" + " and "
				+ KEY_iUSER_ID + "='" + strUserId + "' and " + KEY_DT_TERM_FROM
				+ " <='" + format.format(curDate) + "'" + " and " + KEY_ROLE_ID
				+ " ='" + strRoleId + "'";

		L.fv("Device Date is : " + curDate + " and formated date is : "
				+ format.format(curDate));

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRole data = new UserServiceTermRole();
				data.setStrUserServiceTermRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ROLE_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_ROLE_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));
				data.setIuserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setIuserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setPowerPlusScore(cursor.getInt(cursor
						.getColumnIndex(POWER_PLUS_SCORE)));
				data.setField1(cursor.getInt(cursor.getColumnIndex(FIELD_ONE)));
				data.setField2(cursor.getInt(cursor.getColumnIndex(FIELD_TWO)));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	// Code to get all UserServiceTermRole row in a list
	public List<UserServiceTermRole> getUserServiceTermRoleRow() {
		List<UserServiceTermRole> userServiceTermRoleDataList = new ArrayList<UserServiceTermRole>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " where " + KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRole data = new UserServiceTermRole();
				data.setStrUserServiceTermRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ROLE_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_ROLE_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));
				data.setIuserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setIuserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setPowerPlusScore(cursor.getInt(cursor
						.getColumnIndex(POWER_PLUS_SCORE)));
				data.setField1(cursor.getInt(cursor.getColumnIndex(FIELD_ONE)));
				data.setField2(cursor.getInt(cursor.getColumnIndex(FIELD_TWO)));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	public List<UserServiceTermRole> getUserServiceTermRoleRowBiId(
			String strUserId, String strDate) {
		List<UserServiceTermRole> userServiceTermRoleDataList = new ArrayList<UserServiceTermRole>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE " + KEY_iUSER_ID + " = " + "'" + strUserId + "' and "
				+ KEY_DT_TERM_FROM + " <='" + strDate + "' and "
				+ KEY_DT_TERM_TO + ">='" + strDate + "' and " + KEY_FLG_DELETED
				+ " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRole data = new UserServiceTermRole();
				data.setStrUserServiceTermRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ROLE_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_ROLE_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));
				data.setIuserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setIuserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setPowerPlusScore(cursor.getInt(cursor
						.getColumnIndex(POWER_PLUS_SCORE)));
				data.setField1(cursor.getInt(cursor.getColumnIndex(FIELD_ONE)));
				data.setField2(cursor.getInt(cursor.getColumnIndex(FIELD_TWO)));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	/**
	 * Get UserServiceTermRole by Id
	 * 
	 * @param strUserId
	 * @param strDate
	 * @return
	 */
	public List<UserServiceTermRole> getUserServiceTermRoleById(
			String strUserServiceTermRoleId) {
		List<UserServiceTermRole> userServiceTermRoleDataList = new ArrayList<UserServiceTermRole>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE " + KEY_USER_SERVICE_TERM_ROLE_ID + " = " + "'"
				+ strUserServiceTermRoleId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRole data = new UserServiceTermRole();
				data.setStrUserServiceTermRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ROLE_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_ROLE_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));
				data.setIuserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setIuserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setPowerPlusScore(cursor.getInt(cursor
						.getColumnIndex(POWER_PLUS_SCORE)));
				data.setField1(cursor.getInt(cursor.getColumnIndex(FIELD_ONE)));
				data.setField2(cursor.getInt(cursor.getColumnIndex(FIELD_TWO)));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	/**
	 * Get UserServiceTermRole by userservicetermid
	 * 
	 * @param strUserId
	 * @param strDate
	 * @return
	 */
	public List<UserServiceTermRole> getUserServiceTermRoleByuserServicetermId(
			String strUserServiceTermId) {
		List<UserServiceTermRole> userServiceTermRoleDataList = new ArrayList<UserServiceTermRole>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE " + KEY_USER_SERVICE_TERM_ID + " = " + "'"
				+ strUserServiceTermId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRole data = new UserServiceTermRole();
				data.setStrUserServiceTermRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ROLE_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_ROLE_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));
				data.setIuserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setIuserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setPowerPlusScore(cursor.getInt(cursor
						.getColumnIndex(POWER_PLUS_SCORE)));
				data.setField1(cursor.getInt(cursor.getColumnIndex(FIELD_ONE)));
				data.setField2(cursor.getInt(cursor.getColumnIndex(FIELD_TWO)));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	public List<UserServiceTermRole> getUserServiceTermRoleRowBiIdForregisteruser(
			String strUserId, String strDate) {
		List<UserServiceTermRole> userServiceTermRoleDataList = new ArrayList<UserServiceTermRole>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE " + KEY_iUSER_ID + " = " + "'" + strUserId + "' and "
				/* + KEY_DT_TERM_FROM + " <='" + strDate + "' and " */
				+ KEY_DT_TERM_TO + ">='" + strDate + "' and " + KEY_FLG_DELETED
				+ " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRole data = new UserServiceTermRole();
				data.setStrUserServiceTermRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ROLE_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_ROLE_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));
				data.setIuserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setIuserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setPowerPlusScore(cursor.getInt(cursor
						.getColumnIndex(POWER_PLUS_SCORE)));
				data.setField1(cursor.getInt(cursor.getColumnIndex(FIELD_ONE)));
				data.setField2(cursor.getInt(cursor.getColumnIndex(FIELD_TWO)));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	public List<UserServiceTermRole> getUserServiceTermRoleRowBiIdDate(
			String strDate) {
		List<UserServiceTermRole> userServiceTermRoleDataList = new ArrayList<UserServiceTermRole>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE " + KEY_DT_TERM_TO + "<'" + strDate + "' and "
				+ KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRole data = new UserServiceTermRole();
				data.setStrUserServiceTermRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ROLE_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getString(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndex(KEY_ROLE_ID)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));
				data.setIuserId(cursor.getString(cursor
						.getColumnIndex(KEY_iUSER_ID)));
				data.setIuserServiceTermId(cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID)));
				data.setDtTermFrom(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_FROM)));
				data.setDtTermTo(cursor.getString(cursor
						.getColumnIndex(KEY_DT_TERM_TO)));
				data.setPowerPlusScore(cursor.getInt(cursor
						.getColumnIndex(POWER_PLUS_SCORE)));
				data.setField1(cursor.getInt(cursor.getColumnIndex(FIELD_ONE)));
				data.setField2(cursor.getInt(cursor.getColumnIndex(FIELD_TWO)));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	/**
	 * This method return number of days which have differenece of two UST.
	 * 
	 * @param cFrom
	 * @param cTo
	 * @param flgInclude
	 * @return
	 */
	public int getNumberOfDays(Date cFrom, Date cTo, int flgInclude) {
		float days = 0;
		if (cTo.getTime() > new Date().getTime() && flgInclude > 0) {
			days = (removeTime(new Date()).getTime() - cFrom.getTime())
					/ (1000 * 60 * 60 * 24);
		} else {
			days = (cTo.getTime() - cFrom.getTime()) / (1000 * 60 * 60 * 24);
		}

		int days1 = 0;
		days1 = Math.round(days);
		days1 = days1 + flgInclude;
		L.fv("Number of days " + days);
		return days1;
	}

	/**
	 * Remove time for check
	 * 
	 * @param date
	 * @return
	 */
	public Date removeTime(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public String getRoleName(String rid) {
		// Select All Query

		String role = "";
		SQLiteDatabase db = this.getWritableDatabase();

		try {
			String selectQuery = "SELECT " + KEY_ROLE + " FROM " + TABLE_ROLE
					+ " WHERE " + KEY_ROLE_ID + " = " + "'" + rid + "'";
			System.out.println("Query :" + selectQuery);

			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				role = cursor.getString(cursor.getColumnIndex(KEY_ROLE));
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("Role " + role);
		db.close();
		return role;

	}

	public String getUserServiceTermId(String id) {
		// Select All Query

		String userServiceTermId = "";
		try {
			String selectQuery = "SELECT " + KEY_USER_SERVICE_TERM_ID
					+ " FROM " + TABLE_USER_SERVICE_TERM + " WHERE "
					+ KEY_iUSER_ID + " = " + "'" + id + "'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				userServiceTermId = cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("UserServiceTermId :" + userServiceTermId);
		return userServiceTermId;

	}

	public String getShipId(String tenantId) {
		// Select All Query

		String shipId = "";
		try {
			String selectQuery = "SELECT " + KEY_SHIP_ID + " FROM "
					+ TABLE_SHIP_MASTER + " WHERE " + KEY_TENANT_ID + " = "
					+ "'" + tenantId + "'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				shipId = cursor.getString(cursor.getColumnIndex(KEY_SHIP_ID));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println(" :" + shipId);
		return shipId;

	}

	// Getting contacts Count
	public int getContactsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_USER_NFC;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

	/**
	 * return SyncHistory data
	 * 
	 * @return
	 */
	public List<SyncHistory> getSyncHistoryRow() {
		List<SyncHistory> syncHistoryList = new ArrayList<SyncHistory>();

		String selectQuery = "SELECT  * FROM " + TABLE_SYNC_HISTORY + " where "
				+ KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				SyncHistory sh = new SyncHistory();
				sh.setIsyncHistoryId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_SYNC_HISTORY_ID)));
				sh.setDtSyncDate(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_DATE)));
				sh.setLogMessage(cursor.getString(cursor
						.getColumnIndexOrThrow(LOG_MSG)));
				sh.setProgress(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_PROGRESS)));
				sh.setGenerator(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_GENERATOR)));
				sh.setStrFilename(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_FILE_NAME)));
				sh.setSyncOrderid(cursor.getInt(cursor
						.getColumnIndexOrThrow(SYNC_ORDER_ID)));
				sh.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndexOrThrow(FLG_DELETED)));
				sh.setFlgIsDirty(cursor.getInt(cursor
						.getColumnIndexOrThrow(FLG_IS_DIRTY)));
				sh.setDtAcknowledgeDate(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_ACK_DATE)));
				sh.setDtGeneratedDate(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_GEN_DATE)));
				sh.setLastDownLoadDate(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_LAST_DOWN_DATE)));
				sh.setStrMacId(cursor.getString(cursor
						.getColumnIndexOrThrow(MAC_ID)));
				sh.setStrRegisterTabletId(cursor.getString(cursor
						.getColumnIndexOrThrow(REG_TABLET_ID)));
				sh.setiShipId(Integer.parseInt(cursor.getString(cursor
						.getColumnIndexOrThrow(SHIP_ID))));
				sh.setiTenantId(Integer.parseInt(cursor.getString(cursor
						.getColumnIndexOrThrow(TENANT_ID))));
				sh.setStrSyncMode(cursor.getString(cursor
						.getColumnIndexOrThrow(SYNC_MODE)));

				// Adding contact to list
				syncHistoryList.add(sh);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return syncHistoryList;
	}

	/**
	 * @author ripunjay
	 * @param ppt
	 * @return
	 */
	public long insertSyncHistorydata(SyncHistory syncHistory) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SYNC_HISTORY_ID, syncHistory.getSyncOrderid());
		values.put(SYNC_DATE, syncHistory.getDtSyncDate());
		values.put(LOG_MSG, syncHistory.getLogMessage());
		values.put(SYNC_PROGRESS, syncHistory.getProgress());
		values.put(SYNC_GENERATOR, syncHistory.getGenerator());
		values.put(SYNC_FILE_NAME, syncHistory.getStrFilename());
		values.put(KEY_FLG_IS_DIRTY, syncHistory.getFlgIsDirty());
		values.put(SYNC_ORDER_ID, syncHistory.getSyncOrderid());
		values.put(KEY_TENANT_ID, syncHistory.getiTenantId());
		values.put(KEY_SHIP_ID, syncHistory.getiShipId());
		values.put(KEY_FLG_DELETED, syncHistory.getFlgDeleted());
		values.put(SYNC_ACK_DATE, syncHistory.getDtAcknowledgeDate());
		values.put(SYNC_GEN_DATE, syncHistory.getDtGeneratedDate());
		values.put(SYNC_LAST_DOWN_DATE, syncHistory.getLastDownLoadDate());
		values.put(MAC_ID, syncHistory.getStrMacId());
		values.put(REG_TABLET_ID, syncHistory.getStrRegisterTabletId());
		values.put(SYNC_MODE, syncHistory.getStrSyncMode());

		long result = db.insert(TABLE_SYNC_HISTORY, null, values);
		db.close();
		return result;
	}

	/**
	 * @author ripunjay
	 * @param ppt
	 * @return
	 */
	public long updateSyncHistorydata(SyncHistory syncHistory) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SYNC_HISTORY_ID, syncHistory.getIsyncHistoryId());
		values.put(SYNC_DATE, syncHistory.getDtSyncDate());
		values.put(LOG_MSG, syncHistory.getLogMessage());
		values.put(SYNC_PROGRESS, syncHistory.getProgress());
		values.put(SYNC_GENERATOR, syncHistory.getGenerator());
		values.put(SYNC_FILE_NAME, syncHistory.getStrFilename());
		values.put(KEY_FLG_IS_DIRTY, syncHistory.getFlgIsDirty());
		values.put(SYNC_ORDER_ID, syncHistory.getSyncOrderid());
		values.put(KEY_TENANT_ID, syncHistory.getiTenantId());
		values.put(KEY_SHIP_ID, syncHistory.getiShipId());
		values.put(KEY_FLG_DELETED, syncHistory.getFlgDeleted());
		values.put(SYNC_ACK_DATE, syncHistory.getDtAcknowledgeDate());
		values.put(SYNC_GEN_DATE, syncHistory.getDtGeneratedDate());
		values.put(SYNC_LAST_DOWN_DATE, syncHistory.getLastDownLoadDate());
		values.put(MAC_ID, syncHistory.getStrMacId());
		values.put(REG_TABLET_ID, syncHistory.getStrRegisterTabletId());
		values.put(SYNC_MODE, syncHistory.getStrSyncMode());

		String whereClause = KEY_SYNC_HISTORY_ID + " =  '"
				+ syncHistory.getIsyncHistoryId() + "'";

		long result = db.update(TABLE_SYNC_HISTORY, values, whereClause, null);
		db.close();
		return result;
	}

	/**
	 * @author ripunjay
	 * @param ROLEFUNCTIONS
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public long insertRoleFunctiondata(RoleTabletFunction tabletRoleFunctions) {
		SQLiteDatabase db = this.getWritableDatabase();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		ContentValues values = new ContentValues();
		values.put(KEY_ROLE_FUNCTION_ID,
				tabletRoleFunctions.getiTabletRoleFunctionsId());
		values.put(KEY_DT_CREATED, tabletRoleFunctions.getDtCreated());
		values.put(KEY_DT_UPDATED, tabletRoleFunctions.getDtUpdated());
		values.put(KEY_FLG_DELETED, tabletRoleFunctions.getFlgDeleted());
		values.put(ROLE_FUNCTION_CODE, tabletRoleFunctions.getStrFunctionCode());
		values.put(KEY_ROLE_ID, tabletRoleFunctions.getiRoleId());
		values.put(KEY_TENANT_ID, tabletRoleFunctions.getiTenantId());
		values.put(FIELD_ONE, tabletRoleFunctions.getStrField1());
		values.put(FIELD_TWO, tabletRoleFunctions.getStrField2());

		long result = db.insert(TABLE_ROLE_FUNCTION, null, values);
		db.close();
		return result;

	}

	/**
	 * @author pushkar.m
	 * @param rtf
	 * @return
	 */
	public long updateRoleTabletFunctiondata(
			RoleTabletFunction roleTabletFunction) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		values.put(KEY_DT_CREATED, roleTabletFunction.getDtCreated());
		values.put(KEY_DT_UPDATED, roleTabletFunction.getDtUpdated());
		values.put(KEY_FLG_DELETED, roleTabletFunction.getFlgDeleted());
		values.put(ROLE_FUNCTION_CODE, roleTabletFunction.getStrFunctionCode());
		values.put(KEY_ROLE_ID, roleTabletFunction.getiRoleId());
		// values.put(KEY_FLG_IS_DIRTY, roleTabletFunction.getFlgIsDirty());
		values.put(KEY_TENANT_ID, roleTabletFunction.getiTenantId());
		values.put(FIELD_ONE, roleTabletFunction.getStrField1());
		values.put(FIELD_TWO, roleTabletFunction.getStrField2());

		String whereClause = KEY_ROLE_FUNCTION_ID + " =  '"
				+ roleTabletFunction.getiTabletRoleFunctionsId() + "'";

		long result = db.update(TABLE_ROLE_FUNCTION, values, whereClause, null);
		db.close();
		return result;
	}

	/**
	 * @author pushkar.m Return RoleTabletFunction record by id
	 */
	public List<RoleTabletFunction> getTabletRoleFunctionsById(String id) {
		List<RoleTabletFunction> tabletRoleFunctionsList = new ArrayList<RoleTabletFunction>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_FUNCTION
				+ " where " + KEY_ROLE_FUNCTION_ID + " ='" + id + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				RoleTabletFunction data = new RoleTabletFunction();
				data.setiTabletRoleFunctionsId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_FUNCTION_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setStrFunctionCode(cursor.getString(cursor
						.getColumnIndexOrThrow(ROLE_FUNCTION_CODE)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrField1(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_ONE)));
				data.setStrField2(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_TWO)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));

				// Adding contact to list
				tabletRoleFunctionsList.add(data);
			} while (cursor.moveToNext());
		}

		// return contact list
		db.close();
		return tabletRoleFunctionsList;
	}

	/**
	 * return TabletRoleFunctions data
	 * 
	 * @return
	 */
	public List<RoleTabletFunction> getTabletRoleFunctionsRow() {
		List<RoleTabletFunction> tabletRoleFunctionsList = new ArrayList<RoleTabletFunction>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_FUNCTION
				+ " where " + KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				RoleTabletFunction data = new RoleTabletFunction();
				data.setiTabletRoleFunctionsId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_FUNCTION_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setStrFunctionCode(cursor.getString(cursor
						.getColumnIndexOrThrow(ROLE_FUNCTION_CODE)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrField1(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_ONE)));
				data.setStrField2(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_TWO)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));

				// Adding contact to list
				tabletRoleFunctionsList.add(data);
			} while (cursor.moveToNext());
		}

		// return contact list
		db.close();
		return tabletRoleFunctionsList;
	}

	/**
	 * return TabletRoleFunctions data
	 * 
	 * @return
	 */
	public List<RoleTabletFunction> getTabletRoleFunctionsRowByRoleId(
			String strRole) {
		List<RoleTabletFunction> tabletRoleFunctionsList = new ArrayList<RoleTabletFunction>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_FUNCTION
				+ " where " + KEY_FLG_DELETED + " ='0' and " + KEY_ROLE_ID
				+ " = '" + strRole + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				RoleTabletFunction data = new RoleTabletFunction();

				data.setiTabletRoleFunctionsId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_FUNCTION_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setStrFunctionCode(cursor.getString(cursor
						.getColumnIndexOrThrow(ROLE_FUNCTION_CODE)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrField1(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_ONE)));
				data.setStrField2(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_TWO)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));

				// Adding contact to list
				tabletRoleFunctionsList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return tabletRoleFunctionsList;
	}

	/**
	 * return TabletRoleFunctions data
	 * 
	 * @return
	 */
	public List<RoleTabletFunction> getTabletRoleFunctionsRowByRoleIdAndFunctionCode(
			String strRoleId, String strFunctionCode) {
		List<RoleTabletFunction> tabletRoleFunctionsList = new ArrayList<RoleTabletFunction>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_FUNCTION
				+ " where " + KEY_FLG_DELETED + " ='0' and " + KEY_ROLE_ID
				+ " = '" + strRoleId + "' and " + ROLE_FUNCTION_CODE + " ='"
				+ strFunctionCode + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				RoleTabletFunction data = new RoleTabletFunction();

				data.setiTabletRoleFunctionsId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_FUNCTION_ID)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_DT_UPDATED)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndexOrThrow(KEY_FLG_DELETED)));
				data.setStrFunctionCode(cursor.getString(cursor
						.getColumnIndexOrThrow(ROLE_FUNCTION_CODE)));
				data.setiRoleId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_ROLE_ID)));
				data.setStrField1(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_ONE)));
				data.setStrField2(cursor.getString(cursor
						.getColumnIndexOrThrow(FIELD_TWO)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndexOrThrow(KEY_TENANT_ID)));

				// Adding contact to list
				tabletRoleFunctionsList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return tabletRoleFunctionsList;
	}

	/**
	 * This method will insert single record in Task table and return the value
	 * whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertTaskTable(Tasks tasks) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(TASK_ID, tasks.getItaskId());
			contentValues.put(TENANT_ID, tasks.getiTenantId());
			contentValues.put(TASK_NAME, tasks.getStrTaskName());
			contentValues.put(KEY_S_DESCRIPTION, tasks.getStrDescription());
			contentValues.put(KEY_DT_CREATED, tasks.getDtCreated());
			contentValues.put(KEY_DT_UPDATED, tasks.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED, tasks.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY, tasks.getFlgIsDirty());

			result = db.insert(TABLE_TASKS, null, contentValues);
			db.close();
			Log.i("Tasks value :== ", "inside insert of Tasks table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in Task table and return the value
	 * whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateTaskTable(Tasks tasks) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(TASK_ID, tasks.getItaskId());
			contentValues.put(TENANT_ID, tasks.getiTenantId());
			contentValues.put(TASK_NAME, tasks.getStrTaskName());
			contentValues.put(KEY_S_DESCRIPTION, tasks.getStrDescription());
			contentValues.put(KEY_DT_CREATED, tasks.getDtCreated());
			contentValues.put(KEY_DT_UPDATED, tasks.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED, tasks.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY, tasks.getFlgIsDirty());

			String whereClause = TASK_ID + " =  '" + tasks.getItaskId() + "'";
			result = db.update(TABLE_TASKS, contentValues, whereClause, null);
			db.close();
			Log.i("Tasks value :== ", "inside insert of Tasks table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * return Task data by Id
	 * 
	 * @return
	 */
	public List<Tasks> getTasksDataById(String strTaskId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<Tasks> taskList = new ArrayList<Tasks>();

		String selectQuery = "SELECT  * FROM " + TABLE_TASKS + " where "
				+ TASK_ID + "='" + strTaskId + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					Tasks tasks = new Tasks();

					tasks.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					tasks.setStrTaskName(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_NAME)));
					tasks.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					tasks.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					tasks.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					tasks.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					tasks.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					tasks.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_S_DESCRIPTION)));

					// Adding contact to list
					taskList.add(tasks);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();
			db.close();
		}

		return taskList;
	}

	/**
	 * return Task full data
	 * 
	 * @return
	 */
	public List<Tasks> getTasksData() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<Tasks> taskList = new ArrayList<Tasks>();

		String selectQuery = "SELECT  * FROM " + TABLE_TASKS;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					Tasks tasks = new Tasks();

					tasks.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					tasks.setStrTaskName(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_NAME)));
					tasks.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					tasks.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					tasks.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					tasks.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					tasks.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					tasks.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_S_DESCRIPTION)));

					// Adding contact to list
					taskList.add(tasks);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return taskList;
	}

	/**
	 * This method will insert single record in CrewDepartment table and return
	 * the value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertCrewDepartmentTable(CrewDepartment crewDepartment) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(CREW_DEPARTMENT_ID,
					crewDepartment.getIcrewDepartmentId());
			contentValues.put(TENANT_ID, crewDepartment.getiTenantId());
			contentValues.put(DEPARTMENT_NAME,
					crewDepartment.getStrDepartmentName());
			contentValues.put(KEY_DT_CREATED, crewDepartment.getDtCreated());
			contentValues.put(KEY_DT_UPDATED, crewDepartment.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED, crewDepartment.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY, crewDepartment.getFlgIsDirty());

			result = db.insert(TABLE_CREW_DEPARTMENT, null, contentValues);
			db.close();
			Log.i("CrewDepartMent value :== ",
					"inside insert of CrewDepartMent table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in CrewDepartment table and return
	 * the value whether the record is inserted successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateCrewDepartmentTable(CrewDepartment crewDepartment) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(CREW_DEPARTMENT_ID,
					crewDepartment.getIcrewDepartmentId());
			contentValues.put(TENANT_ID, crewDepartment.getiTenantId());
			contentValues.put(DEPARTMENT_NAME,
					crewDepartment.getStrDepartmentName());
			contentValues.put(KEY_DT_CREATED, crewDepartment.getDtCreated());
			contentValues.put(KEY_DT_UPDATED, crewDepartment.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED, crewDepartment.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY, crewDepartment.getFlgIsDirty());

			String whereClause = CREW_DEPARTMENT_ID + " =  '"
					+ crewDepartment.getIcrewDepartmentId() + "'";
			result = db.update(TABLE_CREW_DEPARTMENT, contentValues,
					whereClause, null);
			db.close();
			Log.i("CrewDepartMent value :== ",
					"inside insert of CrewDepartMent table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * return CrewDepartMent data by Id
	 * 
	 * @return
	 */
	public List<CrewDepartment> getCrewDepartMentDataById(
			String strCrewDepartmentId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<CrewDepartment> crewDepartmentList = new ArrayList<CrewDepartment>();

		String selectQuery = "SELECT  * FROM " + TABLE_CREW_DEPARTMENT
				+ " where " + CREW_DEPARTMENT_ID + "='" + strCrewDepartmentId
				+ "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					CrewDepartment crewDepartment = new CrewDepartment();

					crewDepartment.setIcrewDepartmentId(cursor.getString(cursor
							.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					crewDepartment.setStrDepartmentName(cursor.getString(cursor
							.getColumnIndexOrThrow(DEPARTMENT_NAME)));
					crewDepartment.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					crewDepartment.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					crewDepartment.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					crewDepartment.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					crewDepartment.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					crewDepartmentList.add(crewDepartment);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return crewDepartmentList;
	}

	/**
	 * return CrewDepartMent data
	 * 
	 * @return
	 */
	public List<CrewDepartment> getCrewDepartMentData() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<CrewDepartment> crewDepartmentList = new ArrayList<CrewDepartment>();

		String selectQuery = "SELECT  * FROM " + TABLE_CREW_DEPARTMENT;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					CrewDepartment crewDepartment = new CrewDepartment();

					crewDepartment.setIcrewDepartmentId(cursor.getString(cursor
							.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					crewDepartment.setStrDepartmentName(cursor.getString(cursor
							.getColumnIndexOrThrow(DEPARTMENT_NAME)));
					crewDepartment.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					crewDepartment.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					crewDepartment.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					crewDepartment.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					crewDepartment.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					crewDepartmentList.add(crewDepartment);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return crewDepartmentList;
	}

	/**
	 * This method will insert single record in RoleCrewDepartment table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertRoleCrewDepartmentTable(
			RoleCrewDepartment roleCrewDepartment) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(ROLE_CREW_DEPARTMENT_ID,
					roleCrewDepartment.getIroleCrewDepartmentId());
			contentValues.put(CREW_DEPARTMENT_ID,
					roleCrewDepartment.getIcrewDepartmentId());
			contentValues.put(TENANT_ID, roleCrewDepartment.getiTenantId());
			contentValues.put(KEY_ROLE_ID, roleCrewDepartment.getIroleId());
			contentValues
					.put(KEY_DT_CREATED, roleCrewDepartment.getDtCreated());
			contentValues
					.put(KEY_DT_UPDATED, roleCrewDepartment.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					roleCrewDepartment.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					roleCrewDepartment.getFlgIsDirty());

			result = db.insert(TABLE_ROLE_CREW_DEPARTMENT, null, contentValues);
			db.close();
			Log.i("RoleCrewDepartMent value :== ",
					"inside insert of RoleCrewDepartMent table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will UPDATE single record in RoleCrewDepartment table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateRoleCrewDepartmentTable(
			RoleCrewDepartment roleCrewDepartment) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(ROLE_CREW_DEPARTMENT_ID,
					roleCrewDepartment.getIroleCrewDepartmentId());
			contentValues.put(CREW_DEPARTMENT_ID,
					roleCrewDepartment.getIcrewDepartmentId());
			contentValues.put(TENANT_ID, roleCrewDepartment.getiTenantId());
			contentValues.put(KEY_ROLE_ID, roleCrewDepartment.getIroleId());
			contentValues
					.put(KEY_DT_CREATED, roleCrewDepartment.getDtCreated());
			contentValues
					.put(KEY_DT_UPDATED, roleCrewDepartment.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					roleCrewDepartment.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					roleCrewDepartment.getFlgIsDirty());

			String whereClause = ROLE_CREW_DEPARTMENT_ID + " =  '"
					+ roleCrewDepartment.getIroleCrewDepartmentId() + "'";
			result = db.update(TABLE_ROLE_CREW_DEPARTMENT, contentValues,
					whereClause, null);
			db.close();
			Log.i("RoleCrewDepartMent value :== ",
					"inside insert of RoleCrewDepartMent table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * return RoleCrewDepartMent data by Id
	 * 
	 * @return
	 */
	public List<RoleCrewDepartment> getRoleCrewDepartMentDataById(
			String strRoleCrewDepartmentId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<RoleCrewDepartment> roleCrewDepartmentList = new ArrayList<RoleCrewDepartment>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_CREW_DEPARTMENT
				+ " where " + ROLE_CREW_DEPARTMENT_ID + "='"
				+ strRoleCrewDepartmentId + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					RoleCrewDepartment roleCrewDepartment = new RoleCrewDepartment();

					roleCrewDepartment
							.setIroleCrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(ROLE_CREW_DEPARTMENT_ID)));
					roleCrewDepartment
							.setIcrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					roleCrewDepartment.setIroleId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_ROLE_ID)));
					roleCrewDepartment.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					roleCrewDepartment.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					roleCrewDepartment.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					roleCrewDepartment.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					roleCrewDepartment.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					roleCrewDepartmentList.add(roleCrewDepartment);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return roleCrewDepartmentList;
	}

	/**
	 * return RoleCrewDepartMent data by role Id
	 * 
	 * @return
	 */
	public List<RoleCrewDepartment> getRoleCrewDepartMentDataByRoleId(
			String strRoleId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<RoleCrewDepartment> roleCrewDepartmentList = new ArrayList<RoleCrewDepartment>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_CREW_DEPARTMENT
				+ " where " + KEY_ROLE_ID + "='" + strRoleId + "' and "
				+ KEY_FLG_DELETED + " ='0'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					RoleCrewDepartment roleCrewDepartment = new RoleCrewDepartment();

					roleCrewDepartment
							.setIroleCrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(ROLE_CREW_DEPARTMENT_ID)));
					roleCrewDepartment
							.setIcrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					roleCrewDepartment.setIroleId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_ROLE_ID)));
					roleCrewDepartment.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					roleCrewDepartment.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					roleCrewDepartment.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					roleCrewDepartment.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					roleCrewDepartment.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					roleCrewDepartmentList.add(roleCrewDepartment);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return roleCrewDepartmentList;
	}

	/**
	 * return RoleCrewDepartMent data
	 * 
	 * @return
	 */
	public List<RoleCrewDepartment> getRoleCrewDepartMentData() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<RoleCrewDepartment> roleCrewDepartmentList = new ArrayList<RoleCrewDepartment>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_CREW_DEPARTMENT;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					RoleCrewDepartment roleCrewDepartment = new RoleCrewDepartment();

					roleCrewDepartment
							.setIroleCrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(ROLE_CREW_DEPARTMENT_ID)));
					roleCrewDepartment
							.setIcrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					roleCrewDepartment.setIroleId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_ROLE_ID)));
					roleCrewDepartment.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					roleCrewDepartment.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					roleCrewDepartment.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					roleCrewDepartment.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					roleCrewDepartment.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					roleCrewDepartmentList.add(roleCrewDepartment);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return roleCrewDepartmentList;
	}

	/**
	 * This method will insert single record in CrewDepartmentTask table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertCrewDepartmentTaskTable(
			CrewDepartmentTask crewDepartmentTask) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(CREW_DEPARTMENT_TASK_ID,
					crewDepartmentTask.getIcrewDepartmentTaskId());
			contentValues.put(CREW_DEPARTMENT_ID,
					crewDepartmentTask.getIcrewDepartmentId());
			contentValues.put(TENANT_ID, crewDepartmentTask.getiTenantId());
			contentValues.put(TASK_ID, crewDepartmentTask.getItaskId());
			contentValues
					.put(KEY_DT_CREATED, crewDepartmentTask.getDtCreated());
			contentValues
					.put(KEY_DT_UPDATED, crewDepartmentTask.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					crewDepartmentTask.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					crewDepartmentTask.getFlgIsDirty());

			result = db.insert(TABLE_CREW_DEPARTMENT_TASK, null, contentValues);
			db.close();
			Log.i("CrewDepartMentTask value :== ",
					"inside insert of CrewDepartMentTask table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will UPDATE single record in CrewDepartMentTask table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateCrewDepartmentTaskTable(
			CrewDepartmentTask crewDepartmentTask) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(CREW_DEPARTMENT_TASK_ID,
					crewDepartmentTask.getIcrewDepartmentTaskId());
			contentValues.put(CREW_DEPARTMENT_ID,
					crewDepartmentTask.getIcrewDepartmentId());
			contentValues.put(TENANT_ID, crewDepartmentTask.getiTenantId());
			contentValues.put(TASK_ID, crewDepartmentTask.getItaskId());
			contentValues
					.put(KEY_DT_CREATED, crewDepartmentTask.getDtCreated());
			contentValues
					.put(KEY_DT_UPDATED, crewDepartmentTask.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					crewDepartmentTask.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					crewDepartmentTask.getFlgIsDirty());

			String whereClause = CREW_DEPARTMENT_TASK_ID + " =  '"
					+ crewDepartmentTask.getIcrewDepartmentTaskId() + "'";
			result = db.update(TABLE_CREW_DEPARTMENT_TASK, contentValues,
					whereClause, null);
			db.close();
			Log.i("CrewDepartmentTask value :== ",
					"inside insert of CrewDepartmentTask table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * return CrewDepartmentTask data by Id
	 * 
	 * @return
	 */
	public List<CrewDepartmentTask> getCrewDepartmentTaskDataById(
			String strCrewDepartmentTaskId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<CrewDepartmentTask> crewDepartmentTaskList = new ArrayList<CrewDepartmentTask>();

		String selectQuery = "SELECT  * FROM " + TABLE_CREW_DEPARTMENT_TASK
				+ " where " + CREW_DEPARTMENT_TASK_ID + "='"
				+ strCrewDepartmentTaskId + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					CrewDepartmentTask crewDepartmentTask = new CrewDepartmentTask();

					crewDepartmentTask
							.setIcrewDepartmentTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_TASK_ID)));
					crewDepartmentTask
							.setIcrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					crewDepartmentTask.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					crewDepartmentTask.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					crewDepartmentTask.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					crewDepartmentTask.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					crewDepartmentTask.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					crewDepartmentTask.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					crewDepartmentTaskList.add(crewDepartmentTask);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return crewDepartmentTaskList;
	}

	/**
	 * return CrewDepartmentTask data by crewdepartmentId
	 * 
	 * @return
	 */
	public List<CrewDepartmentTask> getCrewDepartmentTaskDataByCrewDepartmentId(
			String strCrewDepartmentId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<CrewDepartmentTask> crewDepartmentTaskList = new ArrayList<CrewDepartmentTask>();

		String selectQuery = "SELECT  * FROM " + TABLE_CREW_DEPARTMENT_TASK
				+ " where " + CREW_DEPARTMENT_ID + "='" + strCrewDepartmentId
				+ "' and " + KEY_FLG_DELETED + " ='0'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					CrewDepartmentTask crewDepartmentTask = new CrewDepartmentTask();

					crewDepartmentTask
							.setIcrewDepartmentTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_TASK_ID)));
					crewDepartmentTask
							.setIcrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					crewDepartmentTask.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					crewDepartmentTask.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					crewDepartmentTask.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					crewDepartmentTask.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					crewDepartmentTask.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					crewDepartmentTask.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					crewDepartmentTaskList.add(crewDepartmentTask);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return crewDepartmentTaskList;
	}

	/**
	 * return CrewDepartmentTask data by Id
	 * 
	 * @return
	 */
	public List<CrewDepartmentTask> getCrewDepartmentTaskData() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<CrewDepartmentTask> crewDepartmentTaskList = new ArrayList<CrewDepartmentTask>();

		String selectQuery = "SELECT  * FROM " + TABLE_CREW_DEPARTMENT_TASK;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					CrewDepartmentTask crewDepartmentTask = new CrewDepartmentTask();

					crewDepartmentTask
							.setIcrewDepartmentTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_TASK_ID)));
					crewDepartmentTask
							.setIcrewDepartmentId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_ID)));
					crewDepartmentTask.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					crewDepartmentTask.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					crewDepartmentTask.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					crewDepartmentTask.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					crewDepartmentTask.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					crewDepartmentTask.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));

					// Adding contact to list
					crewDepartmentTaskList.add(crewDepartmentTask);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return crewDepartmentTaskList;
	}

	/**
	 * This method will insert single record in UserWorkTimeSheet table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertUserWorkTimeSheetTable(UserWorkTimeSheet userWorkTimeSheet) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(USER_WORK_TIMESHEET_ID,
					userWorkTimeSheet.getiUserWorkTimeSheetId());
			contentValues.put(WORK_START_DATE,
					userWorkTimeSheet.getWorkStartDate());
			contentValues.put(WORK_START_TIME,
					userWorkTimeSheet.getWorkStartTime());
			contentValues
					.put(WORK_END_DATE, userWorkTimeSheet.getWorkEndDate());
			contentValues
					.put(WORK_END_TIME, userWorkTimeSheet.getWorkEndTime());
			contentValues.put(WORK_START_DATE_TIME,
					userWorkTimeSheet.getWorkStartDateTime());
			contentValues.put(WORK_END_DATE_TIME,
					userWorkTimeSheet.getWorkEndDateTime());
			contentValues.put(KEY_DT_CREATED, userWorkTimeSheet.getDtCreated());
			contentValues.put(KEY_DT_UPDATED, userWorkTimeSheet.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					userWorkTimeSheet.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					userWorkTimeSheet.getFlgIsDirty());
			contentValues.put(KEY_FLG_STATUS, userWorkTimeSheet.getFlgStatus());
			contentValues.put(KEY_SHIP_ID, userWorkTimeSheet.getiShipId());
			contentValues.put(KEY_TENANT_ID, userWorkTimeSheet.getiTenantId());
			contentValues.put(KEY_iUSER_ID, userWorkTimeSheet.getiUserId());
			contentValues.put(KEY_USER_SERVICE_TERM_ID,
					userWorkTimeSheet.getiUserServiceTermId());
			contentValues.put(KEY_S_DESCRIPTION,
					userWorkTimeSheet.getStrDescription());
			contentValues.put(FLG_INTERNATIONAL_ORDER_START,
					userWorkTimeSheet.getFlgInternationalOrderStart());
			contentValues.put(FLG_INTERNATIONAL_ORDER_END,
					userWorkTimeSheet.getFlgInternationalOrderEnd());
			contentValues.put(FLG_PROCESSED,
					userWorkTimeSheet.getFlgProcessed());

			result = db.insert(TABLE_USER_WORKTIME_SHEET, null, contentValues);
			db.close();
			Log.i("UserWorkTimeSheet value :== ",
					"inside insert of UserWorkTimeSheet table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in UserWorkTimeSheet table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateUserWorkTimeSheetTable(UserWorkTimeSheet userWorkTimeSheet) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(USER_WORK_TIMESHEET_ID,
					userWorkTimeSheet.getiUserWorkTimeSheetId());
			contentValues.put(WORK_START_DATE,
					userWorkTimeSheet.getWorkStartDate());
			contentValues.put(WORK_START_TIME,
					userWorkTimeSheet.getWorkStartTime());
			contentValues
					.put(WORK_END_DATE, userWorkTimeSheet.getWorkEndDate());
			contentValues
					.put(WORK_END_TIME, userWorkTimeSheet.getWorkEndTime());
			contentValues.put(WORK_START_DATE_TIME,
					userWorkTimeSheet.getWorkStartDateTime());
			contentValues.put(WORK_END_DATE_TIME,
					userWorkTimeSheet.getWorkEndDateTime());
			contentValues.put(KEY_DT_CREATED, userWorkTimeSheet.getDtCreated());
			contentValues.put(KEY_DT_UPDATED, userWorkTimeSheet.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					userWorkTimeSheet.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					userWorkTimeSheet.getFlgIsDirty());
			contentValues.put(KEY_FLG_STATUS, userWorkTimeSheet.getFlgStatus());
			contentValues.put(KEY_SHIP_ID, userWorkTimeSheet.getiShipId());
			contentValues.put(KEY_TENANT_ID, userWorkTimeSheet.getiTenantId());
			contentValues.put(KEY_iUSER_ID, userWorkTimeSheet.getiUserId());
			contentValues.put(KEY_USER_SERVICE_TERM_ID,
					userWorkTimeSheet.getiUserServiceTermId());
			contentValues.put(KEY_S_DESCRIPTION,
					userWorkTimeSheet.getStrDescription());
			contentValues.put(FLG_INTERNATIONAL_ORDER_START,
					userWorkTimeSheet.getFlgInternationalOrderStart());
			contentValues.put(FLG_INTERNATIONAL_ORDER_END,
					userWorkTimeSheet.getFlgInternationalOrderEnd());
			contentValues.put(FLG_PROCESSED,
					userWorkTimeSheet.getFlgProcessed());

			String whereClause = USER_WORK_TIMESHEET_ID + " =  '"
					+ userWorkTimeSheet.getiUserWorkTimeSheetId() + "'";
			result = db.update(TABLE_USER_WORKTIME_SHEET, contentValues,
					whereClause, null);
			db.close();
			Log.i("UserWorkTimeSheet value :== ",
					"inside insert of UserWorkTimeSheet table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * return UserWorkTimeSheet data by Id
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheet> getUserWorkTimeSheetDataById(
			String strUserWorkTimeSheetId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheet> userWorkTimeSheetList = new ArrayList<UserWorkTimeSheet>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET
				+ " where " + USER_WORK_TIMESHEET_ID + "='"
				+ strUserWorkTimeSheetId + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheet userWorkTimeSheet = new UserWorkTimeSheet();

					userWorkTimeSheet
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheet.setWorkStartDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheet.setWorkStartTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheet.setWorkEndDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheet.setWorkEndTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheet
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheet
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheet.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheet.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheet.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheet.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheet.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheet.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheet.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheet
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheet
							.setFlgInternationalOrderStart(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_START)));
					userWorkTimeSheet
							.setFlgInternationalOrderEnd(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_END)));
					userWorkTimeSheet.setFlgProcessed(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_PROCESSED)));

					// Adding contact to list
					userWorkTimeSheetList.add(userWorkTimeSheet);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetList;
	}

	/**
	 * return UserWorkTimeSheet data by user id and flgStatus flgStatus=0 mean
	 * only Tap in flgStatus=1 mean tap out.
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheet> getUserWorkTimeSheetDataByUserIdAndStatus(
			String strUserId, String curDate, String flgStatus) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheet> userWorkTimeSheetList = new ArrayList<UserWorkTimeSheet>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET
				+ " where " + KEY_iUSER_ID + "='" + strUserId + "' and "
				+ WORK_START_DATE + " ='" + curDate + "' and " + FLG_STATUS
				+ " ='" + flgStatus + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheet userWorkTimeSheet = new UserWorkTimeSheet();

					userWorkTimeSheet
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheet.setWorkStartDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheet.setWorkStartTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheet.setWorkEndDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheet.setWorkEndTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheet
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheet
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheet.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheet.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheet.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheet.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheet.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheet.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheet.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheet
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheet
							.setFlgInternationalOrderStart(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_START)));
					userWorkTimeSheet
							.setFlgInternationalOrderEnd(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_END)));
					userWorkTimeSheet.setFlgProcessed(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_PROCESSED)));

					// Adding contact to list
					userWorkTimeSheetList.add(userWorkTimeSheet);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetList;
	}

	/**
	 * @author pushkar.m return UserWorkTimeSheet data by user id and flgStatus
	 *         flgStatus=0 mean only Tap in flgStatus=1 mean tap out.
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheet> getUserWorkTimeSheetDataByStatus(
			String strUserId, String flgStatus) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheet> userWorkTimeSheetList = new ArrayList<UserWorkTimeSheet>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET
				+ " where " + KEY_iUSER_ID + "='" + strUserId + "' and "
				+ FLG_STATUS + " ='" + flgStatus + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheet userWorkTimeSheet = new UserWorkTimeSheet();

					userWorkTimeSheet
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheet.setWorkStartDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheet.setWorkStartTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheet.setWorkEndDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheet.setWorkEndTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheet
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheet
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheet.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheet.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheet.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheet.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheet.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheet.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheet.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheet
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheet
							.setFlgInternationalOrderStart(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_START)));
					userWorkTimeSheet
							.setFlgInternationalOrderEnd(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_END)));
					userWorkTimeSheet.setFlgProcessed(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_PROCESSED)));

					// Adding contact to list
					userWorkTimeSheetList.add(userWorkTimeSheet);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetList;
	}

	/**
	 * return UserWorkTimeSheet data by User Id and date
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheet> getUserWorkTimeSheetDataByUseridDate(
			String strUserId, String strDate) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheet> userWorkTimeSheetList = new ArrayList<UserWorkTimeSheet>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET
				+ " where " + KEY_iUSER_ID + "='" + strUserId + "' and "
				+ WORK_START_DATE + " ='" + strDate + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheet userWorkTimeSheet = new UserWorkTimeSheet();

					userWorkTimeSheet
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheet.setWorkStartDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheet.setWorkStartTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheet.setWorkEndDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheet.setWorkEndTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheet
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheet
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheet.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheet.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheet.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheet.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheet.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheet.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheet.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheet
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheet
							.setFlgInternationalOrderStart(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_START)));
					userWorkTimeSheet
							.setFlgInternationalOrderEnd(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_END)));
					userWorkTimeSheet.setFlgProcessed(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_PROCESSED)));

					// Adding contact to list
					userWorkTimeSheetList.add(userWorkTimeSheet);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetList;
	}

	/**
	 * return UserWorkTimeSheet data
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheet> getUserWorkTimeSheetData() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheet> userWorkTimeSheetList = new ArrayList<UserWorkTimeSheet>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheet userWorkTimeSheet = new UserWorkTimeSheet();

					userWorkTimeSheet
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheet.setWorkStartDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheet.setWorkStartTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheet.setWorkEndDate(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheet.setWorkEndTime(cursor.getString(cursor
							.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheet
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheet
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheet.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheet.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheet.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheet.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheet.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheet.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheet.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheet
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheet
							.setFlgInternationalOrderStart(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_START)));
					userWorkTimeSheet
							.setFlgInternationalOrderEnd(cursor.getInt(cursor
									.getColumnIndexOrThrow(FLG_INTERNATIONAL_ORDER_END)));
					userWorkTimeSheet.setFlgProcessed(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_PROCESSED)));

					userWorkTimeSheet.setFlgStatus(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_STATUS)));

					// Adding contact to list
					userWorkTimeSheetList.add(userWorkTimeSheet);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetList;
	}

	/**
	 * This method will update dirty flag single record in
	 * UserWorkTimeSheetTable table and return the value whether the record is
	 * updated successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateDirtyFlagUserWorkTimeSheetTable(
			String strUserWorkTimeSheetId) {

		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;
		String whereClause = USER_WORK_TIMESHEET_ID + " = '"
				+ strUserWorkTimeSheetId + "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(FLG_IS_DIRTY, 0);

			result = db.update(TABLE_USER_WORKTIME_SHEET, contentValues,
					whereClause, null);
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update dirty flag single record in
	 * UserWorkTimeSheetTaskTable table and return the value whether the record
	 * is updated successfully in database or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateDirtyFlagUserWorkTimeSheetTaskTable(
			String strUserWorkTimeSheetTaskId) {

		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;
		String whereClause = USER_WORK_TIMESHEET_TASK_ID + " = '"
				+ strUserWorkTimeSheetTaskId + "'";

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(FLG_IS_DIRTY, 0);

			result = db.update(TABLE_USER_WORKTIME_SHEET_TASK, contentValues,
					whereClause, null);
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will insert single record in UserWorkTimeSheetTask table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertUserWorkTimeSheetTaskTable(
			UserWorkTimeSheetTask userWorkTimeSheetTask) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(USER_WORK_TIMESHEET_TASK_ID,
					userWorkTimeSheetTask.getiUserWorkTimeSheetTaskId());
			contentValues.put(USER_WORK_TIMESHEET_ID,
					userWorkTimeSheetTask.getiUserWorkTimeSheetId());
			contentValues.put(WORK_START_DATE,
					userWorkTimeSheetTask.getWorkStartDate());
			contentValues.put(WORK_START_TIME,
					userWorkTimeSheetTask.getWorkStartTime());
			contentValues.put(WORK_END_DATE,
					userWorkTimeSheetTask.getWorkEndDate());
			contentValues.put(WORK_END_TIME,
					userWorkTimeSheetTask.getWorkEndTime());
			contentValues.put(WORK_START_DATE_TIME,
					userWorkTimeSheetTask.getWorkStartDateTime());
			contentValues.put(WORK_END_DATE_TIME,
					userWorkTimeSheetTask.getWorkEndDateTime());
			contentValues.put(KEY_DT_CREATED,
					userWorkTimeSheetTask.getDtCreated());
			contentValues.put(KEY_DT_UPDATED,
					userWorkTimeSheetTask.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					userWorkTimeSheetTask.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					userWorkTimeSheetTask.getFlgIsDirty());
			contentValues.put(KEY_FLG_STATUS,
					userWorkTimeSheetTask.getFlgStatus());
			contentValues.put(KEY_SHIP_ID, userWorkTimeSheetTask.getiShipId());
			contentValues.put(KEY_TENANT_ID,
					userWorkTimeSheetTask.getiTenantId());
			contentValues.put(KEY_iUSER_ID, userWorkTimeSheetTask.getiUserId());
			contentValues.put(TASK_ID, userWorkTimeSheetTask.getItaskId());
			contentValues.put(CREW_DEPARTMENT_TASK_ID,
					userWorkTimeSheetTask.getIcrewDepartmentTaskId());
			contentValues.put(KEY_USER_SERVICE_TERM_ID,
					userWorkTimeSheetTask.getiUserServiceTermId());
			contentValues.put(KEY_S_DESCRIPTION,
					userWorkTimeSheetTask.getStrDescription());

			result = db.insert(TABLE_USER_WORKTIME_SHEET_TASK, null,
					contentValues);
			db.close();
			Log.i("UserWorkTimeSheetTask value :== ",
					"inside insert of UserWorkTimeSheetTask table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * This method will update single record in UserWorkTimeSheetTask table and
	 * return the value whether the record is inserted successfully in database
	 * or not.
	 * 
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateUserWorkTimeSheetTaskTable(
			UserWorkTimeSheetTask userWorkTimeSheetTask) {
		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();

			contentValues.put(USER_WORK_TIMESHEET_TASK_ID,
					userWorkTimeSheetTask.getiUserWorkTimeSheetTaskId());
			contentValues.put(USER_WORK_TIMESHEET_ID,
					userWorkTimeSheetTask.getiUserWorkTimeSheetId());
			contentValues.put(WORK_START_DATE,
					userWorkTimeSheetTask.getWorkStartDate());
			contentValues.put(WORK_START_TIME,
					userWorkTimeSheetTask.getWorkStartTime());
			contentValues.put(WORK_END_DATE,
					userWorkTimeSheetTask.getWorkEndDate());
			contentValues.put(WORK_END_TIME,
					userWorkTimeSheetTask.getWorkEndTime());
			contentValues.put(WORK_START_DATE_TIME,
					userWorkTimeSheetTask.getWorkStartDateTime());
			contentValues.put(WORK_END_DATE_TIME,
					userWorkTimeSheetTask.getWorkEndDateTime());
			contentValues.put(KEY_DT_CREATED,
					userWorkTimeSheetTask.getDtCreated());
			contentValues.put(KEY_DT_UPDATED,
					userWorkTimeSheetTask.getDtUpdated());
			contentValues.put(KEY_FLG_DELETED,
					userWorkTimeSheetTask.getFlgDeleted());
			contentValues.put(KEY_FLG_IS_DIRTY,
					userWorkTimeSheetTask.getFlgIsDirty());
			contentValues.put(KEY_FLG_STATUS,
					userWorkTimeSheetTask.getFlgStatus());
			contentValues.put(KEY_SHIP_ID, userWorkTimeSheetTask.getiShipId());
			contentValues.put(KEY_TENANT_ID,
					userWorkTimeSheetTask.getiTenantId());
			contentValues.put(KEY_iUSER_ID, userWorkTimeSheetTask.getiUserId());
			contentValues.put(TASK_ID, userWorkTimeSheetTask.getItaskId());
			contentValues.put(CREW_DEPARTMENT_TASK_ID,
					userWorkTimeSheetTask.getIcrewDepartmentTaskId());
			contentValues.put(KEY_USER_SERVICE_TERM_ID,
					userWorkTimeSheetTask.getiUserServiceTermId());
			contentValues.put(KEY_S_DESCRIPTION,
					userWorkTimeSheetTask.getStrDescription());

			String whereClause = USER_WORK_TIMESHEET_TASK_ID + " =  '"
					+ userWorkTimeSheetTask.getiUserWorkTimeSheetTaskId() + "'";
			result = db.update(TABLE_USER_WORKTIME_SHEET_TASK, contentValues,
					whereClause, null);
			db.close();
			Log.i("UserWorkTimeSheetTask value :== ",
					"inside update of UserWorkTimeSheetTask table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		}

		return result;
	}

	/**
	 * return UserWorkTimeSheetTask data by Id
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheetTask> getUserWorkTimeSheetTaskById(
			String strUserWorkTimeSheetTaskId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheetTask> userWorkTimeSheetTaskList = new ArrayList<UserWorkTimeSheetTask>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET_TASK
				+ " where " + USER_WORK_TIMESHEET_TASK_ID + "='"
				+ strUserWorkTimeSheetTaskId + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheetTask userWorkTimeSheetTask = new UserWorkTimeSheetTask();

					userWorkTimeSheetTask
							.setiUserWorkTimeSheetTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_TASK_ID)));
					userWorkTimeSheetTask
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheetTask.setWorkStartDate(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheetTask.setWorkStartTime(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheetTask.setWorkEndDate(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheetTask.setWorkEndTime(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheetTask
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheetTask
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheetTask.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheetTask.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheetTask.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheetTask.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheetTask.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheetTask.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheetTask.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheetTask
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheetTask.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					userWorkTimeSheetTask
							.setIcrewDepartmentTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_TASK_ID)));

					// Adding contact to list
					userWorkTimeSheetTaskList.add(userWorkTimeSheetTask);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetTaskList;
	}

	/**
	 * return UserWorkTimeSheetTask data by UserWorkTimeSheetId and UserId
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheetTask> getUserWorkTimeSheetTaskByWorkTimeSheetIdAndUserId(
			String strUserWorkTimeSheetId, String strUserId) {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheetTask> userWorkTimeSheetTaskList = new ArrayList<UserWorkTimeSheetTask>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET_TASK
				+ " where " + USER_WORK_TIMESHEET_ID + "='"
				+ strUserWorkTimeSheetId + "' and " + KEY_iUSER_ID + " ='"
				+ strUserId + "'";
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheetTask userWorkTimeSheetTask = new UserWorkTimeSheetTask();

					userWorkTimeSheetTask
							.setiUserWorkTimeSheetTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_TASK_ID)));
					userWorkTimeSheetTask
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheetTask.setWorkStartDate(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheetTask.setWorkStartTime(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheetTask.setWorkEndDate(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheetTask.setWorkEndTime(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheetTask
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheetTask
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheetTask.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheetTask.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheetTask.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheetTask.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheetTask.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheetTask.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheetTask.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheetTask
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheetTask.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					userWorkTimeSheetTask
							.setIcrewDepartmentTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_TASK_ID)));

					// Adding contact to list
					userWorkTimeSheetTaskList.add(userWorkTimeSheetTask);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetTaskList;
	}

	/**
	 * return UserWorkTimeSheetTask data
	 * 
	 * @return
	 */
	public List<UserWorkTimeSheetTask> getUserWorkTimeSheetTaskData() {
		SQLiteDatabase db = this.getWritableDatabase();
		List<UserWorkTimeSheetTask> userWorkTimeSheetTaskList = new ArrayList<UserWorkTimeSheetTask>();

		String selectQuery = "SELECT  * FROM " + TABLE_USER_WORKTIME_SHEET_TASK;
		Cursor cursor = null;

		try {
			cursor = db.rawQuery(selectQuery, null);
			// looping through all rows and adding to list
			if (cursor != null && cursor.moveToFirst()) {
				do {
					UserWorkTimeSheetTask userWorkTimeSheetTask = new UserWorkTimeSheetTask();

					userWorkTimeSheetTask
							.setiUserWorkTimeSheetTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_TASK_ID)));
					userWorkTimeSheetTask
							.setiUserWorkTimeSheetId(cursor.getString(cursor
									.getColumnIndexOrThrow(USER_WORK_TIMESHEET_ID)));
					userWorkTimeSheetTask.setWorkStartDate(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE)));
					userWorkTimeSheetTask.setWorkStartTime(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_START_TIME)));
					userWorkTimeSheetTask.setWorkEndDate(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE)));
					userWorkTimeSheetTask.setWorkEndTime(cursor
							.getString(cursor
									.getColumnIndexOrThrow(WORK_END_TIME)));
					userWorkTimeSheetTask
							.setWorkStartDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_START_DATE_TIME)));
					userWorkTimeSheetTask
							.setWorkEndDateTime(cursor.getString(cursor
									.getColumnIndexOrThrow(WORK_END_DATE_TIME)));
					userWorkTimeSheetTask.setiTenantId(cursor.getString(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					userWorkTimeSheetTask.setiShipId(cursor.getString(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					userWorkTimeSheetTask.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_CREATED)));
					userWorkTimeSheetTask.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_DT_UPDATED)));
					userWorkTimeSheetTask.setFlgDeleted(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_DELETED)));
					userWorkTimeSheetTask.setFlgIsDirty(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_IS_DIRTY)));
					userWorkTimeSheetTask.setiUserId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_iUSER_ID)));
					userWorkTimeSheetTask
							.setiUserServiceTermId(cursor.getString(cursor
									.getColumnIndexOrThrow(KEY_USER_SERVICE_TERM_ID)));
					userWorkTimeSheetTask.setItaskId(cursor.getString(cursor
							.getColumnIndexOrThrow(TASK_ID)));
					userWorkTimeSheetTask
							.setIcrewDepartmentTaskId(cursor.getString(cursor
									.getColumnIndexOrThrow(CREW_DEPARTMENT_TASK_ID)));

					userWorkTimeSheetTask.setFlgStatus(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_FLG_STATUS)));

					// Adding contact to list
					userWorkTimeSheetTaskList.add(userWorkTimeSheetTask);
				} while (cursor.moveToNext());
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return userWorkTimeSheetTaskList;
	}

	/**
	 * Get all International dates
	 * 
	 * @return
	 */
	public List<InternationalDates> getAllInternationalDates() {
		List<InternationalDates> internationalDatesList = new ArrayList<InternationalDates>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_INTERNATIONALDATES
				+ " where " + KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				InternationalDates data = new InternationalDates();
				data.setIinternationalDateId(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATES_ID)));
				data.setInternationalDate(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATE)));
				data.setFlgRepeated(cursor.getInt(cursor
						.getColumnIndex(FLG_REPEATED)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndex(KEY_TXT_DESCRIPTION)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				internationalDatesList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return internationalDatesList;
	}

	/**
	 * Get International date by Id
	 * 
	 * @return
	 */
	public List<InternationalDates> getAllInternationalDatesById(
			String strInternationalDateId) {
		List<InternationalDates> internationalDatesList = new ArrayList<InternationalDates>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_INTERNATIONALDATES
				+ " where " + KEY_FLG_DELETED + " ='0' and "
				+ KEY_INTERNATIONALDATES_ID + " = '" + strInternationalDateId
				+ "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				InternationalDates data = new InternationalDates();
				data.setIinternationalDateId(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATES_ID)));
				data.setInternationalDate(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATE)));
				data.setFlgRepeated(cursor.getInt(cursor
						.getColumnIndex(FLG_REPEATED)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndex(KEY_TXT_DESCRIPTION)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				internationalDatesList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return internationalDatesList;
	}

	/**
	 * Get International date by date and shipId
	 * 
	 * @return
	 */
	public List<InternationalDates> getInternationalDatesByShipIdAndDate(
			String strShipId, String curDate) {
		List<InternationalDates> internationalDatesList = new ArrayList<InternationalDates>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_INTERNATIONALDATES
				+ " where " + KEY_FLG_DELETED + " ='0' and " + KEY_SHIP_ID
				+ " = '" + strShipId + "'" + " and " + KEY_INTERNATIONALDATE
				+ " ='" + curDate + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);
		L.fv("selectQuery for idl is : " + selectQuery);
		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				InternationalDates data = new InternationalDates();
				data.setIinternationalDateId(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATES_ID)));
				data.setInternationalDate(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATE)));
				data.setFlgRepeated(cursor.getInt(cursor
						.getColumnIndex(FLG_REPEATED)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndex(KEY_TXT_DESCRIPTION)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				internationalDatesList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		L.fv("international date list is : " + internationalDatesList);
		return internationalDatesList;
	}

	/**
	 * Get IdlLogWorkById
	 * 
	 * @return
	 */
	public List<IdlLogWork> getIdlLogWorkById(String strIdlLogWorkId) {
		List<IdlLogWork> idlLogWorkList = new ArrayList<IdlLogWork>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_IDL_LOGWORK + " where "
				+ KEY_FLG_DELETED + " ='0' and " + IDL_LOGWORK_ID + " = '"
				+ strIdlLogWorkId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				IdlLogWork data = new IdlLogWork();
				data.setiIdlLogWorkId(cursor.getString(cursor
						.getColumnIndex(IDL_LOGWORK_ID)));
				data.setIinternationalDateId(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATES_ID)));
				data.setInternationalDate(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATE)));
				data.setFlgRepeated(cursor.getInt(cursor
						.getColumnIndex(FLG_REPEATED)));
				data.setFlgDayStart(cursor.getInt(cursor
						.getColumnIndex(FLG_DAY_START)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndex(KEY_TXT_DESCRIPTION)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				idlLogWorkList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return idlLogWorkList;
	}

	/**
	 * Get IdlLogWork by international date id
	 * 
	 * @return
	 */
	public List<IdlLogWork> getIdlLogWorkByInternationalDateId(
			String strInternationalDateId) {
		List<IdlLogWork> idlLogWorkList = new ArrayList<IdlLogWork>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_IDL_LOGWORK + " where "
				+ KEY_FLG_DELETED + " ='0' and " + KEY_INTERNATIONALDATES_ID
				+ " = '" + strInternationalDateId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				IdlLogWork data = new IdlLogWork();
				data.setiIdlLogWorkId(cursor.getString(cursor
						.getColumnIndex(IDL_LOGWORK_ID)));
				data.setIinternationalDateId(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATES_ID)));
				data.setInternationalDate(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATE)));
				data.setFlgRepeated(cursor.getInt(cursor
						.getColumnIndex(FLG_REPEATED)));
				data.setFlgDayStart(cursor.getInt(cursor
						.getColumnIndex(FLG_DAY_START)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndex(KEY_TXT_DESCRIPTION)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				idlLogWorkList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return idlLogWorkList;
	}

	/**
	 * Get IdlLogWork data
	 * 
	 * @return
	 */
	public List<IdlLogWork> getIdlLogWorkData() {
		List<IdlLogWork> idlLogWorkList = new ArrayList<IdlLogWork>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_IDL_LOGWORK + " where "
				+ KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				IdlLogWork data = new IdlLogWork();
				data.setiIdlLogWorkId(cursor.getString(cursor
						.getColumnIndex(IDL_LOGWORK_ID)));
				data.setIinternationalDateId(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATES_ID)));
				data.setInternationalDate(cursor.getString(cursor
						.getColumnIndex(KEY_INTERNATIONALDATE)));
				data.setFlgRepeated(cursor.getInt(cursor
						.getColumnIndex(FLG_REPEATED)));
				data.setFlgDayStart(cursor.getInt(cursor
						.getColumnIndex(FLG_DAY_START)));
				data.setTxtDescription(cursor.getString(cursor
						.getColumnIndex(KEY_TXT_DESCRIPTION)));
				data.setFlgDeleted(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_DELETED)));
				data.setFlgIsDirty(cursor.getInt(cursor
						.getColumnIndex(KEY_FLG_IS_DIRTY)));
				data.setDtCreated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_CREATED)));
				data.setDtUpdated(cursor.getString(cursor
						.getColumnIndex(KEY_DT_UPDATED)));
				data.setiShipId(cursor.getString(cursor
						.getColumnIndex(KEY_SHIP_ID)));
				data.setiTenantId(cursor.getString(cursor
						.getColumnIndex(KEY_TENANT_ID)));

				// Adding contact to list
				idlLogWorkList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return idlLogWorkList;
	}

	/**
	 * @author ripunjay.s This method will insert single record in syns_status
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertSyncStatusTable(SyncStatus syncStatus) {

		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(SYNC_STATUS_ID, syncStatus.getiSyncStatusId());
			contentValues.put(SYNC_DATE, syncStatus.getDtSyncDate());
			contentValues.put(SYNC_MODE, syncStatus.getSyncMode());
			contentValues.put(DATA_SYNC_MODE, syncStatus.getDataSyncMode());
			contentValues.put(SERVER_ADDRESS, syncStatus.getServerAddress());
			contentValues.put(CREATED_DATE, syncStatus.getCreatedDate());
			contentValues.put(MODIFIED_DATE, syncStatus.getModifiedDate());
			contentValues.put(CREATED_BY, syncStatus.getCreatedBy());
			contentValues.put(MODIFIED_BY, syncStatus.getModifiedBy());
			contentValues.put(FLG_DELETED, syncStatus.getFlgDeleted());
			contentValues.put(FLG_STATUS, syncStatus.getFlgStatus());
			contentValues.put(FLG_IS_DIRTY, syncStatus.getFlgIsDirty());
			contentValues.put(TENANT_ID, syncStatus.getiTenantId());
			contentValues.put(MAC_ID, syncStatus.getStrMacId());
			contentValues.put(FLG_DEVICE_IS_DIRTY, syncStatus.getFlgIsDeviceDirty());
			contentValues.put(SYNC_TIME, syncStatus.getSyncTime());
			contentValues.put(SHIP_ID, syncStatus.getiShipId());
			contentValues.put(KEY_MODULE_NAME, syncStatus.getModuleName());

			result = db.insert(TABLE_SYNC_STATUS, null, contentValues);
			Log.i("SYNC_STATUS value :== ",
					"inside insert of sync_status table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		} finally {
			db.close();
		}

		return result;
	}

	/**
	 * @author ripunjay.s This method will update single record in syns_status
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateSyncStatusTable(SyncStatus syncStatus) {

		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;
		String whereClause = SYNC_STATUS_ID + " = '"
				+ syncStatus.getiSyncStatusId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(SYNC_STATUS_ID, syncStatus.getiSyncStatusId());
			contentValues.put(SYNC_DATE, syncStatus.getDtSyncDate());
			contentValues.put(SYNC_MODE, syncStatus.getSyncMode());
			contentValues.put(DATA_SYNC_MODE, syncStatus.getDataSyncMode());
			contentValues.put(SERVER_ADDRESS, syncStatus.getServerAddress());
			contentValues.put(CREATED_DATE, syncStatus.getCreatedDate());
			contentValues.put(MODIFIED_DATE, syncStatus.getModifiedDate());
			contentValues.put(CREATED_BY, syncStatus.getCreatedBy());
			contentValues.put(MODIFIED_BY, syncStatus.getModifiedBy());
			contentValues.put(FLG_DELETED, syncStatus.getFlgDeleted());
			contentValues.put(FLG_STATUS, syncStatus.getFlgStatus());
			contentValues.put(FLG_IS_DIRTY, syncStatus.getFlgIsDirty());
			contentValues.put(TENANT_ID, syncStatus.getiTenantId());
			contentValues.put(MAC_ID, syncStatus.getStrMacId());
			contentValues.put(FLG_DEVICE_IS_DIRTY, syncStatus.getFlgIsDeviceDirty());
			contentValues.put(SYNC_TIME, syncStatus.getSyncTime());
			contentValues.put(SHIP_ID, syncStatus.getiShipId());
			contentValues.put(KEY_MODULE_NAME, syncStatus.getModuleName());

			result = db.update(TABLE_SYNC_STATUS, contentValues, whereClause,
					null);
			Log.i("SYNC_STATUS value :== ",
					"inside update of sync_status table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		} finally {
			db.close();
		}

		return result;
	}

	/**
	 * @author ripunjay.s fetch the single row data of sync_status table By
	 *         syncStatusid.
	 * @return list of SyncStatus object
	 */
	public List<SyncStatus> getSyncStatusDataById(String strSyncStatusId) {

		List<SyncStatus> syncStatusData = new ArrayList<SyncStatus>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = null;

		String whereClause = SYNC_STATUS_ID + " =  '" + strSyncStatusId + "'";

		try {
			cursor = db.query(TABLE_SYNC_STATUS, null, whereClause, null, null,
					null, null);
			Log.i("dbmanager", "inside getSyncStatusDataById");
			if (cursor != null && cursor.moveToFirst()) {
				syncStatusData = new ArrayList<SyncStatus>();

				do {
					SyncStatus syncStatus = new SyncStatus();
					syncStatus.setiSyncStatusId(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_STATUS_ID)));
					syncStatus.setDtSyncDate(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_DATE)));
					syncStatus.setSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_MODE)));
					syncStatus.setDataSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DATA_SYNC_MODE)));
					syncStatus.setServerAddress(cursor.getString(cursor
							.getColumnIndexOrThrow(SERVER_ADDRESS)));
					syncStatus.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_DATE)));
					syncStatus.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_DATE)));
					syncStatus.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_BY)));
					syncStatus.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_BY)));
					syncStatus.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_DELETED)));
					syncStatus.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_IS_DIRTY)));
					syncStatus.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_STATUS)));
					syncStatus.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					syncStatus.setStrMacId(cursor.getString(cursor
							.getColumnIndexOrThrow(MAC_ID)));
					syncStatus.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_DEVICE_IS_DIRTY)));
					syncStatus.setSyncTime(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_TIME)));
					syncStatus.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					
					syncStatus.setModuleName(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_MODULE_NAME)));					
					
					

					syncStatusData.add(syncStatus);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataDataById else");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return syncStatusData;
	}

	/**
	 * @author ripunjay.s fetch the all row data of sync_status table.
	 * @return list of SyncStatus object
	 */
	public List<SyncStatus> getSyncStatusData() {

		List<SyncStatus> syncStatusData = new ArrayList<SyncStatus>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = null;

		try {
			cursor = db.query(TABLE_SYNC_STATUS, null, null, null, null, null,
					null);
			Log.i("dbmanager", "inside getSyncStatusData");
			if (cursor != null && cursor.moveToFirst()) {
				syncStatusData = new ArrayList<SyncStatus>();

				do {
					SyncStatus syncStatus = new SyncStatus();
					syncStatus.setiSyncStatusId(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_STATUS_ID)));
					syncStatus.setDtSyncDate(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_DATE)));
					syncStatus.setSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_MODE)));
					syncStatus.setDataSyncMode(cursor.getString(cursor
							.getColumnIndexOrThrow(DATA_SYNC_MODE)));
					syncStatus.setServerAddress(cursor.getString(cursor
							.getColumnIndexOrThrow(SERVER_ADDRESS)));
					syncStatus.setCreatedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_DATE)));
					syncStatus.setModifiedDate(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_DATE)));
					syncStatus.setModifiedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_BY)));
					syncStatus.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_BY)));
					syncStatus.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_DELETED)));
					syncStatus.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_IS_DIRTY)));
					syncStatus.setFlgStatus(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_STATUS)));
					syncStatus.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					syncStatus.setStrMacId(cursor.getString(cursor
							.getColumnIndexOrThrow(MAC_ID)));
					syncStatus.setFlgIsDeviceDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_DEVICE_IS_DIRTY)));
					syncStatus.setSyncTime(cursor.getString(cursor
							.getColumnIndexOrThrow(SYNC_TIME)));
					syncStatus.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(SHIP_ID)));
					syncStatus.setModuleName(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_MODULE_NAME)));			

					syncStatusData.add(syncStatus);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getsyncStatusDataData");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return syncStatusData;
	}

	/**
	 * @author pushkar.m fetch the all row data of TimeZoneData.
	 * @return list of TimeZoneData object
	 */
	public List<TimeZoneData> getAllTimeZoneData() {

		List<TimeZoneData> timeZoneDataList = new ArrayList<TimeZoneData>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = null;

		try {
			cursor = db.query(TABLE_TIMEZONEDATA, null, null, null, null, null,
					null);
			Log.i("dbmanager", "inside getAllTimeZoneData");
			if (cursor != null && cursor.moveToFirst()) {
				timeZoneDataList = new ArrayList<TimeZoneData>();

				do {
					TimeZoneData timeZoneData = new TimeZoneData();

					timeZoneData.setiTimeZoneDataId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_TIMEZONEDATA_ID)));
					timeZoneData.setStrZoneId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_STRZONE_ID)));
					timeZoneData.setStrTimeZoneName(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_STRZONE_NAME)));
					timeZoneData.setStrTime(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_STRTIME)));
					timeZoneData.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_S_DESCRIPTION)));
					timeZoneData.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_DATE)));
					timeZoneData.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_DATE)));
					timeZoneData.setUpdatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_BY)));
					timeZoneData.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_BY)));
					timeZoneData.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_DELETED)));
					timeZoneData.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_IS_DIRTY)));
					timeZoneData.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					timeZoneData.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(SHIP_ID)));

					timeZoneDataList.add(timeZoneData);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getAllTimeZoneData");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return timeZoneDataList;
	}

	/**
	 * @author pushkar.m fetch the all row data of TimeZoneData.
	 * @return list of TimeZoneData object
	 */
	public List<TimeZoneData> getTimeZoneDataByShipId(int shipId) {

		List<TimeZoneData> timeZoneDataList = new ArrayList<TimeZoneData>();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = null;

		String whereClause = SHIP_ID + " =  '" + shipId + "'";

		try {
			cursor = db.query(TABLE_TIMEZONEDATA, null, whereClause, null,
					null, null, null);
			Log.i("dbmanager", "inside getAllTimeZoneData");
			if (cursor != null && cursor.moveToFirst()) {
				timeZoneDataList = new ArrayList<TimeZoneData>();

				do {
					TimeZoneData timeZoneData = new TimeZoneData();

					timeZoneData.setiTimeZoneDataId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_TIMEZONEDATA_ID)));
					timeZoneData.setStrZoneId(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_STRZONE_ID)));
					timeZoneData.setStrTimeZoneName(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_STRZONE_NAME)));
					timeZoneData.setStrTime(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_STRTIME)));
					timeZoneData.setStrDescription(cursor.getString(cursor
							.getColumnIndexOrThrow(KEY_S_DESCRIPTION)));
					timeZoneData.setDtUpdated(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_DATE)));
					timeZoneData.setDtCreated(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_DATE)));
					timeZoneData.setUpdatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(MODIFIED_BY)));
					timeZoneData.setCreatedBy(cursor.getString(cursor
							.getColumnIndexOrThrow(CREATED_BY)));
					timeZoneData.setFlgDeleted(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_DELETED)));
					timeZoneData.setFlgIsDirty(cursor.getInt(cursor
							.getColumnIndexOrThrow(FLG_IS_DIRTY)));
					timeZoneData.setiTenantId(cursor.getInt(cursor
							.getColumnIndexOrThrow(TENANT_ID)));
					timeZoneData.setiShipId(cursor.getInt(cursor
							.getColumnIndexOrThrow(SHIP_ID)));

					timeZoneDataList.add(timeZoneData);
				} while (cursor.moveToNext());
			} else {
				Log.i("dbmanager", "inside getAllTimeZoneData");
			}
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		return timeZoneDataList;
	}

	/**
	 * @author Pushkar.m This method will insert single record in TimezoneData
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long insertTimeZoneDataTable(TimeZoneData timeZoneData) {

		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(KEY_TIMEZONEDATA_ID,
					timeZoneData.getiTimeZoneDataId());
			contentValues.put(KEY_STRZONE_ID, timeZoneData.getStrZoneId());
			contentValues.put(KEY_STRTIME, timeZoneData.getStrTime());

			contentValues.put(KEY_STRZONE_NAME,
					timeZoneData.getStrTimeZoneName());

			contentValues.put(KEY_S_DESCRIPTION,
					timeZoneData.getStrDescription());

			contentValues.put(CREATED_DATE, timeZoneData.getDtCreated());
			contentValues.put(MODIFIED_DATE, timeZoneData.getDtUpdated());

			contentValues.put(MODIFIED_BY, timeZoneData.getUpdatedBy());
			contentValues.put(CREATED_BY, timeZoneData.getCreatedBy());
			contentValues.put(FLG_DELETED, timeZoneData.getFlgDeleted());

			contentValues.put(FLG_IS_DIRTY, timeZoneData.getFlgIsDirty());

			contentValues.put(TENANT_ID, timeZoneData.getiTenantId());

			contentValues.put(SHIP_ID, timeZoneData.getiShipId());

			result = db.insert(TABLE_TIMEZONEDATA, null, contentValues);
			Log.i("TimeZone value :== ", "inside insert of TimeZoneData table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		} finally {
			db.close();
		}

		return result;
	}

	/**
	 * @author pushkar.m This method will update single record in TimeZoneData
	 *         table and return the value whether the record is inserted
	 *         successfully in database or not.
	 * @param tag
	 * @return 0 = success -1 = failure
	 */

	public long updateTimeZoneTable(TimeZoneData timeZoneData) {

		SQLiteDatabase db = this.getWritableDatabase();
		long result = 0;
		String whereClause = SHIP_ID + " = '" + timeZoneData.getiShipId() + "'";

		try {
			ContentValues contentValues = new ContentValues();
			contentValues.put(KEY_TIMEZONEDATA_ID,
					timeZoneData.getiTimeZoneDataId());
			contentValues.put(KEY_STRZONE_ID, timeZoneData.getStrZoneId());
			contentValues.put(KEY_STRTIME, timeZoneData.getStrTime());

			contentValues.put(KEY_STRZONE_NAME,
					timeZoneData.getStrTimeZoneName());

			contentValues.put(KEY_S_DESCRIPTION,
					timeZoneData.getStrDescription());

			contentValues.put(CREATED_DATE, timeZoneData.getDtCreated());
			contentValues.put(MODIFIED_DATE, timeZoneData.getDtUpdated());

			contentValues.put(MODIFIED_BY, timeZoneData.getUpdatedBy());
			contentValues.put(CREATED_BY, timeZoneData.getCreatedBy());
			contentValues.put(FLG_DELETED, timeZoneData.getFlgDeleted());

			contentValues.put(FLG_IS_DIRTY, timeZoneData.getFlgIsDirty());

			contentValues.put(TENANT_ID, timeZoneData.getiTenantId());

			result = db.update(TABLE_TIMEZONEDATA, contentValues, whereClause,
					null);
			Log.i("SYNC_STATUS value :== ",
					"inside update of sync_status table ");
		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception", exceptionString);
		} finally {
			db.close();
		}

		return result;
	}

	public String getAvailableIds(String tableName) {
		StringBuffer availableIds = new StringBuffer();

		String selectQuery = "SELECT  * FROM " + tableName;
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {
			db = this.getWritableDatabase();
			cursor = db.rawQuery(selectQuery, null);

			if (tableName.equals(DatabaseSupport.TABLE_USER_WORKTIME_SHEET)) {
				if (cursor.moveToFirst()) {
					do {

						availableIds
								.append("'"
										+ cursor.getString(cursor
												.getColumnIndexOrThrow(DatabaseSupport.USER_WORK_TIMESHEET_ID))
										+ "',");

					} while (cursor.moveToNext());
				}
			} else if (tableName
					.equals(DatabaseSupport.TABLE_USER_WORKTIME_SHEET_TASK)) {
				if (cursor.moveToFirst()) {
					do {

						availableIds
								.append("'"
										+ cursor.getString(cursor
												.getColumnIndexOrThrow(DatabaseSupport.USER_WORK_TIMESHEET_TASK_ID))
										+ "',");

					} while (cursor.moveToNext());
				}
			}

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		if (availableIds.length() > 0)
			return availableIds.substring(0, availableIds.length() - 1);
		else
			return "";
	}

	public String getMasterAvailableIds(String tableName) {
		StringBuffer availableIds = new StringBuffer();

		// String selectQuery = "SELECT  * FROM " + tableName ;
		SQLiteDatabase db = null;
		Cursor cursor = null;
		try {

			String pkField = getPKFieldName(tableName);

			db = this.getWritableDatabase();
			cursor = db.query(tableName, null, null, null, null, null, null);
			if (cursor != null && cursor.moveToFirst()) {
				if(tableName.equals("Roles")){
					do {
						availableIds.append(cursor.getString(cursor
										.getColumnIndexOrThrow(pkField)) + ",");
	
					} while (cursor.moveToNext());
				}else{
					do {
						availableIds.append("'"
								+ cursor.getString(cursor
										.getColumnIndexOrThrow(pkField)) + "',");
	
					} while (cursor.moveToNext());
				}
			}

		} catch (Exception ex) {

			String exceptionString = ex.toString();
			Log.i("exception : ", exceptionString);
		} finally {
			if (cursor != null)
				cursor.close();

			db.close();
		}

		if (availableIds.length() > 0)
			return availableIds.substring(0, availableIds.length() - 1);
		else
			return "";
	}

	/**
	 * Get all table Details from teh sqlite_master table in Db.
	 * 
	 * @return An ArrayList of table details.
	 */
	public ArrayList<String[]> getDbTableDetails() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(
				"SELECT name FROM sqlite_master WHERE type='table'", null);
		ArrayList<String[]> result = new ArrayList<String[]>();
		int i = 0;
		result.add(c.getColumnNames());
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			String[] temp = new String[c.getColumnCount()];
			for (i = 0; i < temp.length; i++) {
				temp[i] = c.getString(i);
			}
			result.add(temp);
		}

		db.close();
		return result;
	}

	/**
	 * Get all table Details from teh sqlite_master table in Db.
	 * 
	 * @return An ArrayList of table details.
	 */
	public ArrayList<String[]> getDbTableColumnDetails(String tableName) {
		ArrayList<String[]> result = new ArrayList<String[]>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("PRAGMA table_info(" + tableName + ")",
				null);
		try {
			int nameIdx = cursor.getColumnIndexOrThrow("name");
			int typeIdx = cursor.getColumnIndexOrThrow("type");
			int notNullIdx = cursor.getColumnIndexOrThrow("notnull");
			int dfltValueIdx = cursor.getColumnIndexOrThrow("dflt_value");

			ArrayList<String> integerDefault1NotNull = new ArrayList<String>();

			while (cursor.moveToNext()) {
				String[] temp = new String[2];
				String type = cursor.getString(typeIdx);
				if (!cursor.getString(nameIdx).equalsIgnoreCase("id")) {
					if ("INTEGER".equals(type)) {
						// Integer column
						if (cursor.getInt(notNullIdx) == 1) {
							// NOT NULL
							String defaultValue = cursor
									.getString(dfltValueIdx);
							if ("1".equals(defaultValue)) {
								integerDefault1NotNull.add(cursor
										.getString(nameIdx));
							}
						}
					}
					temp[0] = cursor.getString(nameIdx);
					temp[1] = cursor.getString(typeIdx);

					result.add(temp);
				}
			}
			System.out
					.println("integerDefault1NotNull now contains a list of all columns "
							+ " defined as INTEGER NOT NULL DEFAULT 1, "
							+ integerDefault1NotNull);
		} finally {
			cursor.close();
		}

		// db.close();
		return result;
	}

	public Cursor getDataForSync(String tableName) {
		try {
			Cursor cursor;
			StringBuffer selectQuery = null;
			if (!tableName.equalsIgnoreCase("PowerPlusTabletSyncHistory")
					&& !tableName.equalsIgnoreCase("UserNfc")) {
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName
						+ " where " + KEY_FLG_IS_DIRTY + "='1'");
			} else {
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName);
			}

			SQLiteDatabase db = this.getWritableDatabase();
			cursor = db.rawQuery(selectQuery.toString(), null);

			if (cursor != null && cursor.getCount() > 0) {

			}
			return cursor;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public List<String> getPkDataForSync(String strPk, String tableName) {

		String selectQuery = "SELECT  " + strPk + " FROM " + tableName;// +" where "+
																		// KEY_FLG_DELETED+"='0'";

		List<String> pkList = new ArrayList<String>();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				String strPkId = "";
				strPkId = cursor.getString(0);
				pkList.add(strPkId);

			} while (cursor.moveToNext());
		}

		db.close();
		return pkList;
	}

	public void insertRecordInTables(String strTablename, ContentValues values) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Inserting Row
		db.insert(strTablename, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	public long updateRecordInTables(String strTablename, String strPk_Nmae,
			String pkValues, ContentValues values) {
		SQLiteDatabase db = this.getWritableDatabase();

		String whereClause = strPk_Nmae + " =  '" + pkValues + "'";

		long result = db.update(strTablename, values, whereClause, null);
		db.close();
		return result;
	}

	public long updateDirtyRecordInTables(String strTablename) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FLG_IS_DIRTY, "0");
		long result = db.update(strTablename, values, null, null);
		db.close();
		return result;
	}

	public long deleteRecordFromTables(String strTablename, String strDate) {
		SQLiteDatabase db = this.getWritableDatabase();

		String whereClause = KEY_DT_CREATED + " <  '" + strDate + "'";
		long result = db.delete(strTablename, whereClause, null);

		db.close();
		return result;
	}

	@SuppressWarnings("unchecked")
	private String getPKFieldName(String tableName) {
		String fieldName = "";
		SQLiteDatabase db = this.getWritableDatabase();
		ArrayList<String[]> result = new ArrayList<String[]>();
		Cursor cursor = db.rawQuery("PRAGMA table_info(" + tableName + ")",
				null);
		try {
			int nameIdx = cursor.getColumnIndexOrThrow("name");
			int typeIdx = cursor.getColumnIndexOrThrow("type");
			int notNullIdx = cursor.getColumnIndexOrThrow("notnull");
			int dfltValueIdx = cursor.getColumnIndexOrThrow("dflt_value");
			int keyIndex = cursor.getColumnIndexOrThrow("pk");

			// ArrayList<String> integerDefault1NotNull = new
			// ArrayList<String>();

			while (cursor.moveToNext()) {
				/*
				 * String[] temp = new String[2]; String type =
				 * cursor.getString(typeIdx);
				 */

				if (cursor.getInt(keyIndex) == 1) {
					fieldName = cursor.getString(nameIdx);
				}

				/*
				 * if (!cursor.getString(nameIdx).equalsIgnoreCase("id")) { if
				 * ("INTEGER".equals(type)) { // Integer column if
				 * (cursor.getInt(notNullIdx) == 1) { // NOT NULL String
				 * defaultValue = cursor .getString(dfltValueIdx); if
				 * ("1".equals(defaultValue)) {
				 * integerDefault1NotNull.add(cursor .getString(nameIdx)); } } }
				 * temp[0] = cursor.getString(nameIdx); temp[1] =
				 * cursor.getString(typeIdx);
				 * 
				 * result.add(temp); }
				 */
			}
			/*
			 * System.out
			 * .println("integerDefault1NotNull now contains a list of all columns "
			 * + " defined as INTEGER NOT NULL DEFAULT 1, " +
			 * integerDefault1NotNull);
			 */
		} finally {
			cursor.close();
		}

		return fieldName;
	}
}