package com.rms;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class AuthActivity extends Activity {
	
	private TextView authMessageTextView;
	private Button exitButton;
	private ImageView authImageView;
	
	Intent intent;
	String nameAndRole = "";
	String userId = "";
	String strAuthMessage="";
	String strAuthMessagePrv="";
	
	Context ctx;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_auth);
		
		ActionBar ab = getActionBar(); 
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
        ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);
	    
	    /**
		 * below code for hide the icon.
		 */
		ab.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
	    
	    authMessageTextView = (TextView) findViewById(R.id.authMessageTextView);
	    exitButton = (Button) findViewById(R.id.exitButton);
	    authImageView = (ImageView) findViewById(R.id.authImageView);
	    
	    ctx=this;
	
	    intent = getIntent();
	    userId = intent.getStringExtra("userId"); 
	    nameAndRole = intent.getStringExtra("nameAndRole");	    
	    strAuthMessage = intent.getStringExtra("strAuthMessage");
	    strAuthMessagePrv =  intent.getStringExtra("strAuthMessagePrv");
	    
	    if(nameAndRole != null && nameAndRole.startsWith("en")){
	    	nameAndRole = nameAndRole.substring(3, nameAndRole.length()-1);
	    }
	    
	    authMessageTextView.setText(strAuthMessagePrv + nameAndRole + strAuthMessage);
	    authMessageTextView.invalidate(); 
	    
	    
	    exitButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
				
			}
		});
	    
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.auth, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
	/*	int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}*/
		return super.onOptionsItemSelected(item);
	}
}
