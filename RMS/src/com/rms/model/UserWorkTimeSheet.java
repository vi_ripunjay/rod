package com.rms.model;

public class UserWorkTimeSheet {

	private String iUserWorkTimeSheetId;
	private String workStartDate;
	private String workStartTime;
	private String workEndDate;
	private String workEndTime;
	private String workStartDateTime;
	private String workEndDateTime;
	private String iUserId;
	private String iUserServiceTermId;
	private String strDescription;
	private String dtCreated;
	private String dtUpdated;
	private String flgStatus = "0"; // 0:tap in and 1 tap out
	private String flgDeleted = "0";
	private String flgIsDirty = "1";
	private String iShipId;
	private String iTenantId;
	private Integer flgInternationalOrderStart = 0;
	private Integer flgInternationalOrderEnd = 0;
	private Integer flgProcessed;

	private String createdBy;
	private String updatedBy;

	public UserWorkTimeSheet() {
		super();
	}

	public UserWorkTimeSheet(String iUserWorkTimeSheetId, String workStartDate,
			String workStartTime, String workEndDate, String workEndTime,
			String workStartDateTime, String workEndDateTime, String iUserId,
			String iUserServiceTermId, String strDescription, String dtCreated,
			String dtUpdated, String flgStatus, String flgDeleted,
			String flgIsDirty, String iShipId, String iTenantId,
			Integer flgInternationalOrderStart,
			Integer flgInternationalOrderEnd, Integer flgProcessed,
			String createdBy, String updatedBy) {
		super();
		this.iUserWorkTimeSheetId = iUserWorkTimeSheetId;
		this.workStartDate = workStartDate;
		this.workStartTime = workStartTime;
		this.workEndDate = workEndDate;
		this.workEndTime = workEndTime;
		this.workStartDateTime = workStartDateTime;
		this.workEndDateTime = workEndDateTime;
		this.iUserId = iUserId;
		this.iUserServiceTermId = iUserServiceTermId;
		this.strDescription = strDescription;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.flgInternationalOrderStart = flgInternationalOrderStart;
		this.flgInternationalOrderEnd = flgInternationalOrderEnd;
		this.flgProcessed = flgProcessed;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}



	public String getiUserWorkTimeSheetId() {
		return iUserWorkTimeSheetId;
	}

	public void setiUserWorkTimeSheetId(String iUserWorkTimeSheetId) {
		this.iUserWorkTimeSheetId = iUserWorkTimeSheetId;
	}

	public String getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(String workStartDate) {
		this.workStartDate = workStartDate;
	}

	public String getWorkStartTime() {
		return workStartTime;
	}

	public void setWorkStartTime(String workStartTime) {
		this.workStartTime = workStartTime;
	}

	public String getWorkEndDate() {
		return workEndDate;
	}

	public void setWorkEndDate(String workEndDate) {
		this.workEndDate = workEndDate;
	}

	public String getWorkEndTime() {
		return workEndTime;
	}

	public void setWorkEndTime(String workEndTime) {
		this.workEndTime = workEndTime;
	}

	public String getWorkStartDateTime() {
		return workStartDateTime;
	}

	public void setWorkStartDateTime(String workStartDateTime) {
		this.workStartDateTime = workStartDateTime;
	}

	public String getWorkEndDateTime() {
		return workEndDateTime;
	}

	public void setWorkEndDateTime(String workEndDateTime) {
		this.workEndDateTime = workEndDateTime;
	}

	public String getiUserId() {
		return iUserId;
	}

	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}

	public String getiUserServiceTermId() {
		return iUserServiceTermId;
	}

	public void setiUserServiceTermId(String iUserServiceTermId) {
		this.iUserServiceTermId = iUserServiceTermId;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Integer getFlgInternationalOrderStart() {
		return flgInternationalOrderStart;
	}

	public void setFlgInternationalOrderStart(Integer flgInternationalOrderStart) {
		this.flgInternationalOrderStart = flgInternationalOrderStart;
	}

	public Integer getFlgInternationalOrderEnd() {
		return flgInternationalOrderEnd;
	}

	public void setFlgInternationalOrderEnd(Integer flgInternationalOrderEnd) {
		this.flgInternationalOrderEnd = flgInternationalOrderEnd;
	}

	public Integer getFlgProcessed() {
		return flgProcessed;
	}

	public void setFlgProcessed(Integer flgProcessed) {
		this.flgProcessed = flgProcessed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iUserWorkTimeSheetId == null) ? 0 : iUserWorkTimeSheetId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserWorkTimeSheet other = (UserWorkTimeSheet) obj;
		if (iUserWorkTimeSheetId == null) {
			if (other.iUserWorkTimeSheetId != null)
				return false;
		} else if (!iUserWorkTimeSheetId.equals(other.iUserWorkTimeSheetId))
			return false;
		return true;
	}

}
