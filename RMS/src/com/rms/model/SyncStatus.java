package com.rms.model;

public class SyncStatus {

	private String iSyncStatusId;
	private String dtSyncDate;
	private String syncMode;
	private String dataSyncMode;//cable or web
	private String serverAddress;	
	private String strMacId;
	private String moduleName;
	private int iTenantId;
	private int iShipId;
	private int flgStatus = 0;
	private int flgDeleted = 0;
	private int flgIsDirty = 1;
	private int flgIsDeviceDirty = 1;
	private String syncTime;
	private String createdDate;
	private String modifiedDate;
	private String createdBy;
	private String modifiedBy;
	
	public SyncStatus() {
		super();
		
	}
	
	public SyncStatus(String iSyncStatusId, String dtSyncDate, String syncMode,
			String dataSyncMode, String serverAddress, String strMacId,
			String moduleName, int iTenantId, int iShipId, int flgStatus,
			int flgDeleted, int flgIsDirty, int flgIsDeviceDirty,
			String syncTime, String createdDate, String modifiedDate,
			String createdBy, String modifiedBy) {
		super();
		this.iSyncStatusId = iSyncStatusId;
		this.dtSyncDate = dtSyncDate;
		this.syncMode = syncMode;
		this.dataSyncMode = dataSyncMode;
		this.serverAddress = serverAddress;
		this.strMacId = strMacId;
		this.moduleName = moduleName;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
		this.flgStatus = flgStatus;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgIsDeviceDirty = flgIsDeviceDirty;
		this.syncTime = syncTime;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.createdBy = createdBy;
		this.modifiedBy = modifiedBy;
	}


	public String getiSyncStatusId() {
		return iSyncStatusId;
	}

	public void setiSyncStatusId(String iSyncStatusId) {
		this.iSyncStatusId = iSyncStatusId;
	}

	public String getDtSyncDate() {
		return dtSyncDate;
	}

	public void setDtSyncDate(String dtSyncDate) {
		this.dtSyncDate = dtSyncDate;
	}

	public String getSyncMode() {
		return syncMode;
	}

	public void setSyncMode(String syncMode) {
		this.syncMode = syncMode;
	}

	public String getServerAddress() {
		return serverAddress;
	}

	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}
	
	public String getDataSyncMode() {
		return dataSyncMode;
	}

	public void setDataSyncMode(String dataSyncMode) {
		this.dataSyncMode = dataSyncMode;
	}

	
	public int getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(int flgStatus) {
		this.flgStatus = flgStatus;
	}
	
	
	public String getStrMacId() {
		return strMacId;
	}


	public void setStrMacId(String strMacId) {
		this.strMacId = strMacId;
	}

	
	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public int getFlgIsDeviceDirty() {
		return flgIsDeviceDirty;
	}


	public void setFlgIsDeviceDirty(int flgIsDeviceDirty) {
		this.flgIsDeviceDirty = flgIsDeviceDirty;
	}


	public String getSyncTime() {
		return syncTime;
	}


	public void setSyncTime(String syncTime) {
		this.syncTime = syncTime;
	}


	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public int getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	
	
	public int getiShipId() {
		return iShipId;
	}

	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}

	public int getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iSyncStatusId == null) ? 0 : iSyncStatusId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SyncStatus other = (SyncStatus) obj;
		if (iSyncStatusId == null) {
			if (other.iSyncStatusId != null)
				return false;
		} else if (!iSyncStatusId.equals(other.iSyncStatusId))
			return false;
		return true;
	}
}
