package com.rms.model;

public class CrewDepartment {
	
	String icrewDepartmentId;
	String strDepartmentName;
	String dtCreated;
	String dtUpdated;
	String flgIsDirty;
	String flgDeleted;
	String iTenantId;
	
	public CrewDepartment() {
		super();
	}

	public CrewDepartment(String icrewDepartmentId, String strDepartmentName,
			String dtCreated, String dtUpdated, String flgIsDirty,
			String flgDeleted, String iTenantId) {
		super();
		this.icrewDepartmentId = icrewDepartmentId;
		this.strDepartmentName = strDepartmentName;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgIsDirty = flgIsDirty;
		this.flgDeleted = flgDeleted;
		this.iTenantId = iTenantId;
	}

	public String getIcrewDepartmentId() {
		return icrewDepartmentId;
	}

	public void setIcrewDepartmentId(String icrewDepartmentId) {
		this.icrewDepartmentId = icrewDepartmentId;
	}

	public String getStrDepartmentName() {
		return strDepartmentName;
	}

	public void setStrDepartmentName(String strDepartmentName) {
		this.strDepartmentName = strDepartmentName;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((icrewDepartmentId == null) ? 0 : icrewDepartmentId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CrewDepartment))
			return false;
		CrewDepartment other = (CrewDepartment) obj;
		if (icrewDepartmentId == null) {
			if (other.icrewDepartmentId != null)
				return false;
		} else if (!icrewDepartmentId.equals(other.icrewDepartmentId))
			return false;
		return true;
	}

}
