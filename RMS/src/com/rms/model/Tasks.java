package com.rms.model;

public class Tasks {
	
	String itaskId;
	String strTaskName;
	String dtCreated;
	String dtUpdated;
	String flgIsDirty;
	String flgDeleted;
	String strDescription;
	String iTenantId;
	
	public Tasks() {
		super();
	}

	public Tasks(String itaskId, String strTaskName, String dtCreated,
			String dtUpdated, String flgIsDirty, String flgDeleted,
			String strDescription, String iTenantId) {
		super();
		this.itaskId = itaskId;
		this.strTaskName = strTaskName;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgIsDirty = flgIsDirty;
		this.flgDeleted = flgDeleted;
		this.strDescription = strDescription;
		this.iTenantId = iTenantId;
	}

	public String getItaskId() {
		return itaskId;
	}

	public void setItaskId(String itaskId) {
		this.itaskId = itaskId;
	}

	public String getStrTaskName() {
		return strTaskName;
	}

	public void setStrTaskName(String strTaskName) {
		this.strTaskName = strTaskName;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itaskId == null) ? 0 : itaskId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Tasks))
			return false;
		Tasks other = (Tasks) obj;
		if (itaskId == null) {
			if (other.itaskId != null)
				return false;
		} else if (!itaskId.equals(other.itaskId))
			return false;
		return true;
	}

}
