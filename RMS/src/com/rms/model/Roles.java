package com.rms.model;

public class Roles {

	String iRoleId;
	String strRole;
	String dtCreated;
	String dtUpdated;
	String flgIsDirty;
	String strType;
	String flgDeleted;
	String txtDescription;
	String iRankPriority;
	String iTenantId;
	String strRoleType;

	public String getStrRoleType() {
		return strRoleType;
	}

	public void setStrRoleType(String strRoleType) {
		this.strRoleType = strRoleType;
	}

	public Roles(String iRoleId, String strRole, String dtCreated,
			String dtUpdated, String flgIsDirty, String strType,
			String flgDeleted, String txtDescription, String iRankPriority,
			String iTenantId, String strRoleType) {
		this.iRoleId = iRoleId;
		this.strRole = strRole;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgIsDirty = flgIsDirty;
		this.strType = strType;
		this.flgDeleted = flgDeleted;
		this.txtDescription = txtDescription;
		this.iRankPriority = iRankPriority;
		this.iTenantId = iTenantId;
		this.strRoleType = strRoleType;

	}

	public Roles() {

	}

	public String getiRoleId() {
		return iRoleId;
	}

	public void setiRoleId(String iRoleId) {
		this.iRoleId = iRoleId;
	}

	public String getStrRole() {
		return strRole;
	}

	public void setStrRole(String strRole) {
		this.strRole = strRole;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getStrType() {
		return strType;
	}

	public void setStrType(String strType) {
		this.strType = strType;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	public String getiRankPriority() {
		return iRankPriority;
	}

	public void setiRankPriority(String iRankPriority) {
		this.iRankPriority = iRankPriority;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iRoleId == null) ? 0 : iRoleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Roles other = (Roles) obj;
		if (iRoleId == null) {
			if (other.iRoleId != null)
				return false;
		} else if (!iRoleId.equals(other.iRoleId))
			return false;
		return true;
	}

}
