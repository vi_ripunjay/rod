package com.rms.model;


public class InternationalDates {

	private String iinternationalDateId;
	private String iShipId;
	private String iTenantId;
	private String internationalDate;
	private int flgRepeated;
	private Integer flgIsDirty = 1;
	private Integer flgDeleted = 0;
	private String txtDescription;
	private String dtCreated;
	private String dtUpdated;

	public InternationalDates() {
		super();
	}

	public InternationalDates(String iinternationalDateId, String iShipId,
			String iTenantId, String internationalDate, int flgRepeated,
			Integer flgIsDirty, Integer flgDeleted, String txtDescription,
			String dtCreated, String dtUpdated) {
		super();
		this.iinternationalDateId = iinternationalDateId;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.internationalDate = internationalDate;
		this.flgRepeated = flgRepeated;
		this.flgIsDirty = flgIsDirty;
		this.flgDeleted = flgDeleted;
		this.txtDescription = txtDescription;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
	}

	public String getIinternationalDateId() {
		return iinternationalDateId;
	}

	public void setIinternationalDateId(String iinternationalDateId) {
		this.iinternationalDateId = iinternationalDateId;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getInternationalDate() {
		return internationalDate;
	}

	public void setInternationalDate(String internationalDate) {
		this.internationalDate = internationalDate;
	}

	public int getFlgRepeated() {
		return flgRepeated;
	}

	public void setFlgRepeated(int flgRepeated) {
		this.flgRepeated = flgRepeated;
	}

	public Integer getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(Integer flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public Integer getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(Integer flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iinternationalDateId == null) ? 0 : iinternationalDateId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		InternationalDates other = (InternationalDates) obj;
		if (iinternationalDateId == null) {
			if (other.iinternationalDateId != null)
				return false;
		} else if (!iinternationalDateId.equals(other.iinternationalDateId))
			return false;
		return true;
	}

}
