package com.rms.model;

public class UserServiceTermRole {

	String strUserServiceTermRoleId;
	String dtCreated;
	String dtUpdated;
	String flgDeleted;
	String flgIsDirty;
	String iRoleId;
	String iShipId;
	String iTenantId;
	String iuserId;
	String iuserServiceTermId;
	String dtTermFrom;
	String dtTermTo;
	String ishipHolidayListId;
	int powerPlusScore;
	int field1;
	int field2;

	public UserServiceTermRole() {

	}

	public UserServiceTermRole(String strUserServiceTermRoleId,
			String dtCreated, String dtUpdated, String flgDeleted,
			String flgIsDirty, String iRoleId, String iShipId,
			String iTenantId, String iuserId, String iuserServiceTermId,
			String dtTermFrom, String dtTermTo,String ishipHolidayListId, int powerPlusScore, int field1,
			int field2) {
		super();
		this.strUserServiceTermRoleId = strUserServiceTermRoleId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.iRoleId = iRoleId;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.iuserId = iuserId;
		this.iuserServiceTermId = iuserServiceTermId;
		this.dtTermFrom = dtTermFrom;
		this.dtTermTo = dtTermTo;
		this.powerPlusScore = powerPlusScore;
		this.field1 = field1;
		this.field2 = field2;
		this.ishipHolidayListId = ishipHolidayListId;
	}

	public String getStrUserServiceTermRoleId() {
		return strUserServiceTermRoleId;
	}

	public void setStrUserServiceTermRoleId(String strUserServiceTermRoleId) {
		this.strUserServiceTermRoleId = strUserServiceTermRoleId;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getiRoleId() {
		return iRoleId;
	}

	public void setiRoleId(String iRoleId) {
		this.iRoleId = iRoleId;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getIuserId() {
		return iuserId;
	}

	public void setIuserId(String iuserId) {
		this.iuserId = iuserId;
	}

	public String getIuserServiceTermId() {
		return iuserServiceTermId;
	}

	public void setIuserServiceTermId(String iuserServiceTermId) {
		this.iuserServiceTermId = iuserServiceTermId;
	}

	public String getDtTermFrom() {
		return dtTermFrom;
	}

	public void setDtTermFrom(String dtTermFrom) {
		this.dtTermFrom = dtTermFrom;
	}

	public String getDtTermTo() {
		return dtTermTo;
	}

	public void setDtTermTo(String dtTermTo) {
		this.dtTermTo = dtTermTo;
	}

	public String getIshipHolidayListId() {
		return ishipHolidayListId;
	}

	public void setIshipHolidayListId(String ishipHolidayListId) {
		this.ishipHolidayListId = ishipHolidayListId;
	}

	public int getPowerPlusScore() {
		return powerPlusScore;
	}

	public void setPowerPlusScore(int powerPlusScore) {
		this.powerPlusScore = powerPlusScore;
	}

	public int getField1() {
		return field1;
	}

	public void setField1(int field1) {
		this.field1 = field1;
	}

	public int getField2() {
		return field2;
	}

	public void setField2(int field2) {
		this.field2 = field2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((strUserServiceTermRoleId == null) ? 0
						: strUserServiceTermRoleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserServiceTermRole other = (UserServiceTermRole) obj;
		if (strUserServiceTermRoleId == null) {
			if (other.strUserServiceTermRoleId != null)
				return false;
		} else if (!strUserServiceTermRoleId
				.equals(other.strUserServiceTermRoleId))
			return false;
		return true;
	}

}