package com.rms.model;

/**
 * This table have record for ideal repeate start or not
 * @author ripunjay.s
 *
 */
public class IdlLogWork {
	
	private String iIdlLogWorkId;
	private String iinternationalDateId;
	private String iShipId;
	private String iTenantId;
	private String internationalDate;
	private int flgRepeated=0;
	private int flgDayStart=0;
	private Integer flgIsDirty = 1;
	private Integer flgDeleted = 0;
	private String txtDescription;
	private String dtCreated;
	private String dtUpdated;
	
	public IdlLogWork() {
		super();
	}

	public String getiIdlLogWorkId() {
		return iIdlLogWorkId;
	}

	public void setiIdlLogWorkId(String iIdlLogWorkId) {
		this.iIdlLogWorkId = iIdlLogWorkId;
	}

	public String getIinternationalDateId() {
		return iinternationalDateId;
	}

	public void setIinternationalDateId(String iinternationalDateId) {
		this.iinternationalDateId = iinternationalDateId;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getInternationalDate() {
		return internationalDate;
	}

	public void setInternationalDate(String internationalDate) {
		this.internationalDate = internationalDate;
	}

	public int getFlgRepeated() {
		return flgRepeated;
	}

	public void setFlgRepeated(int flgRepeated) {
		this.flgRepeated = flgRepeated;
	}

	public int getFlgDayStart() {
		return flgDayStart;
	}

	public void setFlgDayStart(int flgDayStart) {
		this.flgDayStart = flgDayStart;
	}

	public Integer getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(Integer flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public Integer getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(Integer flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getTxtDescription() {
		return txtDescription;
	}

	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iIdlLogWorkId == null) ? 0 : iIdlLogWorkId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IdlLogWork other = (IdlLogWork) obj;
		if (iIdlLogWorkId == null) {
			if (other.iIdlLogWorkId != null)
				return false;
		} else if (!iIdlLogWorkId.equals(other.iIdlLogWorkId))
			return false;
		return true;
	}	

}
