package com.rms.model;

public class ValidateUser {

	String iUserId;
	String strFirstName;
	String strLastName;
	String iRoleId;
	String dtTermFrom;
	String dtTermTo;
	String iShipId;
	String iTenantId;

	public ValidateUser() {
		super();
	}

	public ValidateUser(String iUserId, String strFirstName,
			String strLastName, String iRoleId, String dtTermFrom,
			String dtTermTo, String iShipId, String iTenantId) {
		super();
		this.iUserId = iUserId;
		this.strFirstName = strFirstName;
		this.strLastName = strLastName;
		this.iRoleId = iRoleId;
		this.dtTermFrom = dtTermFrom;
		this.dtTermTo = dtTermTo;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
	}

	public String getiUserId() {
		return iUserId;
	}

	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}

	public String getStrFirstName() {
		return strFirstName;
	}

	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}

	public String getStrLastName() {
		return strLastName;
	}

	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}

	public String getiRoleId() {
		return iRoleId;
	}

	public void setiRoleId(String iRoleId) {
		this.iRoleId = iRoleId;
	}

	public String getDtTermFrom() {
		return dtTermFrom;
	}

	public void setDtTermFrom(String dtTermFrom) {
		this.dtTermFrom = dtTermFrom;
	}

	public String getDtTermTo() {
		return dtTermTo;
	}

	public void setDtTermTo(String dtTermTo) {
		this.dtTermTo = dtTermTo;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iUserId == null) ? 0 : iUserId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValidateUser other = (ValidateUser) obj;
		if (iUserId == null) {
			if (other.iUserId != null)
				return false;
		} else if (!iUserId.equals(other.iUserId))
			return false;
		return true;
	}

}
