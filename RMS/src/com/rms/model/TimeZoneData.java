package com.rms.model;

public class TimeZoneData {
	String iTimeZoneDataId;
	String strZoneId;
	String strTimeZoneName;
	String strTime;
	String strDescription;
	String dtCreated;
	String dtUpdated;
	String updatedBy;
	String createdBy;
	int flgIsDirty;
	int flgDeleted;
	int iShipId;
	int iTenantId;
	
	public TimeZoneData() {
		super();	
	}
		
	
	public TimeZoneData(String iTimeZoneDataId, String strZoneId,
			String strTimeZoneName, String strTime, String strDescription,
			String dtCreated, String dtUpdated, String updatedBy,
			String createdBy, int flgIsDirty, int flgDeleted, int iShipId,
			int iTenantId) {
		super();
		this.iTimeZoneDataId = iTimeZoneDataId;
		this.strZoneId = strZoneId;
		this.strTimeZoneName = strTimeZoneName;
		this.strTime = strTime;
		this.strDescription = strDescription;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.updatedBy = updatedBy;
		this.createdBy = createdBy;
		this.flgIsDirty = flgIsDirty;
		this.flgDeleted = flgDeleted;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
	}




	public String getiTimeZoneDataId() {
		return iTimeZoneDataId;
	}


	public void setiTimeZoneDataId(String iTimeZoneDataId) {
		this.iTimeZoneDataId = iTimeZoneDataId;
	}


	public String getStrZoneId() {
		return strZoneId;
	}

	public void setStrZoneId(String strZoneId) {
		this.strZoneId = strZoneId;
	}

	public String getStrTimeZoneName() {
		return strTimeZoneName;
	}

	public void setStrTimeZoneName(String strTimeZoneName) {
		this.strTimeZoneName = strTimeZoneName;
	}

	public String getStrTime() {
		return strTime;
	}

	public void setStrTime(String strTime) {
		this.strTime = strTime;
	}
	
	public String getStrDescription() {
		return strDescription;
	}


	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}


	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	
	
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	
	public int getFlgIsDirty() {
		return flgIsDirty;
	}


	public void setFlgIsDirty(int flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}


	public int getFlgDeleted() {
		return flgDeleted;
	}


	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}


	public int getiShipId() {
		return iShipId;
	}


	public void setiShipId(int iShipId) {
		this.iShipId = iShipId;
	}


	public int getiTenantId() {
		return iTenantId;
	}


	public void setiTenantId(int iTenantId) {
		this.iTenantId = iTenantId;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((iTimeZoneDataId == null) ? 0 : iTimeZoneDataId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimeZoneData other = (TimeZoneData) obj;
		if (iTimeZoneDataId == null) {
			if (other.iTimeZoneDataId != null)
				return false;
		} else if (!iTimeZoneDataId.equals(other.iTimeZoneDataId))
			return false;
		return true;
	}
	
	
	
}
