package com.rms.model;

public class TenantData {

	int id;
	String iTenantId;
	String strCompanyName;
	
	public TenantData()
	{
		
	}
	
	public TenantData(int id, String iTenantId , String strCompanyName)
	{
		this.id = id;
		this.iTenantId  = iTenantId;
		this.strCompanyName = strCompanyName;
			
	}
	
	public TenantData(String iTenantId , String strCompanyName)
	{
		this.iTenantId  = iTenantId;
		this.strCompanyName = strCompanyName;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}
	public String getStrCompanyName() {
		return strCompanyName;
	}
	public void setStrCompanyName(String strCompanyName) {
		this.strCompanyName = strCompanyName;
	}
	
}
