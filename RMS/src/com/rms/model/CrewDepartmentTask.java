package com.rms.model;

public class CrewDepartmentTask {
	
	String icrewDepartmentTaskId;
	String icrewDepartmentId;
	String itaskId;
	String dtCreated;
	String dtUpdated;
	String flgIsDirty;
	String flgDeleted;
	String iTenantId;
	
	public CrewDepartmentTask() {
		super();
	}

	public CrewDepartmentTask(String icrewDepartmentTaskId,
			String icrewDepartmentId, String itaskId, String dtCreated,
			String dtUpdated, String flgIsDirty, String flgDeleted,
			String iTenantId) {
		super();
		this.icrewDepartmentTaskId = icrewDepartmentTaskId;
		this.icrewDepartmentId = icrewDepartmentId;
		this.itaskId = itaskId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgIsDirty = flgIsDirty;
		this.flgDeleted = flgDeleted;
		this.iTenantId = iTenantId;
	}

	public String getIcrewDepartmentTaskId() {
		return icrewDepartmentTaskId;
	}

	public void setIcrewDepartmentTaskId(String icrewDepartmentTaskId) {
		this.icrewDepartmentTaskId = icrewDepartmentTaskId;
	}

	public String getIcrewDepartmentId() {
		return icrewDepartmentId;
	}

	public void setIcrewDepartmentId(String icrewDepartmentId) {
		this.icrewDepartmentId = icrewDepartmentId;
	}

	public String getItaskId() {
		return itaskId;
	}

	public void setItaskId(String itaskId) {
		this.itaskId = itaskId;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((icrewDepartmentTaskId == null) ? 0 : icrewDepartmentTaskId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof CrewDepartmentTask))
			return false;
		CrewDepartmentTask other = (CrewDepartmentTask) obj;
		if (icrewDepartmentTaskId == null) {
			if (other.icrewDepartmentTaskId != null)
				return false;
		} else if (!icrewDepartmentTaskId.equals(other.icrewDepartmentTaskId))
			return false;
		return true;
	}

}
