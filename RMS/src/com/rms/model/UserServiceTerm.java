package com.rms.model;

public class UserServiceTerm {

	String iUserServiceTermId;
	String iUserId;
	String dtTermFrom;
	String dtTermTo;
	String flgDeleted;
	String flgIsDirty;
	String dtCreated;
	String dtUpdated;
	String iShipId;
	String iTenantId;

	public UserServiceTerm() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserServiceTerm(String iUserServiceTermId, String iUserId,
			String dtTermFrom, String dtTermTo, String flgDeleted,
			String flgIsDirty, String dtCreated, String dtUpdated,
			String iShipId, String iTenantId) {
		super();
		this.iUserServiceTermId = iUserServiceTermId;
		this.iUserId = iUserId;
		this.dtTermFrom = dtTermFrom;
		this.dtTermTo = dtTermTo;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
	}

	public String getiUserServiceTermId() {
		return iUserServiceTermId;
	}

	public void setiUserServiceTermId(String iUserServiceTermId) {
		this.iUserServiceTermId = iUserServiceTermId;
	}

	public String getiUserId() {
		return iUserId;
	}

	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}

	public String getDtTermFrom() {
		return dtTermFrom;
	}

	public void setDtTermFrom(String dtTermFrom) {
		this.dtTermFrom = dtTermFrom;
	}

	public String getDtTermTo() {
		return dtTermTo;
	}

	public void setDtTermTo(String dtTermTo) {
		this.dtTermTo = dtTermTo;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iUserServiceTermId == null) ? 0 : iUserServiceTermId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserServiceTerm other = (UserServiceTerm) obj;
		if (iUserServiceTermId == null) {
			if (other.iUserServiceTermId != null)
				return false;
		} else if (!iUserServiceTermId.equals(other.iUserServiceTermId))
			return false;
		return true;
	}

}
