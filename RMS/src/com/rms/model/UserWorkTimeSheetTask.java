package com.rms.model;

public class UserWorkTimeSheetTask {
	
	private String iUserWorkTimeSheetTaskId;
	private String iUserWorkTimeSheetId;
	private String workStartDate;
	private String workStartTime;
	private String workEndDate;
	private String workEndTime;
	private String workStartDateTime;
	private String workEndDateTime;
	private String iUserId;
	private String iUserServiceTermId;
	private String itaskId;
	private String icrewDepartmentTaskId;
	private String strDescription; 
	private String dtCreated;
	private String dtUpdated;
	private String flgStatus="0"; 
	private String flgDeleted="0";
	private String flgIsDirty="1";
	private String iShipId;
	private String iTenantId;
	
	private String createdBy;
	private String updatedBy;
	
	public UserWorkTimeSheetTask() {
		super();
	}

	public UserWorkTimeSheetTask(String iUserWorkTimeSheetTaskId,
			String iUserWorkTimeSheetId, String workStartDate,
			String workStartTime, String workEndDate, String workEndTime,
			String workStartDateTime, String workEndDateTime, String iUserId,
			String iUserServiceTermId, String itaskId,
			String icrewDepartmentTaskId, String strDescription,
			String flgDeleted, String dtCreated, String dtUpdated,
			String flgStatus, String flgIsDirty, String iShipId,
			String iTenantId, String createdBy, String updatedBy) {
		super();
		this.iUserWorkTimeSheetTaskId = iUserWorkTimeSheetTaskId;
		this.iUserWorkTimeSheetId = iUserWorkTimeSheetId;
		this.workStartDate = workStartDate;
		this.workStartTime = workStartTime;
		this.workEndDate = workEndDate;
		this.workEndTime = workEndTime;
		this.workStartDateTime = workStartDateTime;
		this.workEndDateTime = workEndDateTime;
		this.iUserId = iUserId;
		this.iUserServiceTermId = iUserServiceTermId;
		this.itaskId = itaskId;
		this.icrewDepartmentTaskId = icrewDepartmentTaskId;
		this.strDescription = strDescription;
		this.flgDeleted = flgDeleted;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgIsDirty = flgIsDirty;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public String getiUserWorkTimeSheetTaskId() {
		return iUserWorkTimeSheetTaskId;
	}

	public void setiUserWorkTimeSheetTaskId(String iUserWorkTimeSheetTaskId) {
		this.iUserWorkTimeSheetTaskId = iUserWorkTimeSheetTaskId;
	}

	public String getiUserWorkTimeSheetId() {
		return iUserWorkTimeSheetId;
	}

	public void setiUserWorkTimeSheetId(String iUserWorkTimeSheetId) {
		this.iUserWorkTimeSheetId = iUserWorkTimeSheetId;
	}

	public String getWorkStartDate() {
		return workStartDate;
	}

	public void setWorkStartDate(String workStartDate) {
		this.workStartDate = workStartDate;
	}

	public String getWorkStartTime() {
		return workStartTime;
	}

	public void setWorkStartTime(String workStartTime) {
		this.workStartTime = workStartTime;
	}

	public String getWorkEndDate() {
		return workEndDate;
	}

	public void setWorkEndDate(String workEndDate) {
		this.workEndDate = workEndDate;
	}

	public String getWorkEndTime() {
		return workEndTime;
	}

	public void setWorkEndTime(String workEndTime) {
		this.workEndTime = workEndTime;
	}

	public String getWorkStartDateTime() {
		return workStartDateTime;
	}

	public void setWorkStartDateTime(String workStartDateTime) {
		this.workStartDateTime = workStartDateTime;
	}

	public String getWorkEndDateTime() {
		return workEndDateTime;
	}

	public void setWorkEndDateTime(String workEndDateTime) {
		this.workEndDateTime = workEndDateTime;
	}

	public String getiUserId() {
		return iUserId;
	}

	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}

	public String getiUserServiceTermId() {
		return iUserServiceTermId;
	}

	public void setiUserServiceTermId(String iUserServiceTermId) {
		this.iUserServiceTermId = iUserServiceTermId;
	}

	public String getItaskId() {
		return itaskId;
	}

	public void setItaskId(String itaskId) {
		this.itaskId = itaskId;
	}

	public String getIcrewDepartmentTaskId() {
		return icrewDepartmentTaskId;
	}

	public void setIcrewDepartmentTaskId(String icrewDepartmentTaskId) {
		this.icrewDepartmentTaskId = icrewDepartmentTaskId;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iUserWorkTimeSheetTaskId == null) ? 0
						: iUserWorkTimeSheetTaskId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserWorkTimeSheetTask other = (UserWorkTimeSheetTask) obj;
		if (iUserWorkTimeSheetTaskId == null) {
			if (other.iUserWorkTimeSheetTaskId != null)
				return false;
		} else if (!iUserWorkTimeSheetTaskId
				.equals(other.iUserWorkTimeSheetTaskId))
			return false;
		return true;
	}
	
}
