package com.rms.model;

public class ModelData {

int id;
String modeId;
String modelName;

public ModelData(int id, String modeId, String modelName) {
	this.id = id;
	this.modeId = modeId;
	this.modelName = modelName;
}

public ModelData(String modeId, String modelName) {
	this.modeId = modeId;
	this.modelName = modelName;
}

public ModelData(){}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getModeId() {
	return modeId;
}
public void setModeId(String modeId) {
	this.modeId = modeId;
}
public String getModelName() {
	return modelName;
}
public void setModelName(String modelName) {
	this.modelName = modelName;
}


}
