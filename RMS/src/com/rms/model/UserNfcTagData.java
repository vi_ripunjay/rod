package com.rms.model;

public class UserNfcTagData {

	String iUserNfcTagDataId;
	String cardType;
	String iUserId;
	String strNfcTagId;
	String flgIsDirty;
	String flgDeleted;
	String dtCreated;
	String dtUpdated;
	String iTenantId;
	String iShipId;

	public UserNfcTagData() {

	}

	public UserNfcTagData(String iUserNfcTagDataId, String cardType,
			String iUserId, String strNfcTagId, String flgIsDirty,
			String flgDeleted, String dtCreated, String dtUpdated,
			String iTenantId, String iShipId) {
		super();
		this.iUserNfcTagDataId = iUserNfcTagDataId;
		this.cardType = cardType;
		this.iUserId = iUserId;
		this.strNfcTagId = strNfcTagId;
		this.flgIsDirty = flgIsDirty;
		this.flgDeleted = flgDeleted;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}

	public String getiUserNfcTagDataId() {
		return iUserNfcTagDataId;
	}

	public void setiUserNfcTagDataId(String iUserNfcTagDataId) {
		this.iUserNfcTagDataId = iUserNfcTagDataId;
	}

	public String getStrNfcTagId() {
		return strNfcTagId;
	}

	public void setStrNfcTagId(String strNfcTagId) {
		this.strNfcTagId = strNfcTagId;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getiUserId() {
		return iUserId;
	}

	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iUserNfcTagDataId == null) ? 0 : iUserNfcTagDataId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserNfcTagData other = (UserNfcTagData) obj;
		if (iUserNfcTagDataId == null) {
			if (other.iUserNfcTagDataId != null)
				return false;
		} else if (!iUserNfcTagDataId.equals(other.iUserNfcTagDataId))
			return false;
		return true;
	}
}
