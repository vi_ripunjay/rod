package com.rms.model;

public class RoleCrewDepartment {
	
	String iroleCrewDepartmentId;
	String icrewDepartmentId;
	String iroleId;
	String dtCreated;
	String dtUpdated;
	String flgIsDirty;
	String flgDeleted;
	String iTenantId;
	
	public RoleCrewDepartment() {
		super();
	}

	public RoleCrewDepartment(String iroleCrewDepartmentId,
			String icrewDepartmentId, String iroleId, String dtCreated,
			String dtUpdated, String flgIsDirty, String flgDeleted,
			String iTenantId) {
		super();
		this.iroleCrewDepartmentId = iroleCrewDepartmentId;
		this.icrewDepartmentId = icrewDepartmentId;
		this.iroleId = iroleId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgIsDirty = flgIsDirty;
		this.flgDeleted = flgDeleted;
		this.iTenantId = iTenantId;
	}

	public String getIroleCrewDepartmentId() {
		return iroleCrewDepartmentId;
	}

	public void setIroleCrewDepartmentId(String iroleCrewDepartmentId) {
		this.iroleCrewDepartmentId = iroleCrewDepartmentId;
	}

	public String getIcrewDepartmentId() {
		return icrewDepartmentId;
	}

	public void setIcrewDepartmentId(String icrewDepartmentId) {
		this.icrewDepartmentId = icrewDepartmentId;
	}

	public String getIroleId() {
		return iroleId;
	}

	public void setIroleId(String iroleId) {
		this.iroleId = iroleId;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iroleCrewDepartmentId == null) ? 0 : iroleCrewDepartmentId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof RoleCrewDepartment))
			return false;
		RoleCrewDepartment other = (RoleCrewDepartment) obj;
		if (iroleCrewDepartmentId == null) {
			if (other.iroleCrewDepartmentId != null)
				return false;
		} else if (!iroleCrewDepartmentId.equals(other.iroleCrewDepartmentId))
			return false;
		return true;
	}

}
