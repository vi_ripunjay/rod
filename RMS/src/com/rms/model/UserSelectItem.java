package com.rms.model;

public class UserSelectItem {

    private String text;
    private String userId;
    private String iTenantId;
    
    public String getiTenantId() {
		return iTenantId;
	}
	public UserSelectItem(String text, String item,String iTenantId) {
            this.text = text;
            this.userId = item;
            this.iTenantId = iTenantId;
            
    }
    public String getText() {
        return text;
    }
    public String getUserId() {
        return userId;
    }
    
    
    @Override
    public String toString() {
        return getText();
    }

}
