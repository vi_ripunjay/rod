package com.rms.model;

public class TaskDataItem {

	private String iTaskId;
	private String strTaskName;
	private String iCrewDepartmentTaskId;
	private String iTenantId;

	public TaskDataItem() {
		super();
	}

	public TaskDataItem(String iTaskId, String strTaskName,
			String iCrewDepartmentTaskId, String iTenantId) {
		super();
		this.iTaskId = iTaskId;
		this.strTaskName = strTaskName;
		this.iCrewDepartmentTaskId = iCrewDepartmentTaskId;
		this.iTenantId = iTenantId;
	}

	public String getiTaskId() {
		return iTaskId;
	}

	public void setiTaskId(String iTaskId) {
		this.iTaskId = iTaskId;
	}

	public String getStrTaskName() {
		return strTaskName;
	}

	public void setStrTaskName(String strTaskName) {
		this.strTaskName = strTaskName;
	}

	public String getiCrewDepartmentTaskId() {
		return iCrewDepartmentTaskId;
	}

	public void setiCrewDepartmentTaskId(String iCrewDepartmentTaskId) {
		this.iCrewDepartmentTaskId = iCrewDepartmentTaskId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((iTaskId == null) ? 0 : iTaskId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TaskDataItem other = (TaskDataItem) obj;
		if (iTaskId == null) {
			if (other.iTaskId != null)
				return false;
		} else if (!iTaskId.equals(other.iTaskId))
			return false;
		return true;
	}

}
