package com.rms.model;

public class UserScore {
	
 	 int _id ;
	 String u_id;  
	 String score_id;  
	 String score_type;  
	 String score_status;  
	 
	 public String getScore_status() {
		return score_status;
	}

	public void setScore_status(String score_status) {
		this.score_status = score_status;
	}
	String u_value;
	 
	 
	 public UserScore(int id,String _u_id, String _score_id, String _score_type, String _score_status, String u_value){  
	        this._id = id;
	     	this.u_id = _u_id;
	        this.score_id = _score_id;  
	        this.score_type = _score_type;
	        this.score_status = _score_status;

	        this.u_value = u_value;
	        
	    }  
	   
	    public UserScore(String _u_id, String _score_id, String _score_type, String _score_status, String u_value){
	     	this.u_id = _u_id;
	        this.score_id = _score_id;  
	        this.score_type = _score_type;
	        this.score_status = _score_status;
	        this.u_value = u_value;
	    }  
	 
	public UserScore() {
			// TODO Auto-generated constructor stub
		}

	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}
	public String getU_id() {
		return u_id;
	}
	public void setU_id(String u_id) {
		this.u_id = u_id;
	}
	public String getScore_id() {
		return score_id;
	}
	public void setScore_id(String score_id) {
		this.score_id = score_id;
	}
	public String getScore_type() {
		return score_type;
	}
	public void setScore_type(String score_type) {
		this.score_type = score_type;
	}
	public String getU_value() {
		return u_value;
	}
	public void setU_value(String u_value) {
		this.u_value = u_value;
	}  
		   

}
