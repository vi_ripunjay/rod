package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.UserWorkTimeSheetTask;

public class UserWorkTimeSheetTaskParser extends DefaultHandler {

	private static final String TAG = UserWorkTimeSheetTaskParser.class
			.getName().toString();

	private List<UserWorkTimeSheetTask> userWorkTimeSheetTaskList;
	private boolean isSuccess;
	private UserWorkTimeSheetTask userWorkTimeSheetTask;
	private StringBuffer buffer;
	private boolean isUserWorkTimeSheetTaskSucess = false;
	private boolean debug = true;
	private int fullData = 0;
	int errorCode = 0;
	
	int hasMore = 0;

	/**
	 * get the value of UserWorkTimeSheetTask data
	 * 
	 * @return list with content objects
	 */
	public List<UserWorkTimeSheetTask> getUserWorkTimeSheetTaskData() {
		return userWorkTimeSheetTaskList;
	}

	public int getResultCode() {
		return errorCode;
	}
	
	public int getHasMore(){
		return hasMore;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		userWorkTimeSheetTaskList = new ArrayList<UserWorkTimeSheetTask>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		} else if (localName.equals("hasMore")) {				
			
		}
		if (isSuccess) {
			if (localName.equals("UserWorkTimeSheetTask")) {
				isUserWorkTimeSheetTaskSucess = true;
				userWorkTimeSheetTask = new UserWorkTimeSheetTask();
				Log.i("tag list true", "" + isUserWorkTimeSheetTaskSucess
						+ isSuccess);

			} else if (localName.equals("iUserWorkTimeSheetTaskId")) {

			} else if (localName.equals("iUserWorkTimeSheetId")) {

			} else if (localName.equals("workStartDate")) {

			} else if (localName.equals("workStartTime")) {

			} else if (localName.equals("workEndDate")) {

			} else if (localName.equals("workEndTime")) {

			} else if (localName.equals("workStartDateTime")) {

			} else if (localName.equals("workEndDateTime")) {

			} else if (localName.equals("iUserId")) {

			} else if (localName.equals("iUserServiceTermId")) {

			} else if (localName.equals("itaskId")) {

			} else if (localName.equals("icrewDepartmentTaskId")) {

			} else if (localName.equals("strDescription")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("iTenantId")) {

			} 
		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		} else if (localName.equals("hasMore")) {				
			hasMore = Integer.parseInt(buffer.toString());
		}
		if (isSuccess) {
			if (isUserWorkTimeSheetTaskSucess) {

				if (localName.equals("UserWorkTimeSheetTask")) {
					isUserWorkTimeSheetTaskSucess = false;
					userWorkTimeSheetTaskList.add(userWorkTimeSheetTask);
					Log.i("tag list true", "" + isUserWorkTimeSheetTaskSucess
							+ isSuccess);
				} else if (localName.equals("iUserWorkTimeSheetTaskId")) {

					userWorkTimeSheetTask.setiUserWorkTimeSheetTaskId(buffer
							.toString().trim());

				} else if (localName.equals("iUserWorkTimeSheetId")) {

					userWorkTimeSheetTask.setiUserWorkTimeSheetId(buffer
							.toString().trim());

				} else if (localName.equals("workStartDate")) {

					userWorkTimeSheetTask.setWorkStartDate(buffer.toString()
							.trim());

				} else if (localName.equals("workStartTime")) {

					userWorkTimeSheetTask.setWorkStartTime(buffer.toString()
							.trim());

				} else if (localName.equals("workEndDate")) {

					userWorkTimeSheetTask.setWorkEndDate(buffer.toString()
							.trim());

				} else if (localName.equals("workEndTime")) {

					userWorkTimeSheetTask.setWorkEndTime(buffer.toString()
							.trim());

				} else if (localName.equals("workStartDateTime")) {

					userWorkTimeSheetTask.setWorkStartDateTime(buffer
							.toString().trim());

				} else if (localName.equals("workEndDateTime")) {

					userWorkTimeSheetTask.setWorkEndDateTime(buffer.toString()
							.trim());

				} else if (localName.equals("iUserId")) {

					userWorkTimeSheetTask.setiUserId(buffer.toString().trim());

				} else if (localName.equals("iUserServiceTermId")) {

					userWorkTimeSheetTask.setiUserServiceTermId(buffer
							.toString().trim());

				} else if (localName.equals("itaskId")) {

					userWorkTimeSheetTask.setItaskId(buffer.toString().trim());

				} else if (localName.equals("icrewDepartmentTaskId")) {

					userWorkTimeSheetTask.setIcrewDepartmentTaskId(buffer
							.toString().trim());

				} else if (localName.equals("strDescription")) {

					userWorkTimeSheetTask.setStrDescription(buffer.toString()
							.trim());

				} else if (localName.equals("flgStatus")) {

					userWorkTimeSheetTask
							.setFlgStatus(buffer.toString().trim());

				} else if (localName.equals("dtCreated")) {

					userWorkTimeSheetTask
							.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {

					userWorkTimeSheetTask
							.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("flgIsDirty")) {

					userWorkTimeSheetTask.setFlgIsDirty(buffer.toString()
							.trim());

				} else if (localName.equals("flgDeleted")) {

					userWorkTimeSheetTask.setFlgDeleted(buffer.toString()
							.trim());

				} else if (localName.equals("iShipId")) {

					userWorkTimeSheetTask.setiShipId(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					userWorkTimeSheetTask
							.setiTenantId(buffer.toString().trim());
				} 
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}

}