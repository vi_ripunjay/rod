package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.InternationalDates;
import com.rms.utils.CommonUtil;

public class InternationalDatesParser extends DefaultHandler {

	private static final String TAG = InternationalDatesParser.class.getName()
			.toString();

	private List<InternationalDates> idlList;
	private boolean isSuccess;
	private InternationalDates internationalDates;
	private StringBuffer buffer;
	private boolean isInternationalDatesSucess = false;
	private boolean debug = true;
	private int fullData = 0;
	int errorCode = 0;

	/**
	 * get the value of InternationalDates data
	 * 
	 * @return list with content objects
	 */
	public List<InternationalDates> getInternationalDatesData() {
		return idlList;
	}

	public int getResultCode() {
		return errorCode;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		idlList = new ArrayList<InternationalDates>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		}
		if (isSuccess) {
			if (localName.equals("InternationalDates")) {
				isInternationalDatesSucess = true;
				internationalDates = new InternationalDates();
				Log.i("tag list true", "" + isInternationalDatesSucess
						+ isSuccess);

			} else if (localName.equals("iinternationalDateId")) {

			} else if (localName.equals("internationalDate")) {

			} else if (localName.equals("flgRepeated")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("strDescription")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("iTenantId")) {

			}
		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		}
		if (isSuccess) {
			if (isInternationalDatesSucess) {

				if (localName.equals("InternationalDates")) {
					isInternationalDatesSucess = false;
					idlList.add(internationalDates);
					Log.i("tag list true", "" + isInternationalDatesSucess
							+ isSuccess);
				} else if (localName.equals("iinternationalDateId")) {

					internationalDates.setIinternationalDateId(buffer
							.toString().trim());

				} else if (localName.equals("internationalDate")) {

					internationalDates.setInternationalDate(buffer.toString()
							.trim());

				} else if (localName.equals("flgRepeated")) {

					internationalDates.setFlgRepeated(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("dtCreated")) {

					internationalDates.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {

					internationalDates.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("flgIsDirty")) {

					internationalDates.setFlgIsDirty(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("flgDeleted")) {

					internationalDates.setFlgDeleted(Integer.parseInt(buffer
							.toString().trim()));

				} else if (localName.equals("strDescription")) {

					internationalDates.setTxtDescription(CommonUtil.unmarshal(buffer.toString()
							.trim()));

				} else if (localName.equals("iShipId")) {

					internationalDates.setiShipId(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					internationalDates.setiTenantId(buffer.toString().trim());

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}

}
