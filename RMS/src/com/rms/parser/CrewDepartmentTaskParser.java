package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.CrewDepartment;
import com.rms.model.CrewDepartmentTask;

public class CrewDepartmentTaskParser extends DefaultHandler {

	private static final String TAG = CrewDepartment.class.getName().toString();

	private List<CrewDepartmentTask> crewDepartmentTasksList;
	private boolean isSuccess;
	private CrewDepartmentTask crewDepartmentTask;
	private StringBuffer buffer;
	private boolean isCrewDepartmentTaskSucess = false;
	private boolean debug = true;
	private int fullData = 0;
	int errorCode = 0;

	/**
	 * get the value of CrewDepartmentTask data
	 * 
	 * @return list with content objects
	 */
	public List<CrewDepartmentTask> getCrewDepartmentTaskData() {
		return crewDepartmentTasksList;
	}

	public int getResultCode() {
		return errorCode;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		crewDepartmentTasksList = new ArrayList<CrewDepartmentTask>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		}
		if (isSuccess) {
			if (localName.equals("CrewDepartmentTask")) {
				isCrewDepartmentTaskSucess = true;
				crewDepartmentTask = new CrewDepartmentTask();
				Log.i("tag list true", "" + isCrewDepartmentTaskSucess + isSuccess);

			} else if (localName.equals("iCrewDepartmentTaskId")) {

			} else if (localName.equals("iTaskId")) {

			} else if (localName.equals("iCrewDepartmentId")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("iTenantId")) {

			}
		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		}
		if (isSuccess) {
			if (isCrewDepartmentTaskSucess) {

				if (localName.equals("CrewDepartmentTask")) {
					isCrewDepartmentTaskSucess = false;
					crewDepartmentTasksList.add(crewDepartmentTask);
					Log.i("tag list true", "" + isCrewDepartmentTaskSucess + isSuccess);
				} else if (localName.equals("iCrewDepartmentTaskId")) {
					
					crewDepartmentTask.setIcrewDepartmentTaskId(buffer.toString().trim());

				} else if (localName.equals("iTaskId")) {
					
					crewDepartmentTask.setItaskId(buffer.toString().trim());

				} else if (localName.equals("iCrewDepartmentId")) {

					crewDepartmentTask.setIcrewDepartmentId(buffer.toString().trim());

				} else if (localName.equals("dtCreated")) {

					crewDepartmentTask.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {

					crewDepartmentTask.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("flgIsDirty")) {

					crewDepartmentTask.setFlgIsDirty(buffer.toString().trim());

				} else if (localName.equals("flgDeleted")) {

					crewDepartmentTask.setFlgDeleted(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					crewDepartmentTask.setiTenantId(buffer.toString().trim());

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}

}
