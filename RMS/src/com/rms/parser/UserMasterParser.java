package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.UserMaster;

public class UserMasterParser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-05-11
	 */

	private static final String TAG = UserMasterParser.class.getName()
			.toString();

	private List<UserMaster> userMasterList;
	private boolean isSuccess;
	private UserMaster userMaster;
	private StringBuffer buffer;
	private boolean isUserMasterSucess = false;
	private boolean debug = true;

	/**
	 * get the value of UserMaster data
	 * 
	 * @return list with content objects
	 */
	public List<UserMaster> getUserMasterData() {
		return userMasterList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		userMasterList = new ArrayList<UserMaster>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("UserMaster")) {
				isUserMasterSucess = true;
				userMaster = new UserMaster();
				Log.i("tag list true", "" + isUserMasterSucess + isSuccess);
			} else if (localName.equals("iuserId")) {

			} else if (localName.equals("iroleId")) {

			} else if (localName.equals("strUserName")) {

			} else if (localName.equals("strPassword")) {

			} else if (localName.equals("strFirstName")) {

			} else if (localName.equals("strLastName")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("sequence")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("txtDescription")) {

			} else if (localName.equals("strEmail")) {

			} else if (localName.equals("dtDateOfBirth")) {

			} else if (localName.equals("strEmployeeNo")) {

			} else if (localName.equals("strMiddleName")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isUserMasterSucess) {
				if (localName.equals("UserMaster")) {
					isUserMasterSucess = false;
					userMasterList.add(userMaster);
					Log.i("tag list true", "" + isUserMasterSucess + isSuccess);
				} else if (localName.equals("iuserId")) {

					userMaster.setiUserId(buffer.toString().trim());

				} else if (localName.equals("iroleId")) {

					userMaster.setiRoleId(buffer.toString().trim());

				} else if (localName.equals("strUserName")) {

					userMaster.setStrUserName(buffer.toString().trim());

				} else if (localName.equals("strPassword")) {

					userMaster.setStrPassword(buffer.toString().trim());

				} else if (localName.equals("strFirstName")) {

					userMaster.setStrFirstName(buffer.toString().trim());

				} else if (localName.equals("strLastName")) {

					userMaster.setStrLastName(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {

					userMaster.setFlgStatus(buffer.toString().trim());

				} else if (localName.equals("flgDeleted")) {

					userMaster.setFlgDeleted(buffer.toString().trim());

				} else if (localName.equals("flgIsDirty")) {

					userMaster.setFlgIsDirty(buffer.toString().trim());

				} else if (localName.equals("dtCreated")) {

					userMaster.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {

					userMaster.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					userMaster.setiTenantId(buffer.toString().trim());

				} else if (localName.equals("txtDescription")) {
					
					userMaster.setTxtDescription((buffer != null ? buffer.toString().trim() : ""));

				} else if (localName.equals("strEmail")) {
					
					userMaster.setStrEmail(buffer.toString().trim());

				} else if (localName.equals("dtDateOfBirth")) {
					
					userMaster.setDtDateOfBirth(buffer.toString().trim());

				} else if (localName.equals("strEmployeeNo")) {
					
					userMaster.setStrEmployeeNo((buffer != null ? buffer.toString().trim() : ""));

				} else if (localName.equals("strMiddleName")) {
					
					userMaster.setStrMiddleName((buffer != null ? buffer.toString().trim() : ""));

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}