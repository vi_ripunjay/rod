package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.CrewDepartment;
import com.rms.model.ValidateUser;

public class ValidateUserParser extends DefaultHandler {

	private static final String TAG = CrewDepartment.class.getName().toString();

	private List<ValidateUser> validateUserList;
	private boolean isSuccess;
	private ValidateUser validateUser;
	private StringBuffer buffer;
	private boolean isValidateUserSucess = false;
	private boolean debug = true;
	private int fullData = 0;
	int errorCode = 0;

	/**
	 * get the value of ValidateUser data
	 * 
	 * @return list with content objects
	 */
	public List<ValidateUser> getValidateUserData() {
		return validateUserList;
	}

	public int getResultCode() {
		return errorCode;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		validateUserList = new ArrayList<ValidateUser>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		}
		if (isSuccess) {
			if (localName.equals("userMaster")) {
				isValidateUserSucess = true;
				validateUser = new ValidateUser();
				Log.i("tag list true", "" + isValidateUserSucess + isSuccess);

			} else if (localName.equals("strUserId")) {

			} else if (localName.equals("roleId")) {

			} else if (localName.equals("firstName")) {

			} else if (localName.equals("lastName")) {

			} else if (localName.equals("dtTermFrom")) {

			} else if (localName.equals("dtTermTo")) {

			} else if (localName.equals("strShipId")) {

			} else if (localName.equals("iTenantId")) {

			}
		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		}
		if (isSuccess) {
			if (isValidateUserSucess) {

				if (localName.equals("userMaster")) {
					isValidateUserSucess = false;
					validateUserList.add(validateUser);
					Log.i("tag list true", "" + isValidateUserSucess
							+ isSuccess);
				} else if (localName.equals("strUserId")) {

					validateUser.setiUserId(buffer.toString().trim());

				} else if (localName.equals("roleId")) {

					validateUser.setiRoleId(buffer.toString().trim());

				} else if (localName.equals("firstName")) {

					validateUser.setStrFirstName(buffer.toString().trim());

				} else if (localName.equals("lastName")) {

					validateUser.setStrLastName(buffer.toString().trim());

				} else if (localName.equals("dtTermFrom")) {

					validateUser.setDtTermFrom(buffer.toString().trim());

				} else if (localName.equals("dtTermTo")) {

					validateUser.setDtTermTo(buffer.toString().trim());

				} else if (localName.equals("strShipId")) {

					validateUser.setiShipId(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					validateUser.setiTenantId(buffer.toString().trim());

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}

}