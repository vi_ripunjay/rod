package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.RoleTabletFunction;
import com.rms.utils.CommonUtil;

public class RoleTabletFunctionParser extends DefaultHandler{
	
	/**
	 * @author pushkar.m
	 * @since 2016-05-12
	 */

	private static final String TAG = ShipMasterParser.class.getName()
			.toString();

	private List<RoleTabletFunction> roleFunctionList;
	private boolean isSuccess;
	private RoleTabletFunction roleTabletFunction;
	private StringBuffer buffer;
	private boolean isRoleTabletFunctionSuccess = false;
	private boolean debug = true;

	/**
	 * get the value of ShipMaster data
	 * 
	 * @return list with content objects
	 */
	public List<RoleTabletFunction> getRoleTabletFunctionData() {
		return roleFunctionList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		roleFunctionList = new ArrayList<RoleTabletFunction>();
	}

	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		// System.out.println("LOCAL NAME : "+localName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("RoleTabletFunctions")) {
				isRoleTabletFunctionSuccess = true;
				roleTabletFunction = new RoleTabletFunction();
				Log.i("tag list true", "" + isRoleTabletFunctionSuccess + isSuccess);
			} else if (localName.equals("iRoleTabletFunctionsId")) {

			} else if (localName.equals("strFunctionCode")) {

			} else if (localName.equals("roleId")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("strField1")) {

			} else if (localName.equals("strField2")) {

			} else if (localName.equals("iTenantId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isRoleTabletFunctionSuccess) {
				if (localName.equals("RoleTabletFunctions")) {
					isRoleTabletFunctionSuccess = false;
					roleFunctionList.add(roleTabletFunction);
					Log.i("tag list true", "" + isRoleTabletFunctionSuccess + isSuccess);
				}
				  else if (localName.equals("iRoleTabletFunctionsId")) {

					roleTabletFunction.setiTabletRoleFunctionsId(buffer.toString()
							.trim());
				} else if (localName.equals("strFunctionCode")) {
					roleTabletFunction.setStrFunctionCode(CommonUtil.unmarshal(buffer.toString()
							.trim()));
				} else if (localName.equals("roleId")) {
					roleTabletFunction.setiRoleId(buffer.toString()
							.trim());
				} else if (localName.equals("flgDeleted")) {
					roleTabletFunction.setFlgDeleted(Integer.parseInt(buffer.toString()
							.trim()));
				} else if (localName.equals("flgIsDirty")) {

				} else if (localName.equals("dtCreated")) {
					roleTabletFunction.setDtCreated(buffer.toString()
							.trim());
				} else if (localName.equals("dtUpdated")) {
					roleTabletFunction.setDtUpdated(buffer.toString()
							.trim());
				} else if (localName.equals("strField1")) {
					roleTabletFunction.setStrField1(buffer.toString()
							.trim());
				} else if (localName.equals("strField2")) {
					roleTabletFunction.setStrField2(buffer.toString()
							.trim());
				} else if (localName.equals("iTenantId")) {
					roleTabletFunction.setiTenantId(buffer.toString()
							.trim());
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}
