package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.rms.model.ShipMaster;

import android.util.Log;

public class ShipMasterParser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-06-25
	 */

	private static final String TAG = ShipMasterParser.class.getName()
			.toString();

	private List<ShipMaster> shipMasterList;
	private boolean isSuccess;
	private ShipMaster shipMaster;
	private StringBuffer buffer;
	private boolean isShipMasterSuccess = false;
	private boolean debug = true;

	/**
	 * get the value of ShipMaster data
	 * 
	 * @return list with content objects
	 */
	public List<ShipMaster> getShipMasterData() {
		return shipMasterList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		shipMasterList = new ArrayList<ShipMaster>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		// System.out.println("LOCAL NAME : "+localName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("ShipMaster")) {
				isShipMasterSuccess = true;
				shipMaster = new ShipMaster();
				Log.i("tag list true", "" + isShipMasterSuccess + isSuccess);
			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("strShipName")) {

			} else if (localName.equals("strDescription")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("shipType")) {

			} else if (localName.equals("classificationSociety")) {

			} else if (localName.equals("strShipCode")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("ishipImonumber")) {

			} else if (localName.equals("strFlag")) {

			} else if (localName.equals("strLogo")) {

			} else if (localName.equals("strFileName")) {

			} else if (localName.equals("strFileType")) {

			} else if (localName.equals("iTenantId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isShipMasterSuccess) {
				if (localName.equals("ShipMaster")) {
					isShipMasterSuccess = false;
					shipMasterList.add(shipMaster);
					Log.i("tag list true", "" + isShipMasterSuccess + isSuccess);
				}
				else if (localName.equals("iShipId")) {

					shipMaster.setiShipId(buffer.toString()
							.trim());

				} else if (localName.equals("strShipName")) {

					shipMaster.setStrShipName(buffer.toString().trim());

				} else if (localName.equals("strDescription")) {

					shipMaster.setStrDescription((buffer != null ? buffer.toString().trim() : ""));

				} else if (localName.equals("flgDeleted")) {

					shipMaster.setFlgDeleted(buffer.toString()
							.trim());

				} else if (localName.equals("flgIsDirty")) {

					shipMaster.setFlgIsDirty(buffer.toString()
							.trim());

				} else if (localName.equals("dtCreated")) {

					shipMaster.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("shipType")) {
					
					shipMaster.setiShipTypeId(buffer.toString().trim());

				} else if (localName.equals("classificationSociety")) {
					
					shipMaster.setiClassificationSocietyId(buffer.toString().trim());

				} else if (localName.equals("strShipCode")) {
					
					shipMaster.setStrShipCode(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {
					
					shipMaster.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {
					
					shipMaster.setFlgStatus(buffer.toString().trim());

				} else if (localName.equals("ishipImonumber")) {
					
					shipMaster.setiShipIMONumber(buffer.toString().trim());

				} else if (localName.equals("strFlag")) {
					
					shipMaster.setStrFlag(buffer.toString().trim());

				} else if (localName.equals("strLogo")) {
					
					shipMaster.setStrLogo(buffer.toString().trim());

				} else if (localName.equals("strFileName")) {
					
					shipMaster.setStrFileName(buffer.toString().trim());

				} else if (localName.equals("strFileType")) {
					
					shipMaster.setStrFileType(buffer.toString().trim());

				}else if (localName.equals("iTenantId")) {

					shipMaster.setiTenantId(buffer.toString()
							.trim());

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}