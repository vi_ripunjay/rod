package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.SyncHistory;
import com.rms.model.SyncStatus;
import com.rms.utils.CommonUtil;

public class SyncStatusParser extends DefaultHandler{
	/**
	 * @author pushkar.m
	 */
	
	private static final String TAG = SyncStatusParser.class.getName()
			.toString();

	private List<SyncStatus> syncStatusList;
	private boolean isSuccess;
	private SyncStatus syncStatus;
	private StringBuffer buffer;
	private boolean debug = true;
	private boolean isSyncStatusSucess = false;
	
	int errorCode = 0;

	/**
	 * get the value of SyncHistory data
	 * 
	 * @return list with content objects
	 */
	public List<SyncStatus> getSyncStatusData() {
		return syncStatusList;
	}

	public int getResultCode() {
		return errorCode;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		syncStatusList = new ArrayList<SyncStatus>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} 
		
		if (isSuccess) {
			// if (localName.equals("SyncHistoryList")) {

			if (localName.equals("SyncStatus")) {
				isSyncStatusSucess = true;
				syncStatus = new SyncStatus();
				Log.i("tag list true", "" + isSyncStatusSucess + isSuccess);

			} else if (localName.equals("iSyncStatusId")) {

			} else if (localName.equals("dtSyncDate")) {

			} else if (localName.equals("syncMode")) {

			} else if (localName.equals("dataSyncMode")) {

			} else if (localName.equals("serverAddress")) {

			} else if (localName.equals("strMacId")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDeviceDirty")) {

			} else if (localName.equals("syncTime")) {

			} else if (localName.equals("iTenantId")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("createdDate")) {

			} else if (localName.equals("modifiedDate")) {

			} else if (localName.equals("createdBy")) {

			} else if (localName.equals("modifiedBy")) {

			} else if (localName.equals("moduleName")) {

			}

		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} 
		if (isSuccess) {
			if (isSyncStatusSucess) {
				
				if (localName.equals("SyncStatus")) {
					isSyncStatusSucess = false;
					syncStatusList.add(syncStatus);
					Log.i("tag list true", "" + isSyncStatusSucess + isSuccess);
				}  else if (localName.equals("iSyncStatusId")) {
					
					syncStatus.setiSyncStatusId(buffer.toString().trim());
					
				} else if (localName.equals("dtSyncDate")) {
					
					syncStatus.setDtSyncDate(buffer.toString().trim());					

				} else if (localName.equals("syncMode")) {
					
					syncStatus.setSyncMode(buffer.toString().trim());					

				} else if (localName.equals("dataSyncMode")) {
					
					syncStatus.setDataSyncMode(buffer.toString().trim());

				} else if (localName.equals("serverAddress")) {
					
					syncStatus.setServerAddress(CommonUtil.unmarshal(buffer.toString().trim()));

				} else if (localName.equals("strMacId")) {
					
					syncStatus.setStrMacId(buffer.toString().trim());

				} else if (localName.equals("flgStatus")) {
					
					syncStatus.setFlgStatus(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgIsDirty")) {
					
					syncStatus.setFlgIsDirty(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgDeleted")) {
					
					syncStatus.setFlgDeleted(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("flgIsDeviceDirty")) {
					
					syncStatus.setFlgIsDeviceDirty(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("syncTime")) {
					
					syncStatus.setSyncTime(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {
					
					syncStatus.setiTenantId(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("iShipId")) {
					
					syncStatus.setiShipId(Integer.parseInt(buffer.toString().trim()));

				} else if (localName.equals("createdDate")) {
					
					syncStatus.setCreatedDate(buffer.toString().trim());

				} else if (localName.equals("modifiedDate")) {
					
					syncStatus.setModifiedDate(buffer.toString().trim());

				} else if (localName.equals("createdBy")) {
					
					syncStatus.setCreatedBy(buffer.toString().trim());

				} else if (localName.equals("modifiedBy")) {
					
					syncStatus.setModifiedBy(buffer.toString().trim());

				} else if (localName.equals("moduleName")) {

					syncStatus.setModuleName(buffer.toString().trim());
				}

			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}


}
