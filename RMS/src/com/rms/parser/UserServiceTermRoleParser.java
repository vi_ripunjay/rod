package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.CrewDepartment;
import com.rms.model.UserServiceTermRole;

public class UserServiceTermRoleParser extends DefaultHandler {

	private static final String TAG = CrewDepartment.class.getName().toString();

	private List<UserServiceTermRole> userServiceTermRoleList;
	private boolean isSuccess;
	private UserServiceTermRole userServiceTermRole;
	private StringBuffer buffer;
	private boolean isUserServiceTermRoleSucess = false;
	private boolean debug = true;
	private int fullData = 0;
	int errorCode = 0;

	/**
	 * get the value of UserServiceTermRole data
	 * 
	 * @return list with content objects
	 */
	public List<UserServiceTermRole> getUserServiceTermRoleData() {
		return userServiceTermRoleList;
	}

	public int getResultCode() {
		return errorCode;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		userServiceTermRoleList = new ArrayList<UserServiceTermRole>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		}
		if (isSuccess) {
			if (localName.equals("UserServiceTermRole")) {
				isUserServiceTermRoleSucess = true;
				userServiceTermRole = new UserServiceTermRole();
				Log.i("tag list true", "" + isUserServiceTermRoleSucess + isSuccess);

			} else if (localName.equals("strUserServiceTermRoleId")) {

			} else if (localName.equals("iroleId")) {

			} else if (localName.equals("iuserId")) {

			} else if (localName.equals("iuserServiceTermId")) {

			} else if (localName.equals("dtTermFrom")) {

			} else if (localName.equals("dtTermTo")) {

			} else if (localName.equals("powerPlusScore")) {

			} else if (localName.equals("ishipHolidayListId")) {

			} else if (localName.equals("strField1")) {

			} else if (localName.equals("strField2")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("ishipId")) {

			} else if (localName.equals("iTenantId")) {

			}
		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		}
		if (isSuccess) {
			if (isUserServiceTermRoleSucess) {

				if (localName.equals("UserServiceTermRole")) {
					isUserServiceTermRoleSucess = false;
					userServiceTermRoleList.add(userServiceTermRole);
					Log.i("tag list true", "" + isUserServiceTermRoleSucess + isSuccess);
				} else if (localName.equals("strUserServiceTermRoleId")) {
					
					userServiceTermRole.setStrUserServiceTermRoleId(buffer.toString().trim());

				} else if (localName.equals("iroleId")) {
					
					userServiceTermRole.setiRoleId(buffer.toString().trim());

				} else if (localName.equals("iuserId")) {
					
					userServiceTermRole.setIuserId(buffer.toString().trim());

				} else if (localName.equals("iuserServiceTermId")) {
					
					userServiceTermRole.setIuserServiceTermId(buffer.toString().trim());

				} else if (localName.equals("dtTermFrom")) {
					
					userServiceTermRole.setDtTermFrom(buffer.toString().trim());

				} else if (localName.equals("dtTermTo")) {
					
					userServiceTermRole.setDtTermTo(buffer.toString().trim());

				} else if (localName.equals("powerPlusScore")) {
					
					userServiceTermRole.setPowerPlusScore((buffer != null && !"null".equalsIgnoreCase(buffer.toString()) ? Integer.parseInt(buffer.toString().trim()) : 0));

				} else if (localName.equals("ishipHolidayListId")) {
					
					userServiceTermRole.setIshipHolidayListId((buffer != null ? buffer.toString().trim() : ""));
					
				} else if (localName.equals("strField1")) {
					
					userServiceTermRole.setField1((buffer != null && !"null".equalsIgnoreCase(buffer.toString()) ? Integer.parseInt(buffer.toString().trim()) : 0));

				} else if (localName.equals("strField2")) {
					
					userServiceTermRole.setField2((buffer != null && !"null".equalsIgnoreCase(buffer.toString()) ? Integer.parseInt(buffer.toString().trim()) : 0));

				} else if (localName.equals("dtCreated")) {
					
					userServiceTermRole.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {
					
					userServiceTermRole.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("flgIsDirty")) {
					
					userServiceTermRole.setFlgIsDirty(buffer.toString().trim());

				} else if (localName.equals("flgDeleted")) {
					
					userServiceTermRole.setFlgDeleted(buffer.toString().trim());

				} else if (localName.equals("ishipId")) {
					
					userServiceTermRole.setiShipId(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {
					
					userServiceTermRole.setiTenantId(buffer.toString().trim());

				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}

}