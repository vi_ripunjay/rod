package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.rms.model.Roles;

import android.util.Log;

public class RoleParser extends DefaultHandler {

	/**
	 * @author Ripunjay Shukla
	 * @since 2015-06-25
	 */

	private static final String TAG = RoleParser.class.getName().toString();

	private List<Roles> roleList;
	private boolean isSuccess;
	private Roles role;
	private StringBuffer buffer;
	private boolean isRoleSuccess = false;
	private boolean debug = true;

	/**
	 * get the value of Role data
	 * 
	 * @return list with content objects
	 */
	public List<Roles> getRoleData() {
		return roleList;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		roleList = new ArrayList<Roles>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (localName.equals("Roles")) {
				isRoleSuccess = true;
				role = new Roles();
				Log.i("tag list true", "" + isRoleSuccess + isSuccess);
			} else if (localName.equals("roleId")) {

			} else if (localName.equals("strRole")) {

			} else if (localName.equals("strRoleType")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("iRankPriority")) {

			} else if (localName.equals("txtDescription")) {

			} else if (localName.equals("iTenantId")) {

			}

		}

	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
		} else if (localName.equals("message")) {

		}
		if (isSuccess) {
			if (isRoleSuccess) {
				if (localName.equals("Roles")) {
					isRoleSuccess = false;
					roleList.add(role);
					Log.i("tag list true", "" + isRoleSuccess + isSuccess);
				} else if (localName.equals("roleId")) {

					role.setiRoleId(buffer.toString().trim());

				} else if (localName.equals("strRole")) {

					role.setStrRole(buffer.toString().trim());

				} else if (localName.equals("iRankPriority")) {

					role.setiRankPriority(buffer.toString().trim());

				} else if (localName.equals("strRoleType")) {
					
					role.setStrRoleType(buffer.toString().trim());

				} else if (localName.equals("txtDescription")) {
					
					role.setTxtDescription(buffer.toString().trim());

				} else if (localName.equals("flgDeleted")) {

					role.setFlgDeleted(buffer.toString().trim());

				} else if (localName.equals("flgIsDirty")) {

					role.setFlgIsDirty(buffer.toString()
							.trim());

				} else if (localName.equals("dtCreated")) {

					role.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {

					role.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					role.setiTenantId(buffer.toString().trim());

				} 
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

}