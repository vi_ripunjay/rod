package com.rms.parser;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import android.util.Log;

import com.rms.model.UserWorkTimeSheet;

public class UserWorkTimeSheetParser extends DefaultHandler {

	private static final String TAG = UserWorkTimeSheetParser.class.getName()
			.toString();

	private List<UserWorkTimeSheet> userWorkTimeSheetList;
	private boolean isSuccess;
	private UserWorkTimeSheet userWorkTimeSheet;
	private StringBuffer buffer;
	private boolean isUserWorkTimeSheetSucess = false;
	private boolean debug = true;
	private int fullData = 0;
	int errorCode = 0;
	int hasMore = 0;

	/**
	 * get the value of UserWorkTimeSheet data
	 * 
	 * @return list with content objects
	 */
	public List<UserWorkTimeSheet> getUserWorkTimeSheetData() {
		return userWorkTimeSheetList;
	}

	public int getResultCode() {
		return errorCode;
	}
	
	public int getHasMore(){
		return hasMore;
	}

	/**
	 * Receive notification of the beginning of a document. It initialize the
	 * required objects
	 */
	@Override
	public void startDocument() throws SAXException {
		super.startDocument();
		userWorkTimeSheetList = new ArrayList<UserWorkTimeSheet>();
	}

	/**
	 * Receive notification of the beginning of an element and perform required
	 * .
	 * 
	 */
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		super.startElement(uri, localName, qName, attributes);
		buffer = new StringBuffer();
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {

		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {

		} else if (localName.equals("hasMore")) {

		}
		
		if (isSuccess) {
			if (localName.equals("UserWorkTimeSheet")) {
				isUserWorkTimeSheetSucess = true;
				userWorkTimeSheet = new UserWorkTimeSheet();
				Log.i("tag list true", "" + isUserWorkTimeSheetSucess
						+ isSuccess);

			} else if (localName.equals("iUserWorkTimeSheetId")) {

			} else if (localName.equals("workStartDate")) {

			} else if (localName.equals("workStartTime")) {

			} else if (localName.equals("workEndDate")) {

			} else if (localName.equals("workEndTime")) {

			} else if (localName.equals("workStartDateTime")) {

			} else if (localName.equals("workEndDateTime")) {

			} else if (localName.equals("iUserId")) {

			} else if (localName.equals("iUserServiceTermId")) {

			} else if (localName.equals("strDescription")) {

			} else if (localName.equals("flgInternationalOrderStart")) {

			} else if (localName.equals("flgInternationalOrderEnd")) {

			} else if (localName.equals("flgStatus")) {

			} else if (localName.equals("dtCreated")) {

			} else if (localName.equals("dtUpdated")) {

			} else if (localName.equals("flgIsDirty")) {

			} else if (localName.equals("flgDeleted")) {

			} else if (localName.equals("iShipId")) {

			} else if (localName.equals("iTenantId")) {

			} 
		}
	}

	/**
	 * Receive notification of character data. It append the data in buffer
	 * string for further use.
	 * 
	 */
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
		String string = String.valueOf(ch, start, length);
		buffer.append(string);
		printInfoLog(buffer.toString());
	}

	/**
	 * Receive notification of the end of an element. Here It stores the value
	 * in terms of objects in corresponding list.
	 */
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);
		if (localName.equals("status")) {

		} else if (localName.equals("errorCode")) {
			if (Integer.parseInt(buffer.toString().toString()) == 0) {
				isSuccess = true;
			}
			errorCode = Integer.parseInt(buffer.toString().toString());
		} else if (localName.equals("message")) {

		} else if (localName.equals("fullData")) {
			fullData = Integer.parseInt(buffer.toString());
		}else if (localName.equals("hasMore")) {
			hasMore = Integer.parseInt(buffer.toString());
		}
		
		if (isSuccess) {
			if (isUserWorkTimeSheetSucess) {

				if (localName.equals("UserWorkTimeSheet")) {
					isUserWorkTimeSheetSucess = false;
					userWorkTimeSheetList.add(userWorkTimeSheet);
					Log.i("tag list true", "" + isUserWorkTimeSheetSucess
							+ isSuccess);
				} else if (localName.equals("iUserWorkTimeSheetId")) {

					userWorkTimeSheet.setiUserWorkTimeSheetId(buffer.toString()
							.trim());

				} else if (localName.equals("workStartDate")) {

					userWorkTimeSheet
							.setWorkStartDate(buffer.toString().trim());

				} else if (localName.equals("workStartTime")) {

					userWorkTimeSheet
							.setWorkStartTime(buffer.toString().trim());

				} else if (localName.equals("workEndDate")) {

					userWorkTimeSheet.setWorkEndDate(buffer.toString().trim());

				} else if (localName.equals("workEndTime")) {

					userWorkTimeSheet.setWorkEndTime(buffer.toString().trim());

				} else if (localName.equals("workStartDateTime")) {

					userWorkTimeSheet.setWorkStartDateTime(buffer.toString()
							.trim());

				} else if (localName.equals("workEndDateTime")) {

					userWorkTimeSheet.setWorkEndDateTime(buffer.toString()
							.trim());

				} else if (localName.equals("iUserId")) {

					userWorkTimeSheet.setiUserId(buffer.toString().trim());

				} else if (localName.equals("iUserServiceTermId")) {

					userWorkTimeSheet.setiUserServiceTermId(buffer.toString()
							.trim());

				} else if (localName.equals("strDescription")) {

					userWorkTimeSheet.setStrDescription(buffer.toString()
							.trim());

				} else if (localName.equals("flgInternationalOrderStart")) {

					userWorkTimeSheet.setFlgInternationalOrderStart((buffer != null && !"null".equalsIgnoreCase(buffer.toString().trim()) ? Integer.parseInt(buffer.toString().trim()) : 0));

				} else if (localName.equals("flgInternationalOrderEnd")) {

					userWorkTimeSheet.setFlgInternationalOrderEnd((buffer != null && !"null".equalsIgnoreCase(buffer.toString().trim()) ? Integer.parseInt(buffer.toString().trim()) : 0));

				} else if (localName.equals("flgStatus")) {

					userWorkTimeSheet.setFlgStatus(buffer.toString().trim());

				} else if (localName.equals("dtCreated")) {

					userWorkTimeSheet.setDtCreated(buffer.toString().trim());

				} else if (localName.equals("dtUpdated")) {

					userWorkTimeSheet.setDtUpdated(buffer.toString().trim());

				} else if (localName.equals("flgIsDirty")) {

					userWorkTimeSheet.setFlgIsDirty(buffer.toString().trim());

				} else if (localName.equals("flgDeleted")) {

					userWorkTimeSheet.setFlgDeleted(buffer.toString().trim());

				} else if (localName.equals("iShipId")) {

					userWorkTimeSheet.setiShipId(buffer.toString().trim());

				} else if (localName.equals("iTenantId")) {

					userWorkTimeSheet.setiTenantId(buffer.toString().trim());

				} else if (localName.equals("hasMore")) {
					
					hasMore = Integer.parseInt(buffer.toString().trim());
					
				}
			}
		}
		clearBuffer();
	}

	/**
	 * This function will clear the buffer string
	 */
	private void clearBuffer() {
		int size = buffer.length();
		buffer.delete(0, size);
	}

	/**
	 * Receive notification of the end of a document. It stores the value
	 * whether parsing is completed or not.
	 */
	@Override
	public void endDocument() throws SAXException {
		super.endDocument();

	}

	/**
	 * This function will print the value on the log.
	 * 
	 * @param msg
	 */
	private void printInfoLog(String msg) {
		if (debug) {
			Log.i(TAG, msg);
		}
	}

	public int getFullData() {
		return fullData;
	}

	public void setFullData(int fullData) {
		this.fullData = fullData;
	}

}