package com.rms;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.rms.db.DatabaseSupport;
import com.rms.model.SyncStatus;
import com.rms.model.UserMaster;
import com.rms.model.ValidateUser;
import com.rms.parser.ValidateUserParser;
import com.rms.service.WebServiceMethod;
import com.rms.utils.CommonUtil;

/**
 * @author Ripunjay Shukla A login screen that offers login via email/password.
 */
public class LoginActivity extends Activity implements LoaderCallbacks<Cursor> {

	/**
	 * A dummy authentication store containing known user names and passwords.
	 * TODO: remove after connecting to a real authentication system.
	 */
	private static final String[] DUMMY_CREDENTIALS = new String[] {
			"foo@example.com:hello", "bar@example.com:world" };
	/**
	 * Keep track of the login task to ensure we can cancel it if requested.
	 */
	private UserLoginTask mAuthTask = null;

	// UI references.
	private AutoCompleteTextView mEmailView;
	private EditText mPasswordView;
	private View mProgressView;
	private View mLoginFormView;
	private TextView validText;
	private Button mEmailSignInButton;
	Context context;
	String activeAccessMode;

	public static boolean isNewUser = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		ab.setDisplayShowHomeEnabled(false);
		
		/**
		 * below code for hide the icon.
		 */
		ab.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

		setContentView(R.layout.activity_login);

		validText = (TextView) findViewById(R.id.validText);
		// Set up the login form.
		mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
		populateAutoComplete();
		mPasswordView = (EditText) findViewById(R.id.password);
		mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
		mLoginFormView = findViewById(R.id.login_form);
		mProgressView = findViewById(R.id.login_progress);
		
		/**
		 * Call this method for schedule.
		 */
		CommonUtil.scheduleProcess(context);

		DatabaseSupport db = new DatabaseSupport(getApplicationContext());
		UserMaster usr = new UserMaster();

		String webLogin = getResources().getString(R.string.webLogin);
		if (!"1".equalsIgnoreCase(webLogin)) {

			Intent i = new Intent(LoginActivity.this, WelcomeAdmin.class);
			startActivity(i);
			finish();

		} else {
			List<UserMaster> umList = db.getUserMasterRow();

			if (umList != null && umList.size() > 0) {
				Intent i = new Intent(LoginActivity.this, WelcomeAdmin.class);
				startActivity(i);
				finish();

			} else {

				mPasswordView
						.setOnEditorActionListener(new TextView.OnEditorActionListener() {
							@Override
							public boolean onEditorAction(TextView textView,
									int id, KeyEvent keyEvent) {
								if (id == R.id.login
										|| id == EditorInfo.IME_NULL) {
									CommonUtil.setUserLogin(context, "1");
									attemptLogin();
									return true;
								}
								return false;
							}
						});

				mEmailSignInButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View view) {
						CommonUtil.setUserLogin(context, "1");
						attemptLogin();
					}
				});

			}

		}

		db.close();

	}

	private void populateAutoComplete() {
		getLoaderManager().initLoader(0, null, this);
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		if (mAuthTask != null) {
			return;
		}

		boolean isReady = false;
		// Reset errors.
		mEmailView.setError(null);
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		String email = mEmailView.getText().toString();
		String password = mPasswordView.getText().toString();
		String strmacid = "";

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password, if the user entered one.
		if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
			mPasswordView.setError(getString(R.string.error_invalid_password));
			focusView = mPasswordView;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(email)) {
			mEmailView.setError(getString(R.string.error_field_required));
			focusView = mEmailView;
			cancel = true;
		} else if (!isEmailValid(email)) {
			mEmailView.setError(getString(R.string.error_invalid_email));
			focusView = mEmailView;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {

			isReady = CommonUtil.checkConnectivity(context);

			if (isReady) {
				strmacid = CommonUtil.getIMEI(context);
				showProgress(true);
				mAuthTask = new UserLoginTask(email, password, strmacid);
				mAuthTask.execute((Void) null);
			}
		}
	}

	private boolean isEmailValid(String email) {
		// TODO: Replace this with your own logic
		// return email.contains("@");
		return email.contains("");
	}

	private boolean isPasswordValid(String password) {
		// TODO: Replace this with your own logic
		return password.length() > 4;
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	public void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			mLoginFormView.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mLoginFormView.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});

			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mProgressView.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							mProgressView.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
		return new CursorLoader(this,
				// Retrieve data rows for the device user's 'profile' contact.
				Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
						ContactsContract.Contacts.Data.CONTENT_DIRECTORY),
				ProfileQuery.PROJECTION,

				// Select only email addresses.
				ContactsContract.Contacts.Data.MIMETYPE + " = ?",
				new String[] { ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE },

				// Show primary email addresses first. Note that there won't be
				// a primary email address if the user hasn't specified one.
				ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
		List<String> emails = new ArrayList<String>();
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			emails.add(cursor.getString(ProfileQuery.ADDRESS));
			cursor.moveToNext();
		}

		addEmailsToAutoComplete(emails);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> cursorLoader) {

	}

	private interface ProfileQuery {
		String[] PROJECTION = { ContactsContract.CommonDataKinds.Email.ADDRESS,
				ContactsContract.CommonDataKinds.Email.IS_PRIMARY, };

		int ADDRESS = 0;
		@SuppressWarnings("unused")
		int IS_PRIMARY = 1;
	}

	private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
		// Create adapter to tell the AutoCompleteTextView what to show in its
		// dropdown list.
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(
				LoginActivity.this,
				android.R.layout.simple_dropdown_item_1line,
				emailAddressCollection);

		mEmailView.setAdapter(adapter);
	}

	/**
	 * Represents an asynchronous login/registration task used to authenticate
	 * the user.
	 */
	public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

		private final String mEmail;
		private final String mPassword;
		private final String mStrMacId;
		private String data = "";

		UserLoginTask(String email, String password, String strMacId) {
			mEmail = email;
			mPassword = password;
			mStrMacId = strMacId;
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {				

				WebServiceMethod services = new WebServiceMethod(
						getApplicationContext());
				data = services.loginUser(mEmail, mPassword, mStrMacId, context.getResources().getString(R.string.moduleName));
				Log.i("data", "" + data);
				if (data != null) {
					return true;
				}

				// Simulate network access.
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				return false;
			}

			for (String credential : DUMMY_CREDENTIALS) {
				String[] pieces = credential.split(":");
				if (pieces[0].equals(mEmail)) {
					// Account exists, return true if the password matches.
					return pieces[1].equals(mPassword);
				}
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {
			mAuthTask = null;
			String tenantId = "0";
			// showProgress(false);

			if (success) {

				DatabaseSupport db = new DatabaseSupport(context);

				// db.updateUserMasterStatus(1);

				List<ValidateUser> userList = loginDeviceUser(data);
				if (userList != null && userList.size() > 0) {
					/*Intent i = new Intent(LoginActivity.this,
							WelcomeAdmin.class);
					startActivity(i);
					finish();*/

					SimpleDateFormat simpleDF = new SimpleDateFormat(
							"yyyy-MM-dd");

					String modifiedDate = simpleDF.format(new Date());
					for (ValidateUser um : userList) {

						tenantId = um.getiTenantId();
						/*UserMaster uMast = db
								.getUserMasterById(um.getiUserId());
						if (uMast != null && uMast.getiUserId() != null
								&& !"".equals(uMast.getiUserId())) {
							uMast.setDtUpdated(modifiedDate);
							uMast.setFlgStatus("0");
							db.updateUserMasterRow(uMast);
						} else {
							um.setStrUserName(mEmail);
							um.setStrPassword(mPassword);
							um.setDtUpdated(modifiedDate);
							um.setFlgStatus("0");
							db.addUserMasterRow(um);
						}*/

						CommonUtil.setUserName(getApplicationContext(), mEmail);
						CommonUtil.setUserFName(context, um.getStrFirstName());
						CommonUtil.setUserLName(context, um.getStrLastName());
						CommonUtil.setTenantID(context,
								String.valueOf(tenantId));
						CommonUtil.setUserId(context, um.getiUserId());
						CommonUtil.setRoleId(context, um.getiRoleId());
						CommonUtil.setShipID(context, um.getiShipId());
					}
					db.close();

					SyncHandler.context = context;
					Message shipMessage = new Message();
					shipMessage.what = SyncHandler.MSG_GET_SHIPMASTER;
					SyncHandler.handler.sendMessage(shipMessage);

					/*CommonUtil.setSynckFrom(context, "admin");
					CommonUtil.setUserLogin(context, "1");
					SyncHandler.context = context;
					Message manualMessage = new Message();
					manualMessage.what = SyncHandler.MSG_MATCH_MANUAL_SYNC_HISTORY;
					SyncHandler.handler.sendMessage(manualMessage);*/

					try {
						Thread.sleep(5000);
					} catch (InterruptedException e) { // TODO Auto-generated
														// catch block
						e.printStackTrace();
					}
				} else {
					validText.setVisibility(View.VISIBLE);
					validText.setText("Invalid user name and password !");
					showProgress(false);
					/*
					 * Intent i = new Intent(LoginActivity.this,
					 * VesselInspectionStartActivity.class); startActivity(i);
					 * finish();
					 */
				}

			} else {
				mPasswordView
						.setError(getString(R.string.error_incorrect_password));
				mPasswordView.requestFocus();
			}
		}

		@Override
		protected void onCancelled() {
			mAuthTask = null;
			showProgress(false);
		}
	}

	/**
	 * This method takes xml in string as input then start parsing of that xml.
	 * Check whether parsing is completed or not.If completed then insert data
	 * into sync table.In case of failure pop up will launch.
	 * 
	 * @param data
	 */
	@SuppressLint("SimpleDateFormat")
	private List<ValidateUser> loginDeviceUser(String data) {
		if (data != null) {
			try {
				InputStream xmlStream = new ByteArrayInputStream(
						data.getBytes());
				SAXParserFactory saxParserFactory = SAXParserFactory
						.newInstance();
				SAXParser saxParser = saxParserFactory.newSAXParser();
				ValidateUserParser validateUserParser = new ValidateUserParser();

				saxParser.parse(xmlStream, validateUserParser);
				List<ValidateUser> userList = validateUserParser.getValidateUserData();

				return userList;

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	/**
	 * @author pushkar.m
	 * @param Message
	 * @param position
	 */
	private void launchPopUpEnablingNetwork(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author pushkar.m update SyncStatus table
	 */
	private void updateSyncStatus() {
		try {
			DatabaseSupport db = new DatabaseSupport(context);
			
			List<SyncStatus> syncStatusList = db.getSyncStatusData();
			SyncStatus syncStatus = null;
			SimpleDateFormat dtFormat=new SimpleDateFormat("yyyy-MM-dd");
			if(syncStatusList != null && syncStatusList.size() > 0){
				
				syncStatus = syncStatusList.get(0);
				
				if(syncStatus.getDtSyncDate() == null || "".equals(syncStatus.getDtSyncDate().trim()))
					syncStatus.setDtSyncDate(dtFormat.format(new Date()));
				if(syncStatus.getServerAddress() == null || "".equals(syncStatus.getServerAddress().trim()))
					syncStatus.setServerAddress(getResources().getString(R.string.defultServiceUrl));
				
				db.updateSyncStatusTable(syncStatus);
			}		
			else{					
				syncStatus = new SyncStatus(CommonUtil.getIMEI(context) + new Date().getTime(),
	                    dtFormat.format(new Date()),activeAccessMode,CommonUtil.getDataSyncModeFromDb(context),
	                    getResources().getString(R.string.defultServiceUrl),CommonUtil.getIMEI(context),context.getResources().getString(R.string.moduleName),
	                    Integer.parseInt(CommonUtil.getTenantId(context)),Integer.parseInt(CommonUtil.getShipId(context)), 0, 0, 1,1,"",
	                    dtFormat.format(new Date()),
	                    dtFormat.format(new Date()),
	                    CommonUtil.getUserId(context),
	                    CommonUtil.getUserId(context));		
				
				db.insertSyncStatusTable(syncStatus);
			}
			db.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
	}

}