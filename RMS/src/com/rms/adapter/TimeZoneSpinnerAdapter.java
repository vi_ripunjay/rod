package com.rms.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.rms.R;
import com.rms.model.TimeZoneItem;

@SuppressLint("InflateParams")
public class TimeZoneSpinnerAdapter extends BaseAdapter implements ListAdapter {

	private List<TimeZoneItem> list = new ArrayList<TimeZoneItem>();
	private Context context;
	
	
	public TimeZoneSpinnerAdapter(List<TimeZoneItem> list, Context context){
		this.list = list;
		this.context = context;
	}
	
	@Override
	public int getCount() {		
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.timezone_spinner_list, null);
		}

		// Handle TextView and display string from your list
		TextView spinnerUsername = (TextView) view
				.findViewById(R.id.timeZoneName);
 
		
		spinnerUsername.setText(list.get(position).getName());
		
		return view;
	}

}
