package com.rms.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.rms.R;
import com.rms.model.TaskDataItem;

@SuppressLint("InflateParams")
public class TaskSpinnerAdapter extends BaseAdapter implements ListAdapter {
	private List<TaskDataItem> list = new ArrayList<TaskDataItem>();
	private Context context;

	public TaskSpinnerAdapter(List<TaskDataItem> list2, Context context) {
		this.list = list2;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.task_spinner_list, null);

		// Handle TextView and display string from your list
		TextView spinnerUsername = (TextView) view
				.findViewById(R.id.trainingName);

		spinnerUsername.setText(list.get(position).getStrTaskName());

		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}