package com.rms.adapter;

import java.util.Comparator;

import com.rms.model.TimeZoneItem;

public class TimeZoneComperator implements Comparator<TimeZoneItem> {

	public TimeZoneComperator() {

	}

	public int compare(TimeZoneItem tzi1, TimeZoneItem tzi2) {

		if (tzi1.getName() != null && tzi1.getName() != null) {

			try {
				String zone1 = tzi1.getName().substring(1, tzi1.getName().indexOf(")"));
				zone1 = zone1.replace("GMT", "").trim();
				zone1 = zone1.replace(":", ".").trim();
				
				String zone2 = tzi2.getName().substring(1, tzi2.getName().indexOf(")"));
				zone2 = zone2.replace("GMT", "").trim();
				zone2 = zone2.replace(":", ".").trim();
				
				
				
				Double formatId1 = Double.parseDouble(zone1);
				Double formatId2 = Double.parseDouble(zone2);

				return formatId1.compareTo(formatId2);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		} else if (tzi1.getId() == null && tzi2.getId() != null) {
			return -1;
		} else if (tzi1.getId() != null && tzi2.getId() == null) {
			return 1;
		} else {
			return 0;
		}
	}

}