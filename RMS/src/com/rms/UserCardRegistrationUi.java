package com.rms;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.app.ActionBar;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.rms.db.DatabaseSupport;
import com.rms.model.UserMaster;
import com.rms.model.UserNfcTagData;
import com.rms.model.UserSelectItem;
import com.rms.utils.CommonUtil;

public class UserCardRegistrationUi extends ActionBarActivity {

	Button register,cancel; 
	Spinner userList;
	String message = "";
	String userId = ""; 
	String userShipId = ""; 
	String userTenantId = ""; 
	
	NfcAdapter adapter;
	PendingIntent pendingIntent;
	IntentFilter writeTagFilters[];
	boolean writeMode;
	Tag mytag;
	Context ctx;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    ActionBar ab = getActionBar(); 
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
        ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);
  /*    
   
      ab.setDisplayOptions(ab.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);
        ImageView imageView = new ImageView(ab.getThemedContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setImageResource(R.drawable.nyk_logo);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                        | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 5;
        imageView.setLayoutParams(layoutParams);
        ab.setCustomView(imageView);

*/
		setContentView(R.layout.activity_user_card_registration_ui);
		ctx=this;
		register = (Button) findViewById(R.id.register);
		cancel = (Button) findViewById(R.id.cancel);
		userList  = (Spinner) findViewById(R.id.userlist);
		
		DatabaseSupport db = new DatabaseSupport(this);
		
		ArrayList<UserMaster> list = db.getUserMasterRow();
		Toast.makeText(getApplicationContext(), "List :" + list, Toast.LENGTH_LONG).show();
		ArrayList<UserSelectItem> userlist = new ArrayList<UserSelectItem>();
	     userlist.add(new UserSelectItem("Select User", "Select User","Select User"));
		 for (int i = 0; i < list.size(); i++) {
			 String roleid = list.get(i).getiRoleId();
			 String itenantId = list.get(i).getiTenantId();
			 String name = list.get(i).getStrFirstName() +" "+list.get(i).getStrFirstName() +" / " +db.getRoleName(roleid) ;
		     userlist.add(new UserSelectItem(name, list.get(i).getiUserId(),itenantId));
		 }
		 
		 ArrayAdapter<UserSelectItem> myAdapter = new ArrayAdapter<UserSelectItem>(this, android.R.layout.simple_spinner_item, userlist);
	     userList.setAdapter(myAdapter);

		 cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(UserCardRegistrationUi.this ,WelcomeAdmin.class);
                startActivity(i);
                finish();
				
			
			}
		});
		userList.setOnItemSelectedListener(new OnItemSelectedListener() {

		    public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
		            long arg3) {
		    	
		    	Toast.makeText(getApplicationContext(), 
		    			"OnItemSelectedListener : " + userList.getSelectedItem(),
		    			Toast.LENGTH_SHORT).show();
		    	
		    	UserSelectItem mydata;
                if(!(userList.getSelectedItem() == null))
                {
                   mydata = (UserSelectItem) userList.getSelectedItem();
                   userId =  mydata.getUserId();
                   userTenantId =mydata.getiTenantId();

                   System.out.println("Value : " + userId);
                }
                   String text =  userList.getSelectedItem().toString();
                
		    	if(text.equals("Select User"))
		    	{
		    		Toast.makeText(getApplicationContext(), "Please Select User", Toast.LENGTH_LONG).show();
		    	}else
		    	{
		   
		    	   //Make String here
				    message  = userId;
				    System.out.println("Message : " + message);
		    		
		    	}
		    	 
		    }
      
		    public void onNothingSelected(AdapterView<?> arg0) {
		        // TODO Auto-generated method stub

		    }
		});

		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				try {
					if(mytag==null){
						Toast.makeText(ctx, ctx.getString(R.string.error_detected), Toast.LENGTH_LONG ).show();
					}else{
						DatabaseSupport db = new DatabaseSupport(getApplicationContext());
						write(message,mytag);
						
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date curDate = new Date(); 
						/*
						db.addUserNFCRow(new UserNfcData(CommonUtil.getId(getApplicationContext()), "User", userId, "S1", "1", "0", "5/5/2015", "5/5/2015", userTenantId, "56"));
						*/
						 /**
						 * Above code comment due to card id use as primary key which is duplicate.
						 * So change pk format and cardid use in iuserservicetermid 
						 */
						String pk = CommonUtil.getId(getApplicationContext())+"_"+curDate.getTime();
						
						db.addUserNFCRow(new UserNfcTagData(pk, "User", userId, CommonUtil
								.getId(getApplicationContext()), "1", "0", df.format(curDate), df.format(curDate), CommonUtil
								.getTenantId(getApplicationContext()), CommonUtil
								.getShipId(getApplicationContext())));
						
						Toast.makeText(ctx, ctx.getString(R.string.ok_writing), Toast.LENGTH_LONG ).show();
                        
						Intent i = new Intent(UserCardRegistrationUi.this , WelcomeAdmin.class);
						startActivity(i);
						finish();
				
					}
				} catch (IOException e) {
					Toast.makeText(ctx, ctx.getString(R.string.error_writing), Toast.LENGTH_LONG ).show();
					e.printStackTrace();
				} catch (FormatException e) {
					Toast.makeText(ctx, ctx.getString(R.string.error_writing) , Toast.LENGTH_LONG ).show();
					e.printStackTrace();
				}
				
			}
		});
		
	    adapter = NfcAdapter.getDefaultAdapter(this);
		pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
		writeTagFilters = new IntentFilter[] { tagDetected };
		
	}

	
	private void write(String text, Tag tag) throws IOException, FormatException {

		NdefRecord[] records = { createRecord(text) };
		NdefMessage  message = new NdefMessage(records);
		// Get an instance of Ndef for the tag.
		Ndef ndef = Ndef.get(tag);
		// Enable I/O
		ndef.connect();
		// Write the message
		ndef.writeNdefMessage(message);
		//make read only
		if(ndef.canMakeReadOnly()){
			//Log.i("TheArkNFCCARD", "writing NFC :" + ndef.canMakeReadOnly());
			//ndef.makeReadOnly();
		}
		// Close the connection
		ndef.close();
	}



	private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
		String lang       = "en";
		byte[] textBytes  = text.getBytes();
		byte[] langBytes  = lang.getBytes("US-ASCII");
		int    langLength = langBytes.length;
		int    textLength = textBytes.length;
		byte[] payload    = new byte[1 + langLength + textLength];

		// set status byte (see NDEF spec for actual bits)
		payload[0] = (byte) langLength;

		// copy langbytes and textbytes into payload
		System.arraycopy(langBytes, 0, payload, 1,              langLength);
		System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

		NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,  NdefRecord.RTD_TEXT,  new byte[0], payload);

		return recordNFC;
	}


	@Override
	protected void onNewIntent(Intent intent){
		if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
			mytag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);    
			 byte[] id = mytag.getId();
             long ids = getDec(id);
             String i = String.valueOf(ids);
             CommonUtil.setId(ctx, i);
		}
	}
	

    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }
	@Override
	public void onPause(){
		super.onPause();
		WriteModeOff();
	}

	@Override
	public void onResume(){
		super.onResume();
		WriteModeOn();
	}

	private void WriteModeOn(){
		writeMode = true;
		adapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
	}

	private void WriteModeOff(){
		writeMode = false;
		adapter.disableForegroundDispatch(this);
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_card_registration_ui, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
