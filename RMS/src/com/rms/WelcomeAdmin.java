package com.rms;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.rms.db.DatabaseSupport;
import com.rms.model.RoleTabletFunction;
import com.rms.model.UserMaster;
import com.rms.model.UserNfcTagData;
import com.rms.model.UserServiceTermRole;
import com.rms.utils.CommonUtil;
import com.rms.utils.L;

/**
 * An {@link Activity} which handles a broadcast of a new tag that the device
 * just discovered.
 */
public class WelcomeAdmin extends Activity {

	private static final DateFormat TIME_FORMAT = SimpleDateFormat
			.getDateTimeInstance();
	// private LinearLayout mTagContent;

	private NfcAdapter mAdapter;
	private PendingIntent mPendingIntent;
	private NdefMessage mNdefPushMessage;
	long ids;
	private AlertDialog mDialog;
	TextView msgStartWork,msgEndWork;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		ab.setDisplayShowHomeEnabled(false);
		ab.hide();

		setContentView(R.layout.activity_main);
		// mTagContent = (LinearLayout) findViewById(R.id.list);
		
		msgStartWork = (TextView) findViewById(R.id.successWstartView);
		msgEndWork = (TextView) findViewById(R.id.successWEndView);
		
		String wNature = getIntent().getStringExtra("workNature");
		if(wNature != null){
			DatabaseSupport dbs = new DatabaseSupport(getApplicationContext());
		UserMaster userMaster = dbs.getUserMasterById(CommonUtil.getUserId(getApplicationContext()));
		if(wNature.equalsIgnoreCase("start")){
			//msgStartWork.setVisibility(View.VISIBLE);
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.wStartMsg)+" "+userMaster.getStrFirstName()+" "+userMaster.getStrLastName(), Toast.LENGTH_LONG).show();
			
		}else{
			//msgEndWork.setVisibility(View.VISIBLE);
			Toast.makeText(getApplicationContext(), getResources().getString(R.string.wEndMsg)+" "+userMaster.getStrFirstName()+" "+userMaster.getStrLastName(), Toast.LENGTH_LONG).show();
		}
		}

		if (CommonUtil.getTag(getApplicationContext()).equalsIgnoreCase("first")) {
			// CommonUtil.setTag(getApplicationContext(), "second");
		}

		resolveIntent(getIntent());

		mDialog = new AlertDialog.Builder(this).setNeutralButton("Ok", null)
				.create();

		mAdapter = NfcAdapter.getDefaultAdapter(this);
		if (mAdapter == null) {
			showMessage(R.string.error, R.string.no_nfc);
			finish();
			return;
		}

		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		mNdefPushMessage = new NdefMessage(new NdefRecord[] { newTextRecord(
				"Message from NFC Reader :-)", Locale.ENGLISH, true) });
	}

	private void showMessage(int title, int message) {
		mDialog.setTitle(title);
		mDialog.setMessage(getText(message));
		mDialog.show();
	}

	private NdefRecord newTextRecord(String text, Locale locale,
			boolean encodeInUtf8) {
		byte[] langBytes = locale.getLanguage().getBytes(
				Charset.forName("US-ASCII"));

		Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset
				.forName("UTF-16");
		byte[] textBytes = text.getBytes(utfEncoding);

		int utfBit = encodeInUtf8 ? 0 : (1 << 7);
		char status = (char) (utfBit + langBytes.length);

		byte[] data = new byte[1 + langBytes.length + textBytes.length];
		data[0] = (byte) status;
		System.arraycopy(langBytes, 0, data, 1, langBytes.length);
		System.arraycopy(textBytes, 0, data, 1 + langBytes.length,
				textBytes.length);

		return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT,
				new byte[0], data);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdapter != null) {
			if (!mAdapter.isEnabled()) {
				showWirelessSettingsDialog();
			}
			mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
			mAdapter.enableForegroundNdefPush(this, mNdefPushMessage);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mAdapter != null) {
			mAdapter.disableForegroundDispatch(this);
			mAdapter.disableForegroundNdefPush(this);
		}
	}

	private void showWirelessSettingsDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.nfc_disabled);
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogInterface, int i) {
						Intent intent = new Intent(
								Settings.ACTION_WIRELESS_SETTINGS);
						startActivity(intent);
					}
				});
		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogInterface, int i) {
						finish();
					}
				});
		builder.create().show();
		return;
	}

	private void makeEntryInUsrNFC(String userId, String rank, String cardType) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date curDate = new Date();
		DatabaseSupport db = new DatabaseSupport(this);

		List<UserServiceTermRole> serviceTermRoleData = new ArrayList<UserServiceTermRole>();
		serviceTermRoleData = db.getUserServiceTermRoleRowBiId(userId,
				df.format(curDate));
		if (serviceTermRoleData != null && serviceTermRoleData.size() > 0) {

			String roleName = db.getRoleName(serviceTermRoleData.get(0)
					.getiRoleId());

			if (userId
					.equalsIgnoreCase(serviceTermRoleData.get(0).getIuserId())
					&& rank.equalsIgnoreCase(roleName)) {
				Integer cardTypeOnCard = null;
				Integer cardTypeOnDB = serviceTermRoleData.get(0).getField2();
				try {
					cardTypeOnCard = Integer.getInteger(rank);
				} catch (Exception e) {
					e.printStackTrace();
				}

				if (cardTypeOnCard == null || cardTypeOnDB == null
						|| cardTypeOnCard.intValue() == cardTypeOnDB.intValue()) {

					if (CommonUtil.getShipId(getApplicationContext()) == null
							|| CommonUtil.getShipId(getApplicationContext())
									.equalsIgnoreCase("1")) {
						CommonUtil.setShipID(getApplicationContext(),
								serviceTermRoleData.get(0).getiShipId());
						CommonUtil.setTenantID(getApplicationContext(),
								serviceTermRoleData.get(0).getiTenantId());
					}

					/**
					 * Above code comment due to card id use as primary key which is duplicate.
					 * So change pk format and cardid use in iuserservicetermid 
					 */
					String pk = CommonUtil.getId(getApplicationContext())+"_"+curDate.getTime();
					
					db.addUserNFCRow(new UserNfcTagData(pk, "User", userId, CommonUtil
							.getId(getApplicationContext()), "1", "0", df.format(curDate), df.format(curDate), CommonUtil
							.getTenantId(getApplicationContext()), CommonUtil
							.getShipId(getApplicationContext())));
				}
			}
		}
	}

	private void resolveIntent(Intent intent) {
		String action = intent.getAction();
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
				|| NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
				|| NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
			Parcelable[] rawMsgs = intent
					.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			NdefMessage[] msgs;
			DatabaseSupport db = new DatabaseSupport(getApplicationContext());
			List<UserNfcTagData> nfcDataList = new ArrayList<UserNfcTagData>();
			nfcDataList = db.getUserNFCRow();
			/*List<ShipMaster> listShipMaster = new ArrayList<ShipMaster>();
			listShipMaster = db.getShipMasterRow();*/

			Log.i("TheArkNFCCARD", "listShipMaster :" + nfcDataList);
			Log.i("TheArkNFCCARD", "rawMsgs :" + rawMsgs);

			if ((nfcDataList != null && nfcDataList.size() > 0)
					|| (rawMsgs != null && !"".equals(rawMsgs))) {

				// IF TAG IS NOT NULL |
				String authMessage = "";
				String authMessagePrv = "";
				if (rawMsgs != null) {
					Parcelable tag = intent
							.getParcelableExtra(NfcAdapter.EXTRA_TAG);
					Tag tag1 = (Tag) tag;
					byte[] id = tag1.getId();
					ids = getDec(id);
					CommonUtil.setId(getApplicationContext(), String.valueOf(ids));

					msgs = new NdefMessage[rawMsgs.length];
					for (int i = 0; i < rawMsgs.length; i++) {

						msgs[i] = (NdefMessage) rawMsgs[i];
					}
					String m = getMessage(msgs);

					System.out.println("Message write on card : " + m);

					if (m != null && m.startsWith("en")) {
						//m = m.substring(3, m.length() - 1);
						m = m.substring(3, m.length());
					}

					String strRoleNameArr[] = null;
					Integer cardTypeOnCard = null;
					String userIdOnCard = "";
					if (m != null && !"".equals(m)) {
						strRoleNameArr = m.split("#");
						userIdOnCard = strRoleNameArr[0];
					}

					Log.i("TheArkNFCCARD", "Card Value :" + m);

					ArrayList<UserNfcTagData> nfcRecord = db
							.getUserNFCSingleRowByUserId(userIdOnCard);

					if (nfcRecord != null && nfcRecord.size() > 0) {
						if (strRoleNameArr != null
								&& strRoleNameArr.length >= 3) {
							makeEntryInUsrNFC(strRoleNameArr[0],
									strRoleNameArr[1], strRoleNameArr[2]);
						}
						nfcRecord = db.getUserNFCSingleRow(String.valueOf(ids));
					} else {

					}

					boolean registerUser = false;

					L.fv("user id on card is "+userIdOnCard);
					if (userIdOnCard != null && !"".equals(userIdOnCard)
							&& m != null && !"".equalsIgnoreCase(m)
							&& !m.equalsIgnoreCase("ShipAdmin")) {
						/**
						 * write here authorization code.
						 */

						Date curDate = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						List<UserServiceTermRole> serviceTermRoleData = new ArrayList<UserServiceTermRole>();
						serviceTermRoleData = db.getUserServiceTermRoleRowBiId(
								userIdOnCard, format.format(curDate));
						L.fv("serviceTermRoleData is "+serviceTermRoleData);
						if (serviceTermRoleData != null
								&& serviceTermRoleData.size() > 0) {

							// String strRoleNameArr[]= m.split(" / ");

							L.fv("serviceTermRoleData of ship is "+serviceTermRoleData.get(0).getiShipId());
							
							String roleName = db
									.getRoleName(serviceTermRoleData.get(0)
											.getiRoleId());
							L.fv("role name is "+roleName);
							if (roleName != null
									&& strRoleNameArr != null
									&& strRoleNameArr.length >= 3
									&& roleName.trim().equalsIgnoreCase(
											strRoleNameArr[1].trim())) {

								// if(roleName.trim().equalsIgnoreCase(strRoleNameArr[1].trim())){
								L.fv("role name is "+roleName);
								
								List<RoleTabletFunction> trfList = new ArrayList<RoleTabletFunction>();
								trfList = db
										.getTabletRoleFunctionsRowByRoleIdAndFunctionCode(
												db.getUserMasterSingleRow(
														userIdOnCard).get(0)
														.getiRoleId(),
												"register");

								registerUser = true;
								/**
								 * below comment open when service is ready
								 */
								/*if (trfList != null && trfList.size() > 0) {
									registerUser = true;
								} else {
									authMessagePrv = "";
									authMessage = "You are not authorized to view this screen. Please login with a different card to access this screen.";
								}*/

								CommonUtil.setUserId(getApplicationContext(),
										userIdOnCard);
								CommonUtil.setUserForTurbo(
										getApplicationContext(), userIdOnCard);

								CommonUtil.setTenantID(
										getApplicationContext(),
										(serviceTermRoleData.get(0)
												.getiTenantId() != null ? serviceTermRoleData
												.get(0).getiTenantId() : "1"));
								CommonUtil.setShipID(
										getApplicationContext(),
										(serviceTermRoleData.get(0)
												.getiShipId() != null ? serviceTermRoleData
												.get(0).getiShipId() : "1"));

							} else {
								authMessagePrv = "This card is registered for ";
								authMessage = ". Please register with  a fresh card if you have been promoted.";
							}
						} else {
							authMessagePrv = "There is no valid service term for ";
							authMessage = " on this device. In case this crew is on board please update The Ark system by editing his service term or adding him as a new crew. Synchronize this device after such an update to login with this card.";
						}
					} else if (!m.equalsIgnoreCase("ShipAdmin")) {

						authMessagePrv = "";
						authMessage = "This card needs to be initialized through the card registration process before being used.";

					}
					if (m != null && !"".equalsIgnoreCase(m)
							&& m.equalsIgnoreCase("ShipAdmin") || registerUser) {

						/*db = new DatabaseSupport(this);
						List<UserNfcTagData> data = db.getUserNFCRow();
						db.close();
						List<ShipMaster> sm = db.getShipMasterRow();
						List<TenantData> tn = db.getTenantRow();
						if (data != null && data.size() > 0) {
							String shipId = "1";
							String tenantId = "1";
							if (sm != null && sm.size() > 0) {
								if (data.get(0).getiShipId() != null
										&& "1".equalsIgnoreCase(data.get(0)
												.getiShipId())) {
									shipId = sm.get(0).getiShipId();
								}
								if (data.get(0).getiTenantId() != null
										&& "1".equalsIgnoreCase(data.get(0)
												.getiTenantId())) {
									tenantId = tn.get(0).getiTenantId();
								}
							}
							CommonUtil.setTenantID(getApplicationContext(),
									tenantId);
							CommonUtil.setShipID(getApplicationContext(), shipId);
						}
*/						/*
						 * Toast.makeText(getApplicationContext(), "Msg1 :" + m,
						 * Toast.LENGTH_LONG).show();
						 */
						if(registerUser){
							Intent i = new Intent(WelcomeAdmin.this,
									WorkTimesheetActivity.class);
							 i.putExtra("userName", "Ripunjay Shukla");
							 i.putExtra("userId", userIdOnCard);
							 i.putExtra("role", "Ripunjay");
							startActivity(i);
							finish();
						}else{
							 Intent i = new Intent(this,  AdminMainActivity.class); 
		            		 i.putExtra("name", "Ripunjay");
		            		 startActivity(i);
		            		 finish();
							
						}		
						
						
					} else {
						/*
						 * Toast.makeText(getApplicationContext(),
						 * "You are not authorize for this screen.",
						 * Toast.LENGTH_LONG).show();
						 */

						if (nfcDataList != null && nfcDataList.size() > 0
								&& m != null && !"".equalsIgnoreCase(m)) {
							
							Intent i = new Intent(this, AuthActivity.class);
							i.putExtra("userId", "userid");
							if (m != null && !"".equalsIgnoreCase(m)
									&& !"".equalsIgnoreCase(authMessagePrv)) {
								i.putExtra("nameAndRole", strRoleNameArr[3]
										+ " / " + strRoleNameArr[1]);
							} else {
								i.putExtra("nameAndRole", "");
							}
							i.putExtra("strAuthMessagePrv", authMessagePrv);
							i.putExtra("strAuthMessage", authMessage);
							startActivity(i);
							finish();
						} else if(m != null && !"".equalsIgnoreCase(m)){
							
							Intent i = new Intent(this, AuthActivity.class);
							i.putExtra("userId", "userid");
							if (m != null && !"".equalsIgnoreCase(m)
									&& !"".equalsIgnoreCase(authMessagePrv)) {
								i.putExtra("nameAndRole", strRoleNameArr[3]
										+ " / " + strRoleNameArr[1]);
							} else {
								i.putExtra("nameAndRole", "");
							}
							i.putExtra("strAuthMessagePrv", authMessagePrv);
							i.putExtra("strAuthMessage", authMessage);
							startActivity(i);
							finish();
						}
						else{
							
							byte[] empty = new byte[0];
							byte[] id1 = intent
									.getByteArrayExtra(NfcAdapter.EXTRA_ID);
							Parcelable tag2 = intent
									.getParcelableExtra(NfcAdapter.EXTRA_TAG);
							Tag tag3 = (Tag) tag2;
							byte[] ida = tag3.getId();
							ids = getDec(ida);
							// Toast.makeText(getApplicationContext(), "ID :" +
							// ids,
							// Toast.LENGTH_LONG).show();

							Intent i = new Intent(WelcomeAdmin.this,
									CreateAdminActivity.class);
							startActivity(i);
							finish();
						}
					}
				} else {
					Log.i("TheArkNFCCARD else part ", "nfcDataList :"
							+ nfcDataList);
					Log.i("TheArkNFCCARD else part ", "rawMsgs :" + rawMsgs);

					if ((nfcDataList != null && nfcDataList.size() > 0)) {

						Log.i("TheArkNFCCARD else part size ",
								"listShipMaster :" + nfcDataList.size());
						authMessagePrv = "";

						authMessage = "This card needs to be initialized through the card registration process before being used.";
						Intent i = new Intent(this, AuthActivity.class);
						i.putExtra("userId", "userid");
						i.putExtra("nameAndRole", "");
						i.putExtra("strAuthMessagePrv", authMessagePrv);
						i.putExtra("strAuthMessage", authMessage);
						startActivity(i);
						finish();
					} else {
						Log.i("TheArkNFCCARD else part of else create admin",
								"rawMsgs :" + rawMsgs);
						byte[] empty = new byte[0];
						byte[] id = intent
								.getByteArrayExtra(NfcAdapter.EXTRA_ID);
						Parcelable tag = intent
								.getParcelableExtra(NfcAdapter.EXTRA_TAG);
						Tag tag1 = (Tag) tag;
						byte[] ida = tag1.getId();
						ids = getDec(ida);
						// Toast.makeText(getApplicationContext(), "ID :" + ids,
						// Toast.LENGTH_LONG).show();

						Intent i = new Intent(WelcomeAdmin.this,
								CreateAdminActivity.class);
						startActivity(i);
						finish();
					}
				}

			} else {

				byte[] empty = new byte[0];
				byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
				Parcelable tag = intent
						.getParcelableExtra(NfcAdapter.EXTRA_TAG);
				Tag tag1 = (Tag) tag;
				byte[] ida = tag1.getId();
				ids = getDec(ida);
				// Toast.makeText(getApplicationContext(), "ID :" + ids,
				// Toast.LENGTH_LONG).show();

				Intent i = new Intent(WelcomeAdmin.this,
						CreateAdminActivity.class);
				startActivity(i);
				finish();

			}
		}
	}

	private String getMessage(NdefMessage[] msgs) {
		// TODO Auto-generated method stub
		String tags = "";
		if (msgs == null || msgs.length == 0) {
			return "";
		}

		NdefRecord[] records = msgs[0].getRecords();
		for (final NdefRecord record : records) {
			tags = new String(record.getPayload());
		}
		return tags.toString();
	}

	private long getDec(byte[] bytes) {
		long result = 0;
		long factor = 1;
		for (int i = 0; i < bytes.length; ++i) {
			long value = bytes[i] & 0xffl;
			result += value * factor;
			factor *= 256l;
		}
		return result;
	}

	/*
	 * void buildTagViews(NdefMessage[] msgs) { if (msgs == null || msgs.length
	 * == 0) { return; } LayoutInflater inflater = LayoutInflater.from(this);
	 * LinearLayout content = mTagContent;
	 * 
	 * // Parse the first message in the list // Build views for all of the sub
	 * records Date now = new Date(); List<ParsedNdefRecord> records =
	 * NdefMessageParser.parse(msgs[0]); final int size = records.size(); for
	 * (int i = 0; i < size; i++) { TextView timeView = new TextView(this);
	 * timeView.setText(TIME_FORMAT.format(now)); content.addView(timeView, 0);
	 * ParsedNdefRecord record = records.get(i);
	 * content.addView(record.getView(this, inflater, content, i), 1 + i);
	 * content.addView(inflater.inflate(R.layout.tag_divider, content, false), 2
	 * + i); } }
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
	}

	@Override
	public void onNewIntent(Intent intent1) {
		setIntent(intent1);
		resolveIntent(intent1);
	}
}
