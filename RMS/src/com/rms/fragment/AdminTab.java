package com.rms.fragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import android.app.AlarmManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rms.AdminMainActivity;
import com.rms.LoginActivity;
import com.rms.R;
import com.rms.SyncHandler;
import com.rms.adapter.TimeZoneComperator;
import com.rms.adapter.TimeZoneSpinnerAdapter;
import com.rms.db.DatabaseSupport;
import com.rms.model.SyncStatus;
import com.rms.model.TimeZoneData;
import com.rms.model.TimeZoneItem;
import com.rms.utils.CommonUtil;

public class AdminTab extends Fragment {

	Context mContext;
	TextView appVersion;
	TextView wsdlPreUrl, wrongWsdl, lastSyncDate, timeZone;
	Button exportData, syncData, syncMasterData, setTimeZoneButton;
	View switchServerSepetor,wipeDataSeperator,exportDataSeperator;
	LinearLayout wsdlUrlLayout;
	LinearLayout changeServerLayout,exportDataLayout,wipeDataLayout,appVersionDataLayout;
	Button changeSoapUrl;
	EditText newWsdlUrl;
	Button saveWsdlUrl, resetButton;
	private static Spinner taskList;
	private static List<TimeZoneItem> timeZoneList;
	private static int selectedTimeZoneIndex;
	
	private int counter = 0;
	private long oldTime = 0;
	private long newTime = 0;


	// Sync Info 0->date , 1->mode , 2->serverAddress
	String syncInfo[] = new String[3];

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.admin_tab, container, false);
		mContext = getActivity();

		exportData = (Button) rootView.findViewById(R.id.exportData);
		syncData = (Button) rootView.findViewById(R.id.syncData);
		syncMasterData = (Button) rootView.findViewById(R.id.syncMasterData);
		appVersion = (TextView) rootView.findViewById(R.id.appVerId);
		changeSoapUrl = (Button) rootView.findViewById(R.id.changeSoapUrl);
		newWsdlUrl = (EditText) rootView.findViewById(R.id.wsdlUrl);
		switchServerSepetor = (View) rootView
				.findViewById(R.id.changeServerSeperator);
		changeServerLayout = (LinearLayout) rootView
				.findViewById(R.id.changeServerLayout);
		
		exportDataLayout = (LinearLayout) rootView
				.findViewById(R.id.exportDataLayout);
		
		wipeDataLayout = (LinearLayout) rootView
				.findViewById(R.id.wipeDataLayout);
		
		
		switchServerSepetor = (View) rootView
				.findViewById(R.id.changeServerSeperator);
		
		wipeDataSeperator = (View) rootView
				.findViewById(R.id.wipeDataSeperator);
		
		exportDataSeperator = (View) rootView
				.findViewById(R.id.exportDataSeperator);
		
		appVersionDataLayout = (LinearLayout) rootView
				.findViewById(R.id.appVersionDataLayout);
		
		wrongWsdl = (TextView) rootView.findViewById(R.id.wrongWsdlId);
		saveWsdlUrl = (Button) rootView.findViewById(R.id.saveWsdlUrl);
		resetButton = (Button) rootView.findViewById(R.id.wipeDataButton);
		wsdlUrlLayout = (LinearLayout) rootView.findViewById(R.id.wsdlLayer);
		wsdlPreUrl = (TextView) rootView.findViewById(R.id.preWsdlUrl);

		lastSyncDate = (TextView) rootView.findViewById(R.id.lastSyncDateId);
		setTimeZoneButton = (Button) rootView
				.findViewById(R.id.setTimeZoneButton);
		timeZone = (TextView) rootView.findViewById(R.id.prevTimeZone);

		timeZone.setText(getTimeZone());
		if (timeZoneList == null || timeZoneList.size() <= 0) {
			timeZoneList = getTimeZoneList();
		}
		selectedTimeZoneIndex = getSelectedTimeZoneIndex();

		/**
		 * This is return array of sync status.
		 */
		syncInfo = getSyncInfo();

		wsdlPreUrl.setText(syncInfo[2]);
		String dtArray[] = syncInfo[0].split("-");
		lastSyncDate.setText(dtArray[2] + " "
				+ CommonUtil.getMonth(Integer.parseInt(dtArray[1])) + " "
				+ dtArray[0]);

		PackageInfo pInfo = null;
		String version = "";
		try {
			pInfo = mContext.getPackageManager().getPackageInfo(
					mContext.getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		if (pInfo != null && pInfo.versionName != null) {
			version = pInfo.versionName;
		} else {
			version = "1.5.8";
		}

		appVersion.setText(version);

		exportData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				exportDB();
			}
		});

		syncData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				boolean isReady = CommonUtil.checkConnectivity(mContext);
				if (isReady) {
					AdminMainActivity.miActionProgressItem.setVisible(true);
					CommonUtil.setUserLogin(mContext, "0");
					SyncHandler.context = getActivity();
					Message shipMessage = new Message();
					shipMessage.what = SyncHandler.MSG_SEND_SYNC_HISTORY;
					SyncHandler.handler.sendMessage(shipMessage);
				}

				// CommonUtil.scheduleProcess(mContext);

			}
		});
		
		counter=0;
		
		appVersionDataLayout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				counter++;
				if (counter == 6) {
					
					changeServerLayout.setVisibility(View.VISIBLE);
					switchServerSepetor.setVisibility(View.VISIBLE);
					exportDataLayout.setVisibility(View.VISIBLE);
					exportDataSeperator.setVisibility(View.VISIBLE);
					wipeDataLayout.setVisibility(View.VISIBLE);
					wipeDataSeperator.setVisibility(View.VISIBLE);					
				}
				newTime = new Date().getTime();
				if (oldTime == 0 || counter == 1) {
					oldTime = newTime;
				}
				if ((newTime - oldTime) > 5000) {
					counter = 0;
				} else {
					oldTime = newTime;
				}
			}
		});
		

		syncMasterData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				boolean isReady = CommonUtil.checkConnectivity(mContext);
				if (isReady) {
					AdminMainActivity.miActionProgressItem.setVisible(true);

					SyncHandler.context = getActivity();
					Message shipMessage = new Message();
					shipMessage.what = SyncHandler.MSG_GET_SHIPMASTER;
					SyncHandler.handler.sendMessage(shipMessage);
				}

			}
		});

		changeSoapUrl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (wsdlUrlLayout.getVisibility() == View.GONE)
					wsdlUrlLayout.setVisibility(View.VISIBLE);
				else
					wsdlUrlLayout.setVisibility(View.GONE);

				wrongWsdl.setVisibility(View.GONE);
			}
		});

		saveWsdlUrl.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				String preUrl = getResources().getString(R.string.preUrl);
				String urlPort = getResources().getString(R.string.portUrl);
				String postUrl = getResources().getString(R.string.postUrl);

				CommonUtil.setServerAddress(mContext, "");
				String url = newWsdlUrl.getText().toString();
				if (url != null && !"".equals(url.trim())) {
					if (url != null && !"".equals(url)
							&& !url.contains("TheArk-TheArk")) {
						if (!url.contains(preUrl))
							url = preUrl + url;
						if (url.contains("thearkbis.com")) {
							// urlPort = "80";
						} else {
							urlPort = getResources()
									.getString(R.string.portUrl);
							;
							if (!url.contains(urlPort))
								url = url + ":" + urlPort;
						}

						url = url + postUrl;
					}
					CommonUtil.setServerAddress(mContext, url);
					CommonUtil.updateServerAddress(mContext, url);
					;
					wsdlUrlLayout.setVisibility(View.GONE);

					wsdlPreUrl.setText(url);

					if (CommonUtil.getServerAddress(mContext) != null
							&& !"".equals(CommonUtil.getServerAddress(mContext))) {
						wsdlPreUrl.setText(CommonUtil
								.getServerAddress(mContext).toString());
					}

					newWsdlUrl.setText("");

				} else {
					wrongWsdl.setVisibility(View.VISIBLE);
				}
			}
		});

		setTimeZoneButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				launchPopUpForSelectTimeZone(mContext);
			}
		});

		resetButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				displayWarningForResetData(mContext.getResources().getString(
						R.string.resetMsg));

			}
		});

		return rootView;
	}

	/**
	 * @author pushkar.m
	 * @param tz
	 * @return
	 */
	private static String getTimeZoneName(TimeZone tz) {

		long hours = TimeUnit.MILLISECONDS.toHours(tz.getRawOffset());
		long minutes = TimeUnit.MILLISECONDS.toMinutes(tz.getRawOffset())
				- TimeUnit.HOURS.toMinutes(hours);
		// avoid -4:-30 issue
		minutes = Math.abs(minutes);

		String result = "";
		if (hours > 0) {
			result = String.format("(GMT+%d:%02d) %s", hours, minutes,
					tz.getID());
		} else {
			result = String.format("(GMT%d:%02d) %s", hours, minutes,
					tz.getID());
		}

		return result;

	}

	/**
	 * Ripunjay.S
	 * 
	 * @param fileOrDirectory
	 */
	private void exportDB() {
		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();
			FileChannel source = null;
			FileChannel destination = null;
			String currentDBPath = "/data/" + mContext.getPackageName()
					+ "/databases/" + DatabaseSupport.DATABASE_NAME;

			StringBuffer sb = new StringBuffer(
					Environment.getExternalStorageDirectory() + "/TheArk/");
			File file = new File(sb.toString());
			if (!file.exists()) {
				file.mkdirs();
			}
			String backupDBPath = DatabaseSupport.DATABASE_NAME;
			File currentDB = new File(data, currentDBPath);
			File backupDB = new File(file, backupDBPath);
			try {
				source = new FileInputStream(currentDB).getChannel();
				destination = new FileOutputStream(backupDB).getChannel();
				destination.transferFrom(source, 0, source.size());
				source.close();
				destination.close();
				Toast.makeText(mContext, "DB Exported!", Toast.LENGTH_LONG)
						.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author pushkar.m This method return admin info like lastSyncDate ,
	 *         serverAddress , accessMode from Database
	 */
	private String[] getSyncInfo() {
		String[] syncInfo = new String[4];
		DatabaseSupport db = new DatabaseSupport(mContext);

		List<SyncStatus> syncStatusList = db.getSyncStatusData();

		if (syncStatusList != null && syncStatusList.size() > 0) {

			SyncStatus syncStatus = syncStatusList.get(0);

			syncInfo[0] = syncStatus.getDtSyncDate();
			syncInfo[1] = syncStatus.getSyncMode();
			syncInfo[2] = syncStatus.getServerAddress();
			syncInfo[3] = syncStatus.getDataSyncMode();
		} else {

			syncInfo[0] = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			syncInfo[1] = mContext.getResources().getString(
					R.string.defultAccessMode);
			syncInfo[2] = mContext.getResources().getString(
					R.string.defultServiceUrl);
			syncInfo[3] = mContext.getResources().getString(
					R.string.defultDataSyncMode);
		}
		db.close();
		return syncInfo;
	}

	/**
	 * @author pushkar.m return time zone
	 */

	private String getTimeZone() {
		String timeZone = "";
		DatabaseSupport db = new DatabaseSupport(mContext);
		List<TimeZoneData> timeZoneDataList = db
				.getTimeZoneDataByShipId(Integer.parseInt(CommonUtil
						.getShipId(mContext)));
		db.close();

		if (timeZoneDataList != null && timeZoneDataList.size() > 0) {
			timeZone = timeZoneDataList.get(0).getStrTimeZoneName();
		} else {
			Calendar cal = Calendar.getInstance();
			TimeZone tz = cal.getTimeZone();

			timeZone = getTimeZoneName(tz);
		}

		return timeZone;
	}

	/**
	 * @author pushkar.m
	 * @return
	 */
	private List<TimeZoneItem> getTimeZoneList() {

		String[] tzArray = TimeZone.getAvailableIDs();
		List<TimeZoneItem> timeZoneList = new ArrayList<TimeZoneItem>();

		for (String id : tzArray) {
			TimeZoneItem timeZoneItem = new TimeZoneItem(id,
					getTimeZoneName(TimeZone.getTimeZone(id)),
					TimeZone.getTimeZone(id));
			timeZoneList.add(timeZoneItem);
		}

		Collections.sort(timeZoneList, new TimeZoneComperator());
		return timeZoneList;
	}

	/**
	 * @author pushkar.m
	 * @return
	 */
	private int getSelectedTimeZoneIndex() {
		int index = 0;
		for (TimeZoneItem tdi : timeZoneList) {
			if (tdi.getName().equalsIgnoreCase(getTimeZone())) {
				break;
			}
			index++;
		}
		return index;
	}

	/**
	 * @author pushkar.m
	 * @param context
	 */
	private void launchPopUpForSelectTimeZone(Context context) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.timezone_confirmation_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancelButton = (Button) dialog.findViewById(R.id.cancel);
		Button okButton = (Button) dialog.findViewById(R.id.ok);

		taskList = (Spinner) dialog.findViewById(R.id.TimeZoneList);

		if (timeZoneList != null && timeZoneList.size() > 0) {
			TimeZoneSpinnerAdapter spin = new TimeZoneSpinnerAdapter(
					timeZoneList, context);
			taskList.setAdapter(spin);

			taskList.setSelection(selectedTimeZoneIndex, true);
			crossButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});

			cancelButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			okButton.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {

					TimeZoneItem tzi = (TimeZoneItem) taskList
							.getSelectedItem();

					// set selected time zone in device
					AlarmManager am = (AlarmManager) mContext
							.getSystemService(Context.ALARM_SERVICE);
					am.setTimeZone(tzi.getId());

					// persist timezone data in db

					DatabaseSupport db = new DatabaseSupport(mContext);
					Date curDate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

					Calendar calander = Calendar.getInstance();
					DateFormat simpleDateFormat = new SimpleDateFormat(
							"HH:mm:ss");

					String time = simpleDateFormat.format(calander.getTime());

					List<TimeZoneData> timeZoneDataList = db
							.getTimeZoneDataByShipId(Integer
									.parseInt(CommonUtil.getShipId(mContext)));

					if (timeZoneDataList != null && timeZoneDataList.size() > 0) {
						TimeZoneData timeZoneData = timeZoneDataList.get(0);
						timeZoneData.setStrTimeZoneName(getTimeZoneName(tzi
								.getTimeZone()));
						timeZoneData.setDtUpdated(format.format(curDate));
						timeZoneData.setUpdatedBy(CommonUtil
								.getUserId(mContext));
						timeZoneData.setStrZoneId(tzi.getId());
						timeZoneData.setStrTime(time);
						db.updateTimeZoneTable(timeZoneData);
					} else {
						TimeZoneData timeZoneData = new TimeZoneData();
						timeZoneData.setCreatedBy(CommonUtil
								.getUserId(mContext));
						timeZoneData.setDtCreated(format.format(curDate));
						timeZoneData.setDtUpdated(format.format(curDate));
						timeZoneData.setFlgDeleted(0);
						timeZoneData.setFlgIsDirty(1);
						timeZoneData.setiShipId(Integer.parseInt(CommonUtil
								.getShipId(mContext)));
						timeZoneData.setiTenantId(Integer.parseInt(CommonUtil
								.getTenantId(mContext)));
						timeZoneData.setiTimeZoneDataId(CommonUtil
								.getIMEI(mContext) + "_" + curDate.getTime());
						timeZoneData.setStrDescription("");
						timeZoneData.setStrTime(time);
						timeZoneData.setStrTimeZoneName(getTimeZoneName(tzi
								.getTimeZone()));
						timeZoneData.setStrZoneId(tzi.getId());
						timeZoneData.setUpdatedBy(CommonUtil
								.getUserId(mContext));

						db.insertTimeZoneDataTable(timeZoneData);
					}

					db.close();
					timeZone.setText(getTimeZoneName(tzi.getTimeZone()));
					dialog.dismiss();

				}
			});

			dialog.show();
			dialog.setCanceledOnTouchOutside(false);
			dialog.setCancelable(false);
		}
	}

	/**
	 * @author pushkar.m
	 */
	public void displayWarningForResetData(String message) {

		final Dialog dialog = new Dialog(mContext);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_reset_confirmation_popup);
		Button crossButton = (Button) dialog
				.findViewById(R.id.cancelCrossButton);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog
				.findViewById(R.id.cancelDialogMessage);
		textView.setText(message);
		Button ok = (Button) dialog.findViewById(R.id.cancelYes);

		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				DatabaseSupport db = new DatabaseSupport(mContext);
				db.updateDataBase();
				db.close();

				Intent intent = new Intent(getActivity(), LoginActivity.class);
				startActivity(intent);
				getActivity().finish();
			}
		});
		Button cancel = (Button) dialog.findViewById(R.id.cancelNo);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);

	}

}
