package com.rms.fragment;

import com.rms.AdminMainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class AdminPassingFragment extends Fragment {
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		 Intent i = new Intent(getActivity(),  AdminMainActivity.class); 
		 i.putExtra("name", "Ripunjay");
		 startActivity(i);
		 getActivity().finish();
		return null;
	}

}
