package com.rms.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rms.R;
import com.rms.UserCardRegistration;
import com.rms.adapter.UserListAdapter;
import com.rms.db.DatabaseSupport;
import com.rms.model.UserMaster;
import com.rms.model.UserNfcTagData;
import com.rms.model.UserSelectItem;
import com.rms.model.UserServiceTermRole;

public class AdminRegisterTab extends Fragment {

	
	ListView lv;
    AlertDialog alertDialog ;
	
    ImageView   imageView;
    TextView userName,role;
    private boolean registerUser = false;
    
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.admin_register_tab, container, false);
	
		lv = (ListView) rootView.findViewById(R.id.userList);
		registerUser = false;
		DatabaseSupport db1 = new DatabaseSupport(getActivity());
		
		ArrayList<UserMaster> list1 = db1.getUserMasterRow();

		final ArrayList<UserSelectItem> userlist = new ArrayList<UserSelectItem>();
		 for (int i = 0; i < list1.size(); i++) {
			 
				Date curDate = new Date();
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");				
				List<UserServiceTermRole> serviceTermRoleData = new ArrayList<UserServiceTermRole>();
					
				/**
				 * 1.5.1
				 */
				//serviceTermRoleData = db1.getUserServiceTermRoleRowBiId(list1.get(i).getiUserId(),format.format(curDate));
				
				/**
				 * 1.5.6
				 */
				serviceTermRoleData = db1.getUserServiceTermRoleRowBiIdForregisteruser(list1.get(i).getiUserId(),format.format(curDate));
				
				if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
					
					 String roleid = list1.get(i).getiRoleId();
					 String tenantId = list1.get(i).getiTenantId();
					 String name = list1.get(i).getStrFirstName() +" "+list1.get(i).getStrLastName() +" / " +db1.getRoleName(roleid) ;
				     userlist.add(new UserSelectItem(name, list1.get(i).getiUserId(),tenantId));
				}
				
			 
		 }

		 userlist.add(0, new UserSelectItem(getResources().getString(R.string.registerUserTxt), "1234","17"));
		
	//	 ArrayAdapter<UserSelectItem> myAdapter = new ArrayAdapter<UserSelectItem>(getActivity(), android.R.layout.simple_spinner_item, userlist);
		
		 UserListAdapter spin  =new UserListAdapter(userlist, getActivity());
         lv.setAdapter(spin);
         
         lv.setOnItemClickListener(new OnItemClickListener() 
         {
         @Override
         public void onItemClick(AdapterView<?> parent, View view,
 					int position, long id) {

 				// ListView Clicked item index
 				int itemPosition = position;
 				
 				imageView=((ImageView)view.findViewById(R.id.select_icon));
				userName=((TextView)view.findViewById(R.id.spinnerUsername));
				role=((TextView)view.findViewById(R.id.spinnerUserRank));
				
				
 				if (position > 0) {
 					
 					
	                String userId = "";
	            	String nameAndRole = "";
			    	UserSelectItem mydata;
	                mydata = (UserSelectItem)userlist.get(position);
	                userId =  mydata.getUserId();
	                nameAndRole =  mydata.getText();
	                
	        		imageView.setVisibility(View.VISIBLE);
					imageView.setImageResource(R.drawable.select_icon);
					imageView.invalidate();
					
					userName.setTextColor(getResources().getColor(R.color.white));
				    role.setTextColor(getResources().getColor(R.color.white));
				    
	                view.setBackgroundColor(Color.parseColor("#06dc1a"));
	                
	                DatabaseSupport db = new DatabaseSupport(getActivity());
	    			ArrayList<UserNfcTagData> data = db.getUserNFCSingleRowByUserId(userId);
	    			Log.i("NFC", "Name and rank" + nameAndRole);
	    			// added by Vibhore
    		        String split[] = nameAndRole.split("/");
    		        Intent intent = new Intent(getActivity() , UserCardRegistration.class);
    		        intent.putExtra("userId", userId);
    		        intent.putExtra("nameAndRole", nameAndRole);
    		        Log.i("NFC", "Name and rank Split" + split[0].trim() + split[1].trim());
    		        if(split.length>0){
    		        	intent.putExtra("name", split[0].trim());
    		        	intent.putExtra("rank", split[1].trim());
    		        }
    		        
	    			if(data != null && data.size() > 0){
	    				showAlertDialogBox(view ,userId , nameAndRole);
	    			}else{
	    		        startActivity(intent);
	    		        getActivity().finish();
	    			}
 				
 				
 				}
         
         }

		private void showAlertDialogBox(final View view, final String userId, final String nameAndRole) {
			// TODO Auto-generated method stub
			
			registerUser = false;
			DatabaseSupport db = new DatabaseSupport(getActivity());
			ArrayList<UserNfcTagData> data = db.getUserNFCSingleRowByUserId(userId);
			StringBuffer strBuf = new StringBuffer("Do you want register card for user "+nameAndRole + "?");
			String title = "User Alert Box";
			if(data != null && data.size() > 0){
				registerUser = true;
				strBuf = new StringBuffer("A card has already been registered to  "+ nameAndRole +". Do you want to register a duplicate card to him?");
				title = "Duplicate Card Registration";
			}
			else{
				registerUser = false;
			}
			db.close();
			
			
			alertDialog = new AlertDialog.Builder(getActivity())
		    .setTitle(title)
		    .setMessage(strBuf.toString())
		    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
			        Intent intent = new Intent(getActivity() , UserCardRegistration.class);
			        intent.putExtra("userId", userId);
			        intent.putExtra("nameAndRole", nameAndRole);
			        String split[] = nameAndRole.split("/");
			        if(split.length>0){
			        	intent.putExtra("name", split[0].trim());
			        	intent.putExtra("rank", split[1].trim());
			        }
				    
			        startActivity(intent);
			        getActivity().finish();
		        
		        }
		     })
		    .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int which) { 
		            // do nothing
		        	if(registerUser){

		        		imageView.setVisibility(View.VISIBLE);
						imageView.setImageResource(R.drawable.select_icon);
						imageView.invalidate();						
						userName.setTextColor(getResources().getColor(R.color.white));
					    role.setTextColor(getResources().getColor(R.color.white));					    
		                view.setBackgroundColor(Color.parseColor("#06dc1a"));
		        	}
		        	else{
		        		imageView.setVisibility(View.GONE);
				        userName.setTextColor(getResources().getColor(R.color.black));
					    role.setTextColor(getResources().getColor(R.color.appcolor));
		        		view.setBackgroundColor(Color.parseColor("#ffffff"));
		        	}
		        	
		        alertDialog.dismiss();
		        }
		     })
		   // .setIcon(android.R.drawable.ic_dialog_alert)
		     .show();
			
			
		}
		});
		return rootView;
	}
	
	
}
