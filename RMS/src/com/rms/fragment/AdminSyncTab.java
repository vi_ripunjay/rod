package com.rms.fragment;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Method;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.storage.StorageManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.rms.AdminMainActivity;
import com.rms.MyBroadcast;
import com.rms.R;
import com.rms.sync.GenerateXmlService;
import com.rms.sync.ParseXmlService;

public class AdminSyncTab extends Fragment {

	public static Button btnSync, cancel;
	public static ImageView roundedImage;
	public static Animation animRound;
	public static LinearLayout synclayOut;
	private ProgressBar mProgress;
	public static TextView syncMessages;
	public static boolean setTime = false;

	public static MyBroadcast myBroadcast;

	TextView textSync;
	public static Button start, wizard1, wizard2, wizard3, wizard4, wizard5;
	public static Context astContext;
	View view;
	public static boolean fileNotFound = false;

	public AdminSyncTab() {

	}

	@SuppressLint("SimpleDateFormat")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.admin_sync_tab, container,
				false);
		view = rootView;
		astContext = getActivity().getApplicationContext();

		isConnected(astContext);
		setTime = false;
		fileNotFound = false;

		start = (Button) rootView.findViewById(R.id.start);
		wizard1 = (Button) rootView.findViewById(R.id.button1);
		wizard2 = (Button) rootView.findViewById(R.id.button2);
		wizard3 = (Button) rootView.findViewById(R.id.button3);
		wizard4 = (Button) rootView.findViewById(R.id.button4);
		wizard5 = (Button) rootView.findViewById(R.id.button5);
		cancel = (Button) rootView.findViewById(R.id.cancel);

		textSync = (TextView) rootView.findViewById(R.id.textSync);
		selectFrag(rootView);

		start.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				selectFrag(v);
				start.setVisibility(View.GONE);
				wizard1.setVisibility(View.VISIBLE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.GONE);
				cancel.setVisibility(View.VISIBLE);
				textSync.setText("Step 1 of 4");

				/*
				 * List<UserMaster> umList = new ArrayList<UserMaster>();
				 * DatabaseSupport db = new DatabaseSupport(getActivity());
				 * umList = db.getUserMasterRow(); if(!setTime && (umList ==
				 * null || umList.size() == 0)){ Intent in = new
				 * Intent(android.provider.Settings.ACTION_DATE_SETTINGS);
				 * startActivity(in); setTime = true; } else { selectFrag(v);
				 * start.setVisibility(View.GONE);
				 * wizard1.setVisibility(View.VISIBLE);
				 * wizard2.setVisibility(View.GONE);
				 * wizard3.setVisibility(View.GONE);
				 * wizard4.setVisibility(View.GONE);
				 * wizard5.setVisibility(View.GONE);
				 * cancel.setVisibility(View.VISIBLE);
				 * textSync.setText("Step 1 of 4"); }
				 */

			}
		});

		wizard1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.GONE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.VISIBLE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.GONE);
				cancel.setVisibility(View.VISIBLE);
				textSync.setText("Step 2 of 4");

			}
		});

		wizard2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.GONE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.VISIBLE);
				wizard5.setVisibility(View.GONE);
				cancel.setVisibility(View.VISIBLE);
				textSync.setText("Step 3 of 4");

				 wizard4.setEnabled(false);
				 wizard4.setTextColor(getResources().getColor(R.color.black));
				 wizard4.invalidate();
			}
		});
		/*
		 * 
		 * wizard2.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub selectFrag(v); start.setVisibility(View.GONE);
		 * wizard1.setVisibility(View.GONE); wizard2.setVisibility(View.GONE);
		 * wizard3.setVisibility(View.VISIBLE);
		 * wizard4.setVisibility(View.GONE); wizard5.setVisibility(View.GONE);
		 * cancel.setVisibility(View.VISIBLE); textSync.setText("Step 3 of 5");
		 * 
		 * } });
		 * 
		 * wizard3.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub selectFrag(v); start.setVisibility(View.GONE);
		 * wizard1.setVisibility(View.GONE); wizard2.setVisibility(View.GONE);
		 * wizard3.setVisibility(View.GONE);
		 * wizard4.setVisibility(View.VISIBLE);
		 * wizard5.setVisibility(View.GONE); cancel.setVisibility(View.VISIBLE);
		 * textSync.setText("Step 4 of 5");
		 * 
		 * } });
		 */
		wizard4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.GONE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.VISIBLE);
				cancel.setVisibility(View.GONE);
				textSync.setText("Step 4 of 4");

			}
		});
		wizard5.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				selectFrag(v);
				start.setVisibility(View.VISIBLE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.GONE);
				textSync.setText("");
				
				/**
				 * After finish call refresh activity.
				 */
				Intent i = new Intent(getActivity(),
						AdminMainActivity.class);
				startActivity(i);

				getActivity().finish();

			}
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selectFrag(v);
				start.setVisibility(View.VISIBLE);
				wizard1.setVisibility(View.GONE);
				wizard2.setVisibility(View.GONE);
				wizard3.setVisibility(View.GONE);
				wizard4.setVisibility(View.GONE);
				wizard5.setVisibility(View.GONE);
				cancel.setVisibility(View.GONE);
				textSync.setText("");

				fileNotFound = false;
				
				try{
					disableUms();
				}
				catch(Exception ex){}
			}
		});

		// synclayOut = (LinearLayout) rootView.findViewById(R.id.synclayOutid);
		// mProgress = (ProgressBar) rootView.findViewById(R.id.progress_bar);
		// mProgress.setVisibility(View.GONE);
		// syncMessages = (TextView) rootView.findViewById(R.id.syncMessages);

		// animRound = AnimationUtils.loadAnimation(getActivity(),
		// R.drawable.rotate);
		// set animation listener
		// animRound.setAnimationListener(null);
		// roundedImage = (ImageView) rootView.findViewById(R.id.syncImage);

		// btnSync = (Button) rootView.findViewById(R.id.btnSync);
		/*
		 * btnSync.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) {
		 * 
		 * Message message = new Message(); message.what = 1;
		 * handler.sendMessage(message); } });
		 */
		return rootView;
	}

	public void disableUms() {
		try {

			StorageManager storage = (StorageManager) astContext
					.getApplicationContext().getSystemService(
							getActivity().STORAGE_SERVICE);
			Method method = storage.getClass().getDeclaredMethod(
					"disableUsbMassStorage");
			method.setAccessible(true);
			Object r = method.invoke(storage);

			// System.out.println("Storage disable ============================== ripunjay");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		
			// System.out.println("Storage disable ============error================== ripunjay");
		}
	}

	public static void generate() {
		AdminSyncTab ast = new AdminSyncTab();
		AdminSyncTab.SynchronizationTask syncTask = ast.new SynchronizationTask();
		syncTask.execute((Void) null);
	}

	public static void fileCheckingTask() {
		AdminSyncTab ast = new AdminSyncTab();
		syncMessages.setText("Waiting for file.");
		AdminSyncTab.SynchronizationFileCheckTask syncFileCheckTask = ast.new SynchronizationFileCheckTask();
		syncFileCheckTask.execute((Void) null);
	}

	public static void parse() {
		AdminSyncTab ast = new AdminSyncTab();
		syncMessages.setText("Waiting for file.");
		AdminSyncTab.SynchronizationParseTask syncParseTask = ast.new SynchronizationParseTask();
		syncParseTask.execute((Void) null);
	}

	public static Handler handler = new Handler(new Handler.Callback() {
		@Override
		public boolean handleMessage(Message msg) {
			if (msg.what == 1) {
				generate();
			} else if (msg.what == 2) {
				fileCheckingTask();
			} else if (msg.what == 3) {
				parse();
			}

			return true;
		}
	});

	public void selectFrag(View view) {
		System.out
				.println("............................................................");

		Fragment fr;
		if (view == this.view.findViewById(R.id.start)) {
			System.out.println("1");
			fr = new SetupSyncWizard2();

		} else if (view == this.view.findViewById(R.id.button1)) {
			System.out.println("2");
			fr = new SetupSyncWizard3();
			// generate();

		} else if (view == this.view.findViewById(R.id.button2)) {
			System.out.println("3");
			// fr = new SetupSyncWizard4();

			fr = new SetupSyncWizard5();

		} else if (view == this.view.findViewById(R.id.button3)) {
			System.out.println("4");
			fr = new SetupSyncWizard5();
			// fileCheckingTask();

		} else if (view == this.view.findViewById(R.id.button4)) {
			System.out.println("5");
			fr = new SetupSyncWizard6();

			// parse();

		} else if (view == this.view.findViewById(R.id.button5)) {
			System.out.println("6");
			fr = new StartSync();
		} else if (view == this.view.findViewById(R.id.cancel)) {
			System.out.println("6");

			fr = new StartSync();
		} else {
			System.out.println("7");
			fr = new StartSync();
		}

		FragmentManager fm = getFragmentManager();
		FragmentTransaction fragmentTransaction = fm.beginTransaction();
		fragmentTransaction.replace(R.id.fragment_container, fr);
		fragmentTransaction.commit();

	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationTask extends AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			syncMessages.setText("Generate is in progress.....");
			showProgress(true);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				GenerateXmlService gxs = new GenerateXmlService(astContext);
				gxs.generateXmlForDirtyRecord();

				// Simulate network access.
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				return false;
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {

			} else {

			}

			Message message = new Message();
			message.what = 2;
			handler.sendMessage(message);
		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationParseTask extends
			AsyncTask<Void, Void, Boolean> {

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			syncMessages
					.setText("File has been received and files are parsing...");
			showProgress(true);

		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				ParseXmlService pxs = new ParseXmlService(astContext);
				pxs.startParseXmlAfterZip();

			} catch (Exception e) {
				return false;
			}

			// TODO: register the new account here.
			return true;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {
				showProgress(false);
				syncMessages.setText("Synchronization is completed.");
			} else {

			}

		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}

	/**
	 * Represents an asynchronous synchronizing task
	 */
	public class SynchronizationFileCheckTask extends
			AsyncTask<Void, Void, Boolean> {

		int counter = 0;
		boolean fileExist = false;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			syncMessages.setText("Waiting for file.");
			showProgress(true);
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			// TODO: attempt authentication against a network service.

			try {

				do {
					if (fileExist()) {
						syncMessages.setText("File has been received.");
						fileExist = true;
					} else {
						fileExist = false;
					}
					if (counter > 100) {
						break;
					}
					counter++;
					Thread.sleep(3000);
				} while (!fileExist);

			} catch (InterruptedException e) {
				return false;
			}

			// TODO: register the new account here.
			return fileExist;
		}

		@Override
		protected void onPostExecute(final Boolean success) {

			if (success) {
				showProgress(true);
				syncMessages.setText("File has been received.");

				Message message = new Message();
				message.what = 3;
				handler.sendMessage(message);
			} else {

				syncMessages.setText("File not found and time will be exceed.");
				showProgress(false);
			}

		}

		@Override
		protected void onCancelled() {
			showProgress(false);
		}
	}

	public boolean fileExist() {
		boolean exist = false;
		String xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {
			exist = true;
		}
		return exist;
	}

	public String getXmlReceivePath() {
		String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/theark/rec/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public void showProgress(boolean start) {
		if (start) {
			btnSync.setVisibility(View.GONE);
			synclayOut.setVisibility(View.VISIBLE);
			roundedImage.setVisibility(View.VISIBLE);
			syncMessages.setVisibility(View.VISIBLE);
			// start the animation
			roundedImage.startAnimation(animRound);
		} else if (!start) {
			animRound.cancel();
			roundedImage.setVisibility(View.GONE);
			synclayOut.setVisibility(View.GONE);
			syncMessages.setVisibility(View.GONE);
			btnSync.setVisibility(View.VISIBLE);

		}
	}

	@SuppressWarnings("deprecation")
	public static boolean isConnected(Context context) {
		myBroadcast = new MyBroadcast();
		IntentFilter inte = new IntentFilter();
		inte.addAction(Intent.ACTION_BATTERY_CHANGED);
		inte.addAction(Intent.ACTION_POWER_CONNECTED);
		inte.addAction(Intent.ACTION_POWER_DISCONNECTED);
		Intent intent = context.registerReceiver(myBroadcast, inte);

		int plugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
		return plugged == BatteryManager.BATTERY_PLUGGED_AC
				|| plugged == BatteryManager.BATTERY_PLUGGED_USB;
	}

	@Override
	public void onStop() {
		super.onStop();
		try {
			if (myBroadcast != null) {
				astContext.unregisterReceiver(myBroadcast);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
