package com.rms.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.rms.AdminMainActivity;
import com.rms.LoginActivity;
import com.rms.R;
import com.rms.SyncHandler;
import com.rms.db.DatabaseSupport;
import com.rms.model.CrewDepartment;
import com.rms.model.CrewDepartmentTask;
import com.rms.model.InternationalDates;
import com.rms.model.RoleCrewDepartment;
import com.rms.model.RoleTabletFunction;
import com.rms.model.Roles;
import com.rms.model.ShipMaster;
import com.rms.model.SyncHistory;
import com.rms.model.SyncStatus;
import com.rms.model.Tasks;
import com.rms.model.UserMaster;
import com.rms.model.UserNfcTagData;
import com.rms.model.UserServiceTerm;
import com.rms.model.UserServiceTermRole;
import com.rms.model.UserWorkTimeSheet;
import com.rms.model.UserWorkTimeSheetTask;
import com.rms.parser.CrewDepartmentParser;
import com.rms.parser.CrewDepartmentTaskParser;
import com.rms.parser.InternationalDatesParser;
import com.rms.parser.RoleCrewDepartmentParser;
import com.rms.parser.RoleParser;
import com.rms.parser.RoleTabletFunctionParser;
import com.rms.parser.ShipMasterParser;
import com.rms.parser.SyncStatusParser;
import com.rms.parser.TaskParser;
import com.rms.parser.UserMasterParser;
import com.rms.parser.UserNfcTagDataParser;
import com.rms.parser.UserServiceTermParser;
import com.rms.parser.UserServiceTermRoleParser;
import com.rms.parser.UserWorkTimeSheetParser;
import com.rms.parser.UserWorkTimeSheetTaskParser;
import com.rms.utils.CommonUtil;

/**
 * @file BackGroundTask . java
 * 
 * @brief class is used to run the task in the backGround
 * 
 * @author Ripunjay Shukla
 * 
 * @date : Jun 21,2015
 */
public class BackGroundTask {

	private static Context context;
	private static ProgressDialog dialog = null;
	public static List<String> viFormList = null;
	public static List<String> miFormList = null;
	public static List<String> checkList = null;
	public static List<String> imageList = null;
	public static List<String> userList = null;

	public static boolean checkGetTask = false;
	public static boolean checkGetCrewdepartment = false;
	public static boolean checkGetCrewdepartmentTask = false;
	public static boolean registerDeviceStatus = false;
	public static boolean checkGetRoleCrewdepartment = false;
	public static boolean checkGetSyncStatus = false;
	public static boolean checkGetWorktimesheetTask = false;
	public static boolean checkSendWorktimesheet = false;
	public static boolean checkSendWorktimesheetTask = false;
	public static boolean checkGetWorktimesheet = false;
	public static boolean checkGetRoles = false;
	public static boolean checkSendSyncHistory = false;
	public static boolean checkSendSyncstatus = false;
	public static boolean checkGetUpdatedFilledFormCheckListData = false;
	public static boolean checkGetRoleTemplateData = false;
	public static boolean checkGetShipExist = false;
	public static boolean checkGetIdl = false;
	public static boolean checkGetRoleTabletFunction = false;
	public static boolean checkGetUserNfcTagData = false;
	public static boolean checkSendUserNfcTagData = false;	

	public static boolean checkUserUpdate = false;
	public static boolean checkGetUserServiceTerm = false;
	public static boolean checkGetUserMaster = false;
	public static boolean checkGetUserServiceTermRole = false;
	public static boolean checkGetShipmaster = false;
	public static boolean checkUpdateManualSyncHistory = false;
	public static boolean checkGetSyncHistory = false;
	
	public static boolean checkGetTransactionalData = false;
	public static boolean checkSendAckTransactionalData = false;
	
	public static boolean checkGetModifiedMasterData = false;
	
	public static boolean checkGetSyncStatusData = false;
	
	public static boolean checkSendSyncStatusData = false;
	
	public static boolean IS_UPDATE_USER_SUCCESS = false;
	public static boolean IS_SEND_FILLED_FORM_SUCCESS = false;
	public static boolean IS_SEND_FILLED_FORM_UNSUCCESS = false;
	public static boolean IS_SEND_FILLED_FORM_RESPONSE_DATA = false;
	public static boolean IS_GET_IMAGES_RESPONSE_DATA = false;
	public static boolean IS_UPDATE_CONTENT_SUCCESS = false;

	public static boolean IS_SEND_FORM_IMAGES_RESPONSE_DATA = false;
	public static boolean IS_SEND_FORM_IMAGES_SUCCESS = false;
	public static boolean IS_SEND_FORM_IMAGES_UNSUCCESS = false;

	public static boolean IS_GET_SYNCHISTORY_RESPONSE_DATA = false;
	public static boolean IS_GET_SYNCHISTORY_SUCCESS = false;
	public static boolean IS_GET_SYNCHISTORY_UNSUCCESS = false;

	public static boolean IS_GET_SYNCSTATUS_RESPONSE_DATA = false;
	public static boolean IS_GET_SYNCSTATUS_SUCCESS = false;
	public static boolean IS_GET_SYNCSTATUS_UNSUCCESS = false;

	public static boolean IS_GET_WORKTIMESHEET_TASK_RESPONSE_DATA = false;
	public static boolean IS_GET_WORKTIMESHEET_TASK_SUCCESS = false;
	public static boolean IS_GET_WORKTIMESHEET_TASK_UNSUCCESS = false;

	public static boolean IS_SEND_WORKTIMESHEET_TASK_RESPONSE_DATA = false;
	public static boolean IS_SEND_WORKTIMESHEET_TASK_SUCCESS = false;
	public static boolean IS_SEND_WORKTIMESHEET_TASK_UNSUCCESS = false;

	public static boolean IS_GET_WORKTIMESHEET_RESPONSE_DATA = false;
	public static boolean IS_GET_WORKTIMESHEET_SUCCESS = false;
	public static boolean IS_GET_WORKTIMESHEET_UNSUCCESS = false;

	public static boolean IS_SEND_WORKTIMESHEET_RESPONSE_DATA = false;
	public static boolean IS_SEND_WORKTIMESHEET_SUCCESS = false;
	public static boolean IS_SEND_WORKTIMESHEET_UNSUCCESS = false;

	public static boolean IS_GET_USERMASTER_RESPONSE_DATA = false;
	public static boolean IS_GET_USERMASTER_SUCCESS = false;
	public static boolean IS_GET_USERMASTER_UNSUCCESS = false;

	public static boolean IS_GET_SERVICETERM_RESPONSE_DATA = false;
	public static boolean IS_GET_SERVICETERM_SUCCESS = false;
	public static boolean IS_GET_SERVICETERM_UNSUCCESS = false;

	public static boolean IS_GET_SERVICETERM_ROLE_RESPONSE_DATA = false;
	public static boolean IS_GET_SERVICETERM_ROLE_SUCCESS = false;
	public static boolean IS_GET_SERVICETERM_ROLE_UNSUCCESS = false;

	public static boolean IS_GET_UPDATED_MANUAL_SYNC_HISTORY_RESPONSE_DATA = false;
	public static boolean IS_GET_UPDATED_MANUAL_SYNC_HISTORY_SUCCESS = false;
	public static boolean IS_GET_UPDATED_MANUAL_SYNC_HISTORY_UNSUCCESS = false;

	public static boolean IS_GET_TASK_SUCCESS = false;
	public static boolean IS_GET_TASK_UNSUCCESS = false;
	public static boolean IS_GET_TASK_RESPONSE_DATA = false;

	public static boolean IS_GET_CREWDEPARTMENT_SUCCESS = false;
	public static boolean IS_GET_CREWDEPARTMENT_UNSUCCESS = false;
	public static boolean IS_GET_CREWDEPARTMENT_RESPONSE_DATA = false;

	public static boolean IS_GET_CREWDEPARTMENTTASK_SUCCESS = false;
	public static boolean IS_GET_CREWDEPARTMENTTASK_UNSUCCESS = false;
	public static boolean IS_GET_CREWDEPARTMENTTASK_RESPONSE_DATA = false;

	public static boolean IS_GET_ROLE_CREWDEPARTMENT_SUCCESS = false;
	public static boolean IS_GET_ROLE_CREWDEPARTMENT_UNSUCCESS = false;
	public static boolean IS_GET_ROLE_CREWDEPARTMENT_RESPONSE_DATA = false;

	public static boolean IS_DATA_PRESENT_IN_DATABASE = false;

	public static boolean IS_GET_ALL_SHIPS_SUCCESS = false;
	public static boolean IS_GET_ALL_SHIPS_UNSUCCESS = false;
	public static boolean IS_GET_ALL_SHIPS_RESPONSE_DATA = false;

	public static boolean IS_GET_ROLE_SUCCESS = false;
	public static boolean IS_GET_ROLE_UNSUCCESS = false;
	public static boolean IS_GET_ROLE_RESPONSE_DATA = false;

	public static boolean IS_GET_SHIP_EXIST_SUCCESS = false;
	public static boolean IS_GET_SHIP_EXIST_UNSUCCESS = false;
	public static boolean IS_GET_SHIP_EXIST_RESPONSE_DATA = false;

	public static boolean IS_GET_IDL_SUCCESS = false;
	public static boolean IS_GET_IDL_UNSUCCESS = false;
	public static boolean IS_GET_IDL_RESPONSE_DATA = false;

	public static boolean IS_GET_ROLETABLETFUNCTION_SUCCESS = false;
	public static boolean IS_GET_ROLETABLETFUNCTION_UNSUCCESS = false;
	public static boolean IS_GET_ROLETABLETFUNCTION_RESPONSE_DATA = false;

	public static boolean IS_SEND_SYNCHISTORY_RESPONSE_DATA = false;
	public static boolean IS_SEND_SYNCHISTORY_SUCCESS = false;
	public static boolean IS_SEND_SYNCHISTORY_UNSUCCESS = false;

	public static boolean IS_GET_USERNFCTAGDATA_SUCCESS = false;
	public static boolean IS_GET_USERNFCTAGDATA_UNSUCCESS = false;
	public static boolean IS_GET_USERNFCTAGDATA_RESPONSE_DATA = false;

	public static boolean IS_SEND_USERNFCTAGDATA_SUCCESS = false;
	public static boolean IS_SEND_USERNFCTAGDATA_UNSUCCESS = false;
	public static boolean IS_SEND_USERNFCTAGDATA_RESPONSE_DATA = false;
	
	public static boolean IS_GET_TRANSACTIONALDATA_SUCCESS = false;
	public static boolean IS_GET_TRANSACTIONALDATA_UNSUCCESS = false;
	public static boolean IS_GET_TRANSACTIONALDATA_RESPONSE_DATA = false;
	
	public static boolean IS_GET_MODIFIEDMASTERDATA_SUCCESS = false;
	public static boolean IS_GET_MODIFIEDMASTERDATA_UNSUCCESS = false;
	public static boolean IS_GET_MODIFIEDMASTERDATA_RESPONSE_DATA = false;
	
	public static boolean IS_GET_SYNCSTATUSDATA_SUCCESS = false;
	public static boolean IS_GET_SYNCSTATUSDATA_UNSUCCESS = false;
	public static boolean IS_GET_SYNCSTATUSDATA_RESPONSE_DATA = false;
	
	public static boolean IS_SEND_SYNCSTATUSDATA_SUCCESS = false;
	public static boolean IS_SEND_SYNCSTATUSDATA_UNSUCCESS = false;
	public static boolean IS_SEND_SYNCSTATUSDATA_RESPONSE_DATA = false;

	String toastMsg = "";
	public StringBuffer formBuff = new StringBuffer();
	public int no = 1;
	
	public static String[] transactionTables = {DatabaseSupport.TABLE_USER_WORKTIME_SHEET,DatabaseSupport.TABLE_USER_WORKTIME_SHEET_TASK};
	public static int tableIndex = 0;
	
	public static String[] masterTables = { 
			DatabaseSupport.TABLE_USER_MASTER, DatabaseSupport.TABLE_ROLE,
			DatabaseSupport.TABLE_USER_SERVICE_TERM,
			DatabaseSupport.TABLE_USER_SERVICE_TERM_ROLE,
			DatabaseSupport.TABLE_TASKS, DatabaseSupport.TABLE_CREW_DEPARTMENT,
			DatabaseSupport.TABLE_CREW_DEPARTMENT_TASK,
			DatabaseSupport.TABLE_ROLE_CREW_DEPARTMENT,
			DatabaseSupport.TABLE_INTERNATIONALDATES,			
			DatabaseSupport.TABLE_USER_NFC };
	
	
	//DatabaseSupport.TABLE_IDL_LOGWORK, 
	
	public static String notInId="";
	public static  String ackIds="";
	public static int hasMore = 0;

	/**
	 * This method call at the time of backGroundTask object initialization and
	 * create required object
	 * 
	 * @param context
	 */
	public BackGroundTask(Context context) {
		super();
		BackGroundTask.context = context;

		viFormList = new ArrayList<String>();
		miFormList = new ArrayList<String>();
		checkList = new ArrayList<String>();
		imageList = new ArrayList<String>();
		userList = new ArrayList<String>();
		Log.i("background task", "inside constructor");
		// toastMsg = "";
	}

	/**
	 * This method will get tasks data from web service in the background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getTasks(String tenantId, String deviceId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetTasksDataTask dataTask = null;
			if (checkGetTask) {
				Log.i("inside get tasks", "test" + checkGetTask);
				try {
					dataTask = new GetTasksDataTask(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetTask = !checkGetTask;
					Log.i("inside send Filled Form", "test" + checkGetTask);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get Tasks master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetTasksDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetTasksDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_TASK_SUCCESS == true
						&& IS_GET_TASK_UNSUCCESS == true) {

					IS_GET_TASK_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			// String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getTask(macId, tenantId, "0");

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetTasksResponse(data);
				IS_GET_TASK_SUCCESS = true;

			} else {
				/**
				 * TODO
				 */
			}

			if (IS_GET_TASK_SUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_ROLES;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get Roles data from web service in the background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getRoles(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetRolesDataTask dataTask = null;
			if (checkGetRoles) {
				Log.i("inside get roles", "test" + checkGetRoles);
				try {
					dataTask = new GetRolesDataTask(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetRoles = !checkGetRoles;
					Log.i("inside get roles", "test" + checkGetRoles);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get Roles master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetRolesDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetRolesDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_ROLE_SUCCESS == true
						&& IS_GET_ROLE_UNSUCCESS == true) {

					IS_GET_ROLE_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			// String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getRoles(macId, tenantId, "0");

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetRolesResponse(data);
				IS_GET_ROLE_SUCCESS = true;

			} else {
				/**
				 * TODO
				 */
			}

			if (IS_GET_ROLE_SUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_CREWDEPARTMENT;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get CrewDepartment data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getCrewDepartment(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetCrewDepartmentDataTask dataTask = null;
			if (checkGetCrewdepartment) {
				Log.i("inside get roles", "test" + checkGetCrewdepartment);
				try {
					dataTask = new GetCrewDepartmentDataTask(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetCrewdepartment = !checkGetCrewdepartment;
					Log.i("inside get roles", "test" + checkGetCrewdepartment);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get CrewDepartment master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetCrewDepartmentDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetCrewDepartmentDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_CREWDEPARTMENT_SUCCESS == true
						&& IS_GET_CREWDEPARTMENT_UNSUCCESS == true) {

					IS_GET_CREWDEPARTMENT_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			// String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getCrewDepartment(macId, tenantId, "0");

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetCrewDepartmentResponse(data);
				IS_GET_CREWDEPARTMENT_SUCCESS = true;

			} else {
				/*
				 * TODO
				 */
			}

			if (IS_GET_CREWDEPARTMENT_SUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_ROLE_CREWDEPARTMENT;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get RoleCrewDepartment data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getRoleCrewDepartment(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetRoleCrewDepartmentDataTask dataTask = null;
			if (checkGetRoleCrewdepartment) {
				Log.i("inside get roles", "test" + checkGetRoleCrewdepartment);
				try {
					dataTask = new GetRoleCrewDepartmentDataTask(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetRoleCrewdepartment = !checkGetRoleCrewdepartment;
					Log.i("inside get roles", "test"
							+ checkGetRoleCrewdepartment);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get RoleCrewDepartment master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetRoleCrewDepartmentDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetRoleCrewDepartmentDataTask(String macId, String tenantId,
				String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_ROLE_CREWDEPARTMENT_SUCCESS == true
						&& IS_GET_ROLE_CREWDEPARTMENT_UNSUCCESS == true) {

					IS_GET_ROLE_CREWDEPARTMENT_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			// String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getRoleCrewDepartment(macId, tenantId, "0");

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetRoleCrewDepartmentResponse(data);
				IS_GET_ROLE_CREWDEPARTMENT_SUCCESS = true;

			} else {
				/**
				 * TODO
				 */

			}

			if (IS_GET_ROLE_CREWDEPARTMENT_SUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_CREWDEPARTMENT_TASK;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get CrewDepartmentTask data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getCrewDepartmentTask(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetCrewDepartmentTaskDataTask dataTask = null;
			if (checkGetCrewdepartmentTask) {
				Log.i("inside get roles", "test" + checkGetCrewdepartmentTask);
				try {
					dataTask = new GetCrewDepartmentTaskDataTask(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetCrewdepartmentTask = !checkGetCrewdepartmentTask;
					Log.i("inside get roles", "test"
							+ checkGetCrewdepartmentTask);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get CrewDepartmentTask master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetCrewDepartmentTaskDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetCrewDepartmentTaskDataTask(String macId, String tenantId,
				String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_CREWDEPARTMENTTASK_SUCCESS == true
						&& IS_GET_CREWDEPARTMENTTASK_UNSUCCESS == true) {

					IS_GET_CREWDEPARTMENTTASK_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			// String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getCrewDepartmentTask(macId, tenantId, "0");

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetCrewDepartmentTaskResponse(data);
				IS_GET_CREWDEPARTMENTTASK_SUCCESS = true;

			} else {
				/**
				 * TODO
				 */
			}

			if (IS_GET_CREWDEPARTMENTTASK_SUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_IDL;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get ShipMaster data from web service in the background
	 * task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getShipMasterTask(String tenantId, String shipId,
			String deviceId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetShipMasterDataTask dataTask = null;
			if (checkGetShipmaster) {
				Log.i("inside get roles", "test" + checkGetShipmaster);
				try {
					dataTask = new GetShipMasterDataTask(
							CommonUtil.getMacId(context), tenantId, shipId);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetShipmaster = !checkGetShipmaster;
					Log.i("inside get roles", "test" + checkGetShipmaster);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);
					serviceProcessEnd();
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get ShipMaster master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetShipMasterDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetShipMasterDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_SHIP_EXIST_SUCCESS == true
						&& IS_GET_SHIP_EXIST_UNSUCCESS == true) {

					IS_GET_SHIP_EXIST_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getShipMaster(macId, tenantId, shipId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetShipMasterResponse(data);
				IS_GET_SHIP_EXIST_UNSUCCESS = true;

			} else {
				/**
				 * TODO
				 */
			}

			if (IS_GET_SHIP_EXIST_UNSUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_USERMASTER;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get UserServiceTermRole data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getUserServiceTermRoleTask(String tenantId, String shipId,
			String deviceId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetUserServiceTermRoleTask dataTask = null;
			if (checkGetUserServiceTermRole) {
				Log.i("inside get roles", "test" + checkGetUserServiceTermRole);
				try {
					dataTask = new GetUserServiceTermRoleTask(
							CommonUtil.getMacId(context), tenantId, shipId);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetUserServiceTermRole = !checkGetUserServiceTermRole;
					Log.i("inside get roles", "test"
							+ checkGetUserServiceTermRole);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();

		}

	}

	/**
	 * Get UserServiceTermRole master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetUserServiceTermRoleTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetUserServiceTermRoleTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_SERVICETERM_ROLE_SUCCESS == true
						&& IS_GET_SERVICETERM_ROLE_UNSUCCESS == true) {

					IS_GET_SERVICETERM_ROLE_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getUserServiceTermRole(macId, tenantId, shipId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetUserServiceTermRoleResponse(data);
				IS_GET_SERVICETERM_ROLE_UNSUCCESS = true;

			} else {
				/*
				 * TODO
				 */
			}

			if (IS_GET_SERVICETERM_ROLE_UNSUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_TASK;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get UserServiceTerm data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getUserServiceTermTask(String tenantId, String shipId,
			String deviceId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetUserServiceTermTask dataTask = null;
			if (checkGetUserServiceTerm) {
				Log.i("inside get roles", "test" + checkGetUserServiceTerm);
				try {
					dataTask = new GetUserServiceTermTask(
							CommonUtil.getMacId(context), tenantId, shipId);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetUserServiceTerm = !checkGetUserServiceTerm;
					Log.i("inside get roles", "test" + checkGetUserServiceTerm);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get UserServiceTerm master data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetUserServiceTermTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetUserServiceTermTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_SERVICETERM_SUCCESS == true
						&& IS_GET_SERVICETERM_UNSUCCESS == true) {

					IS_GET_SERVICETERM_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getUserServiceTerm(macId, tenantId, shipId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetUserServiceTermResponse(data);
				IS_GET_SERVICETERM_UNSUCCESS = true;

			} else {
				/**
				 * TODO
				 */
			}

			if (IS_GET_SERVICETERM_UNSUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_USER_SERVICETERM_ROLE;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get UserMaster data from web service in the background
	 * task
	 * 
	 * @param tenantId
	 * @param deviceId
	 */
	public void getUserMaster(String tenantId, String shipId, String deviceId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetUserMasterTask dataTask = null;
			if (checkGetUserMaster) {
				Log.i("inside get roles", "test" + checkGetUserMaster);
				try {
					dataTask = new GetUserMasterTask(
							CommonUtil.getMacId(context), tenantId, shipId);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetUserMaster = !checkGetUserMaster;
					Log.i("inside get roles", "test" + checkGetUserMaster);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get Usermaster data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetUserMasterTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetUserMasterTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_USERMASTER_SUCCESS == true
						&& IS_GET_USERMASTER_UNSUCCESS == true) {

					IS_GET_USERMASTER_SUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getUserMaster(macId, tenantId, shipId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetUserMasterResponse(data);
				IS_GET_USERMASTER_UNSUCCESS = true;

			} else {
				/**
				 * TODO
				 */
			}

			if (IS_GET_USERMASTER_UNSUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_USER_SERVICETERM;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get WorktimeSheet data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 * @param shipId
	 */
	public void getWorktimeSheet(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetUserWorkTimeSheet dataTask = null;
			if (checkGetWorktimesheet) {
				Log.i("inside get roles", "test" + checkGetWorktimesheet);
				try {
					dataTask = new GetUserWorkTimeSheet(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetWorktimesheet = !checkGetWorktimesheet;
					Log.i("inside get roles", "test" + checkGetWorktimesheet);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get UserWorkTimeSheet data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetUserWorkTimeSheet extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetUserWorkTimeSheet(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_WORKTIMESHEET_SUCCESS == true
						&& IS_GET_WORKTIMESHEET_UNSUCCESS == true) {

					IS_GET_WORKTIMESHEET_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getWorkTimeSheet(shipId, macId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetUserWorkTimeSheetResponse(data);
				IS_GET_WORKTIMESHEET_UNSUCCESS = true;

			} else {
				if (IS_DATA_PRESENT_IN_DATABASE == false) {

					Log.i("Toast msg in get Roles==", "==" + toastMsg);

					IS_GET_WORKTIMESHEET_UNSUCCESS = true;
					SyncHandler.context = context;
					Message message = new Message();
					SyncHandler.buildToast(toastMsg);
					message.what = SyncHandler.MSG_GET_USER_SERVICETERM;
					SyncHandler.handler.sendMessage(message);

				} else if (IS_GET_WORKTIMESHEET_RESPONSE_DATA == false) {
					cancelProgressdialog();

				}

			}

			if (IS_GET_WORKTIMESHEET_UNSUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_USER_SERVICETERM;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get WorktimeSheetTask data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 * @param shipId
	 */
	public void getWorktimeSheetTask(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetUserWorkTimeSheetTask dataTask = null;
			if (checkGetWorktimesheetTask) {
				Log.i("inside get roles", "test" + checkGetWorktimesheetTask);
				try {
					dataTask = new GetUserWorkTimeSheetTask(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetWorktimesheetTask = !checkGetWorktimesheetTask;
					Log.i("inside get roles", "test"
							+ checkGetWorktimesheetTask);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get UserWorkTimeSheetTsk data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetUserWorkTimeSheetTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetUserWorkTimeSheetTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_WORKTIMESHEET_TASK_SUCCESS == true
						&& IS_GET_WORKTIMESHEET_TASK_UNSUCCESS == true) {

					IS_GET_WORKTIMESHEET_TASK_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getWorkTimeSheetTask(shipId, macId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetUserWorkTimeSheetTaskResponse(data);
				IS_GET_WORKTIMESHEET_TASK_UNSUCCESS = true;

			} else {
				if (IS_DATA_PRESENT_IN_DATABASE == false) {

					Log.i("Toast msg in get Roles==", "==" + toastMsg);

					IS_GET_WORKTIMESHEET_TASK_UNSUCCESS = true;
					SyncHandler.context = context;
					Message message = new Message();
					SyncHandler.buildToast(toastMsg);
					message.what = SyncHandler.MSG_GET_USER_SERVICETERM;
					SyncHandler.handler.sendMessage(message);

				} else if (IS_GET_WORKTIMESHEET_TASK_RESPONSE_DATA == false) {
					cancelProgressdialog();

				}

			}

			if (IS_GET_WORKTIMESHEET_TASK_UNSUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_USER_SERVICETERM;
				SyncHandler.handler.sendMessage(message);
			}
		}

	}

	/**
	 * This method will get InternationalDates data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 * @param shipId
	 */
	public void getInternationalDates(String deviceId, String tenantId,
			String shipId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetInternationalDatesTask dataTask = null;
			if (checkGetIdl) {
				Log.i("inside get roles", "test" + checkGetIdl);
				try {
					dataTask = new GetInternationalDatesTask(
							CommonUtil.getMacId(context), tenantId, shipId);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetIdl = !checkGetIdl;
					Log.i("inside get roles", "test" + checkGetIdl);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();

		}

	}

	/**
	 * Get InternationalDates data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class GetInternationalDatesTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetInternationalDatesTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_IDL_SUCCESS == true && IS_GET_IDL_UNSUCCESS == true) {

					IS_GET_IDL_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			data = services.getIdl(macId, tenantId, shipId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetIdlResponse(data);
				IS_GET_IDL_UNSUCCESS = true;

			} else {
				/**
				 * TODO
				 */

			}

			if (IS_GET_IDL_UNSUCCESS) {
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_ROLE_TABLET_FUNCTION;
				SyncHandler.handler.sendMessage(message);

				// serviceProcessEnd();
			}
		}

	}

	
	public void getTransactionalDataData() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetTransactionalDataTask dataTask = null;
			if (checkGetTransactionalData) {
				Log.i("inside get roles", "test" + checkGetIdl);
				try {
					dataTask = new GetTransactionalDataTask(
							CommonUtil.getIMEI(context), CommonUtil.getShipId(context), context.getResources().getString(R.string.moduleName));
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetTransactionalData = !checkGetTransactionalData;
					Log.i("inside get transactionalData", "test" + checkGetTransactionalData);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();

		}

	}

	/**
	 * Get Transactional data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class GetTransactionalDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String moduleName;
		private String shipId;
		private String macId;

		GetTransactionalDataTask(String macId, String shipId, String moduleName) {
			this.macId = macId;
			this.shipId = shipId;
			this.moduleName = moduleName;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_TRANSACTIONALDATA_SUCCESS == true && IS_GET_TRANSACTIONALDATA_UNSUCCESS == true) {

					IS_GET_TRANSACTIONALDATA_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;
			
			

			WebServiceMethod services = new WebServiceMethod(context);
			DatabaseSupport dbSupport = new DatabaseSupport(context);
			
			int logInFlag = Integer.parseInt(CommonUtil.getUserLogin(context));
			
			if(logInFlag == 1){
				
				notInId = dbSupport.getAvailableIds(transactionTables[tableIndex]);
				
			}else{
				
				notInId = "";
				
			}
					
			dbSupport.close();		
			data = services.getTransactionalData(macId, shipId, moduleName,transactionTables[tableIndex], notInId, logInFlag);

			if (data != null) {
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetTransactionalDataResponse(data);
				IS_GET_TRANSACTIONALDATA_UNSUCCESS = true;

			} else {
				
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_SYNC_STATUS_DATA;
				SyncHandler.handler.sendMessage(message);

			}			
		}

	}
	
	/**
	 * @author pushkar.m
	 */
	public void getModifiedMasterData(){
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetModifiedMasterDataTask dataTask = null;
			if (checkGetModifiedMasterData) {
				Log.i("inside get roles", "test" + checkGetIdl);
				try {
					dataTask = new GetModifiedMasterDataTask();
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetModifiedMasterData = !checkGetModifiedMasterData;
					Log.i("inside get modifiedMasterData", "test" + checkGetModifiedMasterData);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}
	}
	
	
	
	class GetModifiedMasterDataTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String moduleName;
		private String shipId;
		private String macId;

		GetModifiedMasterDataTask() {
			
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_MODIFIEDMASTERDATA_SUCCESS == true && IS_GET_MODIFIEDMASTERDATA_UNSUCCESS == true) {

					IS_GET_MODIFIEDMASTERDATA_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;
			
			

			WebServiceMethod services = new WebServiceMethod(context);
			DatabaseSupport dbSupport = new DatabaseSupport(context);
			
						
			notInId = dbSupport.getMasterAvailableIds(masterTables[tableIndex]);
				
			
					
			dbSupport.close();		
			//data = services.getTransactionalData(macId, shipId, moduleName,transactionTables[tableIndex], notInId, logInFlag);
			data = services.getModifiedMasterData(notInId, masterTables[tableIndex], Integer.parseInt(CommonUtil.getTenantId(context)), Integer.parseInt(CommonUtil.getShipId(context)));

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetModifiedMasterDataResponse(data);
				IS_GET_MODIFIEDMASTERDATA_UNSUCCESS = true;

			} else {
				/*serviceProcessEnd();
				CommonUtil.updateSyncStatus(context);*/
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_SYNC_STATUS_DATA;
				SyncHandler.handler.sendMessage(message);

			}
		}

	}
	
	/**
	 * 
	 */
	public void getSyncStatusData(){
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetSyncStatusDataTask dataTask = null;
			if (checkGetSyncStatusData) {
				Log.i("inside get roles", "test" + checkGetIdl);
				try {
					dataTask = new 	GetSyncStatusDataTask();
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetSyncStatusData = !checkGetSyncStatusData;
					Log.i("inside get getSyncStatusData", "test" + checkGetSyncStatusData);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}
	}
	
	
	
	class GetSyncStatusDataTask extends AsyncTask<String, Integer, Boolean> {

		private int tenantId;
		private String moduleName;
		private int shipId;
		private String macId;
		private String data = "";

		GetSyncStatusDataTask() {
			tenantId = Integer.parseInt(CommonUtil.getTenantId(context));
			shipId = Integer.parseInt(CommonUtil.getShipId(context));
			macId = CommonUtil.getIMEI(context);
			moduleName = context.getResources().getString(R.string.moduleName);
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_SYNCSTATUSDATA_SUCCESS == true && IS_GET_SYNCSTATUSDATA_UNSUCCESS == true) {

					IS_GET_SYNCSTATUSDATA_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod service = new WebServiceMethod(context);
			
			data = service.getSyncStatusData(macId, shipId , tenantId, moduleName);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetSyncStatusDataResponse(data);
				IS_GET_SYNCSTATUSDATA_UNSUCCESS = true;

			}
			
			if(CommonUtil.getUserLogin(context) != null && CommonUtil.getUserLogin(context).equals("1")){
				
				DatabaseSupport db = new DatabaseSupport(context);				
				
				List<SyncStatus> syncStatusList = db.getSyncStatusData();

				db.close();
				
				if (syncStatusList != null && syncStatusList.size() > 0) {
					
					serviceProcessEnd();
					CommonUtil.updateSyncStatus(context);
					
				}else{
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_SEND_SYNC_STATUS_DATA;
					SyncHandler.handler.sendMessage(message);
				}
						
				
			}else{
			
				serviceProcessEnd();
				CommonUtil.updateSyncStatus(context);
				
			}
		}

	}
	
	
	/**
	 * This method will send WorktimeSheet data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 * @param shipId
	 */
	public void sendWorktimeSheet(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			SendUserWorkTimeSheet dataTask = null;
			if (checkSendWorktimesheet) {
				Log.i("inside get roles", "test" + checkSendWorktimesheet);
				try {
					dataTask = new SendUserWorkTimeSheet(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendWorktimesheet = !checkSendWorktimesheet;
					Log.i("inside get roles", "test" + checkSendWorktimesheet);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Send UserWorkTimeSheet data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class SendUserWorkTimeSheet extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		SendUserWorkTimeSheet(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_SEND_WORKTIMESHEET_SUCCESS == true
						&& IS_SEND_WORKTIMESHEET_UNSUCCESS == true) {

					IS_SEND_WORKTIMESHEET_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			xmlData = genrateXmlDataForUserWorkTimeSheet();

			if (xmlData != null) {
				data = services
						.sendWorkTimeSheet(params[0], params[1], xmlData);

				if (data != null)
					IS_SEND_WORKTIMESHEET_RESPONSE_DATA = true;
				else
					IS_SEND_WORKTIMESHEET_UNSUCCESS = true;
			}

			if (xmlData != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseSendUserWorkTimeSheetResponse(data);
				IS_SEND_WORKTIMESHEET_UNSUCCESS = true;

			} else {
				/**
				 * TODO
				 */

			}

			/* if (IS_SEND_WORKTIMESHEET_UNSUCCESS) { */

			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_SEND_WORKTIMESHEET_TASK;
			SyncHandler.handler.sendMessage(message);
			// }
		}

	}

	/**
	 * This method will send WorktimeSheetTask data from web service in the
	 * background task
	 * 
	 * @param tenantId
	 * @param deviceId
	 * @param shipId
	 */
	public void sendWorktimeSheetTask(String deviceId, String tenantId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			SendUserWorkTimeSheetTask dataTask = null;
			if (checkSendWorktimesheetTask) {
				Log.i("inside get roles", "test" + checkSendWorktimesheetTask);
				try {
					dataTask = new SendUserWorkTimeSheetTask(
							CommonUtil.getMacId(context), tenantId, null);
					dataTask.execute(deviceId, tenantId);
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendWorktimesheetTask = !checkSendWorktimesheetTask;
					Log.i("inside get WorktimeSheetTask", "test"
							+ checkSendWorktimesheetTask);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Send UserWorkTimeSheetTask data
	 * 
	 * @author ripunjay.s
	 * 
	 */
	class SendUserWorkTimeSheetTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		SendUserWorkTimeSheetTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_SEND_WORKTIMESHEET_TASK_SUCCESS == true
						&& IS_SEND_WORKTIMESHEET_TASK_UNSUCCESS == true) {

					IS_SEND_WORKTIMESHEET_TASK_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			xmlData = genrateXmlDataForUserWorkTimeSheetTask();

			if (xmlData != null) {
				data = services.sendWorkTimeSheetTask(params[0], params[1],
						xmlData);

				if (data != null)
					IS_SEND_WORKTIMESHEET_TASK_RESPONSE_DATA = true;
				else
					IS_SEND_WORKTIMESHEET_TASK_UNSUCCESS = true;
			}

			if (xmlData != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseSendUserWorkTimeSheetTaskResponse(data);
				IS_SEND_WORKTIMESHEET_TASK_UNSUCCESS = true;

			}

			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_SEND_USER_NFC_TAGDATA;
			SyncHandler.handler.sendMessage(message);
		}

	}

	/**
	 * This method will send RmsSyncHistoryTask data from web service in the
	 * background task
	 * 
	 * @author pushkar.m
	 */
	public void sendRmsSyncHistory() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			SendRmsSyncHistoryTask dataTask = null;
			if (checkSendSyncHistory) {
				Log.i("inside SendRmsSyncHistory", "test"
						+ checkSendSyncHistory);
				try {
					dataTask = new SendRmsSyncHistoryTask();
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendWorktimesheetTask = !checkSendWorktimesheetTask;
					Log.i("inside get WorktimeSheetTask", "test"
							+ checkSendWorktimesheetTask);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	/**
	 * Send SyncHistoryTask data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class SendRmsSyncHistoryTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		SendRmsSyncHistoryTask() {

		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_SEND_SYNCHISTORY_SUCCESS == true
						&& IS_SEND_SYNCHISTORY_UNSUCCESS == true) {

					IS_SEND_SYNCHISTORY_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			xmlData = genrateXmlDataForSyncHistory();

			if (xmlData != null) {
				data = services.sendSyncHistoryTask(xmlData);

				if (data != null)
					IS_SEND_SYNCHISTORY_RESPONSE_DATA = true;
				else
					IS_SEND_SYNCHISTORY_UNSUCCESS = true;
			}

			if (xmlData != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			/*
			 * if (result) {
			 * 
			 * parseSendUserWorkTimeSheetTaskResponse(data);
			 * IS_SEND_WORKTIMESHEET_TASK_UNSUCCESS = true;
			 * 
			 * } else {
			 */
			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_SEND_WORKTIMESHEET;
			SyncHandler.handler.sendMessage(message);

			/*
			 * serviceProcessEnd();
			 * 
			 * }
			 */
		}

	}

	
	/**
	 * This method will send RmsSyncHistoryTask data from web service in the
	 * background task
	 * 
	 * @author pushkar.m
	 */
	public void sendSyncStatusData() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			SendSyncStatusDataTask dataTask = null;
			if (checkSendSyncStatusData) {
				Log.i("inside SendSyncStatus", "test"
						+ checkSendSyncHistory);
				try {
					dataTask = new SendSyncStatusDataTask();
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendSyncStatusData = !checkSendSyncStatusData;
					Log.i("inside get sendSyncStatusData", "test"
							+ checkSendSyncStatusData);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}

	}

	/**
	 * Send SyncHistoryTask data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class SendSyncStatusDataTask extends AsyncTask<String, Integer, Boolean> {
		String data = "";
		SendSyncStatusDataTask() {

		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_SEND_SYNCHISTORY_SUCCESS == true
						&& IS_SEND_SYNCHISTORY_UNSUCCESS == true) {

					IS_SEND_SYNCHISTORY_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			xmlData = genrateXmlDataForSyncStatus();

			if (xmlData != null) {
				data = services.sendSyncStatusData(xmlData);

				if (data != null)
					IS_SEND_SYNCSTATUSDATA_RESPONSE_DATA = true;
				else
					IS_SEND_SYNCSTATUSDATA_UNSUCCESS = true;
			}

			if (xmlData != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			serviceProcessEnd();
				 
		}

	}
	
	
	/**
	 * @author pushkar.m This method will get RoleTabletFunction data from web
	 *         service in the background task *
	 * @param tenantId
	 * @param deviceId
	 * @param shipId
	 */
	public void getRoleTabletFunction(String tenantId, String shipId,
			String macId) {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetRoleTabletFunctionTask dataTask = null;
			if (checkGetRoleTabletFunction) {
				Log.i("inside get roles", "test" + checkGetIdl);
				try {
					dataTask = new GetRoleTabletFunctionTask(macId, tenantId,
							shipId);
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetRoleTabletFunction = !checkGetRoleTabletFunction;
					Log.i("inside get roleTabletFunction", "test"
							+ checkGetRoleTabletFunction);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}

	}

	/**
	 * Get RoleTabletFunctionTask data
	 * 
	 * @author pushkar.m
	 * 
	 */
	class GetRoleTabletFunctionTask extends AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetRoleTabletFunctionTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_ROLETABLETFUNCTION_SUCCESS == true
						&& IS_GET_ROLETABLETFUNCTION_UNSUCCESS == true) {

					IS_GET_ROLETABLETFUNCTION_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");

			WebServiceMethod services = new WebServiceMethod(context);

			data = services.getRoleTabletFunction(tenantId, shipId, macId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetRoleTabletFunctionResponse(data);
				IS_GET_ROLETABLETFUNCTION_UNSUCCESS = true;

			}

			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_GET_USER_NFC_TAGDATA;
			SyncHandler.handler.sendMessage(message);
		}

	}

	/**
	 * @author pushkar.m
	 * @param macId
	 * @param tenantId
	 * @param shipId
	 */
	public void getUserNfcTagData(String macId, String tenantId, String shipId) {

		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			GetUserNfcTagDataTask dataTask = null;
			if (checkGetUserNfcTagData) {
				Log.i("inside get roles", "test" + checkGetIdl);
				try {
					dataTask = new GetUserNfcTagDataTask(macId, tenantId,
							shipId);
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkGetRoleTabletFunction = !checkGetRoleTabletFunction;
					Log.i("inside get roleTabletFunction", "test"
							+ checkGetRoleTabletFunction);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();

					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			// serviceProcessEnd();
			serviceEndDueToNetwork();
		}
	}

	/**
	 * 
	 * @author pushkar.m
	 * 
	 */
	public class GetUserNfcTagDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		GetUserNfcTagDataTask(String macId, String tenantId, String shipId) {
			this.macId = macId;
			this.shipId = shipId;
			this.tenantId = tenantId;
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_GET_USERNFCTAGDATA_SUCCESS == true
						&& IS_GET_USERNFCTAGDATA_UNSUCCESS == true) {

					IS_GET_USERNFCTAGDATA_UNSUCCESS = false;
				}
			}

		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");

			WebServiceMethod services = new WebServiceMethod(context);

			data = services.getUserNfcTagData(macId, tenantId, shipId);

			if (data != null) {
				return true;
			}
			return false;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseGetUserNfcTagDataResponse(data);
				IS_GET_USERNFCTAGDATA_UNSUCCESS = true;

			} else {
				SyncHandler.context = context;
				Message message = new Message();
				/*
				 * message.what = SyncHandler.MSG_GET_USER_SERVICETERM;
				 * SyncHandler.handler.sendMessage(message);
				 */
				//serviceProcessEnd();

			}
			
			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
			SyncHandler.handler.sendMessage(message);
			
			//serviceProcessEnd();
		}

	}

	/**
	 * @author pushkar.m
	 */
	public void sendUserNfcTagData() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			SendUserNfcTagDataTask dataTask = null;
			if (checkSendUserNfcTagData) {
				Log.i("inside SendRmsSyncHistory", "test"
						+ checkSendUserNfcTagData);
				try {
					dataTask = new SendUserNfcTagDataTask();
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendUserNfcTagData = !checkSendUserNfcTagData;
					Log.i("inside get WorktimeSheetTask", "test"
							+ checkSendUserNfcTagData);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}
	}

	/**
	 * 
	 * @author pushkar.m
	 * 
	 */
	public class SendUserNfcTagDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
		private String tenantId;
		private String shipId;
		private String macId;

		SendUserNfcTagDataTask() {

		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_SEND_USERNFCTAGDATA_SUCCESS == true
						&& IS_SEND_USERNFCTAGDATA_UNSUCCESS == true) {

					IS_SEND_USERNFCTAGDATA_UNSUCCESS = false;
				}
			}
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);

			xmlData = genrateXmlDataForUserNfcTagData();

			if (xmlData != null && !xmlData.equals("")) {
				data = services.sendUserNfcTagDataTask(xmlData);

				if (data != null)
					IS_SEND_USERNFCTAGDATA_RESPONSE_DATA = true;
				else
					IS_SEND_USERNFCTAGDATA_UNSUCCESS = true;
			}

			if (xmlData != null) {
				return true;
			} else {
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {

				parseSendUserNfcTagDataTaskResponse(data);
				IS_SEND_WORKTIMESHEET_TASK_UNSUCCESS = true;

			}
			/*
			serviceProcessEnd(;
			CommonUtil.updateSyncStatus(context);
			*/
			
			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
			SyncHandler.handler.sendMessage(message);

		}

	}
	
	/**
	 * @author pushkar.m
	 */
	public void sendAckForTransactionalData() {
		boolean isReady = CommonUtil.checkConnectivity(context);
		if (isReady) {
			SendAckForTransactionalDataTask dataTask = null;
			if (checkSendAckTransactionalData) {
				Log.i("inside SendAckForTransactionalData", "test"
						+ checkSendAckTransactionalData);
				try {
					dataTask = new SendAckForTransactionalDataTask();
					dataTask.execute();
					dataTask.get(
							Integer.parseInt(context.getResources().getString(
									R.string.serviceCallTime)),
							TimeUnit.SECONDS);
					checkSendAckTransactionalData = !checkSendAckTransactionalData;
					Log.i("inside get SendAckForTransactionalData", "test"
							+ checkSendAckTransactionalData);
				} catch (TimeoutException ex) {
					dataTask.cancel(true);

					serviceProcessEnd();
					if (CommonUtil.getLastActiveShip(context) != null
							&& !"".equals(CommonUtil.getLastActiveShip(context))) {
						CommonUtil.setShipID(context,
								CommonUtil.getLastActiveShip(context));
						CommonUtil.setLastActiveShip(context, null);
					}
					launchPopUpEnablingNetwork(context.getResources()
							.getString(R.string.serviceTimeOutMsg), 1);

				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			serviceProcessEnd();
			if (CommonUtil.getLastActiveShip(context) != null
					&& !"".equals(CommonUtil.getLastActiveShip(context))) {
				CommonUtil.setShipID(context,
						CommonUtil.getLastActiveShip(context));
				CommonUtil.setLastActiveShip(context, null);
			}
		}
	}

	/**
	 * 
	 * @author pushkar.m
	 * 
	 */
	public class SendAckForTransactionalDataTask extends
			AsyncTask<String, Integer, Boolean> {

		private String data;
	

		SendAckForTransactionalDataTask() {
			
		}

		@SuppressWarnings("unused")
		@Override
		protected void onPreExecute() {

			super.onPreExecute();
			if (true) {
				// startProgressdialog();
			} else {
				if (IS_SEND_USERNFCTAGDATA_SUCCESS == true
						&& IS_SEND_USERNFCTAGDATA_UNSUCCESS == true) {

					IS_SEND_USERNFCTAGDATA_UNSUCCESS = false;
				}
			}
		}

		@Override
		protected Boolean doInBackground(String... params) {
			Log.i("data", "taking");
			String xmlData = null;

			WebServiceMethod services = new WebServiceMethod(context);
			
			data = services.ackTransactionData(ackIds, CommonUtil
					.getIMEI(context), transactionTables[tableIndex], context
					.getResources().getString(R.string.moduleName), Integer
					.parseInt(CommonUtil.getShipId(context)), hasMore);

			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			
			if(data.equals("0")){
				//stop service calling
				ackIds = null;
				notInId = null;
				
				tableIndex = tableIndex + 1;
				
				if(tableIndex < transactionTables.length){
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
					SyncHandler.handler.sendMessage(message);
				}else{
					tableIndex = 0;
					/*serviceProcessEnd();
					CommonUtil.updateSyncStatus(context);*/
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_GET_MODIFIED_MASTER_DATA;
					SyncHandler.handler.sendMessage(message);
				}			
				
			}else{
				
				SyncHandler.context = context;
				Message message = new Message();
				message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
				SyncHandler.handler.sendMessage(message);
			}
		}
	}

	private void cancelProgressdialog() {
		// if(dialog != null)
		// dialog.dismiss();
	}

	// ====================Parsing logic
	// here==================================================

	/**
	 * @author pushkar.m
	 * @param data
	 */
	private void parseGetUserNfcTagDataResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserNfcTagDataParser userNfcTagDataParser = new UserNfcTagDataParser();

			saxParser.parse(xmlStream, userNfcTagDataParser);

			List<UserNfcTagData> list = userNfcTagDataParser
					.getUserNfcTagDataData();

			Iterator<UserNfcTagData> untdIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (untdIterator.hasNext()) {
				UserNfcTagData userNfcTagData = (UserNfcTagData) untdIterator
						.next();
				Log.i("test", userNfcTagData.getiUserNfcTagDataId());

				List<UserNfcTagData> untdDbList = db
						.getUserNFCTagDataById(userNfcTagData
								.getiUserNfcTagDataId());
				if (untdDbList != null && untdDbList.size() > 0) {

					result = db.updateUserNFCRow(userNfcTagData);
				} else {

					result = db.insertUserNFCRow(userNfcTagData);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_WORKTIMESHEET_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author pushkar.m
	 * @param data
	 */
	private void parseSendUserNfcTagDataTaskResponse(String data) {
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserNfcTagDataParser userNfcDataParser = new UserNfcTagDataParser();

			saxParser.parse(xmlStream, userNfcDataParser);

			List<UserNfcTagData> list = userNfcDataParser
					.getUserNfcTagDataData();

			Iterator<UserNfcTagData> untdIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (untdIterator.hasNext()) {
				UserNfcTagData userNfcTagData = (UserNfcTagData) untdIterator
						.next();
				Log.i("test", userNfcTagData.getiUserNfcTagDataId());

				List<UserNfcTagData> untdDbList = db
						.getUserNFCTagDataById(userNfcTagData
								.getiUserNfcTagDataId());
				if (untdDbList != null && untdDbList.size() > 0) {

					result = db.updateDirtyFlagUserNfcTagData(userNfcTagData);
				}
			}
			Log.i("result ", "" + result);
			db.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * This method parse the xml response get after getting of tasks.
	 * 
	 * @param data
	 */
	private void parseGetTasksResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			TaskParser taskParsar = new TaskParser();

			saxParser.parse(xmlStream, taskParsar);

			List<Tasks> list = taskParsar.getTaskData();

			Iterator<Tasks> taskIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (taskIterator.hasNext()) {
				Tasks tasks = (Tasks) taskIterator.next();
				Log.i("test", tasks.getItaskId());

				List<Tasks> taskDbList = db
						.getTasksDataById(tasks.getItaskId());
				if (taskDbList != null && taskDbList.size() > 0) {

					result = db.updateTaskTable(tasks);
				} else {

					result = db.insertTaskTable(tasks);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_TASK_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of roles.
	 * 
	 * @param data
	 */
	private void parseGetRolesResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			RoleParser roleParsar = new RoleParser();

			saxParser.parse(xmlStream, roleParsar);

			List<Roles> list = roleParsar.getRoleData();

			Iterator<Roles> roleIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (roleIterator.hasNext()) {
				Roles roles = (Roles) roleIterator.next();
				Log.i("test", roles.getiRoleId());

				List<Roles> roleDbList = db.getRolesById(roles.getiRoleId());
				if (roleDbList != null && roleDbList.size() > 0) {

					result = db.updateRoleRow(roles);
				} else {

					result = db.addRoleRow(roles);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_ROLE_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of CrewDepartment.
	 * 
	 * @param data
	 */
	private void parseGetCrewDepartmentResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			CrewDepartmentParser crewDepartmentParser = new CrewDepartmentParser();

			saxParser.parse(xmlStream, crewDepartmentParser);

			List<CrewDepartment> list = crewDepartmentParser
					.getCrewDepartmentData();

			Iterator<CrewDepartment> crewDepartmentIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (crewDepartmentIterator.hasNext()) {
				CrewDepartment crewDepartment = (CrewDepartment) crewDepartmentIterator
						.next();
				Log.i("test", crewDepartment.getIcrewDepartmentId());

				List<CrewDepartment> crewDepartmentDbList = db
						.getCrewDepartMentDataById(crewDepartment
								.getIcrewDepartmentId());
				if (crewDepartmentDbList != null
						&& crewDepartmentDbList.size() > 0) {

					result = db.updateCrewDepartmentTable(crewDepartment);
				} else {

					result = db.insertCrewDepartmentTable(crewDepartment);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_CREWDEPARTMENT_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of
	 * RoleCrewDepartment.
	 * 
	 * @param data
	 */
	private void parseGetRoleCrewDepartmentResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			RoleCrewDepartmentParser roleCrewDepartmentParser = new RoleCrewDepartmentParser();

			saxParser.parse(xmlStream, roleCrewDepartmentParser);

			List<RoleCrewDepartment> list = roleCrewDepartmentParser
					.getRoleCrewDepartmentData();

			Iterator<RoleCrewDepartment> roleCrewDepartmentIterator = list
					.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (roleCrewDepartmentIterator.hasNext()) {
				RoleCrewDepartment roleCrewDepartment = (RoleCrewDepartment) roleCrewDepartmentIterator
						.next();
				Log.i("test", roleCrewDepartment.getIroleCrewDepartmentId());

				List<RoleCrewDepartment> roleCrewDepartmentDbList = db
						.getRoleCrewDepartMentDataById(roleCrewDepartment
								.getIroleCrewDepartmentId());
				if (roleCrewDepartmentDbList != null
						&& roleCrewDepartmentDbList.size() > 0) {

					result = db
							.updateRoleCrewDepartmentTable(roleCrewDepartment);
				} else {

					result = db
							.insertRoleCrewDepartmentTable(roleCrewDepartment);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_ROLE_CREWDEPARTMENT_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of
	 * CrewDepartmentTask.
	 * 
	 * @param data
	 */
	private void parseGetCrewDepartmentTaskResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			CrewDepartmentTaskParser crewDepartmentTaskParser = new CrewDepartmentTaskParser();

			saxParser.parse(xmlStream, crewDepartmentTaskParser);

			List<CrewDepartmentTask> list = crewDepartmentTaskParser
					.getCrewDepartmentTaskData();

			Iterator<CrewDepartmentTask> crewDepartmentTaskIterator = list
					.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (crewDepartmentTaskIterator.hasNext()) {
				CrewDepartmentTask crewDepartmentTask = (CrewDepartmentTask) crewDepartmentTaskIterator
						.next();
				Log.i("test", crewDepartmentTask.getIcrewDepartmentTaskId());

				List<CrewDepartmentTask> crewDepartmentTaskDbList = db
						.getCrewDepartmentTaskDataById(crewDepartmentTask
								.getIcrewDepartmentTaskId());
				if (crewDepartmentTaskDbList != null
						&& crewDepartmentTaskDbList.size() > 0) {

					result = db
							.updateCrewDepartmentTaskTable(crewDepartmentTask);
				} else {

					result = db
							.insertCrewDepartmentTaskTable(crewDepartmentTask);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_CREWDEPARTMENTTASK_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of ShipMaster.
	 * 
	 * @param data
	 */
	private void parseGetShipMasterResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			ShipMasterParser shipMasterParser = new ShipMasterParser();

			saxParser.parse(xmlStream, shipMasterParser);

			List<ShipMaster> list = shipMasterParser.getShipMasterData();

			Iterator<ShipMaster> shipMasterIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (shipMasterIterator.hasNext()) {
				ShipMaster shipMaster = (ShipMaster) shipMasterIterator.next();
				Log.i("test", shipMaster.getiShipId());

				List<ShipMaster> shipDbList = db
						.getShipMasterRowById(shipMaster.getiShipId());
				if (shipDbList != null && shipDbList.size() > 0) {

					result = db.updateShipMasterRow(shipMaster);
				} else {

					result = db.addShipMasterRow(shipMaster);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_SHIP_EXIST_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of
	 * UserServicetermRole.
	 * 
	 * @param data
	 */
	private void parseGetUserServiceTermRoleResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserServiceTermRoleParser userServiceTermRoleParser = new UserServiceTermRoleParser();

			saxParser.parse(xmlStream, userServiceTermRoleParser);

			List<UserServiceTermRole> list = userServiceTermRoleParser
					.getUserServiceTermRoleData();

			Iterator<UserServiceTermRole> ustrIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (ustrIterator.hasNext()) {
				UserServiceTermRole userServiceTermRole = (UserServiceTermRole) ustrIterator
						.next();
				Log.i("test", userServiceTermRole.getStrUserServiceTermRoleId());

				List<UserServiceTermRole> ustrDbList = db
						.getUserServiceTermRoleById(userServiceTermRole
								.getStrUserServiceTermRoleId());
				if (ustrDbList != null && ustrDbList.size() > 0) {

					result = db.updateUserServiceTermRole(userServiceTermRole);
				} else {

					result = db.addUserServiceTermRoleRow(userServiceTermRole);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_SERVICETERM_ROLE_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of UserMaster.
	 * 
	 * @param data
	 */
	private void parseGetUserMasterResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserMasterParser useraMasterParser = new UserMasterParser();

			saxParser.parse(xmlStream, useraMasterParser);

			List<UserMaster> list = useraMasterParser.getUserMasterData();

			Iterator<UserMaster> umIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (umIterator.hasNext()) {
				UserMaster userMaster = (UserMaster) umIterator.next();
				Log.i("test", userMaster.getiUserId());

				UserMaster umDbList = db.getUserMasterById(userMaster
						.getiUserId());
				if (umDbList != null && umDbList.getiUserId() != null
						&& !"".equals(umDbList.getiUserId())) {

					result = db.updateUserMasterRow(userMaster);
				} else {

					result = db.addUserMasterRow(userMaster);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_USERMASTER_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of UserServiceterm.
	 * 
	 * @param data
	 */
	private void parseGetUserServiceTermResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserServiceTermParser userServiceTermParser = new UserServiceTermParser();

			saxParser.parse(xmlStream, userServiceTermParser);

			List<UserServiceTerm> list = userServiceTermParser
					.getUserServiceTermData();

			Iterator<UserServiceTerm> ustIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (ustIterator.hasNext()) {
				UserServiceTerm userServiceTerm = (UserServiceTerm) ustIterator
						.next();
				Log.i("test", userServiceTerm.getiUserServiceTermId());

				List<UserServiceTerm> ustDbList = db
						.getUserServiceTermById(userServiceTerm
								.getiUserServiceTermId());
				if (ustDbList != null && ustDbList.size() > 0) {

					result = db.updateUserServiceTermRow(userServiceTerm);
				} else {

					result = db.addUserServiceTermRow(userServiceTerm);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_SERVICETERM_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of
	 * UserWorkTimeSheet.
	 * 
	 * @param data
	 */
	private void parseGetUserWorkTimeSheetResponse(String data) {
		Log.i("Stremdata", "" + data);
		//int hasMore = 0;
		StringBuffer ids = new StringBuffer();
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserWorkTimeSheetParser userWorkTimeSheetParser = new UserWorkTimeSheetParser();

			saxParser.parse(xmlStream, userWorkTimeSheetParser);

			List<UserWorkTimeSheet> list = userWorkTimeSheetParser
					.getUserWorkTimeSheetData();
			
			hasMore = userWorkTimeSheetParser.getHasMore();

			Iterator<UserWorkTimeSheet> uwtsIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (uwtsIterator.hasNext()) {
				UserWorkTimeSheet userWorkTimeSheet = (UserWorkTimeSheet) uwtsIterator
						.next();
				Log.i("test", userWorkTimeSheet.getiUserWorkTimeSheetId());

				List<UserWorkTimeSheet> uwtsDbList = db
						.getUserWorkTimeSheetDataById(userWorkTimeSheet
								.getiUserWorkTimeSheetId());
				if (uwtsDbList != null && uwtsDbList.size() > 0) {

					result = db.updateUserWorkTimeSheetTable(userWorkTimeSheet);
				} else {

					result = db.insertUserWorkTimeSheetTable(userWorkTimeSheet);
				}
				
				ids.append("'"+userWorkTimeSheet.getiUserWorkTimeSheetId()+"',");

			}
			Log.i("result ", "" + result);
			db.close();
			
			
			//if(hasMore > 0){
				
				if(Integer.parseInt(CommonUtil.getUserLogin(context)) == 1){
					if(hasMore == 1){
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
						SyncHandler.handler.sendMessage(message);
					}else{
						tableIndex = tableIndex + 1;
						
						if(tableIndex < transactionTables.length){
							SyncHandler.context = context;
							Message message = new Message();
							message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
							SyncHandler.handler.sendMessage(message);
						}else{
							tableIndex = 0;
							
							SyncHandler.context = context;
							Message message = new Message();
							message.what = SyncHandler.MSG_GET_SYNC_STATUS_DATA;
							SyncHandler.handler.sendMessage(message);
						}	
					}
					
				}else{
					
					if(ids != null && ids.length() > 0){
						ackIds = ids.substring(0, ids.length()-1);
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_SEND_ACK_TRANSATIONAL_DATA;
						SyncHandler.handler.sendMessage(message);
					}else{
						tableIndex = tableIndex + 1;
					
						if(tableIndex < transactionTables.length){
							SyncHandler.context = context;
							Message message = new Message();
							message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
							SyncHandler.handler.sendMessage(message);
						}else{
							tableIndex = 0;
							/*serviceProcessEnd();
							CommonUtil.updateSyncStatus(context);*/
							SyncHandler.context = context;
							Message message = new Message();
							message.what = SyncHandler.MSG_GET_MODIFIED_MASTER_DATA;
							SyncHandler.handler.sendMessage(message);
						}	
				}
					
			}			
			
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response after send data of UserWorkTimeSheet.
	 * 
	 * @param data
	 */
	private void parseSendUserWorkTimeSheetResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserWorkTimeSheetParser userWorkTimeSheetParser = new UserWorkTimeSheetParser();

			saxParser.parse(xmlStream, userWorkTimeSheetParser);

			List<UserWorkTimeSheet> list = userWorkTimeSheetParser
					.getUserWorkTimeSheetData();

			Iterator<UserWorkTimeSheet> uwtsIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (uwtsIterator.hasNext()) {
				UserWorkTimeSheet userWorkTimeSheet = (UserWorkTimeSheet) uwtsIterator
						.next();
				Log.i("test", userWorkTimeSheet.getiUserWorkTimeSheetId());

				List<UserWorkTimeSheet> uwtsDbList = db
						.getUserWorkTimeSheetDataById(userWorkTimeSheet
								.getiUserWorkTimeSheetId());
				if (uwtsDbList != null && uwtsDbList.size() > 0) {

					result = db
							.updateDirtyFlagUserWorkTimeSheetTable(userWorkTimeSheet
									.getiUserWorkTimeSheetId());
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_SEND_WORKTIMESHEET_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of
	 * UserWorkTimeSheetTask.
	 * 
	 * @param data
	 */
	private void parseGetUserWorkTimeSheetTaskResponse(String data) {
		Log.i("Stremdata", "" + data);
		//int hasMore = 0;
		StringBuffer ids = new StringBuffer();
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserWorkTimeSheetTaskParser userWorkTimeSheetTaskParser = new UserWorkTimeSheetTaskParser();

			saxParser.parse(xmlStream, userWorkTimeSheetTaskParser);

			List<UserWorkTimeSheetTask> list = userWorkTimeSheetTaskParser
					.getUserWorkTimeSheetTaskData();
			
			hasMore = userWorkTimeSheetTaskParser.getHasMore();

			Iterator<UserWorkTimeSheetTask> uwtstIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (uwtstIterator.hasNext()) {
				UserWorkTimeSheetTask userWorkTimeSheetTask = (UserWorkTimeSheetTask) uwtstIterator
						.next();
				Log.i("test", userWorkTimeSheetTask.getiUserWorkTimeSheetId());

				List<UserWorkTimeSheetTask> uwtstDbList = db
						.getUserWorkTimeSheetTaskById(userWorkTimeSheetTask
								.getiUserWorkTimeSheetTaskId());
				if (uwtstDbList != null && uwtstDbList.size() > 0) {

					result = db
							.updateUserWorkTimeSheetTaskTable(userWorkTimeSheetTask);
				} else {

					result = db
							.insertUserWorkTimeSheetTaskTable(userWorkTimeSheetTask);
				}
				
				ids.append("'"+userWorkTimeSheetTask.getiUserWorkTimeSheetTaskId()+"',");

			}
			Log.i("result ", "" + result);
			db.close();

			if (Integer.parseInt(CommonUtil.getUserLogin(context)) == 1) {
				if(hasMore == 1){
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
					SyncHandler.handler.sendMessage(message);
				}else{
					tableIndex = tableIndex + 1;
					
					if(tableIndex < transactionTables.length){
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
						SyncHandler.handler.sendMessage(message);
					}else{
						tableIndex = 0;
						
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_GET_SYNC_STATUS_DATA;
						SyncHandler.handler.sendMessage(message);
					}	
				}

			} else {

				if (ids != null && ids.length() > 0) {
					ackIds = ids.substring(0, ids.length() - 1);
					SyncHandler.context = context;
					Message message = new Message();
					message.what = SyncHandler.MSG_SEND_ACK_TRANSATIONAL_DATA;
					SyncHandler.handler.sendMessage(message);
				} else {
					tableIndex = tableIndex + 1;

					if (tableIndex < transactionTables.length) {
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_GET_TRANSACTIONAL_DATA;
						SyncHandler.handler.sendMessage(message);
					} else {
						tableIndex = 0;
						/*serviceProcessEnd();
						CommonUtil.updateSyncStatus(context);*/
						SyncHandler.context = context;
						Message message = new Message();
						message.what = SyncHandler.MSG_GET_MODIFIED_MASTER_DATA;
						SyncHandler.handler.sendMessage(message);
					}
				}

			}

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response get after getting of
	 * InternationalDates.
	 * 
	 * @param data
	 */
	private void parseGetIdlResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			InternationalDatesParser internationalDatesParser = new InternationalDatesParser();

			saxParser.parse(xmlStream, internationalDatesParser);

			List<InternationalDates> list = internationalDatesParser
					.getInternationalDatesData();

			Iterator<InternationalDates> idlIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (idlIterator.hasNext()) {
				InternationalDates internationalDates = (InternationalDates) idlIterator
						.next();
				Log.i("test", internationalDates.getIinternationalDateId());

				List<InternationalDates> idlDbList = db
						.getAllInternationalDatesById(internationalDates
								.getIinternationalDateId());
				if (idlDbList != null && idlDbList.size() > 0) {

					result = db.updateInternationalDatesRow(internationalDates);
				} else {

					result = db.insertInternationaldatesRow(internationalDates);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_IDL_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method parse the xml response after send data of
	 * UserWorkTimeSheetTask.
	 * 
	 * @param data
	 */
	private void parseSendUserWorkTimeSheetTaskResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			UserWorkTimeSheetTaskParser userWorkTimeSheetTaskParser = new UserWorkTimeSheetTaskParser();

			saxParser.parse(xmlStream, userWorkTimeSheetTaskParser);

			List<UserWorkTimeSheetTask> list = userWorkTimeSheetTaskParser
					.getUserWorkTimeSheetTaskData();

			Iterator<UserWorkTimeSheetTask> uwtstIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (uwtstIterator.hasNext()) {
				UserWorkTimeSheetTask userWorkTimeSheetTask = (UserWorkTimeSheetTask) uwtstIterator
						.next();
				Log.i("test", userWorkTimeSheetTask.getiUserWorkTimeSheetTaskId());

				List<UserWorkTimeSheetTask> uwtstDbList = db
						.getUserWorkTimeSheetTaskById(userWorkTimeSheetTask
								.getiUserWorkTimeSheetTaskId());
				if (uwtstDbList != null && uwtstDbList.size() > 0) {

					result = db
							.updateDirtyFlagUserWorkTimeSheetTaskTable(userWorkTimeSheetTask
									.getiUserWorkTimeSheetTaskId());
				}
			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_WORKTIMESHEET_TASK_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @author pushkar.m
	 * @param data
	 */
	private void parseGetRoleTabletFunctionResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			RoleTabletFunctionParser roleTabletFunctionParser = new RoleTabletFunctionParser();

			saxParser.parse(xmlStream, roleTabletFunctionParser);

			List<RoleTabletFunction> list = roleTabletFunctionParser
					.getRoleTabletFunctionData();

			Iterator<RoleTabletFunction> rtfIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (rtfIterator.hasNext()) {
				RoleTabletFunction roleTabletFunction = (RoleTabletFunction) rtfIterator
						.next();
				Log.i("test", roleTabletFunction.getiTabletRoleFunctionsId());

				List<RoleTabletFunction> rtfDbList = db
						.getTabletRoleFunctionsById(roleTabletFunction
								.getiTabletRoleFunctionsId());
				if (rtfDbList != null && rtfDbList.size() > 0) {

					result = db
							.updateRoleTabletFunctiondata(roleTabletFunction);
				} else {

					result = db.insertRoleFunctiondata(roleTabletFunction);
				}

			}
			Log.i("result ", "" + result);
			db.close();

			SyncHandler.context = context;
			IS_GET_WORKTIMESHEET_SUCCESS = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	private void parseGetTransactionalDataResponse(String data){
		
		if(transactionTables[tableIndex].equals(DatabaseSupport.TABLE_USER_WORKTIME_SHEET)){
			parseGetUserWorkTimeSheetResponse(data);
		}else if(transactionTables[tableIndex].equals(DatabaseSupport.TABLE_USER_WORKTIME_SHEET_TASK)){
			parseGetUserWorkTimeSheetTaskResponse(data);
		}	
	}
	
	private void parseGetModifiedMasterDataResponse(String data){
		
		if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_USER_MASTER)){
			parseGetUserMasterResponse(data);
		} else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_ROLE)){
			parseGetRolesResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_USER_SERVICE_TERM)){
			parseGetUserServiceTermResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_USER_SERVICE_TERM_ROLE)){
			parseGetUserServiceTermRoleResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_TASKS)){
			parseGetTasksResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_CREW_DEPARTMENT)){
			parseGetCrewDepartmentResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_ROLE_CREW_DEPARTMENT)){
			parseGetRoleCrewDepartmentResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_CREW_DEPARTMENT_TASK)){
			parseGetCrewDepartmentTaskResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_USER_NFC)){
			parseGetUserNfcTagDataResponse(data);
		}else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_INTERNATIONALDATES)){
			parseGetIdlResponse(data);
		}/*else if(masterTables[tableIndex].equals(DatabaseSupport.TABLE_IDL_LOGWORK)){
			parseGetIdlResponse(data);
		}*/
		
		tableIndex = tableIndex + 1;
		
		if(tableIndex < masterTables.length){
			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_GET_MODIFIED_MASTER_DATA;
			SyncHandler.handler.sendMessage(message);
		}else{
			tableIndex = 0;			
			SyncHandler.context = context;
			Message message = new Message();
			message.what = SyncHandler.MSG_GET_SYNC_STATUS_DATA;
			SyncHandler.handler.sendMessage(message);
		}
		
	}
	
	private void parseGetSyncStatusDataResponse(String data) {
		Log.i("Stremdata", "" + data);
		try {
			InputStream xmlStream = new ByteArrayInputStream(data.getBytes());
			SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
			SAXParser saxParser = saxParserFactory.newSAXParser();
			SyncStatusParser syncStatusParser = new SyncStatusParser();

			saxParser.parse(xmlStream, syncStatusParser);

			List<SyncStatus> list = syncStatusParser
					.getSyncStatusData();

			Iterator<SyncStatus> untdIterator = list.iterator();
			DatabaseSupport db = new DatabaseSupport(context);
			long result = 0;

			while (untdIterator.hasNext()) {
				SyncStatus syncStatus = (SyncStatus) untdIterator
						.next();
				Log.i("test", syncStatus.getiSyncStatusId());

				List<SyncStatus> untdDbList = db
						.getSyncStatusDataById(syncStatus
								.getiSyncStatusId());
				if (untdDbList != null && untdDbList.size() > 0) {
					
					//ServerAddress from server will only update on device if flgStatus is 9 from server.					
					if(!(syncStatus.getFlgStatus() == 9)){
						syncStatus .setServerAddress(untdDbList.get(0).getServerAddress());
					}
					result = db.updateSyncStatusTable(syncStatus);
					
				} else {

					result = db.insertSyncStatusTable(syncStatus);
				}

			}
			Log.i("result ", "" + result);
			db.close();

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// =========================generate xml logic
	// here=====================================================

	/**
	 * This method gets UserWorkTimeSheet data from database then create a xml
	 * for those forms and return as xml in form of string.
	 * 
	 * @return xmlString
	 */
	private String genrateXmlDataForUserWorkTimeSheet() {
		List<UserWorkTimeSheet> uwtsList = new ArrayList<UserWorkTimeSheet>();
		DatabaseSupport db = new DatabaseSupport(context);

		List<UserWorkTimeSheet> list = db.getUserWorkTimeSheetData();
		db.close();
		int count = 0;
		if (list != null && list.size() > 0) {
			for (UserWorkTimeSheet uwts : list) {
				Log.i("UserWorkTimeSheet", uwts.toString());
				if (count >= 100)
					break;
				if (uwts.getFlgIsDirty() != null
						&& uwts.getFlgIsDirty().equalsIgnoreCase("1")
						&& uwts.getiShipId().equalsIgnoreCase(
								CommonUtil.getShipId(context))) {
					uwtsList.add(uwts);
					count = count + 1;
				}
			}

			if (uwtsList != null && uwtsList.size() > 0) {
				IS_DATA_PRESENT_IN_DATABASE = true;
				Log.i("get form data ", "inside");
				Iterator<UserWorkTimeSheet> uwtsIterator = uwtsList.iterator();
				StringBuffer xmlData = new StringBuffer();
				xmlData.append("<UpdateRmsData>");

				xmlData.append("<shipId>");
				xmlData.append(CommonUtil.getShipId(context));
				xmlData.append("</shipId>");
				xmlData.append("<macId>");
				xmlData.append(CommonUtil.getIMEI(context));
				xmlData.append("</macId>");
				xmlData.append("<moduleName>");
				xmlData.append(context.getResources().getString(R.string.moduleName));
				xmlData.append("</moduleName>");

				while (uwtsIterator.hasNext()) {
					UserWorkTimeSheet userWorkTimeSheet = (UserWorkTimeSheet) uwtsIterator
							.next();

					xmlData.append("<table>");

					xmlData.append("<tableName>");
					xmlData.append("UserWorkTimeSheet");
					xmlData.append("</tableName>");

					xmlData.append("<iUserWorkTimeSheetId>");
					if (userWorkTimeSheet.getiUserWorkTimeSheetId() != null) {
						xmlData.append(userWorkTimeSheet
								.getiUserWorkTimeSheetId());
					}
					xmlData.append("</iUserWorkTimeSheetId>");

					xmlData.append("<workStartDate>");
					if (userWorkTimeSheet.getWorkStartDate() != null) {
						xmlData.append(userWorkTimeSheet.getWorkStartDate());
					}
					xmlData.append("</workStartDate>");

					xmlData.append("<workStartTime>");
					if (userWorkTimeSheet.getWorkStartTime() != null) {
						xmlData.append(userWorkTimeSheet.getWorkStartTime());
					}
					xmlData.append("</workStartTime>");

					xmlData.append("<workEndDate>");
					if (userWorkTimeSheet.getWorkEndDate() != null) {
						xmlData.append(userWorkTimeSheet.getWorkEndDate());
					}
					xmlData.append("</workEndDate>");

					xmlData.append("<workEndTime>");
					if (userWorkTimeSheet.getWorkEndTime() != null) {
						xmlData.append(userWorkTimeSheet.getWorkEndTime());
					}
					xmlData.append("</workEndTime>");

					xmlData.append("<workStartDateTime>");
					if (userWorkTimeSheet.getWorkStartDateTime() != null) {
						xmlData.append(userWorkTimeSheet.getWorkStartDateTime());
					}
					xmlData.append("</workStartDateTime>");

					xmlData.append("<workEndDateTime>");
					if (userWorkTimeSheet.getWorkEndDateTime() != null) {
						xmlData.append(userWorkTimeSheet.getWorkEndDateTime());
					}
					xmlData.append("</workEndDateTime>");

					xmlData.append("<flgDeleted>");

					xmlData.append(userWorkTimeSheet.getFlgDeleted());

					xmlData.append("</flgDeleted>");

					xmlData.append("<flgProcessed>");

					xmlData.append(userWorkTimeSheet.getFlgProcessed());

					xmlData.append("</flgProcessed>");

					xmlData.append("<flgInternationalOrderStart>");

					xmlData.append(userWorkTimeSheet.getFlgInternationalOrderStart());

					xmlData.append("</flgInternationalOrderStart>");
					
					xmlData.append("<flgInternationalOrderEnd>");

					xmlData.append(userWorkTimeSheet.getFlgInternationalOrderEnd());

					xmlData.append("</flgInternationalOrderEnd>");

					xmlData.append("<flgStatus>");

					xmlData.append(userWorkTimeSheet.getFlgStatus());

					xmlData.append("</flgStatus>");

					xmlData.append("<iUserId>");

					xmlData.append(userWorkTimeSheet.getiUserId());

					xmlData.append("</iUserId>");

					xmlData.append("<iUserServiceTermId>");

					xmlData.append(userWorkTimeSheet.getiUserServiceTermId());

					xmlData.append("</iUserServiceTermId>");

					xmlData.append("<strDescription>");

					xmlData.append(userWorkTimeSheet.getStrDescription());

					xmlData.append("</strDescription>");

					xmlData.append("<dtCreated>");
					if (userWorkTimeSheet.getDtCreated() != null
							&& !"null".equalsIgnoreCase(userWorkTimeSheet
									.getDtCreated())) {
						xmlData.append(userWorkTimeSheet.getDtCreated());
					}
					xmlData.append("</dtCreated>");

					xmlData.append("<createdBy>");
					if (userWorkTimeSheet.getCreatedBy() != null) {

						xmlData.append(userWorkTimeSheet.getCreatedBy());
					}
					xmlData.append("</createdBy>");

					xmlData.append("<dtUpdated>");
					if (userWorkTimeSheet.getDtUpdated() != null
							&& !"null".equalsIgnoreCase(userWorkTimeSheet
									.getDtUpdated())) {
						xmlData.append(userWorkTimeSheet.getDtUpdated());
					}
					xmlData.append("</dtUpdated>");

					xmlData.append("<updatedBy>");
					if (userWorkTimeSheet.getUpdatedBy() != null) {
						xmlData.append(userWorkTimeSheet.getUpdatedBy());
					}
					xmlData.append("</updatedBy>");

					xmlData.append("</table>");
				}
				xmlData.append("</UpdateRmsData>");
				return xmlData.toString().trim();
			} else {
				Log.i("get form data ", "else");
				toastMsg = toastMsg + "No Update present for the form,";
				return null;
			}
		} else {
			Log.i("get form data ", "else");
			toastMsg = toastMsg + "No update present for send filled form,";
			return null;
		}

		// return null;

	}

	/**
	 * This method gets UserWorkTimeSheetTask data from database then create a
	 * xml for those forms and return as xml in form of string.
	 * 
	 * @return xmlString
	 */
	private String genrateXmlDataForUserWorkTimeSheetTask() {
		List<UserWorkTimeSheetTask> uwtstList = new ArrayList<UserWorkTimeSheetTask>();
		DatabaseSupport db = new DatabaseSupport(context);

		List<UserWorkTimeSheetTask> list = db.getUserWorkTimeSheetTaskData();
		db.close();
		int count = 0;
		if (list != null && list.size() > 0) {
			for (UserWorkTimeSheetTask uwtst : list) {
				Log.i("UserWorkTimeSheetTask", uwtst.toString());
				if (count >= 100)
					break;
				if (uwtst.getFlgIsDirty() != null
						&& uwtst.getFlgIsDirty().equalsIgnoreCase("1")
						&& uwtst.getiShipId() != null
						&& uwtst.getiShipId().equalsIgnoreCase(
								CommonUtil.getShipId(context))) {
					uwtstList.add(uwtst);
					count = count + 1;
				}
			}

			if (uwtstList != null && uwtstList.size() > 0) {
				IS_DATA_PRESENT_IN_DATABASE = true;
				Log.i("get form data ", "inside");
				Iterator<UserWorkTimeSheetTask> uwtstIterator = uwtstList
						.iterator();
				StringBuffer xmlData = new StringBuffer();
				xmlData.append("<UpdateRmsData>");

				xmlData.append("<shipId>");
				xmlData.append(CommonUtil.getShipId(context));
				xmlData.append("</shipId>");
				xmlData.append("<macId>");
				xmlData.append(CommonUtil.getIMEI(context));
				xmlData.append("</macId>");
				xmlData.append("<moduleName>");
				xmlData.append(context.getResources().getString(R.string.moduleName));
				xmlData.append("</moduleName>");

				while (uwtstIterator.hasNext()) {
					UserWorkTimeSheetTask userWorkTimeSheetTask = (UserWorkTimeSheetTask) uwtstIterator
							.next();

					xmlData.append("<table>");

					xmlData.append("<tableName>");
					xmlData.append("UserWorkTimeSheetTask");
					xmlData.append("</tableName>");

					xmlData.append("<iUserWorkTimeSheetTaskId>");
					if (userWorkTimeSheetTask.getiUserWorkTimeSheetTaskId() != null) {
						xmlData.append(userWorkTimeSheetTask
								.getiUserWorkTimeSheetTaskId());
					}
					xmlData.append("</iUserWorkTimeSheetTaskId>");

					xmlData.append("<iUserWorkTimeSheetId>");
					if (userWorkTimeSheetTask.getiUserWorkTimeSheetId() != null) {
						xmlData.append(userWorkTimeSheetTask
								.getiUserWorkTimeSheetId());
					}
					xmlData.append("</iUserWorkTimeSheetId>");

					xmlData.append("<workStartDate>");
					if (userWorkTimeSheetTask.getWorkStartDate() != null) {
						xmlData.append(userWorkTimeSheetTask.getWorkStartDate());
					}
					xmlData.append("</workStartDate>");

					xmlData.append("<workStartTime>");
					if (userWorkTimeSheetTask.getWorkStartTime() != null) {
						xmlData.append(userWorkTimeSheetTask.getWorkStartTime());
					}
					xmlData.append("</workStartTime>");

					xmlData.append("<workEndDate>");
					if (userWorkTimeSheetTask.getWorkEndDate() != null) {
						xmlData.append(userWorkTimeSheetTask.getWorkEndDate());
					}
					xmlData.append("</workEndDate>");

					xmlData.append("<workEndTime>");
					if (userWorkTimeSheetTask.getWorkEndTime() != null) {
						xmlData.append(userWorkTimeSheetTask.getWorkEndTime());
					}
					xmlData.append("</workEndTime>");

					xmlData.append("<workStartDateTime>");
					if (userWorkTimeSheetTask.getWorkStartDateTime() != null) {
						xmlData.append(userWorkTimeSheetTask
								.getWorkStartDateTime());
					}
					xmlData.append("</workStartDateTime>");

					xmlData.append("<workEndDateTime>");
					if (userWorkTimeSheetTask.getWorkEndDateTime() != null) {
						xmlData.append(userWorkTimeSheetTask
								.getWorkEndDateTime());
					}
					xmlData.append("</workEndDateTime>");

					xmlData.append("<flgDeleted>");

					xmlData.append(userWorkTimeSheetTask.getFlgDeleted());

					xmlData.append("</flgDeleted>");

					xmlData.append("<flgStatus>");

					xmlData.append(userWorkTimeSheetTask.getFlgStatus());

					xmlData.append("</flgStatus>");

					xmlData.append("<iUserId>");

					xmlData.append(userWorkTimeSheetTask.getiUserId());

					xmlData.append("</iUserId>");

					xmlData.append("<iUserServiceTermId>");

					xmlData.append(userWorkTimeSheetTask
							.getiUserServiceTermId());

					xmlData.append("</iUserServiceTermId>");

					xmlData.append("<itaskId>");

					xmlData.append(userWorkTimeSheetTask.getItaskId());

					xmlData.append("</itaskId>");

					xmlData.append("<icrewDepartmentTaskId>");

					xmlData.append(userWorkTimeSheetTask
							.getIcrewDepartmentTaskId());

					xmlData.append("</icrewDepartmentTaskId>");

					xmlData.append("<strDescription>");

					xmlData.append(userWorkTimeSheetTask.getStrDescription());

					xmlData.append("</strDescription>");

					xmlData.append("<dtCreated>");
					if (userWorkTimeSheetTask.getDtCreated() != null
							&& !"null".equalsIgnoreCase(userWorkTimeSheetTask
									.getDtCreated())) {
						xmlData.append(userWorkTimeSheetTask.getDtCreated());
					}
					xmlData.append("</dtCreated>");

					xmlData.append("<createdBy>");
					if (userWorkTimeSheetTask.getCreatedBy() != null) {

						xmlData.append(userWorkTimeSheetTask.getCreatedBy());
					}
					xmlData.append("</createdBy>");

					xmlData.append("<dtUpdated>");
					if (userWorkTimeSheetTask.getDtUpdated() != null
							&& !"null".equalsIgnoreCase(userWorkTimeSheetTask
									.getDtUpdated())) {
						xmlData.append(userWorkTimeSheetTask.getDtUpdated());
					}
					xmlData.append("</dtUpdated>");

					xmlData.append("<updatedBy>");
					if (userWorkTimeSheetTask.getUpdatedBy() != null) {
						xmlData.append(userWorkTimeSheetTask.getUpdatedBy());
					}
					xmlData.append("</updatedBy>");

					xmlData.append("</table>");
				}
				xmlData.append("</UpdateRmsData>");
				return xmlData.toString().trim();
			} else {
				Log.i("get form data ", "else");
				toastMsg = toastMsg + "No Update present for the form,";
				return null;
			}
		} else {
			Log.i("get form data ", "else");
			toastMsg = toastMsg + "No update present for send filled form,";
			return null;
		}

		// return null;

	}

	/**
	 * Parse synchistory login
	 * 
	 * @return
	 */
	private String genrateXmlDataForSyncHistory() {
		StringBuffer xmlData = new StringBuffer();
		DatabaseSupport db = new DatabaseSupport(context);
		SyncHistory syncHistory = new SyncHistory();

		List<SyncHistory> syncHistoryList = db.getSyncHistoryRow();

		if (syncHistoryList != null && syncHistoryList.size() > 0) {
			syncHistory = syncHistoryList.get(0);
			syncHistory.setSyncOrderid(syncHistory.getSyncOrderid() + 1);
		} else {

			Date curDate = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			syncHistory.setDtAcknowledgeDate(format.format(curDate));
			syncHistory.setDtGeneratedDate(format.format(curDate));
			syncHistory.setDtSyncDate(format.format(curDate));
			syncHistory.setFlgDeleted(0);
			syncHistory.setFlgIsDirty(1);
			syncHistory.setGenerator("");
			syncHistory.setiShipId(Integer.parseInt(CommonUtil
					.getShipId(context)));
			syncHistory.setIsyncHistoryId(CommonUtil.getIMEI(context) + "_"
					+ curDate.getTime());
			syncHistory.setiTenantId(Integer.parseInt(CommonUtil
					.getTenantId(context)));
			syncHistory.setLastDownLoadDate(format.format(curDate));
			syncHistory.setLogMessage("");
			syncHistory.setProgress("");
			syncHistory.setStrFilename("");
			syncHistory.setStrMacId(CommonUtil.getIMEI(context));
			syncHistory.setStrRegisterTabletId("");
			syncHistory.setStrSyncMode("SHORE");
			syncHistory.setSyncOrderid(1);

			db.insertSyncHistorydata(syncHistory);
		}
		db.close();

		xmlData.append("<UpdateRmsData>");

		xmlData.append("<shipId>");
		xmlData.append(CommonUtil.getShipId(context));
		xmlData.append("</shipId>");
		xmlData.append("<macId>");
		xmlData.append(CommonUtil.getIMEI(context));
		xmlData.append("</macId>");
		xmlData.append("<table>");

		xmlData.append("<tableName>");
		xmlData.append("RmsSyncHistory");
		xmlData.append("</tableName>");

		xmlData.append("<irmsSyncHistoryId>");
		xmlData.append(syncHistory.getIsyncHistoryId());
		xmlData.append("</irmsSyncHistoryId>");

		xmlData.append("<dtSyncDate>");
		xmlData.append(syncHistory.getDtSyncDate());
		xmlData.append("</dtSyncDate>");

		xmlData.append("<logMessage>");
		xmlData.append(syncHistory.getLogMessage());
		xmlData.append("</logMessage>");

		xmlData.append("<progress>");
		xmlData.append(syncHistory.getProgress());
		xmlData.append("</progress>");

		xmlData.append("<generator>");
		xmlData.append(syncHistory.getGenerator());
		xmlData.append("</generator>");

		xmlData.append("<strFilename>");
		xmlData.append(syncHistory.getStrFilename());
		xmlData.append("</strFilename>");

		xmlData.append("<flgIsDirty>");
		xmlData.append(syncHistory.getFlgIsDirty());
		xmlData.append("</flgIsDirty>");

		xmlData.append("<syncOrderid>");
		xmlData.append(syncHistory.getSyncOrderid());
		xmlData.append("</syncOrderid>");
		xmlData.append("<flgDeleted>");
		xmlData.append(syncHistory.getFlgDeleted());
		xmlData.append("</flgDeleted>");

		xmlData.append("<dtAcknowledgeDate>");
		xmlData.append(syncHistory.getDtAcknowledgeDate());
		xmlData.append("</dtAcknowledgeDate>");

		xmlData.append("<dtGeneratedDate>");
		xmlData.append(syncHistory.getDtGeneratedDate());
		xmlData.append("</dtGeneratedDate>");

		xmlData.append("<lastDownLoadDate>");
		xmlData.append(syncHistory.getLastDownLoadDate());
		xmlData.append("</lastDownLoadDate>");

		xmlData.append("<strMacId>");
		xmlData.append(syncHistory.getStrMacId());
		xmlData.append("</strMacId>");

		xmlData.append("<strRegisterTabletId>");
		xmlData.append(syncHistory.getStrRegisterTabletId());
		xmlData.append("</strRegisterTabletId>");

		xmlData.append("<strSyncMode>");
		xmlData.append(syncHistory.getStrSyncMode());
		xmlData.append("</strSyncMode>");

		xmlData.append("<iTenantId>");
		xmlData.append(syncHistory.getiTenantId());
		xmlData.append("</iTenantId>");

		xmlData.append("<iShipId>");
		xmlData.append(syncHistory.getiShipId());
		xmlData.append("</iShipId>");

		xmlData.append("</table>");

		xmlData.append("</UpdateRmsData>");

		return xmlData.toString();
	}

	
	
	private String genrateXmlDataForSyncStatus() {
		StringBuffer xmlData = new StringBuffer();
		DatabaseSupport db = new DatabaseSupport(context);
		SyncStatus syncStatus = new SyncStatus();

		List<SyncStatus> syncStatusList = db.getSyncStatusData();

		if (syncStatusList != null && syncStatusList.size() > 0) {
			syncStatus = syncStatusList.get(0);			
		} else {
			Date curDate = new Date();
			
			DateFormat dtFormat = new SimpleDateFormat("yyyy-MM-dd");
			syncStatus.setCreatedBy(CommonUtil.getUserId(context));
			syncStatus.setCreatedDate(dtFormat.format(new Date()));
			syncStatus.setDataSyncMode("");
			syncStatus.setFlgDeleted(0);
			syncStatus.setFlgIsDeviceDirty(1);
			syncStatus.setFlgIsDirty(1);
			syncStatus.setFlgStatus(0);
			syncStatus.setiShipId(Integer.parseInt(CommonUtil.getShipId(context)));
			syncStatus.setiSyncStatusId(CommonUtil.getIMEI(context) + new Date().getTime());
			syncStatus.setiTenantId(Integer.parseInt(CommonUtil.getTenantId(context)));
			syncStatus.setModifiedBy(CommonUtil.getUserId(context));
			syncStatus.setModifiedDate(dtFormat.format(new Date()));
			syncStatus.setModuleName(context.getResources().getString(R.string.moduleName));
			syncStatus.setServerAddress(context.getResources().getString(R.string.defultServiceUrl));
			syncStatus.setStrMacId(CommonUtil.getIMEI(context));
			syncStatus.setDtSyncDate(dtFormat.format(new Date()));
			syncStatus.setSyncMode("");
			syncStatus.setSyncTime("");
			
			db.insertSyncStatusTable(syncStatus);
		}
		db.close();

		xmlData.append("<UpdateRmsData>");

		xmlData.append("<shipId>");
		xmlData.append(CommonUtil.getShipId(context));
		xmlData.append("</shipId>");
		xmlData.append("<macId>");
		xmlData.append(CommonUtil.getIMEI(context));
		xmlData.append("</macId>");
		xmlData.append("<table>");

		xmlData.append("<tableName>");
		xmlData.append("SyncStatus");
		xmlData.append("</tableName>");

		xmlData.append("<iSyncStatusId>");
		xmlData.append(syncStatus.getiSyncStatusId());
		xmlData.append("</iSyncStatusId>");

		xmlData.append("<dtSyncDate>");
		xmlData.append(syncStatus.getDtSyncDate());
		xmlData.append("</dtSyncDate>");

		xmlData.append("<syncMode>");
		xmlData.append(syncStatus.getSyncMode());
		xmlData.append("</syncMode>");

		xmlData.append("<dataSyncMode>");
		xmlData.append(syncStatus.getDataSyncMode());
		xmlData.append("</dataSyncMode>");

		xmlData.append("<serverAddress>");
		xmlData.append(syncStatus.getServerAddress());
		xmlData.append("</serverAddress>");

		xmlData.append("<strMacId>");
		xmlData.append(syncStatus.getStrMacId());
		xmlData.append("</strMacId>");

		xmlData.append("<flgIsDirty>");
		xmlData.append(syncStatus.getFlgIsDirty());
		xmlData.append("</flgIsDirty>");

		xmlData.append("<moduleName>");
		xmlData.append(syncStatus.getModuleName());
		xmlData.append("</moduleName>");
		
		xmlData.append("<flgDeleted>");
		xmlData.append(syncStatus.getFlgDeleted());
		xmlData.append("</flgDeleted>");

		xmlData.append("<flgStatus>");
		xmlData.append(syncStatus.getFlgStatus());
		xmlData.append("</flgStatus>");

		xmlData.append("<flgIsDeviceDirty>");
		xmlData.append(syncStatus.getFlgIsDeviceDirty());
		xmlData.append("</flgIsDeviceDirty>");

		xmlData.append("<syncTime>");
		xmlData.append(syncStatus.getSyncTime());
		xmlData.append("</syncTime>");

		xmlData.append("<createdDate>");
		xmlData.append(syncStatus.getCreatedDate());
		xmlData.append("</createdDate>");

		xmlData.append("<createdBy>");
		xmlData.append(syncStatus.getCreatedBy());
		xmlData.append("</createdBy>");

		xmlData.append("<modifiedDate>");
		xmlData.append(syncStatus.getModifiedDate());
		xmlData.append("</modifiedDate>");

		xmlData.append("<iTenantId>");
		xmlData.append(syncStatus.getiTenantId());
		xmlData.append("</iTenantId>");

		xmlData.append("<iShipId>");
		xmlData.append(syncStatus.getiShipId());
		xmlData.append("</iShipId>");
		
		xmlData.append("<modifiedBy>");
		xmlData.append(syncStatus.getModifiedBy());
		xmlData.append("</modifiedBy>");

		xmlData.append("</table>");

		xmlData.append("</UpdateRmsData>");

		return xmlData.toString();
	}

	
	
	/**
	 * @author pushkar.m
	 * @return
	 */
	private String genrateXmlDataForUserNfcTagData() {
		StringBuffer xmlData = new StringBuffer();
		DatabaseSupport db = new DatabaseSupport(context);

		List<UserNfcTagData> userNfcTagDataList = new ArrayList<UserNfcTagData>();
		List<UserNfcTagData> dataList = db.getUserNFCRow();

		if (dataList != null && dataList.size() > 0) {
			for (UserNfcTagData untd : dataList) {
				if (untd.getFlgIsDirty() != null
						&& untd.getFlgIsDirty().equals("1"))
					userNfcTagDataList.add(untd);
			}
		}

		xmlData.append("<UpdateRmsData>");

		xmlData.append("<shipId>");
		xmlData.append(CommonUtil.getShipId(context));
		xmlData.append("</shipId>");
		xmlData.append("<macId>");
		xmlData.append(CommonUtil.getIMEI(context));
		xmlData.append("</macId>");

		if (userNfcTagDataList != null && userNfcTagDataList.size() > 0) {
			for (UserNfcTagData userNfcTagData : userNfcTagDataList) {
				xmlData.append("<table>");
				xmlData.append("<tableName>");
				xmlData.append("UserNfcTagData");
				xmlData.append("</tableName>");

				xmlData.append("<iUserNfcTagDataId>");
				xmlData.append(userNfcTagData.getiUserNfcTagDataId());
				xmlData.append("</iUserNfcTagDataId>");

				xmlData.append("<cardType>");
				xmlData.append(userNfcTagData.getCardType());
				xmlData.append("</cardType>");

				xmlData.append("<iUserId>");
				xmlData.append(userNfcTagData.getiUserId());
				xmlData.append("</iUserId>");

				xmlData.append("<strNfcTagId>");
				xmlData.append(userNfcTagData.getStrNfcTagId());
				xmlData.append("</strNfcTagId>");

				xmlData.append("<dtUpdated>");
				xmlData.append(userNfcTagData.getDtUpdated());
				xmlData.append("</dtUpdated>");

				xmlData.append("<dtCreated>");
				xmlData.append(userNfcTagData.getDtCreated());
				xmlData.append("</dtCreated>");

				xmlData.append("<flgIsDirty>");
				xmlData.append(userNfcTagData.getFlgIsDirty());
				xmlData.append("</flgIsDirty>");

				xmlData.append("<flgDeleted>");
				xmlData.append(userNfcTagData.getFlgDeleted());
				xmlData.append("</flgDeleted>");

				xmlData.append("<iTenantId>");
				xmlData.append(userNfcTagData.getiTenantId());
				xmlData.append("</iTenantId>");

				xmlData.append("<iShipId>");
				xmlData.append(userNfcTagData.getiShipId());
				xmlData.append("</iShipId>");
				xmlData.append("</table>");

			}
		}
		db.close();

		xmlData.append("</UpdateRmsData>");

		return xmlData.toString();
	}

	/**
	 * @author ripunjay.s This method closes progress bar and open desire
	 *         page(home or admin)
	 */
	private void serviceProcessEnd() {

		if (AdminMainActivity.miActionProgressItem != null) {
			AdminMainActivity.miActionProgressItem.setVisible(false);
		}
		
		if (!(context instanceof CronService)) {
			Intent i = new Intent(context, LoginActivity.class);
			context.startActivity(i);
			((Activity) context).finish();
		}
		else{
	    	 CommonUtil.scheduleProcess(context);
		}
	}

	/**
	 * When network is not connected then after msg send stop on that place.
	 */
	private void serviceEndDueToNetwork() {

	}

	/**
	 * @author ripunjay.s
	 * @param Message
	 * @param position
	 */
	private void launchPopUpEnablingNetwork(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		Context contex = null;
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				if (AdminMainActivity.miActionProgressItem != null) {
					AdminMainActivity.miActionProgressItem.setVisible(false);
				}
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				if (AdminMainActivity.miActionProgressItem != null) {
					AdminMainActivity.miActionProgressItem.setVisible(false);
				}
			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author ripunjay.s
	 * @param Message
	 * @param position
	 */
	private void displayPopUpNorNoShipExist(String Message) {
		// final Editor editor = msharedPreferences.edit();
		Context context = null;
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();

				// serviceProcessEnd();

			}
		});

		dialog.show();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
	}

	/**
	 * @author ripunjay.s
	 * @param Message
	 * @param position
	 */
	private void launchPopUpForNoSync(String Message, final int position) {
		// final Editor editor = msharedPreferences.edit();
		final Context context = null;
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.custom_dialog_confirmation_network_enable_popup);
		Button crossButton = (Button) dialog.findViewById(R.id.crossButton);
		Button cancel = (Button) dialog.findViewById(R.id.cancel);
		cancel.setVisibility(View.GONE);
		crossButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				// serviceProcessEnd();
				if (CommonUtil.getLastActiveShip(context) != null
						&& !"".equals(CommonUtil.getLastActiveShip(context))) {
					CommonUtil.setShipID(context,
							CommonUtil.getLastActiveShip(context));
					CommonUtil.setLastActiveShip(context, null);
				}
			}
		});

		TextView textView = (TextView) dialog.findViewById(R.id.dialogMessage);
		textView.setText(Message);
		Button ok = (Button) dialog.findViewById(R.id.ok);
		ok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				// serviceProcessEnd();
				if (CommonUtil.getLastActiveShip(context) != null
						&& !"".equals(CommonUtil.getLastActiveShip(context))) {
					CommonUtil.setShipID(context,
							CommonUtil.getLastActiveShip(context));
					CommonUtil.setLastActiveShip(context, null);
				}

			}
		});

		dialog.show();
		dialog.setCanceledOnTouchOutside(false);
		dialog.setCancelable(false);
	}
}
