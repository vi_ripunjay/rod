package com.rms.service;

import java.util.Date;

import com.rms.SyncHandler;
import com.rms.utils.CommonUtil;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

/**
 * @author Ripunjay Shukla
 * 
 * 
 */
public class CronService extends Service {

	String tag = "TestService";
	public static long installerHangClockStart = new Date().getTime();
	SharedPreferences settings;
	public static Context mContext = null;

	@Override
	public void onCreate() {
		// super.onCreate();
		mContext = this;
		//Toast.makeText(this, "Service created...", Toast.LENGTH_LONG).show();

		settings = getSharedPreferences("Counters", Context.MODE_PRIVATE);
		Editor settingsEditor = settings.edit();
		settingsEditor.putBoolean("installerRunning", false);
		settingsEditor.putLong("hangStartTime", new Date().getTime());
		settingsEditor.apply();
		Log.i(tag, "Service created...");
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		Log.i(tag, "Service started...");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		/**
		 * below commwnt need to open for auto sync
		 */
		/*SyncHandler.context = mContext;
		Message shipMessage = new Message();
		shipMessage.what = SyncHandler.MSG_SEND_SYNC_HISTORY;
		SyncHandler.handler.sendMessage(shipMessage);*/
		Log.i(tag, "Service onStartCommand started...");

		return 0;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		//Toast.makeText(this, "Service destroyed...", Toast.LENGTH_LONG).show();
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

}
