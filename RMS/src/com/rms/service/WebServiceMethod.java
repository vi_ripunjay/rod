/**
 * @file WebServiceMethod.java
 * 
 * @author Prashant Jha
 * 
 * @date  September 21,2013
 */

package com.rms.service;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;

import android.content.Context;
import android.util.Log;

public class WebServiceMethod extends SyncServices {

	private static final String TAG = WebServiceMethod.class.getName();

	/*private static final String METHOD_LOGIN_USER = "validateUser";// loginDeviceUser";*/
	private static final String METHOD_LOGIN_USER = "validateUserOnDevice";
	private static final String METHOD_GET_TASKS = "getTasks";
	private static final String METHOD_GET_ROLES = "getAllRoles";
	private static final String METHOD_GET_CREWDEPARTMENT = "getCrewDepartment";
	private static final String METHOD_GET_ROLE_CREWDEPARTMENT = "getRoleCrewDepartment";
	private static final String METHOD_GET_CREWDEPARTMENT_TASK = "getCrewDepartmentTask";
	private static final String METHOD_GET_IDL = "getInternationalDates";
	private static final String METHOD_GET_SHIPMASTER = "getShipMaster";
	private static final String METHOD_GET_USERMASTER = "getUserMaster";
	private static final String METHOD_GET_USERSERVICETERM = "getAllUserServiceTerm";
	private static final String METHOD_GET_USERSERVICETERM_ROLE = "getAllUserServiceTermRole";
	private static final String METHOD_GET_WORKTIMESHEET = "getWorkTimeSheet";
	private static final String METHOD_GET_WORKTIMESHEET_TASK = "getWorkTimeSheettask";

	private static final String METHOD_SEND_WORKTIMESHEET = "updateUorkWorkTimeSheet";
	private static final String METHOD_SEND_WORKTIMESHEET_TASK = "updateUserWorkTimeSheetTask";
	private static final String METHOD_SEND_RODData = "updateRodData";

	private static final String METHOD_GETSHIPATTRIBUTES = "GetShipAttributes";
	private static final String METHOD_GETCREWLISTDATA = "GetCrewListData";

	private static final String METHOD_ACK_USERWORKTIMESHEET = "ackUserWorkTimeSheet";
	private static final String METHOD_ACK_USERWORKTIMESHEET_TASK = "ackUserWorkTimeSheetTask";
	private static final String METHOD_MATCH_VISYNC_HISTORY = "matchVISyncHistory";
	private static final String METHOD_GET_SHIP_EXIST = "isShipExist";

	private static final String METHOD_GET_ROLE_TABLET_FUNCTION = "getRoleTabletFunction";
	private static final String METHOD_SEND_RMSSYNCHISTORY = "updateRodSyncHistory";

	private static final String METHOD_GET_USER_NFC_TAG_DATA = "getUserNfcTagData";
	private static final String METHOD_SEND_USER_NFC_TAG_DATA = "updateUserNfcTagData";
	
	private static final String METHOD_GET_TRANSACTIONAL_DATA = "getDeviceTransactionalData";
	private static final String METHOD_SEND_ACK_TRANSACTIONAL_DATA = "ackRodTransactionData";
	
	private static final String METHOD_MODIFIED_MASTER_DATA = "getModifiedMasterData";
	
	private static final String METHOD_GET_SYNC_STATUS_DATA = "getSyncStatusData";
	
	private static final String METHOD_SEND_SYNC_STATUS_DATA = "updateSyncStatusData";

	private PropertyInfo propertyMACID = null;
	private PropertyInfo propertyShipID = null;
	private PropertyInfo propertyModuleName = null;
	private PropertyInfo propertyUserID = null;
	private PropertyInfo propertyUserPassword = null;
	private PropertyInfo propertyTenantId = null;

	/**
	 * This method will call at the time of web service object creation
	 */
	public WebServiceMethod() {
		super();
	}

	/**
	 * This method will call at the time of web service object creation
	 * 
	 * @param port
	 */
	public WebServiceMethod(Context port) {
		super(port);
	}

	/**
	 * @author ripunjay.s
	 * @param tenantId
	 * @param shipId
	 * @return
	 */
	public String ackSendWorkTimeSheet(String data, String shipId) {
		String method = METHOD_ACK_USERWORKTIMESHEET;
		PropertyInfo propertyXmlData = new PropertyInfo();
		propertyXmlData.setName("arg0");
		propertyXmlData.setValue(data);
		propertyXmlData.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyXmlData, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String ackSendWorkTimeSheetTask(String ids, String shipId) {
		String method = METHOD_ACK_USERWORKTIMESHEET_TASK;
		PropertyInfo idsInfo = new PropertyInfo();
		idsInfo.setName("arg0");
		idsInfo.setValue(ids);
		idsInfo.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {
			init(method, idsInfo, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param xmlData
	 * @return
	 */
	public String matchVISyncHistory(String xmlData) {
		String method = METHOD_MATCH_VISYNC_HISTORY;

		PropertyInfo propertyXmlDataInfo = new PropertyInfo();
		propertyXmlDataInfo.setName("arg0");
		propertyXmlDataInfo.setValue(xmlData);
		propertyXmlDataInfo.setType(String.class);

		try {
			init(method, propertyXmlDataInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception ex) {
			Log.e(TAG, "" + ex.getMessage());
		}
		return response;

	}

	public String getShipExistOrNot(int shipId) {
		String method = METHOD_GET_SHIP_EXIST;

		PropertyInfo propertyShipDataInfo = new PropertyInfo();
		propertyShipDataInfo.setName("arg0");
		propertyShipDataInfo.setValue(shipId);
		propertyShipDataInfo.setType(Integer.class);

		try {
			init(method, propertyShipDataInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception ex) {
			Log.e(TAG, "" + ex.getMessage());
		}
		return response;
	}

	/**
	 * 
	 * @param shipId
	 * @param macId
	 * @return xml as string
	 */
	public String getWorkTimeShhet(String shipId, String macId) {

		String method = METHOD_GET_WORKTIMESHEET;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		Log.i("shipId inside webservice", shipId);
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * Login check for user
	 * 
	 * @param strUserId
	 * @param strPassword
	 * @param macId
	 * @return
	 */
	public String loginUser(String strUserId, String strPassword, String macId, String moduleName) {

		String method = METHOD_LOGIN_USER;

		propertyUserID = new PropertyInfo();
		propertyUserID.setName("arg0");
		propertyUserID.setValue(strUserId);
		propertyUserID.setType(String.class);

		propertyUserPassword = new PropertyInfo();
		propertyUserPassword.setName("arg1");
		propertyUserPassword.setValue(strPassword);
		propertyUserPassword.setType(String.class);

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg2"); 
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);
		
		PropertyInfo moduleNameInfo = new PropertyInfo();
		moduleNameInfo.setName("arg3");
		moduleNameInfo.setValue(moduleName);
		moduleNameInfo.setType(String.class);
		

		try {
			init(method, propertyUserID, propertyUserPassword , propertyMACID, moduleNameInfo);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
			Log.e(TAG, "" + e.toString());
		}
		return response;
	}

	public String getWorkTimeSheetTask(String shipId, String macId) {

		String method = METHOD_GET_WORKTIMESHEET_TASK;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;

	}

	public String getWorkTimeSheet(String shipId, String macId) {

		String method = METHOD_GET_WORKTIMESHEET_TASK;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;

	}

	public String getShipAttributes(String shipId, String macId) {

		String method = METHOD_GETSHIPATTRIBUTES;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getCrewListData(String shipId, String macId) {

		String method = METHOD_GETCREWLISTDATA;
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {
			init(method, propertyMACID, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendWorkTimeSheet(String shipId, String macId, String xmlData) {

		Log.i("ship Id and deviceId for get updated user insdie web service  : ",
				"shipId=" + shipId + " device Id = " + macId);
		String method = METHOD_SEND_RODData;
		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 * 
		 * propertyShipID = new PropertyInfo(); propertyShipID.setName("arg1");
		 * propertyShipID.setValue(Integer.parseInt((shipId != null &&
		 * !"".equals(shipId) ? shipId : "0")));
		 * propertyShipID.setType(Integer.class);
		 */

		PropertyInfo propertyXmlData = new PropertyInfo();
		propertyXmlData.setName("arg0");
		propertyXmlData.setValue(xmlData);
		propertyXmlData.setType(String.class);

		try {
			init(method, propertyXmlData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;

	}

	/**
	 * Get Task
	 * 
	 * @param shipId
	 * @param macId
	 * @param xmlData
	 * @return
	 */
	public String getTask(String macId, String tenantId, String shipId) {

		String method = METHOD_GET_TASKS;

		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		try {
			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/*
	 * Get roles data
	 */
	public String getRoles(String macId, String tenantId, String shipId) {

		String method = METHOD_GET_ROLES;

		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		try {
			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * get CrewDepartment data
	 * 
	 * @param macId
	 * @param tenantId
	 * @param shipId
	 * @return
	 */
	public String getCrewDepartment(String macId, String tenantId, String shipId) {

		String method = METHOD_GET_CREWDEPARTMENT;

		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		try {
			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * getRoleCrewDepartment
	 * 
	 * @param macId
	 * @param tenantId
	 * @param shipId
	 * @return
	 */
	public String getRoleCrewDepartment(String macId, String tenantId,
			String shipId) {

		String method = METHOD_GET_ROLE_CREWDEPARTMENT;

		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		try {
			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * getCrewDepartmentTask
	 * 
	 * @param macId
	 * @param tenantId
	 * @param shipId
	 * @return
	 */
	public String getCrewDepartmentTask(String macId, String tenantId,
			String shipId) {

		String method = METHOD_GET_CREWDEPARTMENT_TASK;
		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		try {

			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getShipMaster(String macId, String tenantId, String shipId) {

		String method = METHOD_GET_SHIPMASTER;
		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */

		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {

			// init(method, propertyMACID, propertyShipID, propertyTenantId);
			init(method, propertyTenantId, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUserMaster(String macId, String tenantId, String shipId) {

		String method = METHOD_GET_USERMASTER;
		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {

			init(method, propertyTenantId, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getUserServiceTerm(String macId, String tenantId,
			String shipId) {
		String method = METHOD_GET_USERSERVICETERM;

		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);
		try {

			init(method, propertyTenantId, propertyShipID);
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();

		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * UserServiceTermRole
	 * 
	 * @param macId
	 * @param tenantId
	 * @param hasMore
	 * @return
	 */
	public String getUserServiceTermRole(String macId, String tenantId,
			String shipId) {
		String method = METHOD_GET_USERSERVICETERM_ROLE;
		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);
		try {

			init(method, propertyTenantId, propertyShipID);
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}
	
	public String getTransactionalData(String macId, String shipId, String moduleName, String tableName, String notInId, int loginFlag) {
		String method = METHOD_GET_TRANSACTIONAL_DATA;
		
		propertyMACID = new PropertyInfo(); 
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);		

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);
		
		PropertyInfo propertyModuleName = new PropertyInfo();
		propertyModuleName.setName("arg2");
		propertyModuleName.setValue(moduleName);
		propertyModuleName.setType(String.class);
		
		PropertyInfo propertyTableName = new PropertyInfo();
		propertyTableName.setName("arg3");
		propertyTableName.setValue(tableName);
		propertyTableName.setType(String.class);
		
		PropertyInfo propertyNotInId = new PropertyInfo();
		propertyNotInId.setName("arg4");
		propertyNotInId.setValue(notInId);
		propertyNotInId.setType(String.class);
		
		PropertyInfo propertyLogInFlag = new PropertyInfo();
		propertyLogInFlag.setName("arg5");
		propertyLogInFlag.setValue(loginFlag);
		propertyLogInFlag.setType(Integer.class);
		
		try {

			init(method, propertyMACID, propertyShipID, propertyModuleName,propertyTableName, propertyNotInId, propertyLogInFlag);
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}
	
	public String ackTransactionData(String ids, String macId, String tableName, String moduleName, int shipId, int hasMore){
		String method = METHOD_SEND_ACK_TRANSACTIONAL_DATA;
		
		PropertyInfo propertyIds = new PropertyInfo(); 
		propertyIds.setName("arg0");
		propertyIds.setValue(ids);
		propertyIds.setType(String.class);		
		
		propertyMACID = new PropertyInfo(); 
		propertyMACID.setName("arg1");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);	
		
		PropertyInfo propertyTableName = new PropertyInfo();
		propertyTableName.setName("arg2");
		propertyTableName.setValue(tableName);
		propertyTableName.setType(String.class);
		
		PropertyInfo propertyModuleName = new PropertyInfo();
		propertyModuleName.setName("arg3");
		propertyModuleName.setValue(moduleName);
		propertyModuleName.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg4");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(Integer.class);		
		
		PropertyInfo propertyHasMore = new PropertyInfo();
		propertyHasMore.setName("arg5");
		propertyHasMore.setValue(hasMore);
		propertyHasMore.setType(Integer.class);
		
		try {

			init(method, propertyIds, propertyMACID, propertyTableName, propertyModuleName, propertyShipID, propertyHasMore);
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		
		return response;
	}
	
	
	public String getModifiedMasterData(String notInId, String tableName, int tenantId, int shipId){
		
		String method = METHOD_MODIFIED_MASTER_DATA;
		
		PropertyInfo propertyNotInId = new PropertyInfo();
		propertyNotInId.setName("arg0");
		propertyNotInId.setValue(notInId);
		propertyNotInId.setType(String.class);
		
		PropertyInfo propertyTableName = new PropertyInfo();
		propertyTableName.setName("arg1");
		propertyTableName.setValue(tableName);
		propertyTableName.setType(String.class);
		
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg2");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);
		
		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg3");
		propertyShipID.setValue(shipId);
		propertyShipID.setType(Integer.class);
		
		try {

			init(method, propertyNotInId, propertyTableName, propertyTenantId, propertyShipID);
			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		
		return response;
	}
	

	public String getIdl(String macId, String tenantId, String shipId) {

		String method = METHOD_GET_IDL;

		/*
		 * propertyMACID = new PropertyInfo(); propertyMACID.setName("arg0");
		 * propertyMACID.setValue(macId); propertyMACID.setType(String.class);
		 */
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {

			init(method, propertyTenantId, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param xmlData
	 * @return
	 */
	public String sendSyncHistoryTask(String xmlData) {

		String method = METHOD_SEND_RMSSYNCHISTORY;

		PropertyInfo propertyXmlData = new PropertyInfo();
		propertyXmlData.setName("arg0");
		propertyXmlData.setValue(xmlData);
		propertyXmlData.setType(String.class);

		try {

			init(method, propertyXmlData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}
	
	
	/**
	 * @author pushkar.m
	 * @param xmlData
	 * @return
	 */
	public String sendSyncStatusData(String xmlData) {

		String method = METHOD_SEND_SYNC_STATUS_DATA;

		PropertyInfo propertyXmlData = new PropertyInfo();
		propertyXmlData.setName("arg0");
		propertyXmlData.setValue(xmlData);
		propertyXmlData.setType(String.class);

		try {

			init(method, propertyXmlData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String sendWorkTimeSheetTask(String macId, String tenantId,
			String xmlData) {

		String method = METHOD_SEND_RODData;

		PropertyInfo propertyXmlData = new PropertyInfo();
		propertyXmlData.setName("arg0");
		propertyXmlData.setValue(xmlData);
		propertyXmlData.setType(String.class);

		try {

			init(method, propertyXmlData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getRoleTabletFunction(String tenantId, String shipId,
			String macId) {

		String method = METHOD_GET_ROLE_TABLET_FUNCTION;

		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {

			init(method, propertyTenantId);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}

		return response;
	}

	/**
	 * @author pushkar.m
	 * @param macId
	 * @param tenantId
	 * @param shipId
	 * @return
	 */

	public String getUserNfcTagData(String macId, String tenantId, String shipId) {

		String method = METHOD_GET_USER_NFC_TAG_DATA;

		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg0");
		propertyTenantId.setValue(Integer.parseInt((tenantId != null
				&& !"".equals(tenantId) ? tenantId : "0")));
		propertyTenantId.setType(Integer.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(Integer.parseInt((shipId != null
				&& !"".equals(shipId) ? shipId : "0")));
		propertyShipID.setType(Integer.class);

		try {

			init(method, propertyTenantId, propertyShipID);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}
	
	
	/**
	 * @author pushkar.m
	 * @param shipId
	 * @param tenantId
	 * @return
	 */
	public String getSyncStatusData(String macId, int shipId , int tenantId, String moduleName){
		String method = METHOD_GET_SYNC_STATUS_DATA;
		
		propertyMACID = new PropertyInfo();
		propertyMACID.setName("arg0");
		propertyMACID.setValue(macId);
		propertyMACID.setType(String.class);

		propertyShipID = new PropertyInfo();
		propertyShipID.setName("arg1");
		propertyShipID.setValue(shipId );
		propertyShipID.setType(Integer.class);
		
		propertyTenantId = new PropertyInfo();
		propertyTenantId.setName("arg2");
		propertyTenantId.setValue(tenantId);
		propertyTenantId.setType(Integer.class);
		
		PropertyInfo propertyModuleName = new PropertyInfo();
		propertyModuleName.setName("arg3");
		propertyModuleName.setValue(moduleName);
		propertyModuleName.setType(String.class);

		

		try {

			init(method, propertyMACID, propertyShipID, propertyTenantId, propertyModuleName);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	/**
	 * @author pushkar.m
	 * @param xmlData
	 * @return
	 */
	public String sendUserNfcTagDataTask(String xmlData) {

		String method = METHOD_SEND_USER_NFC_TAG_DATA;

		PropertyInfo propertyXmlData = new PropertyInfo();
		propertyXmlData.setName("arg0");
		propertyXmlData.setValue(xmlData);
		propertyXmlData.setType(String.class);

		try {

			init(method, propertyXmlData);

			SoapObject soapobj = (SoapObject) envelope.bodyIn;
			Object property = soapobj.getProperty(0);
			response = property.toString();
		} catch (Exception e) {
			Log.e(TAG, "" + e.getMessage());
		}
		return response;
	}

	public String getWeather(String xmlInput) throws MalformedURLException,
			IOException {

		// Code to make a webservice HTTP request
		String responseString = "";
		String outputString = "";
		String wsURL = "http://thearkbis.com:8282/com.ark.session.smsWS.service/SmsService?wsdl";
		// Uri url = Uri.parse(wsURL);
		java.net.URL url = new java.net.URL(wsURL);
		URLConnection connection = url.openConnection();
		HttpURLConnection httpConn = (HttpURLConnection) connection;
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		// String xmlInput =
		// "  <soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"http://litwinconsulting.com/webservices/\">\n"
		// +
		// "   <soapenv:Header/>\n" +
		// "   <soapenv:Body>\n" +
		// "      <web:GetWeather>\n" +
		// "         <!--Optional:-->\n" +
		// "         <web:City>" + city + "</web:City>\n" +
		// "      </web:GetWeather>\n" +
		// "   </soapenv:Body>\n" +
		// "  </soapenv:Envelope>";

		byte[] buffer = new byte[xmlInput.length()];
		buffer = xmlInput.getBytes();
		bout.write(buffer);
		byte[] b = bout.toByteArray();
		String SOAPAction = "http://service.smsWS.session.ark.com/ackContent";
		// Set the appropriate HTTP parameters.
		httpConn.setRequestProperty("Content-Length", String.valueOf(b.length));
		httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
		httpConn.setRequestProperty("SOAPAction", SOAPAction);
		httpConn.setRequestMethod("POST");
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		OutputStream out = httpConn.getOutputStream();
		// Write the content of the request to the outputstream of the HTTP
		// Connection.
		out.write(b);
		out.close();
		// Ready with sending the request.

		// Read the response.
		InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
		BufferedReader in = new BufferedReader(isr);

		// Write the SOAP message response to a String.
		while ((responseString = in.readLine()) != null) {
			outputString = outputString + responseString;
		}
		// Parse the String output to a org.w3c.dom.Document and be able to
		// reach every node with the org.w3c.dom API.

		return outputString;
	}

	public String CallWebService(String url, String soapAction, String envelope) {
		final DefaultHttpClient httpClient = new DefaultHttpClient();
		// request parameters

		// HttpParams params = httpClient.getParams();
		// HttpConnectionParams.setConnectionTimeout(params, 20000);
		// HttpConnectionParams.setSoTimeout(params, 25000);
		// set parameter
		HttpProtocolParams.setUseExpectContinue(httpClient.getParams(), true);

		// POST the envelope
		HttpPost httppost = new HttpPost(
				"http://thearkbis.com:8282/com.ark.session.smsWS.service/SmsService?wsdl");
		// add headers
		httppost.setHeader("soapaction",
				"http://service.smsWS.session.ark.com/ackContent");
		httppost.setHeader("Content-Type", "text/xml; charset=utf-8");
		Log.i("Alok", "Alok=====");
		String responseString = "";
		try {

			// the entity holds the request
			HttpEntity entity = new StringEntity(envelope);
			httppost.setEntity(entity);

			// Response handler

			ResponseHandler<String> rh = new ResponseHandler<String>() {
				// invoked when client receives response

				public String handleResponse(HttpResponse response)
						throws ClientProtocolException, IOException {

					// get response entity
					HttpEntity entity = response.getEntity();

					// read the response as byte array
					StringBuffer out = new StringBuffer();
					byte[] b = EntityUtils.toByteArray(entity);

					// write the response byte array to a string buffer
					out.append(new String(b, 0, b.length));

					return out.toString();
				}
			};

			responseString = httpClient.execute(httppost, rh);

		} catch (Exception e) {
			Log.v("exception", e.toString());
		}

		String xml = responseString.toString();
		// close the connection
		System.out.println("xml file ------" + xml);
		httpClient.getConnectionManager().shutdown();
		return responseString;
	}

}
