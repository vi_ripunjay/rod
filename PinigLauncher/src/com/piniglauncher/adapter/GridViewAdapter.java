package com.piniglauncher.adapter;

import com.piniglauncher.R;
import com.piniglauncher.model.GridItems;

import android.content.Context;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class GridViewAdapter extends BaseAdapter {
 
 Context context;
 private PackageManager packageManager;

 
 public class ViewHolder {
  public ImageView imageView;
  public TextView textTitle;
 }
 
 private GridItems[] items;
 private LayoutInflater mInflater;
 
 public GridViewAdapter(Context context, GridItems[] locations) {
 
  mInflater = (LayoutInflater) context
    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
  this.context = context;
  items = locations;
  packageManager = context.getPackageManager();

 }
 
 public GridItems[] getItems() {
  return items;
 }
 
 public void setItems(GridItems[] items) {
  this.items = items;
 }
 
 @Override
 public int getCount() {
  if (items != null) {
   return items.length;
  }
  return 0;
 }
 
 @Override
 public void notifyDataSetChanged() {
  super.notifyDataSetChanged();
 }
 
 @Override
 public Object getItem(int position) {
  if (items != null && position >= 0 && position < getCount()) {
   return items[position];
  }
  return null;
 }
 
 @Override
 public long getItemId(int position) {
  if (items != null && position >= 0 && position < getCount()) {
   return items[position].id;
  }
  return 0;
 }
 
 public void setItemsList(GridItems[] locations) {
  this.items = locations;
 }
 
 @Override
 public View getView(int position, View convertView, ViewGroup parent) {
 
  View view = convertView;
  ViewHolder viewHolder;
 
  if (view == null) {
 
   view = mInflater.inflate(R.layout.row_grid, parent, false);
   viewHolder = new ViewHolder();
   viewHolder.imageView = (ImageView) view
     .findViewById(R.id.item_image);
   viewHolder.textTitle = (TextView) view
     .findViewById(R.id.item_text);
   view.setTag(viewHolder);
  } else {
   viewHolder = (ViewHolder) view.getTag();
  }
 
  GridItems gridItems = items[position];
  
  viewHolder.textTitle.setText(""+gridItems.list.loadLabel(packageManager));
	viewHolder.imageView.setImageDrawable(gridItems.list.loadIcon(packageManager));
  
  return view;
 }
 
} 	  