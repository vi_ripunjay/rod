package com.piniglauncher.framework;

public interface ReturnRequestCallback extends RequestCallback {
    void result(Object result);
}
