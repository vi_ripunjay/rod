package com.piniglauncher.model;

public class DeviceData {
	int id;
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	String name; 
	String email;
	String imei;
	String regid;
	

	public DeviceData(){}
	
    public DeviceData(String name, String email,String imei , String regid){
    	this.name = name;
        this.email = email;
        this.imei = imei;
        this.regid = regid;
        
    }  

    public DeviceData(int id , String name, String email,String imei , String regid){
    	this.id = id;
    	this.name = name;
        this.email = email;
        this.imei = imei;
        this.regid = regid;        
    }  

    
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getImei() {
		return imei;
	}


	public void setImei(String imei) {
		this.imei = imei;
	}


	public String getRegid() {
		return regid;
	}


	public void setRegid(String regid) {
		this.regid = regid;
	}



}
