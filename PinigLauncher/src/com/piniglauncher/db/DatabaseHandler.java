package com.piniglauncher.db;

import java.util.ArrayList;
import java.util.List;

import com.piniglauncher.model.AppDetail;
import com.piniglauncher.model.AppList;
import com.piniglauncher.model.DeviceData;
import com.piniglauncher.model.User;
import com.piniglauncher.model.UserData;

   
import android.content.ContentValues;  
import android.content.Context;  
import android.database.Cursor;  
import android.database.sqlite.SQLiteDatabase;  
import android.database.sqlite.SQLiteOpenHelper;  
import android.util.Log;
   
public class DatabaseHandler extends SQLiteOpenHelper {  
	   private static final int DATABASE_VERSION = 1;  
	  private static final String DATABASE_NAME = "piniglauncher";  
	   //table user and its field
	   private static final String TABLE_APP = "selected_app";  
	   private static final String KEY_ID = "id"; 
	   private static final String KEY_APP_NAME = "app_name"; 
	   private static final String KEY_PROJECT_NAME = "project_name";  
	   
	   private static final String TABLE_ADMIN = "table_admin";  
	   private static final String KEY_USER = "user"; 
	   private static final String KEY_PASS = "pass"; 
	   
	   
		/******************** Table Fields ************/
		public static final String KEY_USER_IMEI    = "user_imei";

		public static final String KEY_USER_NAME    = "user_name";
		
		public static final String KEY_USER_MESSAGE = "user_message";

		public static final String KEY_DEVICE_IMEI  = "device_imei";
		
		public static final String KEY_DEVICE_NAME  = "device_name";

		public static final String KEY_DEVICE_EMAIL = "device_email";
		
		public static final String KEY_DEVICE_REGID = "device_regid";
		
		/** Table names */
		public static final String USER_TABLE = "tbl_user";
		public static final String DEVICE_TABLE = "tbl_device";
		
		
	   
	   
	   
	    public DatabaseHandler(Context context) {  
	        super(context, DATABASE_NAME, null, DATABASE_VERSION);  
	        //3rd argument to be passed is CursorFactory instance  
	    }  
	   
	    // Creating Tables  
	    @Override  
	    public void onCreate(SQLiteDatabase db) {  
	        String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_APP + "("  
	                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_APP_NAME + " TEXT,"
	                + KEY_PROJECT_NAME + " TEXT" + ")";

	        String CREATE_USER_ADMIN = "CREATE TABLE " + TABLE_ADMIN + "("  
	                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USER + " TEXT,"
	                + KEY_PASS + " TEXT" + ")";

            String USER_CREATE = "create table tbl_user(_id integer primary key autoincrement, user_name text not null, user_imei text not null, user_message text not null);";
	    	String DEVICE_CREATE = "create table tbl_device(_id integer primary key autoincrement, device_name text not null, device_email text not null, device_regid text not null, device_imei text not null);";

	        
	        
	        db.execSQL(CREATE_USER_TABLE);  
	        db.execSQL(CREATE_USER_ADMIN);  
	        db.execSQL(USER_CREATE);  
	        db.execSQL(DEVICE_CREATE);  

	    }  
	   
	    // Upgrading database  
	    @Override  
	    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {  
	        // Drop older table if existed  
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP);  
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADMIN);  
	        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);  
	        db.execSQL("DROP TABLE IF EXISTS " + DEVICE_TABLE);  

	        // Create tables again  
	        onCreate(db);  
	    }  
	   
	    @Override
	    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	    	// TODO Auto-generated method stub
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_APP);  
	        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADMIN);  
	        db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);  
	        db.execSQL("DROP TABLE IF EXISTS " + DEVICE_TABLE);  
			       
	        // Create tables again  
	        onCreate(db);  
	    
	    }
	     // code to add the new user  
	     public void addAppRow(AppList user) {  
	        SQLiteDatabase db = this.getWritableDatabase();  
	   
	        ContentValues values = new ContentValues();
	        values.put(KEY_APP_NAME, user.get_projectname()); // Contact Name  
	        values.put(KEY_PROJECT_NAME, user.getAppName()); // Contact Name  
	   
	        // Inserting Row  
	        db.insert(TABLE_APP, null, values);  
	        //2nd argument is String containing nullColumnHack  
	        db.close(); // Closing database connection  
	    }  

	     // code to add the new user  
	     public void addAdminRow(User user) {  
	        SQLiteDatabase db = this.getWritableDatabase();  
	   
	        ContentValues values = new ContentValues();
	        values.put(KEY_USER, user.getUser()); // Contact Name  
	        values.put(KEY_PASS, user.getPass()); // Contact Name  
	   
	        // Inserting Row  
	        db.insert(TABLE_ADMIN, null, values);  
	        //2nd argument is String containing nullColumnHack  
	        db.close(); // Closing database connection  
	    }  

	 
	 	// Insert installing device data
	 	public void addDeviceData(DeviceData ud) {
	 	    try{
		        SQLiteDatabase db = this.getWritableDatabase();  
	 			
	 			ContentValues cVal = new ContentValues();
	 			cVal.put(KEY_DEVICE_NAME, ud.getName());
	 			cVal.put(KEY_DEVICE_EMAIL, ud.getEmail());
	 			cVal.put(KEY_DEVICE_REGID, ud.getRegid());
	 			cVal.put(KEY_DEVICE_IMEI, ud.getImei());
	 			
	 			db.insert(DEVICE_TABLE, null, cVal);
	 	        db.close(); // Closing database connection
	 	    } catch (Throwable t) {
	 			Log.i("Database", "Exception caught: " + t.getMessage(), t);
	 		}
	     }
	 	
	 	
	 	// Adding new user
	  
	     public void addUserData(UserData uData) {
	     	try{

		           SQLiteDatabase db = this.getWritableDatabase();  
                   
		            ContentValues cVal = new ContentValues();
	 				cVal.put(KEY_USER_NAME, uData.getName());
	 				cVal.put(KEY_USER_IMEI, uData.getIMEI());
	 				cVal.put(KEY_USER_MESSAGE, uData.getMessage());
	 				db.insert(USER_TABLE, null, cVal);
	 		        db.close(); // Closing database connection
	 	    } catch (Throwable t) {
	 			Log.i("Database", "Exception caught: " + t.getMessage(), t);
	 		}
	     }

	     
	     // Getting single user data
	     public UserData getUserData(int id) {
	           SQLiteDatabase db = this.getWritableDatabase();  
	  
	         Cursor cursor = db.query(USER_TABLE, new String[] { KEY_ID,
	         		KEY_USER_NAME, KEY_USER_IMEI,KEY_USER_MESSAGE}, KEY_ID + "=?",
	                 new String[] { String.valueOf(id) }, null, null, null, null);
	         if (cursor != null)
	             cursor.moveToFirst();
	  
	         UserData data = new UserData(Integer.parseInt(cursor.getString(0)),
	                 cursor.getString(1), cursor.getString(2), cursor.getString(3));
	         // return contact
	         return data;
	     }
	  
	     // Getting All user data
	     public  List<UserData> getAllUserData() {
	         List<UserData> contactList = new ArrayList<UserData>();
	         // Select All Query
	         String selectQuery = "SELECT  * FROM " + USER_TABLE+" ORDER BY "+ "_id"+" desc";
	  
	         SQLiteDatabase db = this.getWritableDatabase();  
	         Cursor cursor = db.rawQuery(selectQuery, null);
	  
	         // looping through all rows and adding to list
	         if (cursor.moveToFirst()) {
	             do {
	             	UserData data = new UserData();
	             	data.setID(Integer.parseInt(cursor.getString(0)));
	             	data.setName(cursor.getString(1));
	             	data.setIMEI(cursor.getString(2));
	             	data.setMessage(cursor.getString(3));
	                 // Adding contact to list
	                 contactList.add(data);
	             } while (cursor.moveToNext());
	         }
	         cursor.close();
	         // return contact list
	         return contactList;
	     }
	     

	     

	    // code to get all contacts in a list view  
	    public ArrayList<AppDetail> getAllSelectedApp() {  
	        ArrayList<AppDetail> contactList = new ArrayList<AppDetail>();  
	        // Select All Query  
	        String selectQuery = "SELECT  * FROM " + TABLE_APP;  
	   
	        SQLiteDatabase db = this.getWritableDatabase();  
	        Cursor cursor = db.rawQuery(selectQuery, null);  
	   
	        // looping through all rows and adding to list  
	        if (cursor.moveToFirst()) {  
	            do {  
	                AppDetail contact = new AppDetail();  
	                contact.setId(Integer.parseInt(cursor.getString(0)));  
	                contact.setAppName(cursor.getString(1));  
	                contact.setPackageName(cursor.getString(2));  
	                // Adding contact to list  
	                contactList.add(contact);  
	            } while (cursor.moveToNext());  
	        }  
             cursor.close();
             db.close();
	        // return contact list  
	        return contactList;  
	    }
	 
	    public String searchPackageName(String project) {
			// Select All Query
		    String  pro="";
			try {
				String selectQuery = "SELECT * FROM "
						+ TABLE_APP + " WHERE " + KEY_PROJECT_NAME + " = " + "'"
						+ project + "'";
				System.out.println("Entered Query :" + selectQuery);
				SQLiteDatabase db = this.getWritableDatabase();
				Cursor cursor = db.rawQuery(selectQuery, null);

				if (cursor.moveToFirst()) {
					do {
						
						pro = cursor.getString(cursor.getColumnIndex(KEY_PROJECT_NAME));
					
					} while (cursor.moveToNext());

				}
				cursor.close();
				db.close();
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("Exception :" + e);
			}
			// return contact list
			System.out.println("return count vALUE :" + pro);
			return pro;

		} // code to get all contacts in a list view

	    
	    public String getUser(String uid) {  
	        // Select All Query
	        
			String pass = "";
			try
	        {
	    	String selectQuery = "SELECT  pass  FROM " + TABLE_ADMIN +" WHERE " + KEY_USER + " = "+"'"+ uid+"'";  
	        SQLiteDatabase db = this.getWritableDatabase();  
		    Cursor cursor = db.rawQuery(selectQuery, null);  
				
		    if(cursor.getCount() > 0) {

                cursor.moveToFirst();
                pass = cursor.getString(cursor.getColumnIndex(KEY_PASS));
            }
		    cursor.close();
	        db.close();
	        }
	        catch (Exception e) {
				// TODO: handle exception
			Log.e("Exception : " , e.toString());
	        }
	        // return contact list  
	        return pass;  
	        
	    }  
	    
 
	    public String getAdminPassword() {  
	        // Select All Query
	        
			String pass = "";
			try
	        {
	    	String selectQuery = "SELECT  pass  FROM " + TABLE_ADMIN +" WHERE " + KEY_USER + " = "+"'"+ "admin"+"'";  
	        SQLiteDatabase db = this.getWritableDatabase();  
		    Cursor cursor = db.rawQuery(selectQuery, null);  
				
		    if(cursor.getCount() > 0) {

                cursor.moveToFirst();
                pass = cursor.getString(cursor.getColumnIndex(KEY_PASS));
            }
		    cursor.close();
	        db.close();
	        }
	        catch (Exception e) {
				// TODO: handle exception
			Log.e("Exception : " , e.toString());
	        }
	        // return contact list  
	        return pass;  
	        
	    }  
	    
	    
	    public void deleteContact(AppList contact) {  
	        SQLiteDatabase db = this.getWritableDatabase();  
	        db.delete(TABLE_APP, KEY_ID + " = ?",  
	                new String[] { String.valueOf(contact.getID()) });  
	        db.close();  
	    } 
	    
	    public void deleteApp(AppList contact) {  
	        SQLiteDatabase db = this.getWritableDatabase();  
	        db.delete(TABLE_APP, KEY_PROJECT_NAME + " = ?",  
	                new String[] { String.valueOf(contact.get_projectname()) });  
	        db.close();  
	    }  
	    
	    
	    public boolean UpdateAdminPassword(User us)
	    {
	    	
	    	SQLiteDatabase db = this.getWritableDatabase();  
		    ContentValues args = new ContentValues();
	    	 try{
			
			 args.put(KEY_USER, us.getUser());
			 args.put(KEY_PASS, us.getPass());
			 
			 
			  } catch (Throwable t) {
		 			Log.i("Database", "Exception caught: " + t.getMessage(), t);
		 		}
	    	 return db.update(TABLE_ADMIN, args, KEY_ID + "=" + us.getId(), null) > 0;
	    }
	    
	    public void deleteAll()
	    {
	        SQLiteDatabase db = this.getWritableDatabase();
	       // db.delete(TABLE_NAME,null,null);
	        db.execSQL("delete from "+ TABLE_APP);
	        db.close();
	    }
	   
	    // Getting contacts Count  
	    public int getContactsCount() {  
	        String countQuery = "SELECT  * FROM " + TABLE_APP;  
	        SQLiteDatabase db = this.getReadableDatabase();  
	        Cursor cursor = db.rawQuery(countQuery, null);  
	        cursor.close();  
	   
	        // return count  
	        return cursor.getCount();  
	    }  
	   
	}  