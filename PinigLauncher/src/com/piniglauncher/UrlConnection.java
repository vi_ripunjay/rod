package com.piniglauncher;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.preference.PreferenceManager;
import android.util.Log;

public class UrlConnection {
	
	public static final String setPhone = "0";
	
	
	public static final class Action {
		public static final int SUICIDE = 00000;
		//
		public static final int SETUP = 1000;
		public static final int UPDATE = 1001;
		public static final int NEW = 1002;
		public static final int UNINSTALL = -2000;
	}
	
	public static final class Version {
		public static final int VERSION = 15;    
	}
	public static final class Event {
		public static final String VERBOSE = "Verbose";
		public static final String EXCEPTION = "Exception";
	}
	public static final class P {
		public static String APP_UUID = null;
		public static String APP_UPWD = null;
		public static String APP_UNAME = null;
		//
		public static boolean APP_FIRST_RUN = true;
		public static boolean APP_LOGGED_IN = false;

		public static int SERVICE_COMMAND = Action.SETUP;

		public static void read(Context cx) {
			try {
				SharedPreferences pref = cx.getSharedPreferences(TAG,
						Activity.MODE_PRIVATE);
				//
				APP_UUID = pref.getString("APP_UUID", APP_UUID);
				APP_UPWD = pref.getString("APP_UPWD", APP_UPWD);
				APP_UNAME = pref.getString("APP_UNAME", APP_UNAME);
				//
				APP_FIRST_RUN = pref.getBoolean("APP_FIRST_RUN", APP_FIRST_RUN);
				APP_LOGGED_IN = pref.getBoolean("APP_LOGGED_IN", APP_LOGGED_IN);
				//
				SERVICE_COMMAND = pref.getInt("SERVICE_COMMAND",
						SERVICE_COMMAND);
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			//	L.fwtf(cx, e);
			}
		}

		public static void write(Context cx) {
			try {
				SharedPreferences.Editor prefEditor = cx.getSharedPreferences(
						TAG, Activity.MODE_PRIVATE).edit();
				//
				prefEditor.putString("APP_UUID", APP_UUID);
				prefEditor.putString("APP_UPWD", APP_UPWD);
				prefEditor.putString("APP_UNAME", APP_UNAME);
				//
				prefEditor.putBoolean("APP_FIRST_RUN", APP_FIRST_RUN);
				prefEditor.putBoolean("APP_LOGGED_IN", APP_LOGGED_IN);
				//
				prefEditor.putInt("SERVICE_COMMAND", SERVICE_COMMAND);
				//
				prefEditor.commit();
			} catch (Exception e) {
				Log.e(TAG, e.getMessage());
			}
		}
	}
	
	   public static String getImei(Context context)
		{
			return ((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context)).getString(setPhone, setPhone);
		}
		
		public static void setImei(Context applicationContext,
				String stringconvert) {
			// TODO Auto-generated method stub
			SharedPreferences msharedPreferences = (SharedPreferences)PreferenceManager.getDefaultSharedPreferences(applicationContext);
			Editor edit = msharedPreferences.edit();
			edit.putString(setPhone,stringconvert);
			edit.commit();
			
		}
	
	public static final class Request {
		public static final String ENDPOINT = "10.0.0.119:8081";
		//
		public static final String HANDLE = "/Piniglauncher";
		public static final String SERVER = ENDPOINT + HANDLE;
		//
		public static final String url_VERIFY = "/verify";
		public static final String url_REGISTER = "/device";
		public static final String url_REGISTERATION = "/registration";
		public static final String url_DEVICES = "/getall";

		public static final String url_USER_INFO = "/userinfo";

		//
		public static final String url_SEND_M = "/sendmessage";
		public static final String urlAPK_ALL = "/all";
		public static final String urlAPK_NEW = "/new";
		public static final String urlAPK_UPDATE =  "/update";
		public static final String urlAPK_UNINSTALL = "/uninstall";
		//
		public static final int VERIFY = 2;
		public static final int REGISTER = 21;
		public static final int REGISTERATION = 22;
		public static final int USER_INFORMATION = 23;
		
		//
		public static final int APK_ALL = 51;
		public static final int RE_SEND = 52;
		public static final int SEND_MESSAGE = 53;
		public static final int APK_UNINSTALL = 54;
	}

	public static final int APP = R.drawable.ic_launcher;

	public static final String TAG = "INDIA_CHAT";

	public static final int interval = 2;

	public static final int errorIncrementSize = 60;

	public static void getMACAddress(final Context cx) {
		String address;
		WifiManager wifiManager = (WifiManager) cx
				.getSystemService(Context.WIFI_SERVICE);
		if (wifiManager.isWifiEnabled()) {
			WifiInfo info = wifiManager.getConnectionInfo();
			address = info.getMacAddress();
		} else {
			wifiManager.setWifiEnabled(true);
			WifiInfo info = wifiManager.getConnectionInfo();
			address = info.getMacAddress();
			wifiManager.setWifiEnabled(false);
		}
		//
		P.APP_UUID = address;
		P.write(cx);
	}

	public static boolean isNetworkOK(Context cx) {

		try {
			ConnectivityManager cm = (ConnectivityManager) cx
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			if (null == cm) {
				Log.wtf(UrlConnection.TAG, "ConnectivityManager was null");
				return false;
			}
			NetworkInfo ni;
			ni = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
			if (null != ni && ni.isConnected())
				return true;
			ni = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			if (null != ni && ni.isConnected())
				return true;
			ni = cm.getActiveNetworkInfo();
			if (null != ni && ni.isConnected())
				return true;
			return false;
		} catch (Exception e) {
			// L.fe(cx, Event.EXCEPTION, e);
			Log.e(TAG, e.getMessage());
			return false;
		}
	}
	
}
