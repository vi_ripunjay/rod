package com.piniglauncher;

import java.io.File;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;

public class MyService extends Service {

      @Override
      public IBinder onBind(Intent intent) {
            return null;
      }
      
      @Override
      public void onCreate() {
            super.onCreate();
                // do something when the service is created
            System.out.println("////////////////////////////////////////////////////////////////");
            
            File sdCard = Environment.getExternalStorageDirectory();
            File PinigLogDir = new File(sdCard.getAbsoluteFile(), File.separator + "PINIG" + File.separator + "flag"+File.separator+"flg.txt");
           if(PinigLogDir.exists()){
            Intent i = new Intent();
            i.setClass(this, Home.class);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
           }
           else{
        	   writeToFile();
           }
      }
      
      
      private static void writeToFile() {
          try {
              File sdCard = Environment.getExternalStorageDirectory();
              File PinigLogDir = new File(sdCard.getAbsoluteFile(), File.separator + "PINIG" + File.separator + "flag");
              if (!PinigLogDir.exists())
                  PinigLogDir.mkdirs();
              File PinigLog = new File(PinigLogDir, "flg.txt");
              if (!PinigLog.exists())
                  PinigLog.createNewFile();
              
          } catch (Exception e) {
              e.printStackTrace();
          }
      }
}