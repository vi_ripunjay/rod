package com.piniglauncher;

import java.util.ArrayList;



import java.util.List;

import com.piniglauncher.adapter.ApplicationAdapter;
import com.piniglauncher.db.DatabaseHandler;
import com.piniglauncher.model.AppDetail;
import com.piniglauncher.model.AppList;

import android.app.ListActivity;
import android.app.ProgressDialog;

import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SelectApps extends ListActivity {

	private PackageManager packageManager = null;
	private List<ApplicationInfo> applist = null;
	private ApplicationAdapter listadaptor = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_apps);
		
		packageManager = getPackageManager();

		new LoadApplications().execute();
		
		Button myButton = (Button) findViewById(R.id.ok);
		  myButton.setOnClickListener(new OnClickListener() {
		 
		   @Override
		   public void onClick(View v) {
		 
		   DatabaseHandler db = new DatabaseHandler(SelectApps.this);
		   db.deleteAll();
				 // Inserting Contacts  
		    Log.d("Insert: ", "Inserting ..");  
		    ArrayList<AppDetail> alllist = listadaptor.AppList ;
		    System.out.println("all list :" + alllist.toString());
		    for(int i=0;i<alllist.size();i++){
		    AppDetail ad = alllist.get(i);
		    if(ad.isSelected()){
		      db.addAppRow(new AppList(ad.getAppName(),ad.getPackageName())); 
             }
		    }
		    Intent i = new Intent(SelectApps.this , Home.class);
		    startActivity(i);
		    finish();
		   }
		  });

	
	}
	
	
	
	
/*	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		ApplicationInfo app = applist.get(position);
		try {
			
			System.out.println("App name :" +app.packageName );
			Intent intent = packageManager
					.getLaunchIntentForPackage(app.packageName);

			if (null != intent) {
				startActivity(intent);
			}
		} catch (ActivityNotFoundException e) {
			Toast.makeText(SelectApps.this, e.getMessage(),
					Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			Toast.makeText(SelectApps.this, e.getMessage(),
					Toast.LENGTH_LONG).show();
		}
	}
*/
	private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
		ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
		for (ApplicationInfo info : list) {
			try {
				if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
					applist.add(info);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return applist;
	}
	
	
	private class LoadApplications extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progress = null;

		@Override
		protected Void doInBackground(Void... params) {
			applist = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));
			listadaptor = new ApplicationAdapter(SelectApps.this,
					R.layout.snippet_list_row, applist);

			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(Void result) {
			setListAdapter(listadaptor);
			progress.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			progress = ProgressDialog.show(SelectApps.this, null,
					"Loading application info...");
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_apps, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
