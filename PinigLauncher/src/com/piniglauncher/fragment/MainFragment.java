package com.piniglauncher.fragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.Time;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.DigitalClock;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.piniglauncher.Home;
import com.piniglauncher.L;
import com.piniglauncher.R;
import com.piniglauncher.SharePref;
import com.piniglauncher.db.DatabaseHandler;
import com.piniglauncher.model.AppDetail;
import com.piniglauncher.model.GridItems;
import com.viewpagerindicator.PageIndicator;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class MainFragment extends Fragment {
	
	
//	SearchView searchView ;
//	ImageView homeButton;
	TextView dateTextView;
	ImageView phone,mail,browser,message;
	@SuppressWarnings("deprecation")
	//DigitalClock dc;
	public PageIndicator mIndicator;
	private ViewPager awesomePager;
	private PagerAdapter pa;
	List<GridFragment> gridFragments;
	FragmentManager frg ;
	FragmentActivity listener;
	View view;
	
	 
	GridView gv;
	private PackageManager packageManager = null;
	private List<ResolveInfo> gridlist = null;
	public MainFragment(){}
	
	@SuppressWarnings("deprecation")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.main_fragment, container, false);
   //     searchView = (SearchView) rootView.findViewById(R.id.searchView1);
    //    dc = (DigitalClock) rootView.findViewById(R.id.digitalClock1);
   //     homeButton = (ImageView) rootView.findViewById(R.id.imageView1);
/*        phone = (ImageView) rootView.findViewById(R.id.phone);
        mail = (ImageView) rootView.findViewById(R.id.mail);
        browser = (ImageView) rootView.findViewById(R.id.browser);
        message = (ImageView) rootView.findViewById(R.id.message);
*/        dateTextView = (TextView) rootView.findViewById(R.id.date);

            getActivity().setTitle("Start");
        
        try {
			packageManager = getActivity().getPackageManager();
			
			try {
				Class.forName("android.os.AsyncTask");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();
			dateTextView.setText(today.monthDay +"-"+(today.month +1)+"-" +today.year );
			//using database set password
/*		if(SharePref.getTag(getActivity()).equals("first"))
			{
			db.addAdminRow(new User("admin","123456"));
			db.close();
			SharePref.setTag(getActivity(), "second");
			}
*/
//		new SelectedApplications().execute();

			 if(SharePref.getTag(getActivity()).equals("first"))
			{
				SharePref.setAdminPassword(getActivity(), "123456");
				SharePref.setTag(getActivity(), "second");
			}

			 gv = (GridView) rootView.findViewById(R.id.gridView1);
				
				
			
			
			awesomePager = (ViewPager) rootView.findViewById(R.id.pager);
			mIndicator = (PageIndicator) rootView.findViewById(R.id.pagerIndicator);
			
			dateTextView.setVisibility(View.VISIBLE);
		/*	phone.setVisibility(View.VISIBLE);
			browser.setVisibility(View.VISIBLE);
			message.setVisibility(View.VISIBLE);
			mail.setVisibility(View.VISIBLE);
		*/	//homeButton.setVisibility(View.VISIBLE);
	//		searchView.setVisibility(View.VISIBLE);
	//		dc.setVisibility(View.VISIBLE);
		/*	searchView.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					launchApp("com.android.quicksearchbox");
				}
			});
*//*			message.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					launchApp("com.android.mms");
					    	
				}
			});
			browser.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					launchApp("com.android.browser");
					            	
				}
			});
			phone.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					launchApp("com.android.dialer");
						
				}
			});
			mail.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					launchApp("com.android.email");
						
				}
			});
*/		/*	homeButton.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub

					new SelectedApplications().execute();
	//		    	searchView.setVisibility(View.GONE);
	//		    	dc.setVisibility(View.GONE);
			    	dateTextView.setVisibility(View.GONE);
			    	phone.setVisibility(View.GONE);
			    	browser.setVisibility(View.GONE);
			    	message.setVisibility(View.GONE);
			    	mail.setVisibility(View.GONE);
			    	homeButton.setVisibility(View.GONE);
			    	awesomePager.setVisibility(View.VISIBLE);
			    	((View) mIndicator).setVisibility(View.VISIBLE);
			    	
				}
			});
			
*/			

			frg =listener.getSupportFragmentManager();
			view = rootView;
			
			view.setFocusableInTouchMode(true);
			view.requestFocus();

			view.setOnKeyListener(new OnKeyListener() {
			        @Override
			        public boolean onKey(View v, int keyCode, KeyEvent event) {
			                if (event.getAction() == KeyEvent.ACTION_DOWN) {
			                    if (keyCode == KeyEvent.KEYCODE_BACK) {
			        	//	    	searchView.setVisibility(View.VISIBLE);
			        	//	    	dc.setVisibility(View.VISIBLE);
			        		    	dateTextView.setVisibility(View.VISIBLE);
/*			        		    	phone.setVisibility(View.VISIBLE);
			        		    	browser.setVisibility(View.VISIBLE);
			        		    	message.setVisibility(View.VISIBLE);
			        		    	mail.setVisibility(View.VISIBLE);
*/			             //       	homeButton.setVisibility(View.VISIBLE);
			                    	awesomePager.setVisibility(View.GONE);
			                        ((View) mIndicator).setVisibility(View.GONE);
			                    	
			                        return true;
			                    }
			                }
			                return false;
			            }
			        });
		} catch (Exception e) {
			// TODO Auto-generated catch block
			L.fe(getActivity(),"MainFragment :", e);
		}
/*		
		gv.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {

			gridlist = gridadaptor.appsList;
			String packageName  = gridlist.get(position).packageName;
			launchApp(packageName);
		    Toast.makeText(getActivity(), packageName, Toast.LENGTH_LONG).show();	
		
		}
		});
*/
        return rootView;
    }
	
	protected void launchApp(String packageName) {
        Intent mIntent = getActivity().getPackageManager().getLaunchIntentForPackage(
                packageName);
        if (mIntent != null) {
            try {
                startActivity(mIntent);
            } catch (ActivityNotFoundException err) {
/*                Toast t = Toast.makeText(getActivity(),
                        "App Not Found", Toast.LENGTH_SHORT);
                t.show();
*/            	L.fe(getActivity(),"MainFragment : launch App ", err);
        		
            }
        }
    }
	
	@Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.listener =  (FragmentActivity) activity;
    }


	private class PagerAdapter extends FragmentStatePagerAdapter {
		  private List<GridFragment> fragments;
		 
		  public PagerAdapter(FragmentManager fm, List<GridFragment> fragments) {
		   super(fm);
		   this.fragments = fragments;
		  }
		 
		  @Override
		  public android.support.v4.app.Fragment getItem(int position) {
		   return this.fragments.get(position);
		  }
		 
		  @Override
		  public int getCount() {
		   return this.fragments.size();
		  }
		 }
	
	@SuppressWarnings("deprecation")
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
    	   super.onResume();
           L.fv("Main Fragment : On Resume");
    	   
           if(SharePref.getWall(getActivity()).equalsIgnoreCase(""))
    	   {
    	   view.setBackground(getActivity().getResources().getDrawable(R.drawable.launchar_wallpaper));
    	   }  
    	   else if(!SharePref.getWall(getActivity()).equalsIgnoreCase(null))
    	   {
    		   System.out.println("Image  : " +SharePref.getWall(getActivity()));
    		   Uri data =Uri.fromFile(new File(SharePref.getWall(getActivity())));
    		// int imageResource = R.drawable.icon;
    		   try {
    			   Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data);
    			   RelativeLayout bg = (RelativeLayout) view.findViewById(R.id.main_fragment);
    			   Drawable drawable = new BitmapDrawable(getResources(), bitmap);
    			   bg.setBackgroundDrawable(drawable);
    			   } catch (FileNotFoundException e) {
    			   // TODO Auto-generated catch block
    				 e.printStackTrace();
    			   } catch (IOException e) {
    			   // TODO Auto-generated catch block
    		    	e.printStackTrace();
    				L.fe(getActivity(),"MainFragment : On Resume:Setting Wall : ", e);
    				
    			   }
        	      
    	   }
		//   new SelectedApplications().execute();
    	   
	}
	
	    @Override
	    public void onActivityCreated(Bundle savedInstanceState) {
	        super.onActivityCreated(savedInstanceState);
	    }

	
	private List<ResolveInfo> checkForLaunchIntent(List<ResolveInfo> list) {
		List<ResolveInfo> applist = new ArrayList<ResolveInfo>();
		DatabaseHandler db = new DatabaseHandler(getActivity());
		List<AppDetail> selected = db.getAllSelectedApp();
		String s = SharePref.getId(getActivity());
		
		System.out.println(" S :" + s);
		
		if(s.equalsIgnoreCase("All"))
		{
			applist = list;
		}else if(!SharePref.getId(getActivity()).equalsIgnoreCase("All"))
		{	 
		for (int i = 0; i < selected.size(); i++) {
			
		for (Object object : list) {
			 ResolveInfo info = (ResolveInfo) object;
			try {
				if (null != packageManager.getLaunchIntentForPackage(info.activityInfo.packageName)) {
					if(selected.get(i).getPackageName().equalsIgnoreCase(info.activityInfo.packageName))
					{	
					applist.add(info);
					}else
					{
						
					}
				}
			} catch (Exception e) {
				L.fe(getActivity(),"MainFragment :Make Application List :", e);
				e.printStackTrace();
			}
		 }
		}
	    }
		return applist;
	}
	private class SelectedApplications extends AsyncTask<Void, Void, Void> {
	//	private ProgressDialog progress = null;

		@Override
		protected Void doInBackground(Void... params) {
			
		    try {
				Intent main=new Intent(Intent.ACTION_MAIN, null);
				
				main.addCategory(Intent.CATEGORY_LAUNCHER);
				List<ResolveInfo> launchables=packageManager.queryIntentActivities(main, 0);
				Collections.sort(launchables,
				        new ResolveInfo.DisplayNameComparator(packageManager)); 
				gridlist = checkForLaunchIntent(launchables);
				gridFragments = new ArrayList<GridFragment>();
				Iterator<ResolveInfo> it = gridlist.iterator();
				
				while (it.hasNext()) 
				{
					List<GridItems> itmLst = new ArrayList<GridItems>();
					for (int item = 0; item < 30; item++ )
					{ 
						   System.out.println("item = "+ item);
						   
						   //GridItems itm = 
						   if (it.hasNext()) {
							   itmLst.add(new GridItems(item, it.next()));
						   }
					}
					   GridItems[] gp = {};
					   GridItems[] gridPage = itmLst.toArray(gp);
					   gridFragments.add(new GridFragment(gridPage, getActivity()));
					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				L.fe(getActivity(),"MainFragment : inBackgreound", e);
				e.printStackTrace();
			}

			return null;
		}
		@Override
		protected void onCancelled() {
			super.onCancelled();
		}
		@Override
		protected void onPostExecute(Void result) {
			 super.onPostExecute(result);
	   		 try {
				 pa = new PagerAdapter(frg, gridFragments);
				 awesomePager.setAdapter(pa);
				 mIndicator.setViewPager(awesomePager);	//		progress.dismiss();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				L.fe(getActivity(), "MainFragment : OnPostCreate :", e);
				e.printStackTrace();
			}
		}

		@Override
		protected void onPreExecute() {
	//		progress = ProgressDialog.show(Home.this, null,
	//				"Loading application info...");
			super.onPreExecute();
		    
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}
	
	

	
}