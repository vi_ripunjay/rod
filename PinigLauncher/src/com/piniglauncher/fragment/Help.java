package com.piniglauncher.fragment;


import java.util.ArrayList;


import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.piniglauncher.R;
import com.piniglauncher.R.array;
import com.piniglauncher.R.id;
import com.piniglauncher.R.layout;
import com.piniglauncher.adapter.FullScreenImageAdapter;

public class Help extends Fragment {
	
	private FullScreenImageAdapter adapter;
	private ViewPager viewPager;
	
	private TypedArray helpImages;
	String[] slideImages;
	

	public Help() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.help, container,
				false);
		viewPager = (ViewPager) rootView.findViewById(R.id.pager);
	
	//	slideImages = getResources().getStringArray(R.array.help_icons);
		helpImages = getResources()
				.obtainTypedArray(R.array.help_icons);

        
		ArrayList<Integer> filePath = new ArrayList<Integer>();
		
		for (int i =0; i< helpImages.length(); i++)
		{
			
			
			filePath.add(helpImages.getResourceId(i, -1));
		}
		
		
		
		Intent i = getActivity().getIntent();
		int position = i.getIntExtra("position", 0);

		adapter = new FullScreenImageAdapter(getActivity(),
				filePath);

		viewPager.setAdapter(adapter);
          
		// displaying selected image first
		viewPager.setCurrentItem(position);
         helpImages.recycle();
		
		
		return rootView;
	}
}
