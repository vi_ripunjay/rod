package com.piniglauncher.fragment;

import java.util.ArrayList;


import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.piniglauncher.R;
import com.piniglauncher.RequestListener;
import com.piniglauncher.SendNotification;
import com.piniglauncher.ServiceHandler;
import com.piniglauncher.UrlConnection;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.SimpleAdapter;



@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class CustomerChat extends Fragment {
	
	Button  bt;
	private ProgressDialog pDialog;
    ListView listUser;
    ArrayList<HashMap<String, String>> scholarList;
    
    JSONArray getList;
    
    String endpoint = RequestListener.UNSECURE + UrlConnection.Request.SERVER
			+ UrlConnection.Request.url_DEVICES;

	public CustomerChat(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
         View rootView = inflater.inflate(R.layout.customer_chat, container, false);
         bt = (Button) rootView.findViewById(R.id.Reg);
         listUser = (ListView) rootView.findViewById(R.id.listView1); 
         scholarList = new ArrayList<HashMap<String, String>>();
         
//          if(!UrlConnection.getImei(getActivity()).equals("0"))
//          {
//        //	bt.setVisibility(View.GONE);  
//        //	new GetScholarship().execute();  
//        	 
//          }  else if(UrlConnection.getImei(getActivity()).equals("0"))
//          {
//        	bt.setVisibility(View.VISIBLE);  
//          }

         bt.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
/*				Intent i = new Intent(getActivity() , RegistrationActivity.class);
				startActivity(i);
*/			}
		 });
         
         listUser.setOnItemClickListener(new OnItemClickListener(){
			@SuppressWarnings("unchecked")
				@Override
          	   public void onItemClick(AdapterView<?> parent, View view, int position,
          	     long id) {
          		   
            	HashMap<String,String> map=  (HashMap<String, String>) parent.getItemAtPosition(position); 
          	    String name =map.get("name"); // get the value using key
          	    String imei = map.get("imei");
          	    String deviceid  = UrlConnection.getImei(getActivity());
                String regID  = map.get("regid");
          	    
          	// Launch Main Activity
				Intent i = new Intent(getActivity(), SendNotification.class);
				
				// Registering user on our server					
				// Sending registraiton details to MainActivity
				i.putExtra("name", name);
				i.putExtra("imeito", imei);  // Send to
				i.putExtra("sendfrom", deviceid);   //  imei
				i.putExtra("regid", regID);

				startActivity(i);
				//finish();
          	    
               
  			} 
          });    

         
        return rootView;
    }

    private class GetScholarship extends AsyncTask<Void, Void, Void> {
   	 
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
 
        }
 
        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();
 
            // Making a request to url and getting response
            JSONObject jsonstr = sh.makeServiceCall(endpoint, ServiceHandler.GET);
 
            Log.d("Response: ", "> " + jsonstr);
 
            if (jsonstr != null) {
                try {
                    Log.d("List :", jsonstr.toString());
                    // Getting JSON Array node
                    getList = jsonstr.getJSONArray("result");
                    System.out.println("length :"+getList.length());    
                    // looping through All Contacts
                    for (int i = 0; i < getList.length(); i++) {
                        JSONObject c = getList.getJSONObject(i);
                         
                        String name = c.getString("name");
                        String imei= c.getString("imei");
                        String regid = c.getString("regid");
                        String status = c.getString("status");
 
                        // tmp hashmap for single contact
                        HashMap<String, String> listData = new HashMap<String, String>();
 
                        Log.d("contact hash map:",listData.toString());
                        // adding each child node to HashMap key => value
                        listData.put("name", name);
                        listData.put("imei", imei);
                        listData.put("regid", regid);
                        listData.put("status", status);
                        
                        // adding contact to contact list
                        scholarList.add(listData);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            return null;
        }
 
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
           ListAdapter adapter = new SimpleAdapter(
                    getActivity(), scholarList,
                    R.layout.schoalrship_list_item, new String[] { "name"}, new int[] { R.id.name });
 
           listUser.setAdapter(adapter);
        }
 
    }
	
	
	
}
