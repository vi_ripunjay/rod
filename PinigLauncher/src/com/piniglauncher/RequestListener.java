package com.piniglauncher;

import android.net.http.AndroidHttpClient;






import android.util.Log;


import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.util.EntityUtils;

import com.piniglauncher.framework.InDeterminateRequestCallback;
import com.piniglauncher.framework.RequestCallback;

import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class RequestListener {
	private final static ExecutorService EXECUTOR = Executors
			.newSingleThreadScheduledExecutor();
	//
	public final static String UNSECURE = "http://";

	public static String state_code;
	public static String classes_code;
	
	private static AndroidHttpClient CLIENT = null;

	public synchronized static AndroidHttpClient c()
			throws NoSuchAlgorithmException, KeyManagementException {
		if (null == CLIENT) {
			CLIENT = AndroidHttpClient.newInstance("Pinig");
		}
		return CLIENT;
	}

	private static HttpUriRequest prepare(final int enc, HttpUriRequest req) {
		switch (enc) {
		case Encoding.JSON: {
			req.addHeader("Accept", "application/json");
			req.addHeader("Content-Type", "application/x-www-form-urlencoded");
			break;
		}
		case Encoding.SEND_JSON: {
			req.addHeader("Content-Type", "application/x-www-form-urlencoded");
			break;
		}
		case Encoding.RECEIVE_JSON: {
			req.addHeader("Accept", "application/json");
			break;
		}
		}
		return req;
	}

	public static void onReceive(final int reqCode,
			final RequestCallback callback, final List<NameValuePair> list) {
		EXECUTOR.execute(new Runnable() {
			@SuppressWarnings("unused")
			@Override
			public void run() {
				switch (reqCode) {
				case UrlConnection.Request.REGISTERATION: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
						String endpoint = UNSECURE + UrlConnection.Request.SERVER
								+ UrlConnection.Request.url_REGISTERATION;
						//
						Log.e("Endpoint :", endpoint + " attaching : "
								+ list.toString());
						HttpPost req = new HttpPost(endpoint);
					
						req.setEntity( new UrlEncodedFormEntity(list));
				//		req.setEntity(new StringEntity(o.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());
						
					    Log.i("Information :", status + " : " + endpoint + " : " + result);
						//
						if (status == HttpURLConnection.HTTP_INTERNAL_ERROR){      //500
							infinite.result(false,
									HttpURLConnection.HTTP_INTERNAL_ERROR);
						
						}
						else if (status == HttpURLConnection.HTTP_OK){      //200
							infinite.result(true);
							Log.i("Server","All found OK on server");
					//		L.fwtf(null, "All found OK on server");
						}
						else if (status == HttpURLConnection.HTTP_NOT_FOUND)       //404
							infinite.result(false,
									HttpURLConnection.HTTP_NOT_FOUND);
						// Did not find device, then did not find user
						else if (status == HttpURLConnection.HTTP_FORBIDDEN)     //403
							infinite.result(false,
									HttpURLConnection.HTTP_FORBIDDEN);
						// Cannot create device as serial already used for
						// another device
						else if (status == HttpURLConnection.HTTP_BAD_REQUEST)   //400
							infinite.result(false,
									HttpURLConnection.HTTP_BAD_REQUEST);
						// Device found but username not correct
						else if (status == HttpURLConnection.HTTP_UNAUTHORIZED)   //401
							infinite.result(false,
									HttpURLConnection.HTTP_UNAUTHORIZED);
						// Device found, username matched but password not
						// correct
						else if (status == HttpURLConnection.HTTP_CONFLICT)     //409
							infinite.result(false,
									HttpURLConnection.HTTP_CONFLICT); 
						// Device found, username and password matched but not found in wp_users
						resp.getEntity().consumeContent();
					} catch (Exception e) {
         //						L.fe(null, Pinig.Event.EXCEPTION, e);
			            Log.i("Exception", Log.getStackTraceString(e));
						infinite.result(false);
					}
					break;
				}
				case UrlConnection.Request.REGISTER: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
						String endpoint = UNSECURE + UrlConnection.Request.SERVER
								+ UrlConnection.Request.url_REGISTER;
						//
						Log.e("Endpoint :", endpoint + " attaching : "
								+ list.toString());
						HttpPost req = new HttpPost(endpoint);
					
						req.setEntity( new UrlEncodedFormEntity(list));
				//		req.setEntity(new StringEntity(o.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());
						
					    Log.i("Information :", status + " : " + endpoint + " : " + result);
						//
						if (status == HttpURLConnection.HTTP_INTERNAL_ERROR){      //500
							infinite.result(false,
									HttpURLConnection.HTTP_INTERNAL_ERROR);
						
						}
						else if (status == HttpURLConnection.HTTP_OK){      //200
							infinite.result(true , result);
							Log.i("Server","All found OK on server");
					//		L.fwtf(null, "All found OK on server");
						}
						else if (status == HttpURLConnection.HTTP_NOT_FOUND)       //404
							infinite.result(false,
									HttpURLConnection.HTTP_NOT_FOUND);
						// Did not find device, then did not find user
						else if (status == HttpURLConnection.HTTP_FORBIDDEN)     //403
							infinite.result(false,
									HttpURLConnection.HTTP_FORBIDDEN);
						// Cannot create device as serial already used for
						// another device
						else if (status == HttpURLConnection.HTTP_BAD_REQUEST)   //400
							infinite.result(false,
									HttpURLConnection.HTTP_BAD_REQUEST);
						// Device found but username not correct
						else if (status == HttpURLConnection.HTTP_UNAUTHORIZED)   //401
							infinite.result(false,
									HttpURLConnection.HTTP_UNAUTHORIZED);
						// Device found, username matched but password not
						// correct
						else if (status == HttpURLConnection.HTTP_CONFLICT)     //409
							infinite.result(false,
									HttpURLConnection.HTTP_CONFLICT); 
						// Device found, username and password matched but not found in wp_users
						resp.getEntity().consumeContent();
					} catch (Exception e) {
         //						L.fe(null, Pinig.Event.EXCEPTION, e);
			            Log.i("Exception", Log.getStackTraceString(e));
						infinite.result(false);
					}
					break;
				}
				case UrlConnection.Request.VERIFY: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
						String endpoint = UNSECURE + UrlConnection.Request.SERVER
								+ UrlConnection.Request.url_VERIFY;
	
						Log.e("Endpoint :", endpoint + " attaching : "
								+ list.toString());
	             	//	L.fwtf(null, "Endpoint : " + endpoint + " attaching : "
						//		+ o.toString());
						//
						HttpPost req = new HttpPost(endpoint);
						
						req.setEntity( new UrlEncodedFormEntity(list));
				//		req.setEntity(new StringEntity(list.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());
						Log.e(null, status + " : " + endpoint + " : " + result);

						if (status == HttpURLConnection.HTTP_INTERNAL_ERROR){      //500
							infinite.result(false,
									HttpURLConnection.HTTP_INTERNAL_ERROR);
						
						}
						else if (status == HttpURLConnection.HTTP_OK){      //200
							infinite.result(true);
							Log.i("Server","VERFIY CODE");
					//		L.fwtf(null, "All found OK on server");
						}
						
						else if (status == HttpURLConnection.HTTP_NOT_FOUND)       //404
							infinite.result(false,
									HttpURLConnection.HTTP_NOT_FOUND);
						// Did not find device, then did not find user
						else if (status == HttpURLConnection.HTTP_FORBIDDEN)     //403
							infinite.result(false,
									HttpURLConnection.HTTP_FORBIDDEN);
						// Cannot create device as serial already used for
						// another device
						else if (status == HttpURLConnection.HTTP_BAD_REQUEST)   //400
							infinite.result(false,
									HttpURLConnection.HTTP_BAD_REQUEST);
						// Device found but username not correct
						else if (status == HttpURLConnection.HTTP_UNAUTHORIZED)   //401
							infinite.result(false,
									HttpURLConnection.HTTP_UNAUTHORIZED);
						// Device found, username matched but password not
						// correct
						else if (status == HttpURLConnection.HTTP_CONFLICT)     //409
							infinite.result(false,
									HttpURLConnection.HTTP_CONFLICT); 
				
						resp.getEntity().consumeContent();
					} catch (Exception e) {
				//		L.fe(null, Pinig.Event.EXCEPTION, e);
						infinite.result(false);
					}
					break;
				}
				case UrlConnection.Request.RE_SEND: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
						String endpoint = UNSECURE + UrlConnection.Request.SERVER
								+ UrlConnection.Request.url_SEND_M;
						//
					//	L.fwtf(null, "Endpoint : " + endpoint + " attaching : "
				//				+ o.toString());
						//
						Log.d("EndPoint",endpoint);
						HttpPost req = new HttpPost(endpoint);
						
						req.setEntity( new UrlEncodedFormEntity(list));
						
					//	req.setEntity(new StringEntity(list.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());

						Log.d("R :", status + " : " + endpoint);
						if (status == HttpURLConnection.HTTP_INTERNAL_ERROR){      //500
							infinite.result(false,
									HttpURLConnection.HTTP_INTERNAL_ERROR);
						
						}
						else if (status == HttpURLConnection.HTTP_OK){      //200
							infinite.result(true);
							Log.i("Server","VERFIY CODE");
					//		L.fwtf(null, "All found OK on server");
						}
						//
						resp.getEntity().consumeContent();
					} catch (Exception e) {
			//			L.fe(null, Pinig.Event.EXCEPTION, e);
						infinite.result(false);
					}
					break;
				}
				
				case UrlConnection.Request.SEND_MESSAGE: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
						String endpoint = UNSECURE + UrlConnection.Request.SERVER
								+ UrlConnection.Request.url_SEND_M;
						//
					//	L.fwtf(null, "Endpoint : " + endpoint + " attaching : "
				//				+ o.toString());
						//
						Log.d("EndPoint",endpoint);
						HttpPost req = new HttpPost(endpoint);
						
						req.setEntity( new UrlEncodedFormEntity(list));
						
					//	req.setEntity(new StringEntity(list.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());

						Log.d("R :", status + " : " + endpoint);
						if (status == HttpURLConnection.HTTP_INTERNAL_ERROR){      //500
							infinite.result(false,
									HttpURLConnection.HTTP_INTERNAL_ERROR);
						
						}
						else if (status == HttpURLConnection.HTTP_OK){      //200
							infinite.result(true);
							
							Log.i("Send Message", "All found OK on server");
						}
						//
						resp.getEntity().consumeContent();
					} catch (Exception e) {
			//			L.fe(null, Pinig.Event.EXCEPTION, e);
						infinite.result(false);
					}
					break;
				}
				case UrlConnection.Request.USER_INFORMATION: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
						String endpoint = UNSECURE + UrlConnection.Request.SERVER
								+ UrlConnection.Request.url_USER_INFO;
	
						Log.e("Endpoint :", endpoint + " attaching : "
								+ list.toString());
	             	//	L.fwtf(null, "Endpoint : " + endpoint + " attaching : "
						//		+ o.toString());
						//
						HttpPost req = new HttpPost(endpoint);
						
						req.setEntity( new UrlEncodedFormEntity(list));
				//		req.setEntity(new StringEntity(list.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());
						Log.e(null, status + " : " + endpoint + " : " + result);

						if (status == HttpURLConnection.HTTP_INTERNAL_ERROR){      //500
							infinite.result(false,
									HttpURLConnection.HTTP_INTERNAL_ERROR);
						
						}
						else if (status == HttpURLConnection.HTTP_OK){      //200
							infinite.result(true);
					//		L.fwtf(null, "All found OK on server");
						}
						
						else if (status == HttpURLConnection.HTTP_NOT_FOUND)       //404
							infinite.result(false,
									HttpURLConnection.HTTP_NOT_FOUND);
						// Did not find device, then did not find user
						else if (status == HttpURLConnection.HTTP_FORBIDDEN)     //403
							infinite.result(false,
									HttpURLConnection.HTTP_FORBIDDEN);
						// Cannot create device as serial already used for
						// another device
						else if (status == HttpURLConnection.HTTP_BAD_REQUEST)   //400
							infinite.result(false,
									HttpURLConnection.HTTP_BAD_REQUEST);
						// Device found but username not correct
						else if (status == HttpURLConnection.HTTP_UNAUTHORIZED)   //401
							infinite.result(false,
									HttpURLConnection.HTTP_UNAUTHORIZED);
						// Device found, username matched but password not
						// correct
						else if (status == HttpURLConnection.HTTP_CONFLICT)     //409
							infinite.result(false,
									HttpURLConnection.HTTP_CONFLICT); 
				
						resp.getEntity().consumeContent();
					} catch (Exception e) {
				//		L.fe(null, Pinig.Event.EXCEPTION, e);
						infinite.result(false);
					}
					break;
				}

				
			/*	case UrlInfo.Request.APK_UPDATE: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
				//		String endpoint = UNSECURE + UrlInfo.Request.SERVER
				//				+ UrlInfo.Request.urlAPK_UPDATE;
						//
				//		L.fwtf(null, "Endpoint : " + endpoint + " attaching : "
					//			+ o.toString());
						//
						HttpPost req = new HttpPost(endpoint);
						req.setEntity(new StringEntity(list.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());
				//		L.fwtf(null, status + " : " + endpoint);
						infinite.result(status == HttpURLConnection.HTTP_OK,
								result);
						//
						resp.getEntity().consumeContent();
					} catch (Exception e) {
				//		L.fe(null, Pinig.Event.EXCEPTION, e);
						infinite.result(false);
					}
					break;
				}
					case UrlInfo.Request.APK_UNINSTALL: {
					InDeterminateRequestCallback infinite = (InDeterminateRequestCallback) callback;
					try {
						String endpoint = UNSECURE + UrlInfo.Request.SERVER
								+ UrlInfo.Request.urlAPK_UNINSTALL;
						//
				//		L.fwtf(null, "Endpoint : " + endpoint + " attaching : "
					//			+ o.toString());
						//
						HttpPost req = new HttpPost(endpoint);
						req.setEntity(new StringEntity(list.toString(), "UTF-8"));
						HttpResponse resp = c().execute(
								prepare(Encoding.JSON, req));
						//
						final int status = resp.getStatusLine().getStatusCode();
						final String result = EntityUtils.toString(resp
								.getEntity());
				//		L.fwtf(null, status + " : " + endpoint);
						infinite.result(status == HttpURLConnection.HTTP_OK,
								result);
						//
						resp.getEntity().consumeContent();
					} catch (Exception e) {
				//		L.fe(null, Pinig.Event.EXCEPTION, e);
						infinite.result(false);
					}
					break;
				}   */
				default: {
					InDeterminateRequestCallback idrc = (InDeterminateRequestCallback) callback;
					idrc.result(false);
					break;
				}
				}
			}
		});
	}

	public final class Encoding {
		public static final int JSON = 3;
		public static final int SEND_JSON = 1;
		public static final int RECEIVE_JSON = 2;
	}

	


}
